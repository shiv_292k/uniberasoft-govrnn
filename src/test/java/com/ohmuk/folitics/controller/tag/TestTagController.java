package com.ohmuk.folitics.controller.tag;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.utils.TestUtils;

/**
 * @author soumya
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestTagController {
	// Test RestTemplate to invoke the APIs.
	protected TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestController() throws JsonProcessingException, IOException {

		testTagUserApi("na"); 
		testTagSentimentApi("sub");
		
	}

	 protected void testTagUserApi(String userName){
  	   
         HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
         UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                 TestUtils.BASE_URL + TestUtils.TAG_CONTROLLER_APIS.TAG_USER).queryParam("match",
              		   userName);           
         HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build()
                 .encode().toUri(), HttpMethod.POST, httpEntity,
                 new ParameterizedTypeReference<ResponseDto<User>>() {
                 });
         assertTrue(response.getBody().getSuccess());
     
  }
	 protected void testTagSentimentApi(String title){
	  	   
         HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
         UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(
                 TestUtils.BASE_URL + TestUtils.TAG_CONTROLLER_APIS.TAG_SENTIMET).queryParam("match",
                		 title);           
         HttpEntity<ResponseDto<Sentiment>> response = restTemplate.exchange(builder.build()
                 .encode().toUri(), HttpMethod.POST, httpEntity,
                 new ParameterizedTypeReference<ResponseDto<Sentiment>>() {
                 });
         assertTrue(response.getBody().getSuccess());
     
  }
 }
