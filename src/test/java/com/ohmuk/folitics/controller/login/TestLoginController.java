package com.ohmuk.folitics.controller.login;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

/**
 * @author soumya
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestLoginController {
	// Test RestTemplate to invoke the APIs.
	protected TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestController() throws JsonProcessingException, IOException {

		testAuthenticateUserByUserNameEmailApi(false);
		testAuthenticateUserByUserNameEmailApi(true);
		testForgotPasswordByEmailApi();
		testForgotUserNameByEmailApi(); 
		
	}

	protected void testResetPasswordApi(Long id) {
		User user = TestDataUtils.getUser(id);
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.RESET_PASSWORD).queryParam("id", id);
		HttpEntity<?> entity = new HttpEntity<>(user);
		HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, entity, new ParameterizedTypeReference<ResponseDto<User>>() {
				});
		assertNotNull(response);
		assertTrue(response.getBody().getSuccess());
	}

	protected void testAuthenticateUserByUserNameEmailApi(boolean isEmalUser) throws JsonProcessingException {

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.AUTHENTICATE_USER)
				.queryParam("username", "username").queryParam("password", 123l);

		if (isEmalUser) {

			builder = UriComponentsBuilder
					.fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.AUTHENTICATE_USER)
					.queryParam("username", "testfoliticsuser@gmail.com").queryParam("password", 12345l);

		}
		HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, new HttpEntity<>(new User()), new ParameterizedTypeReference<ResponseDto<User>>() {
				});
		assertTrue(response.getBody().getSuccess());
	}

	protected void testForgotPasswordByEmailApi() throws JsonProcessingException {

		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.FORGOT_PASSWORD)
				.queryParam("emailId", "testfoliticsfolitics.user@gmail.com");

		HttpEntity<ResponseDto<String>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<String>>() {
				});
		assertTrue(response.getBody().getSuccess());
	}


	protected void testForgotUserNameByEmailApi() throws JsonProcessingException {

		HttpEntity<HttpHeaders> httpEntity = TestUtils.getHttpEntityHttpHeaders();
		
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.USER_CONTROLLER_APIS.FORGOT_USERNAME)
				.queryParam("emailId", "testfoliticsfolitics.user@gmail.com");

		HttpEntity<ResponseDto<String>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<String>>() {
				});
		assertTrue(response.getBody().getSuccess());
	}


	public static Map<String, Object> getUserDataToAuthentictae(boolean isEmalUser) {
		Map<String, Object> requestBody = new HashMap<String, Object>();
		if (isEmalUser) {
			requestBody.put("username", "folitics.username1567448");
			requestBody.put("password", 12345l);
		} else {
			requestBody.put("username", "username");
			requestBody.put("password", 1234l);
		}
		return requestBody;
	}
}
