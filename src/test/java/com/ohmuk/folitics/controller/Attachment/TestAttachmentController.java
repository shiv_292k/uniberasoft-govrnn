package com.ohmuk.folitics.controller.Attachment;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.utils.TestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAttachmentController {
	// Test RestTemplate to invoke the APIs.
	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void testAttachmentCURD() throws IOException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IntrospectionException {

		Long id= testAddAttachmentApi();
		testDownloadAttachmentApi(1l);
		testDeleteAttachmentApi(id);
	}

	private Long testAddAttachmentApi() throws IOException {

		HttpMessageConverter<Object> jackson = new MappingJackson2HttpMessageConverter();
		HttpMessageConverter<byte[]> resource = new ByteArrayHttpMessageConverter();
		FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
		formHttpMessageConverter.addPartConverter(jackson);
		formHttpMessageConverter.addPartConverter(resource);

		RestTemplate restTemplate = new RestTemplate(Arrays.asList(jackson,
				resource, formHttpMessageConverter));

		Path path = Paths.get(TestUtils.TEST_IMAGE);
		byte[] data = Files.readAllBytes(path);

		Resource file = new ByteArrayResource(data) {
			@Override
			public String getFilename() {
				return "testimage.jpg";
			}
		};

		HttpHeaders imageHeaders = new HttpHeaders();
		imageHeaders.setContentType(MediaType.IMAGE_JPEG);
		HttpEntity<Resource> image = new HttpEntity<Resource>(file,
				imageHeaders);

		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("file", image);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(
				map, headers);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL
						+ TestUtils.ATTACHMENT_CONTROLLER_APIS.ADD_ATTACHEMNT);

		HttpEntity<ResponseDto<Long>> response = restTemplate.exchange(builder
				.build().encode().toUri(), HttpMethod.POST, httpEntity,
				new ParameterizedTypeReference<ResponseDto<Long>>() {
				});

		ResponseDto<Long> apiResponse = response.getBody();
		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());
		return apiResponse.getMessages().get(0);
	}

	private void testDeleteAttachmentApi(Long id) throws IOException {
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(
						TestUtils.BASE_URL
								+ TestUtils.ATTACHMENT_CONTROLLER_APIS.DELETE_ATTACHEMNT)
				.queryParam("id", id);
		HttpEntity<HttpHeaders> httpEntity = TestUtils
				.getHttpEntityHttpHeaders();
		HttpEntity<ResponseDto<User>> response = restTemplate.exchange(builder
				.build().encode().toUri(), HttpMethod.POST, httpEntity,
				new ParameterizedTypeReference<ResponseDto<User>>() {
				});

		assertTrue(response.getBody().getSuccess());

	}

	private void testDownloadAttachmentApi(Long id) throws IOException {

		HttpMessageConverter<byte[]> resource = new ByteArrayHttpMessageConverter();
		
		RestTemplate restTemplate = new RestTemplate(Arrays.asList(resource));

		HttpEntity<HttpHeaders> httpEntity = TestUtils
				.getHttpEntityHttpHeaders();
		

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.ATTACHMENT_CONTROLLER_APIS.DOWNLOAD_ATTACHEMNT);

		ResponseEntity<byte[]> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.GET, httpEntity, byte[].class);
		
		assertNotNull(response);
		assertTrue(response.getStatusCode() == HttpStatus.OK);
		
	}

}
