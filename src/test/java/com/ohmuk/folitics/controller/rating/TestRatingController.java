package com.ohmuk.folitics.controller.rating;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.utils.TestDataUtils;
import com.ohmuk.folitics.utils.TestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRatingController {
	// Test RestTemplate to invoke the APIs.
	private TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void TestSentimentCURD() throws IOException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IntrospectionException {
		testRateReferneceRatingApi();
		testRateSentimentNewsRatingApi();
		testGetAllRatingsApi("reference");
		testGetAllRatingsApi("sentimentNews");
		testGetRatingsApi("reference");
		testGetRatingsApi("sentimentNews");
	}

	private void testRateReferneceRatingApi() throws IntrospectionException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(TestDataUtils.getReferenceRatingBean());

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RATING_CONTROLLER_APIS.RATE);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});

		ResponseDto<Object> apiResponse = response.getBody();

		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());

	}

	private void testRateSentimentNewsRatingApi() throws IntrospectionException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(TestDataUtils.getSentimentnewsRatingBean());

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RATING_CONTROLLER_APIS.RATE);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});

		ResponseDto<Object> apiResponse = response.getBody();

		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());
 
	}

	private void testGetAllRatingsApi(String componentType) throws IntrospectionException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {

		Map<String, Object> requestBody = new HashMap<String, Object>();
		requestBody.put("componentType", componentType);

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(requestBody);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RATING_CONTROLLER_APIS.GET_ALL_RATINGS);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});

		ResponseDto<Object> apiResponse = response.getBody();

		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());

	}

	private void testGetRatingsApi(String componentType) throws IntrospectionException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {

		Map<String, Object> requestBody = TestDataUtils.getSentimentnewsRatingBean();
		requestBody.put("componentType", componentType);

		HttpEntity<String> httpEntity = TestUtils.getHttpEntity(requestBody);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(TestUtils.BASE_URL + TestUtils.RATING_CONTROLLER_APIS.GET_RATING);
		HttpEntity<ResponseDto<Object>> response = restTemplate.exchange(builder.build().encode().toUri(),
				HttpMethod.POST, httpEntity, new ParameterizedTypeReference<ResponseDto<Object>>() {
				});

		ResponseDto<Object> apiResponse = response.getBody();

		assertNotNull(apiResponse);
		assertTrue(apiResponse.getSuccess());

	}

}
