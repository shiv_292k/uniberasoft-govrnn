package com.ohmuk.folitics.service;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;

public interface IUserUINotificationService extends IBaseService {
	
	
	public UserUINotification save(UserUINotification userUINotification) throws Exception;
	
	
	public UserUINotification read(Long userUINotification) throws Exception;
	
	
	public UserUINotification update(UserUINotification userUINotification) throws Exception;


	/**
	 * @param user
	 * @param notificationType
	 * @return
	 * @throws Exception
	 */
	public UserUINotification read(User user, String notificationType) throws Exception;

}
