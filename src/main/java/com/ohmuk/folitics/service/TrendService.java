/**
 * 
 */
package com.ohmuk.folitics.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.hibernate.Criteria;
//import javax.persistence.Query;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.ouput.model.UserTrendOutputModel;

/**
 * @author Deewan
 * @param <E>
 *
 */
@Service
@Transactional
public class TrendService<E> implements ITrendService {

	private static Logger logger = LoggerFactory.getLogger(TrendService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public List<Trend> displayTrend(Trend trend) throws Exception {
		logger.info("Inside TrendService displayTrend method");
		Criteria query = getSession().createCriteria(Trend.class);
		query.add(Restrictions.like("name", trend.getName(), MatchMode.ANYWHERE));
		logger.info("Existing from TrendService displayTrend method");
		return query.list();
	}

	@Override
	public boolean verifyIfTrendnameExist(Trend trend) throws Exception {
		logger.info("Inside UserService verifyIfTrendExist method");
		Criteria criteria = getSession().createCriteria(Trend.class);
		criteria.add(Restrictions.eq("name", trend.getName()));
		if (criteria.list().size() == 0) {
			logger.info("Exiting from TrendService verifyIfTrendnameExist method");
			return false;
		} else {
			logger.info("Exiting from TrendService verifyIfTrendnameExis method");
			return true;
		}
	}

	@Override
	public Trend saveTrend(Trend trend) throws Exception {
		logger.info("Inside TrendService save method");
		Long id = (Long) getSession().save(trend);
		logger.info("Exiting TrendService save method");
		return readTrend(id);
	}

	@Override
	public Trend readTrend(Long trendId) throws Exception {
		logger.info("Inside TrendService readTrend method");
		logger.info("Exiting TrendService readTrend method");
		return (Trend) getSession().get(Trend.class, trendId);
	}

	@Override
	public TrendComment readTrendComments(Long trendMappingId) throws Exception {
		logger.info("Inside TrendService readTrendComments method");
		TrendComment comment = null;
		try {
            Criteria criteria = getSession().createCriteria(TrendComment.class).add(
                    Restrictions.eq("trendMappingId",trendMappingId));

            comment = (TrendComment) criteria.uniqueResult();

        } catch (Exception exception) {
            logger.error("Exception in readTrendCpmment for mappping id  : " + trendMappingId);
            logger.info("Exiting TrendService getTrendByName method");
        }

		return comment;
	}


	
	@Override
	public Trend getTrendByName(String name) throws Exception {
		logger.info("Inside TrendService getTrendByName method");

        Trend trend = null; 
        try {
            Criteria criteria = getSession().createCriteria(Trend.class).add(
                    Restrictions.eq("name",name));

            trend = (Trend) criteria.uniqueResult();

        } catch (Exception exception) {
            logger.error("Exception in find trend with  name : " + name);
            logger.info("Exiting TrendService getTrendByName method");
        }

        if (null != trend) {
            logger.debug("Trend found with username name : " + name);
            logger.info("Exiting TrendService getTrendByName method");
            return trend;
        } else {
            logger.debug("No Trend found with name: " + name);
            logger.info("Exiting TrendService getTrendByName method");
            return null;
        }
        
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> top5User(String name) throws Exception {
		logger.info("Inside top5User top5User method in Trend Service");
		Criteria query = getSession().createCriteria(User.class);
		try {
			query.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
		} catch (Exception exception) {
			logger.error("Exception in find while seaching username "
					+ exception);
		}
		return query.list();
	}

	@Override
	public long saveTrendMapping(Trend trend) throws Exception {
		logger.info("Inside TrendService saveTrendMapping method");
		TrendMapping trendMapping = new TrendMapping();
		trendMapping.setTrend(trend);
		getSession().save(trendMapping);
		logger.info("Existing from TrendService saveTrendMapping method");
		return 0;
	}

	@Override
	public Trend findTrendByTrendName(Trend trend) throws Exception {
		logger.info("Inside UserService findTrendByTrendName method");
		Criteria criteria = getSession().createCriteria(Trend.class);
		criteria.add(Restrictions.eq("name", trend.getName()));
		Trend trendResult = (Trend) criteria.uniqueResult();
		return trendResult;
	}

	@Override
	public long addToTrendMapping(TrendMapping trendMapping) {
		// TODO Auto-generated method stub
		logger.info("Inside saveTrend method service");
		Long id = (Long) getSession().save(trendMapping);
		logger.info("Exiting saveTrend method service1");
		return id;
	}

	@Override
	public List<Trend> searchExactTrend(Trend trend) {
		// TODO Auto-generated method stub
		logger.info("Inside TrendService displayTrend method");
		Criteria query = getSession().createCriteria(Trend.class);
		query.add(Restrictions.like("name", trend.getName(), MatchMode.START));
		logger.info("Existing from TrendService displayTrend method");
		return (List<Trend>) query.list();
	}

	@Override
	public List<Opinion> searchTrendOpinion(Long id) {
		// TODO Auto-generated method stub
		logger.info("Inside TrendService searchTrendOpinion method");
		List<Opinion> opinions = new ArrayList<Opinion>();
		String componentIdsQuery = "select componentId from TrendMapping where trendId=:trendId and componentType=:componentType";
		Query q = getSession().createQuery(componentIdsQuery);
		q.setParameter("trendId", id);
		q.setParameter("componentType", "Opinion");
		@SuppressWarnings({ "unchecked" })
		List<Long> componentIds = (List<Long>) q.list();
		if (componentIds != null && !componentIds.isEmpty()) {
			Criteria query = getSession().createCriteria(Opinion.class);
			query.add(Restrictions.in("id", componentIds));
			logger.info("Existing from TrendService searchTrendOpinion method");
			opinions = (List<Opinion>) query.list();
		}
		return opinions;
	}
	@Override
	public List<Response> searchTrendResponse(Long id) {
		// TODO Auto-generated method stub
		logger.info("Inside TrendService searchTrendOpinion method");
		List<Response> responses  = new ArrayList<Response>();
		String componentIdsQuery = "select componentId from TrendMapping where trendId=:trendId and componentType=:componentType";
		Query q = getSession().createQuery(componentIdsQuery);
		q.setParameter("trendId", id);
		q.setParameter("componentType", "Response");
		@SuppressWarnings({ "unchecked" })
		List<Long> componentIds = (List<Long>) q.list();
		if (componentIds != null && !componentIds.isEmpty()) {
			Criteria query = getSession().createCriteria(Response.class);
			query.add(Restrictions.in("id", componentIds));
			logger.info("Existing from TrendService searchTrendOpinion method");
			responses = (List<Response>) query.list();
		}
		return responses;
	}

	@Override
	public Long createTrendMapping(TrendMapping trendMapping) {
		// TODO Auto-generated method stub
		logger.info("Inside createTrendMapping method service");
		Long id = (Long) getSession().save(trendMapping);
		logger.info("Exiting createTrendMapping method service");
		return id;
	}

	@Override
	public List<TrendOutputModel> matchingTrend(String name) {
		logger.info("Inside matchingTrend getUserByMatch method");
		List<TrendOutputModel> models = new ArrayList<TrendOutputModel>();
		List<Trend> trends = null;
		try {
			Criteria criteria = getSession().createCriteria(Trend.class).add(
					Restrictions.like("name", name, MatchMode.START));
			trends = criteria.list();
			for (Trend entity : trends) {
				models.add(TrendOutputModel.getModel(entity));
			}
		} catch (Exception exception) {
			logger.error("Exception in matchingTrend for match name : " + name);
		}
		if (null != trends) {
			logger.debug("Trend list found starting with match name : " + name);
			return models;
		}
		return null;
	}

	@Override
	public void deleteTrend(Trend trend){
		logger.info("Inside deleteTrend method service");
		getSession().delete(trend);
		logger.info("Exiting deleteTrend method service");
	}
	
	@Override
	public void deleteTrendComments(TrendComment comment){
		logger.info("Inside deleteTrendComments method service");
		getSession().delete(comment);
		logger.info("Exiting deleteTrendComments method service");
	}
	
	@Override
	public List<TrendOutputModel> getTopTrends(int count, boolean onlyAdminTrends) {
		logger.info("Inside TrendService getTopTrends method");
		List<TrendOutputModel> trendOutputModels = new ArrayList<TrendOutputModel>();
		ProjectionList projectionList = Projections.projectionList();        
		projectionList.add(Projections.groupProperty("id"))
		        .add(Projections.rowCount(),"rowCount");
		Criteria criteria  = getSession().createCriteria(Trend.class).add(Restrictions.eq("trending", true)).addOrder(Order.desc("rowCount")).setMaxResults(count);
		if(onlyAdminTrends){
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, -1); 
			criteria.add(Restrictions.ge("createTime", c.getTime()));
		}
		criteria.setProjection(projectionList);
		@SuppressWarnings("unchecked")
		List<Object[]> trendObjs = criteria.list();
		for (Object[] trendObj : trendObjs) {
			
			Trend trend = (Trend) getSession().get(Trend.class,	Long.parseLong(trendObj[0].toString()));
			if(onlyAdminTrends){
				if(trend.getAdminTrend()!=null && trend.getAdminTrend().booleanValue())
					trendOutputModels.add(TrendOutputModel.getModel(trend));
			}
			else
			{
				trendOutputModels.add(TrendOutputModel.getModel(trend));
			}
		}
		return trendOutputModels;
	}

	@Override
	public List<TrendOutputModel> getMostUsedTrends(int count) {
		logger.info("Inside TrendService getTopTrends method");
		List<TrendOutputModel> trendOutputModels = new ArrayList<TrendOutputModel>();
		ProjectionList projectionList = Projections.projectionList();        
		projectionList.add(Projections.groupProperty("trend.id"))
		        .add(Projections.rowCount(),"rowCount");
		Criteria criteria  = getSession().createCriteria(TrendMapping.class).addOrder(Order.desc("rowCount")).setMaxResults(count);
		criteria.setProjection(projectionList);
		@SuppressWarnings("unchecked")
		List<Object[]> trendObjs = criteria.list();
		for (Object[] trendObj : trendObjs) {
			Trend trend = (Trend) getSession().get(Trend.class,	Long.parseLong(trendObj[0].toString()));
			trendOutputModels.add(TrendOutputModel.getModel(trend));
		}
		return trendOutputModels;
	}

	@Override
	public Trend update(Trend trend) throws Exception {
		logger.info("Inside Trend Service update method");
		getSession().update(trend);
		logger.info("Existing from TrendService update method");
		return trend;
	}
	
	/*@Override
	public List<UserTrendOutputModel> getTopTrendUsers(int count) {
		logger.info("Inside TrendService searchTrendOpinion method");
		List<UserTrendOutputModel> trendOutputModels = new ArrayList<UserTrendOutputModel>();
		Map<Long,UserTrendOutputModel> trendModels = new HashMap<>();
		ProjectionList projectionList = Projections.projectionList();        
		projectionList.add(Projections.groupProperty("trend.id"))
		        .add(Projections.groupProperty("userId"))
		        .add(Projections.rowCount(),"rowCount");
		Criteria criteria  = getSession().createCriteria(TrendMapping.class).addOrder(Order.desc("rowCount")).setMaxResults(count);
		criteria.setProjection(projectionList);
		@SuppressWarnings("unchecked")
		List<Object[]> trendUsers = criteria.list();
		for (Object[] trendUser : trendUsers) {
		    Trend trend = (Trend) getSession().get(Trend.class,	Long.parseLong(trendUser[0].toString()));
			User user = (User) getSession().get(User.class, Long.parseLong(trendUser[1].toString()));
			UserTrendOutputModel userTrendOutputModel = new UserTrendOutputModel(user, trend);
			userTrendOutputModel.setTrendMappingsCount(Integer.parseInt(trendUser[2].toString()));
			if(trendModels.isEmpty()){
				trendModels.put(user.getId(), userTrendOutputModel);				
			}
			else{
				UserTrendOutputModel model =  trendModels.get(user.getId());
				userTrendOutputModel.setTrendMappingsCount(model.getTrendMappingsCount()+userTrendOutputModel.getTrendMappingsCount());
			}
			trendModels.put(user.getId(), userTrendOutputModel);
			//trendOutputModels.add(userTrendOutputModel);
		}
		if(!trendModels.isEmpty()){
			for(Long key : trendModels.keySet()){
				trendOutputModels.add(trendModels.get(key));
			}
		}
		return trendOutputModels;
		
		logger.info("Inside TrendService searchTrendOpinion method");
		List<UserTrendOutputModel> trendOutputModels = new ArrayList<UserTrendOutputModel>();
		String trendIdsQuery = "select trendId, userId, count(*) as count from TrendMapping group by trendId,userId order by count(*) desc ";//limit "+ count;
		Query q = getSession().createQuery(trendIdsQuery);
		@SuppressWarnings({ "unchecked" })
		List<TrendUser> trendUsersIds = (List<TrendUser>) q
				.setResultTransformer(Transformers.aliasToBean(TrendUser.class))
				.list();
		if (trendUsersIds != null && !trendUsersIds.isEmpty()) {
			for (TrendUser trendUser : trendUsersIds) {
				Trend trend = (Trend) getSession().get(Trend.class,
						trendUser.getTrendId());
				User user = (User) getSession().get(User.class,
						trendUser.getUserId());
				trendOutputModels.add(new UserTrendOutputModel(user, trend));
			}
		}
		return trendOutputModels;
	}*/
	
	@Override
	public List<UserTrendOutputModel> getTopTrendUsers(int count) {
		logger.info("Inside TrendService searchTrendOpinion method");
		List<UserTrendOutputModel> trendOutputModels = new ArrayList<UserTrendOutputModel>();
		Map<Long,UserTrendOutputModel> trendModels = new HashMap<>();

		String sqlQuery = " select tm.userId, count(distinct(tm.trendId)) as trendCount from trendmapping as tm "
				+ " where tm.trendId in (select id from trend as td ) "
				+ " group by tm.userId order by trendCount desc limit "+ count;

		Query query = getSession().createSQLQuery(sqlQuery);

		@SuppressWarnings("unchecked")
		List<Object[]> recordList = query.list();

		for (Object[] trendUser : recordList) {
			User user = (User) getSession().get(User.class, Long.parseLong(trendUser[0].toString()));
			UserTrendOutputModel userTrendOutputModel = new UserTrendOutputModel(user);
			userTrendOutputModel.setTrendMappingsCount(Integer.parseInt(trendUser[1].toString()));
			if(!trendModels.containsKey(user.getId())){
				trendModels.put(user.getId(), userTrendOutputModel);				
			}
			else{
				UserTrendOutputModel model =  trendModels.get(user.getId());
				userTrendOutputModel.setTrendMappingsCount(model.getTrendMappingsCount()+userTrendOutputModel.getTrendMappingsCount());
				trendModels.put(user.getId(), userTrendOutputModel);
			}
		}
		if(!trendModels.isEmpty()){
			for(Long key : trendModels.keySet()){
				trendOutputModels.add(trendModels.get(key));
			}
		}
		return trendOutputModels;
	}


	@Override
	public List<TrendMapping> getTrendByComponetIdType(String type,Long componentId) {
		logger.info("Inside matchingTrend getTrendByComponetIdType method");
		List<TrendMapping> trendMappings = null;
		try {
			Criteria criteria = getSession().createCriteria(TrendMapping.class).add(
					Restrictions.and(Restrictions.eq("componentType",type),Restrictions.eq("componentId", componentId)));
			trendMappings = criteria.list();
			
		} catch (Exception exception) {
			logger.error("Exception in getTrendByComponetIdType for compponentId: " + componentId);
		}
		if (null != trendMappings) {
			logger.debug("Trend list found  for componentId : " + componentId);
			return trendMappings;
		}
		return null;
	}

	
	@Override
	public List<TrendOutputModel> getTopAdminTrends(boolean onlyAdminTrends) {
		logger.info("Inside TrendService getTopAdminTrends method");
		List<TrendOutputModel> trendOutputModels = new ArrayList<TrendOutputModel>();
		ProjectionList projectionList = Projections.projectionList();        
		projectionList.add(Projections.groupProperty("id"))
		        .add(Projections.rowCount(),"rowCount");
		Criteria criteria  = getSession().createCriteria(Trend.class).add(Restrictions.eq("trending", true));
		criteria.setProjection(projectionList);
		@SuppressWarnings("unchecked")
		List<Object[]> trendObjs = criteria.list();
		for (Object[] trendObj : trendObjs) {
			
			Trend trend = (Trend) getSession().get(Trend.class,	Long.parseLong(trendObj[0].toString()));
			if(onlyAdminTrends){
				if(trend.getAdminTrend()!=null && trend.getAdminTrend().booleanValue())
					trendOutputModels.add(TrendOutputModel.getModel(trend));
			}
			else
			{
				trendOutputModels.add(TrendOutputModel.getModel(trend));
			}
		}
		logger.info("Inside TrendService getTopAdminTrends method");
		return trendOutputModels;
	}
	
	
	@Override
	public List<UserTrendOutputModel> fetchAllTrendsByUserId(Long userId) {
		logger.info("Inside TrendService searchTrendOpinion method");
		List<UserTrendOutputModel> userTrendDetails = new ArrayList<UserTrendOutputModel>();

		String sqlQuery = " select distinct(tm.trendId), tm.userId,  td.name from trendmapping as tm "
						+ " left join trend as td on tm.trendId = td.id where tm.userId='"+userId+"'";

		Query query = getSession().createSQLQuery(sqlQuery);

		@SuppressWarnings("unchecked")
		List<Object[]> recordList = query.list();

		for (Object[] trendUser : recordList) {
			UserTrendOutputModel userTrendOutputModel = new UserTrendOutputModel();
			Long trendId = ((BigInteger) trendUser[0] == null ? 0 : ((BigInteger) trendUser[0]).longValue());
			userTrendOutputModel.setTrendId(trendId);
			userTrendOutputModel.setTrendName((String) trendUser[2]);
			userTrendDetails.add(userTrendOutputModel);
		}
		return userTrendDetails;
	}
}


/*class TrendUser {
	private long userId;
	private long trendId;
	private int count;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getTrendId() {
		return trendId;
	}

	public void setTrendId(long trendId) {
		this.trendId = trendId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}*/
