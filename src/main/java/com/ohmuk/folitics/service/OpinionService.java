package com.ohmuk.folitics.service;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.simmetrics.metrics.JaroWinkler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.beans.UserOpinionDetailBean;
import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.model.ImageModel;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.NumberUtil;

@Service
@Transactional
public class OpinionService implements IOpinionService {

    private static Logger logger = LoggerFactory.getLogger(OpinionService.class);

    @Autowired
    private SessionFactory _sessionFactory;

    private Session getSession() {
        return _sessionFactory.getCurrentSession();
    }

    /**
     * Method to save the com.ohmuk.folitics.jpa.entity.Opinion object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.save
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    @Override
    public Opinion create(Opinion opinion) throws Exception {
        logger.info("Inside OpinionService create method");
        Long id = (Long) getSession().save(opinion);
        logger.info("Exiting from OpinionService create method");
        return read(id);
    }

    /**
     * Method to get object of com.ohmuk.folitics.jpa.entity.Opinion using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.find by passing id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    @Override
    public Opinion read(Long id) throws Exception {
        logger.info("Inside OpinionService read method");
        logger.info("Exiting from OpinionService read method");
        return (Opinion) getSession().get(Opinion.class, id);
    }

    /**
     * Method to get all opinions using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.findAll
     * 
     * @author Abhishek
     * @return java.util.List<com.ohmuk.folitics.jpa.entity.Opinion>
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Opinion> readAll() throws Exception {
        logger.info("Inside OpinionService readAll method");
        logger.info("Exiting from OpinionService readAll method");
        return getSession().createCriteria(Opinion.class).list();
    }

    /**
     * Method to update an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.save
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    @Override
    public Opinion update(Opinion opinion) throws Exception {
        logger.info("Inside OpinionService update method");
        opinion.setEditTime(DateUtils.getSqlTimeStamp());
        getSession().update(opinion);
        logger.info("Exiting from OpinionService update method");
        return opinion;
    }

    /**
     * Method to permanent delete an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.delete by passing id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return boolean
     */
    @Override
    public boolean deleteFromDBById(Long id) throws Exception {
        logger.info("Inside OpinionService deleteFromDBById method");
        Opinion opinion = (Opinion) getSession().get(Opinion.class, id);
        if (null != opinion) {
            opinion.setUser(null);
            if (opinion.getResponses() != null) {
                for (Response response : opinion.getResponses()) {
                    response.setOpinion(null);
                    getSession().delete(response);
                }
            }
            getSession().delete(opinion);
            logger.info("Exiting from OpinionService deleteFromDBById method");
            return true;
        }
        logger.info("Exiting from OpinionService deleteFromDBById method");
        return true;
    }

    /**
     * Method to permanent delete an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.delete by passing
     * object of com.ohmuk.folitics.jpa.entity.Opinion
     * 
     * @author Abhishek
     * @param boolean
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    @Override
    public boolean deleteFromDB(Opinion opinion) throws Exception {
        logger.info("Inside OpinionService deleteFromDB method");
        getSession().delete(opinion);
        logger.info("Exiting from OpinionService deleteFromDB method");
        return true;
    }

    @Override
    public ImageModel getImageModel(Long entityId, boolean isThumbnail) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Opinion> getOpinionByUser(Long userId) throws MessageException, Exception {
        logger.info("Inside OpinionService getOpinionByUser method");

        User user = new User();
        user.setId(userId);

        Criteria criteria = getSession().createCriteria(Opinion.class);
        criteria.add(Restrictions.eqOrIsNull("user", user));

        List<Opinion> opinions = criteria.list();
        logger.info("Exiting from OpinionService getOpinionByUser method");
        return opinions;
    }

    @Override
    public List<Opinion> getOpinionForSentiment(Long id) throws Exception {
        logger.info("Inside OpinionService getOpinionForSentiment method");

        Criteria criteria = getSession().createCriteria(Opinion.class);
        criteria.add(Restrictions.eq("sentiment.id", id));
        criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(OpinionMaxResults);
        List<Opinion> opinions = criteria.list();
        logger.info("Exiting from OpinionService getOpinionForSentiment method");
        return opinions;
    }

    @Override
    public List<Opinion> getOpinionForSentimentAndUser(Long sentimentId, Long userId) throws Exception {
        logger.info("Inside OpinionService getOpinionByUser method");

        User user = new User();
        user.setId(userId);

        Criteria criteria = getSession().createCriteria(Opinion.class);
        Criterion criterion = Restrictions.and(Restrictions.eq("sentiment.id", sentimentId),
                Restrictions.eq("user", user));
        criteria.add(criterion);
        List<Opinion> opinions = criteria.list();
        logger.info("Exiting from OpinionService getOpinionForSentiment method");
        return opinions;

    }

    @Override
    public List<Opinion> getOpinionByType(Long sentimentId, String opinionType, int count) throws Exception {
        logger.info("Inside OpinionService getOpinionByType method");

        Criteria criteria = getSession().createCriteria(Opinion.class);
        Criterion criterion = Restrictions.and(Restrictions.eq("sentiment.id", sentimentId),
                Restrictions.eq("type", opinionType));
        criteria.add(criterion);
        criteria.setFirstResult(0);
        criteria.setMaxResults(count);
        List<Opinion> opinions = criteria.list();
        logger.info("Exiting from OpinionService getOpinionByType method");
        return opinions;
    }

    @Override
    public OpinionAggregation getOpinionsAggregate(Long sentimentId) throws Exception {
        logger.info("Inside OpinionService getOpinionsAggregate for sentiment method");

        Criteria criteria = getSession().createCriteria(Opinion.class);
        Sentiment sentiment = (Sentiment) getSession().get(Sentiment.class, sentimentId);
        criteria.add(Restrictions.eq("sentiment", sentiment));

        List<Opinion> opinions = criteria.list();

        List<Opinion> support = new ArrayList<>();
        List<Opinion> against = new ArrayList<>();

        OpinionAggregation aggregation = null;

        if (opinions != null && !opinions.isEmpty()) {
            aggregation = new OpinionAggregation();
            for (Opinion op : opinions) {
                if (op.getType().equalsIgnoreCase("Pro")) {
                    support.add(op);
                } else if (op.getType().equalsIgnoreCase("Anti")) {
                    against.add(op);
                }
            }

            aggregation.setSentimentid(sentimentId);
            aggregation.setTotalVotes(opinions.size());
            aggregation.setAgainst(NumberUtil.formatNumberDecimal(((float) against.size() / opinions.size()) * 100, 1));
            aggregation.setSupport(NumberUtil.formatNumberDecimal(((float) support.size() / opinions.size()) * 100, 1));

        }
        logger.info("Exiting from OpinionService getOpinionsAggregate for sentimet method");
        return aggregation;
    }

    @Override
    public List<Opinion> getOpinionComponent(Long sentimentId, Long componentId, String componentType) throws Exception {
        logger.info("Inside OpinionService getOpinionComponent method");

        Criteria criteria = getSession().createCriteria(Opinion.class);
        Criterion condition = Restrictions.and(Restrictions.eq("sentiment.id", sentimentId),
                Restrictions.eq("componentId", componentId), Restrictions.eq("onComponentType", componentType));
        criteria.add(condition);

        List<Opinion> opinions = criteria.list();
        logger.info("Exiting from OpinionService getOpinionComponent method");
        return opinions;
    }

    @Override
    public @ResponseBody List<Opinion> getLatestOpinions(int count) throws Exception {
        logger.info("Inside OpinionService getOpinionComponent method");
        Criteria criteria = getSession().createCriteria(Opinion.class).addOrder(Order.desc("createTime"));
        List<Opinion> opinions = criteria.list();
        if (opinions != null && !opinions.isEmpty()) {
            if (opinions.size() > count) {
                opinions = criteria.list().subList(0, count);
                return opinions;
            } else {
                return opinions;
            }
        }
        logger.info("Exiting from OpinionService getOpinionComponent method");
        return null;
    }

    @Override
    public LinkedHashSet<Opinion> searchOpinion(String searchKeyword) {

        List<Opinion> opinionsList = new ArrayList<Opinion>();
        searchKeyword = "%" + searchKeyword + "%";

        Criteria criteria = getSession().createCriteria(Opinion.class).add(
                Restrictions.or(Restrictions.like("subject", searchKeyword).ignoreCase(),
                        Restrictions.or(Restrictions.like("text", searchKeyword).ignoreCase())));
        criteria.addOrder(Order.desc("createTime"));
        List<Opinion> opinions = criteria.list();
        opinionsList.addAll(opinions);

        HashSet opinionSet = new LinkedHashSet(opinionsList);
        return (LinkedHashSet<Opinion>) opinionSet;

    }

    @Override
    public LinkedHashSet<Opinion> searchOpinionBySearchKeywordAndSentimentId(String searchKeyword, Long sentimentId) {
        List<Opinion> opinionsList = new ArrayList<Opinion>();
        searchKeyword = "%" + searchKeyword + "%";

        Criteria criteria = getSession().createCriteria(Opinion.class).add(
                Restrictions.or(Restrictions.eq("sentiment.id", sentimentId),
                        Restrictions.like("subject", searchKeyword).ignoreCase(),
                        Restrictions.or(Restrictions.like("text", searchKeyword).ignoreCase())));
        criteria.addOrder(Order.desc("createTime"));
        List<Opinion> opinions = criteria.list();
        opinionsList.addAll(opinions);

        HashSet opinionSet = new LinkedHashSet(opinionsList);
        return (LinkedHashSet<Opinion>) opinionSet;
    }


    @Override
    public @ResponseBody List<Opinion> getRelatedOpinions(Long id ,String text,int count,List<Long> opinionTrendId) throws Exception {
        logger.info("Inside OpinionService getRelatedOpinions method");
        
        Calendar calendar = Calendar.getInstance(); // this would default to now
        calendar.add(Calendar.DAY_OF_MONTH, -10);
        Date agoDate = calendar.getTime();        
        Criteria criteria = getSession().createCriteria(Opinion.class);
        
        String opinionTrendQuery = "select tm.componentId from Opinion o,TrendMapping tm"
        		+ " where o.id = tm.componentId and tm.componentType=:componentType and tm.componentId <> :componentId and tm.trend.id in (:trendId)";
        Query q = getSession().createQuery(opinionTrendQuery);
        // do not include the same opinion
        q.setParameter("componentId", id);
		q.setParameter("componentType", ComponentType.OPINION.getValue());
		q.setParameterList("trendId", opinionTrendId);
		@SuppressWarnings("unchecked")
		
		List<Long> opinionIds = (List<Long>) q.list();
		
		if(opinionIds != null && !opinionIds.isEmpty()){			
				criteria.add(Restrictions.and(Restrictions.gt("createTime", agoDate),Restrictions.eq("componentType", ComponentType.OPINION.getValue()),Restrictions.in("id",opinionIds.toArray())));
	
		}	      
        List<Opinion> opinions = criteria.list();
        List<Opinion> maxResponseOpinions = new ArrayList<Opinion>();
        List<Opinion> releatedOpinion = new ArrayList<Opinion>();
        //get the highest number of responses count
        int responseCount = 0;
        if (opinions != null && !opinions.isEmpty()) {
            for(Opinion op : opinions){
            	if(op.getResponses() != null && !op.getResponses().isEmpty()){
            		responseCount = ( op.getResponses().size() > responseCount ) ?  op.getResponses().size() :responseCount;
            	}
            }
            for(Opinion op : opinions){
            	if(op.getResponses() != null && !op.getResponses().isEmpty()){
            		if(op.getResponses() != null && !op.getResponses().isEmpty() && (op.getResponses().size() == responseCount )){
        				maxResponseOpinions.add(op);
        			}
        		}
            }
        }
        if(!maxResponseOpinions.isEmpty()){
        	for(Opinion ro : maxResponseOpinions){        		
        		 JaroWinkler jaroWinklerDistance = new JaroWinkler();
        		Float distance = jaroWinklerDistance.compare(ro.getSubject(),text);
        		if(distance > 0.8){
        			releatedOpinion.add(ro);
        		}
        	}
        }
        
        logger.info("Exiting from OpinionService getOpinionComponent method");
     // for now the count is 10
        if(releatedOpinion.size() > count){
        	return	releatedOpinion.subList(0, 10);
        }else{
        	return releatedOpinion;
        }
    }

    @Override
    public List<Opinion> getRelatedOpinions(Long id, String text, int count) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }
    
    public List<UserOpinionDetailBean> getOpinionsByUserId(Long userId, String month, String year, String opinionType){
    	logger.info("Inside OpinionService getOpinionsByUserId method");
                
        String sql = " select op.sentimentId, sen.description, sen.image, sen.imageType, sen.type as sentimentType, op.id as opinionId, "
        		   + " op.imageUrl as opinionImage, op.subject as opinionSubject, op.text as opinionDescription, op.type as opinionType, "
        		   + " com.createTime from opinion as op "
        		   + " left join sentiment as sen on op.sentimentId = sen.id "
        		   + " left join component as com on op.id = com.id "
        		   + " where op.id in (select id from component as co where co.componentType='Opinion' and "
        		   + " co.created_by='"+userId +"' and co.createTime like '%"+year+"-"+month+"%') "
        		   + "and op.type='"+opinionType+"';";
		Query query = getSession().createSQLQuery(sql);

		@SuppressWarnings("unchecked")
		List<Object[]> recordList = query.list();
		List<UserOpinionDetailBean> userOpinionList = new ArrayList<>();
		if(null != recordList && !recordList.isEmpty()){
			for(Object[] row : recordList){
				UserOpinionDetailBean userOpinionBean = new UserOpinionDetailBean();
				Long sentimentId = ((BigInteger) row[0] == null ? 0 : ((BigInteger) row[0]).longValue());
				Long opinionId = ((BigInteger) row[5] == null ? 0 : ((BigInteger) row[5]).longValue());
				
				userOpinionBean.setSentimentId(sentimentId);
				userOpinionBean.setDescription((String)row[1]);
				userOpinionBean.setImage((byte[]) row[2]);
				userOpinionBean.setImageType((String) row[3]);
				userOpinionBean.setSentimentType((String) row[4]);
				userOpinionBean.setOpinionId(opinionId);
				userOpinionBean.setOpinionImage((String) row[6]);
				userOpinionBean.setOpinionSubject((String) row[7]);
				userOpinionBean.setOpinionDescription((String) row[8]);
				userOpinionBean.setOpinionType((String) row[9]);
				userOpinionBean.setCreateTime((Timestamp) row[10]);
				userOpinionList.add(userOpinionBean);
			}
		}

		logger.info("Exiting OpinionService getOpinionsByUserId method");
    	return userOpinionList;
    }

}
