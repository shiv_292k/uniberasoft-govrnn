/**
 * 
 */
package com.ohmuk.folitics.service;

/**
 * @author Kalpana
 *
 */
public interface IGPIAggregationSchedular {

	/**
	 * @param scheduleType
	 * @param aggregateOn
	 * @param threshold
	 * @return
	 */
	public String startMonitoring(String scheduleType);

	/**
	 * @param ScheduleType
	 * @return
	 */
	public String stopMonitoring(String ScheduleType);

}
