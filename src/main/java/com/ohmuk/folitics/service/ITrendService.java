/**
 * 
 */
package com.ohmuk.folitics.service;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.ouput.model.UserTrendOutputModel;

/**
 * @author Deewan
 *
 */
public interface ITrendService {
	String 	ADD = "add";
	String REMOVE = "remove";
	List<Trend> displayTrend(Trend trend) throws Exception;
	boolean verifyIfTrendnameExist(Trend trend) throws Exception;
	Trend saveTrend(Trend trend) throws Exception;
	List<User> top5User(String name) throws Exception;
	long saveTrendMapping(Trend trend)throws Exception;
	Trend findTrendByTrendName(Trend trend) throws Exception;
	long addToTrendMapping(TrendMapping trendMapping);
	List<Trend> searchExactTrend(Trend trend);
	List<Opinion> searchTrendOpinion(Long id);
	Trend readTrend(Long trendId) throws Exception;
	Long createTrendMapping(TrendMapping trendMapping);
	List<TrendOutputModel> matchingTrend(String name);
	List<TrendOutputModel> getTopTrends(int count, boolean onlyAdminTrends);
	List<UserTrendOutputModel> getTopTrendUsers(int count);
	void deleteTrend(Trend trend);
	/**
	 * @param trend
	 * @return
	 * @throws Exception
	 */
	Trend update(Trend trend) throws Exception;
	List<TrendOutputModel> getMostUsedTrends(int count) ;
	Trend getTrendByName(String trendIdorName) throws Exception;
	TrendComment readTrendComments(Long trendMappingId) throws Exception;
	void deleteTrendComments(TrendComment comment);
	List<Response> searchTrendResponse(Long id);
	List<TrendMapping> getTrendByComponetIdType(String type, Long componentId);
	List<TrendOutputModel> getTopAdminTrends(boolean onlyAdminTrends);	
	
	public List<UserTrendOutputModel> fetchAllTrendsByUserId(Long userId); 
}
