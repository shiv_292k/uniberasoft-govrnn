package com.ohmuk.folitics.service;

import java.util.List;
import java.util.Set;

import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.SentimentOpinionStat;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;

/**
 * @author Abhishek
 *
 */
public interface ISentimentService extends IBaseService {

	/**
	 * Method is to save {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return Sentiment
	 * @throws Exception
	 */
	Sentiment save(Sentiment sentiment) throws Exception;

	/**
	 * Method is to get {@link Sentiment} by id
	 * 
	 * @param id
	 * @return Sentiment
	 * @throws Exception
	 */
	Sentiment read(Long id) throws Exception;

	/**
	 * Method is to get all {@link Sentiment}
	 * 
	 * @return List<Sentiment>
	 * @throws Exception
	 */
	List<Sentiment> readAll() throws Exception;

	/**
	 * Method is to update sentiment
	 * 
	 * @param sentiment
	 * @return Sentiment
	 * @throws Exception
	 */
	Sentiment update(Sentiment sentiment) throws Exception;

	/**
	 * Method is to soft delete {@link Sentiment}
	 * 
	 * @param id
	 * @return boolean
	 * @throws Exception
	 */
	boolean delete(Long id) throws Exception;

	/**
	 * Method is to soft delete {@link Sentiment} by id
	 * 
	 * @param sentiment
	 * @return boolean
	 * @throws Exception
	 */
	boolean delete(Sentiment sentiment) throws Exception;

	/**
	 * Method is to hard delete {@link Sentiment}
	 * 
	 * @param Sentiment
	 * @return boolean
	 * @throws Exception
	 */
	boolean deleteFromDB(Sentiment sentiment) throws Exception;

	/**
	 * Method is to update {@link Sentiment} status by id
	 * 
	 * @param id
	 * @param status
	 * @return boolean
	 * @throws Exception
	 */
	boolean updateSentimentStatus(Long id, String status)
			throws Exception;

	/**
	 * Method is to clone {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return Sentiment
	 * @throws Exception
	 */
	Sentiment clone(Sentiment sentiment) throws Exception;

	/**
	 * Method is to get all {@link Sentiment} not id sentimentIds
	 * 
	 * @author Mayank Sharma
	 * @param sentimentIds
	 * @return List<Sentiment>
	 * @throws Exception
	 */
	List<Sentiment> getAllSentimentNotIn(Set<Long> sentimentIds)
			throws Exception;

	/**
	 * Method is to get all sources for {@link Sentiment}
	 * 
	 * @param sentiment
	 * @return List<Link>
	 * @throws Exception
	 */
	List<Link> getAllSourcesForSentiment(Sentiment sentiment)
			throws Exception;

	List<Sentiment> findByType(String type)throws Exception;
	
	Set<Sentiment> getRelatedSentiment(Long sentimentId) throws Exception;
	
	SentimentOpinionStat getSentimentOpinionStat(Long sentimentId) throws Exception;

	List<SentimentOutputModel> getAllSentimentByMatch(String title) throws Exception;

	List<SentimentOutputModel> getAllSentimentByType(String type) throws Exception;
	
	List<SentimentOutputModel> getAllSentimentsByType(String type) throws Exception;

    List<Sentiment> searchSentiment(String searchKeyword);

	List<Sentiment> findByName(String subject) throws Exception;
	
	SentimentOutputModel findById(String id) throws Exception;

	List<Sentiment> getAllSentimentByExactMatch(String subject) throws Exception;

	List<SentimentOutputModel> getPaginatedSentimentByType(String type, int firstResult, int maxResults)
			throws Exception;

	List<SentimentOutputModel> getSentimentNameList(String type) throws Exception;
	
	Sentiment updateSentimentNews(Long id, List<SentimentNews> sentimentNews) throws Exception;
}

