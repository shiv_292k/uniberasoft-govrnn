package com.ohmuk.folitics.service.newsfeed;

import com.ohmuk.folitics.ouput.model.SentimentOutputModel;

public interface ISentimentNewsLoaderService {
	
	boolean loadNewsForSentiment(SentimentOutputModel som);
	
	boolean loadNewsForAllSentiments();

}
