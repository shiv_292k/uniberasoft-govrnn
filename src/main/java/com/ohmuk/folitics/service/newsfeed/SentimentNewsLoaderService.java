package com.ohmuk.folitics.service.newsfeed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.ohmuk.folitics.enums.SentimentType;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedChannel;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedData;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedSource;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.service.ISentimentService;
import com.ohmuk.folitics.textsearch.lucene.SearchDocumentService;
import com.ohmuk.folitics.util.FoliticsUtils;

@Service
public class SentimentNewsLoaderService implements ISentimentNewsLoaderService {

	private static Logger LOGGER = LoggerFactory
			.getLogger(SentimentNewsLoaderService.class);

	public static final int MAX_NEWS_ITEMS = 50;

	@Autowired
	private volatile IFeedSourceService feedSourceService;

	@Autowired
	private volatile ISentimentService sentimentService;

	public boolean loadNewsForSentiment(SentimentOutputModel som) {
		try {
			LOGGER.info("sentiments ---- " + som.getSubject());
			List<String> keywordList = getKeywords(som);
			List<String> feedSources = getFeedSources(som);
			LOGGER.info("keywordList : " + keywordList);
			LOGGER.info("feedSources : " + feedSources);
			List<SentimentNews> sentimentNewsList = new ArrayList<SentimentNews>();
			if (!keywordList.isEmpty()) {
				List<FeedSource> fds = feedSourceService.readByName(
						feedSources, true);

				for (FeedSource fd : fds) {
					LOGGER.info("FeedSource : " + fd.getName());
					Set<FeedChannel> fdChannels = fd.getFeedChannels();
					Iterator<FeedChannel> iter = fdChannels.iterator();
					while (iter.hasNext()) {
						FeedChannel fdChannel = iter.next();
						LOGGER.info("FeedChannel : " + fdChannel.getTitle());
						Set<FeedData> feedDataList = fdChannel
								.getFeedDataLinks();
						Iterator<FeedData> iterfd = feedDataList.iterator();
						int count = 0;
						while (iterfd.hasNext()) {
							FeedData fdData = iterfd.next();
							LOGGER.info("FeeData : " + fdData.getTitle());
							SentimentNews sentimentNews = SentimentNews
									.getSentimentNewsFromFeeData(som.getId(),
											fdData, fd.getName(), fd.getGovrnnRanking());
							sentimentNewsList.add(sentimentNews);
							count++;
							if (count >= 10)
								break;
						}
					}
					if (sentimentNewsList.size() > MAX_NEWS_ITEMS) {
						break;
					}
				}
			}
			if (!sentimentNewsList.isEmpty()){
				sentimentNewsList = SearchDocumentService.search(keywordList,
						sentimentNewsList);
				sentimentNewsList = FoliticsUtils.removeDuplicateNews(sentimentNewsList);
				LOGGER.info("Final List of Sentiment news: ");
				LOGGER.info(sentimentNewsList.toString());
				sentimentService.updateSentimentNews(som.getId(), sentimentNewsList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean loadNewsForAllSentiments() {
		LOGGER.info("loadNewsForAllSentiments ----");
		loadNewsForAllSentiments(SentimentType.PERMANENT);
		//loadNewsForAllSentiments(SentimentType.TEMPORARY);
		return false;
	}

	private boolean loadNewsForAllSentiments(SentimentType type) {
		try {
			LOGGER.info("loadNewsForAllSentiments ---- " + type.getValue());
			List<SentimentOutputModel> sentiments = sentimentService
					.getSentimentNameList(type.getValue());
			LOGGER.info("sentiments ---- " + sentiments.size());
			for (SentimentOutputModel som : sentiments) {
				LOGGER.info("Sentiment Name : " + som.getSubject());
				loadNewsForSentiment(som);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static final List<String> getListFromString(String keywords,
			String separator) {
		List<String> list = new ArrayList<>();
		if (keywords == null || keywords.isEmpty())
			return list;
		list = Arrays.asList(keywords.split(separator));
		return trimStrings(list);
	}

	private static final List<String> trimStrings(List<String> list) {
		List<String> trimmedList = new ArrayList<String>();
		for (String l : list) {
			trimmedList.add(StringUtils.trimWhitespace(l));
		}
		return trimmedList;
	}

	private static final List<String> getKeywords(SentimentOutputModel som) {
		String keywordString = som.getKeywords();
		LOGGER.info("keywordString ---- " + keywordString);
		return getKeywords(keywordString);
	}

	private static final List<String> getFeedSources(SentimentOutputModel som) {
		String feedSources = som.getFeedSources();
		LOGGER.info("feedSources ---- " + feedSources);
		return getFeedSources(feedSources);
	}

	private static final List<String> getKeywords(String keywords) {
		return getListFromString(keywords, ",");
	}

	private static final List<String> getFeedSources(String feedSources) {
		return getListFromString(feedSources, ",");
	}
}
