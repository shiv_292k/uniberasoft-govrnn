package com.ohmuk.folitics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.elasticsearch.service.IESService;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.SentimentState;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.SentimentOpinionStat;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.model.ImageModel;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ElasticSearchUtils;
import com.ohmuk.folitics.util.Serialize_JSON;

/**
 * @author Abhishek
 *
 */
@Service
@Transactional
public class SentimentService implements ISentimentService {

	@Autowired
	IPollService pollService;

	@Autowired
	IESService esService;

	@Autowired
	IOpinionService opinionService;

	private static Logger logger = LoggerFactory.getLogger(SentimentOpinionStatService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}

	@Override
	public Sentiment save(Sentiment sentiment) throws Exception {
		logger.info("Inside SentimentService save method");
		Long id = (Long) getSession().save(sentiment);
		if (Constants.useElasticSearch)
			esService.save(ElasticSearchUtils.INDEX, ElasticSearchUtils.TYPE_SENTIMENT, String.valueOf(id),
					Serialize_JSON.getJSONString(sentiment));
		logger.info("Existing from SentimentService save method");
		return read(id);
	}

	@Override
	public Sentiment read(Long id) throws Exception {
		logger.info("Inside SentimentService read method");
		Sentiment sentiment = (Sentiment) getSession().get(Sentiment.class, id);
		if (sentiment != null) {
			Hibernate.initialize(sentiment.getPolls());
			if (sentiment.getPolls() != null && sentiment.getPolls().size() > 0) {
				for (Poll poll : sentiment.getPolls()) {
					Hibernate.initialize(poll.getOptions());
				}
			}
			Hibernate.initialize(sentiment.getCategories());
			Hibernate.initialize(sentiment.getSentimentNews());
			Hibernate.initialize(sentiment.getRelatedSentiments());
			Hibernate.initialize(sentiment.getAttachments());
		}
		logger.info("Existing from SentimentService read method");
		return sentiment;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sentiment> readAll() throws Exception {
		logger.info("Inside SentimentService readAll method");
		List<Sentiment> sentiments = getSession().createCriteria(Sentiment.class).list();
		logger.info("Existing from SentimentService readAll method");
		return sentiments;
	}

	@Override
	public Sentiment updateSentimentNews(Long id, List<SentimentNews> sentimentNews) throws Exception {
		logger.info("Inside SentimentService update method");
		Sentiment sentiment = read(id);
		if (sentiment != null) {
			logger.info("Inside SentimentService update method : " + sentiment.getSubject());

			for (SentimentNews sn : sentimentNews) {
				sn.setSentiment(sentiment);
				logger.info("HTML TextLenth : " + sn.getHtmlText().length() + " Plain text Length :  "
						+ sn.getPlainText().length());
			}
			sentiment.getSentimentNews().addAll(sentimentNews);
			sentiment.setEditTime(DateUtils.getSqlTimeStamp());
			getSession().update(sentiment);
		}
		logger.info("Existing from SentimentService update method");
		return sentiment;
	}

	@Override
	public Sentiment update(Sentiment sentiment) throws Exception {
		logger.info("Inside SentimentService update method");
		sentiment.setEditTime(DateUtils.getSqlTimeStamp());
		getSession().update(sentiment);
		logger.info("Existing from SentimentService update method");
		return sentiment;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside SentimentService delete method");
		Sentiment sentiment = read(id);
		if (sentiment != null) {
			sentiment.setState(SentimentState.DELETED.getValue());
			getSession().update(sentiment);
			logger.info("Existing from SentimentService delete method");

			return true;
		} else
			return false;
	}

	@Override
	public boolean delete(Sentiment sentiment) throws Exception {
		logger.info("Inside SentimentService delete method");
		if (read(sentiment.getId()) != null) {
			sentiment.setState(ComponentState.DELETED.getValue());
			getSession().update(sentiment);
			logger.info("Existing from SentimentService delete method");
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteFromDB(Sentiment sentiment) throws Exception {
		if (sentiment != null) {
			logger.info("Inside SentimentService deleteFromDB method : " + sentiment.getId());
			getSession().delete(sentiment);
			logger.info("Existing from SentimentService deleteFromDB method : ");
			return true;
		} else
			return false;
	}

	@Override
	public boolean updateSentimentStatus(Long id, String status) throws Exception {
		logger.info("Inside SentimentService updateSentimentStatus method");
		Sentiment sentiment = (Sentiment) getSession().get(Sentiment.class, id);
		logger.debug("sentiment id: " + sentiment.getId() + " sentiment State: " + sentiment.getState());
		sentiment.setState(status);
		logger.debug("sentiment id: " + sentiment.getId() + " sentiment State: " + sentiment.getState());
		getSession().update(sentiment);
		logger.info("Existing from SentimentService updateSentimentStatus method");
		return true;
	}

	@Override
	public Sentiment clone(Sentiment sentiment) throws Exception {
		logger.info("Inside SentimentService clone method");
		sentiment = (Sentiment) getSession().save(sentiment);
		logger.info("Existing from SentimentService clone method");
		return sentiment;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Sentiment> getAllSentimentNotIn(Set<Long> sId) throws Exception {
		logger.info("Inside SentimentService getAllSentimentNotIn method");
		Criteria criteria = getSession().createCriteria(Sentiment.class).add(Restrictions.and(
				Restrictions.not(Restrictions.in("id", sId)), Restrictions.eq("state", SentimentState.ALIVE.getValue()))

		);
		List<Sentiment> sentiments = criteria.list();
		logger.info("Existing from SentimentService getAllSentimentNotIn method");
		return sentiments;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Link> getAllSourcesForSentiment(Sentiment sentiment) throws Exception {
		logger.info("Inside SentimentService getAllSourcesForSentiment method");
		Set<Sentiment> relatedSentiments = sentiment.getRelatedSentiments();
		Criteria criteria = getSession().createCriteria(Link.class)
				.add(Restrictions.and(Restrictions.in("sentiment", relatedSentiments),
						Restrictions.eq("state", SentimentState.ALIVE.getValue())));
		List<Link> links = criteria.list();
		logger.info("Existing from SentimentService getAllSourcesForSentiment method");
		return links;
	}

	@Override
	public List<Sentiment> findByType(String type) throws Exception {
		logger.info("Inside SentimentService findbytype method");
		Criteria criteria = getSession().createCriteria(Sentiment.class);

		criteria.add(Restrictions.and(Restrictions.eq("type", type),
				Restrictions.eq("state", SentimentState.ALIVE.getValue()))).addOrder(Order.desc("createTime"));
		List<Sentiment> sentiments = criteria.list();
		return sentiments;

	}

	@Override
	public ImageModel getImageModel(Long entityId, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Sentiment> getRelatedSentiment(Long sentimentId) throws Exception {
		Sentiment sentiment = read(sentimentId);
		Hibernate.initialize(sentiment.getRelatedSentiments());
		return sentiment.getRelatedSentiments();
	}

	@Override
	public SentimentOpinionStat getSentimentOpinionStat(Long sentimentId) throws Exception {
		Criteria criteria = getSession().createCriteria(SentimentOpinionStat.class);
		return (SentimentOpinionStat) criteria.add(Restrictions.and(Restrictions.eq("id", sentimentId),
				Restrictions.eq("state", SentimentState.ALIVE.getValue()))).uniqueResult();
	}

	@Override
	public List<SentimentOutputModel> getAllSentimentByMatch(String title) throws Exception {

		logger.info("Inside SentimentSerivce getAllSentimentByMatch method");
		List<Sentiment> sentiments = null;
		List<SentimentOutputModel> models = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class)
					.add(Restrictions.and(Restrictions.like("subject", title, MatchMode.START),
							Restrictions.eq("state", SentimentState.ALIVE.getValue())));

			sentiments = criteria.list();
			if (null != sentiments) {
				models = new ArrayList<SentimentOutputModel>();
				for (Sentiment sentiment : sentiments) {
					SentimentOutputModel sentimentOutputModel = SentimentOutputModel.getModel(sentiment);
					sentimentOutputModel.setOpinionAggregation(opinionService.getOpinionsAggregate(sentiment.getId()));
					models.add(sentimentOutputModel);
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getAllSentimentByMatch for title : " + title);
			logger.info("Exiting SentimentSerivce getAllSentimentByMatch method");
		}

		if (null != sentiments) {
			logger.debug("Matching sentiment list with : " + title);
			logger.info("Exiting SentimentSerivce getAllSentimentByMatch method");
			return models;
		}
		logger.info("Exiting SentimentSerivce getAllSentimentByMatch method");
		return null;

	}

	@Override
	public List<Sentiment> getAllSentimentByExactMatch(String subject) throws Exception {

		logger.info("Inside SentimentSerivce getAllSentimentByExactMatch method");
		List<Sentiment> sentiments = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class).add(Restrictions.and(
					Restrictions.eq("subject", subject), Restrictions.eq("state", SentimentState.ALIVE.getValue())));

			sentiments = criteria.list();

		} catch (Exception exception) {
			logger.error("Exception in getAllSentimentByExactMatch for subject : " + subject);
			logger.info("Exiting SentimentSerivce getAllSentimentByExactMatch method");
		}
		logger.info("Exiting SentimentSerivce getAllSentimentByExactMatch method");
		return sentiments;

	}

	@Override
	public List<SentimentOutputModel> getAllSentimentByType(String type) throws Exception {

		logger.info("Inside SentimentSerivce getAllSentimentByType method");
		List<SentimentOutputModel> models = null;
		List<Sentiment> sentiments = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class)
					.add(Restrictions.and(Restrictions.eq("type", type),
							Restrictions.eq("state", SentimentState.ALIVE.getValue())))
					.addOrder(Order.desc("createTime"));
			sentiments = criteria.list();
			if (null != sentiments) {
				models = new ArrayList<SentimentOutputModel>();
				for (Sentiment sentiment : sentiments) {
					models.add(SentimentOutputModel.getModel(sentiment));
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in getAllSentimentByMatch for title type : " + exception);
			logger.info("Exiting SentimentSerivce getAllSentimentByType method");
		}

		if (null != models) {
			logger.debug("Matching sentiment list with type : " + type);
			logger.info("Exiting SentimentSerivce getAllSentimentByType method");
			return models;
		}
		logger.info("Exiting SentimentSerivce getAllSentimentByType method");
		return null;

	}
	@Override
	public List<SentimentOutputModel> getAllSentimentsByType(String type) throws Exception {

		logger.info("Inside SentimentSerivce getAllSentimentByType method");
		List<SentimentOutputModel> models = null;
		List<Sentiment> sentiments = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class)
					.add(Restrictions.and(Restrictions.eq("type", type)))
					.addOrder(Order.desc("createTime"));
			sentiments = criteria.list();
			if (null != sentiments) {
				models = new ArrayList<SentimentOutputModel>();
				for (Sentiment sentiment : sentiments) {
					models.add(SentimentOutputModel.getModel(sentiment));
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in getAllSentimentByMatch for title type : " + exception);
			logger.info("Exiting SentimentSerivce getAllSentimentByType method");
		}

		if (null != models) {
			logger.debug("Matching sentiment list with type : " + type);
			logger.info("Exiting SentimentSerivce getAllSentimentByType method");
			return models;
		}
		logger.info("Exiting SentimentSerivce getAllSentimentByType method");
		return null;

	}

	@Override
	public List<Sentiment> searchSentiment(String searchKeyword) {
		logger.info("Inside UserService getUserByMatch method");
		List<Sentiment> sentimentList = null;
		try {
			searchKeyword = "%" + searchKeyword + "%";
			Criteria criteria = getSession().createCriteria(Sentiment.class)
					.add(Restrictions.and(
							Restrictions.or(Restrictions.like("subject", searchKeyword),
									Restrictions.like("description", searchKeyword)),
							Restrictions.eq("state", SentimentState.ALIVE.getValue())));
			criteria.addOrder(Order.desc("createTime"));
			sentimentList = criteria.list();
		} catch (Exception exception) {
			logger.error("Exception in getUserByMatch for match name : " + searchKeyword);
		}

		if (null != sentimentList) {
			logger.debug("User list found starting with match name : " + searchKeyword);
			return sentimentList;
		}
		return null;
	}

	@Override
	public List<Sentiment> findByName(String subject) throws Exception {
		logger.info("Inside SentimentService findbytype method");
		List<Sentiment> sentiments = null;
		Criteria criteria = getSession().createCriteria(Sentiment.class);
		criteria.add(Restrictions.and(Restrictions.eq("subject", subject),
				Restrictions.eq("state", SentimentState.ALIVE.getValue())));
		criteria.addOrder(Order.desc("createTime"));
		sentiments = criteria.list();
		return sentiments;

	}

	@Override
	public SentimentOutputModel findById(String id) throws Exception {
		logger.info("Inside SentimentService findbytype method");
		Sentiment sentiment = null;
		logger.info("Inside SentimentSerivce findById method");
		SentimentOutputModel model = null;
		try {
			sentiment = getSession().get(Sentiment.class, Long.parseLong(id));
			if (null != sentiment) {
				model = SentimentOutputModel.getModel(sentiment);
			}

		} catch (Exception exception) {
			logger.error("Exception in findById for id : " + id);
			logger.info("Exiting SentimentSerivce findById method");
		}

		if (null != model) {
			logger.info("Exiting SentimentSerivce findById method");
			return model;
		}
		logger.info("Exiting SentimentSerivce findById method");
		return null;

	}

	@Override
	public List<SentimentOutputModel> getPaginatedSentimentByType(String type, int firstResult, int maxResults)
			throws Exception {

		logger.info("Inside SentimentSerivce getPaginatedSentimentByType method");
		List<SentimentOutputModel> models = null;
		List<Sentiment> sentiments = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class)
					.add(Restrictions.and(Restrictions.eq("type", type),
							Restrictions.eq("state", SentimentState.ALIVE.getValue())))
					.addOrder(Order.desc("createTime")).setFirstResult(firstResult).setMaxResults(8);
			sentiments = criteria.list();
			if (null != sentiments) {
				models = new ArrayList<SentimentOutputModel>();
				for (Sentiment sentiment : sentiments) {
					models.add(SentimentOutputModel.getModel(sentiment));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getPaginatedSentimentByType for title type : " + type);
			logger.info("Exiting SentimentSerivce getPaginatedSentimentByType method");
		}

		if (null != models) {
			logger.debug("Matching sentiment list with type : " + type);
			logger.info("Exiting SentimentSerivce getPaginatedSentimentByType method");
			return models;
		}
		logger.info("Exiting SentimentSerivce getPaginatedSentimentByType method");
		return null;
	}

	@Override
	public List<SentimentOutputModel> getSentimentNameList(String type) throws Exception {

		logger.info("Inside SentimentSerivce getSentimentNameList method");
		List<SentimentOutputModel> models = null;
		List<Sentiment> sentiments = null;
		try {
			Criteria criteria = getSession().createCriteria(Sentiment.class).add(Restrictions
					.and(Restrictions.eq("type", type), Restrictions.eq("state", SentimentState.ALIVE.getValue())));
			criteria.addOrder(Order.desc("createTime"));
			sentiments = criteria.list();
			if (null != sentiments) {
				models = new ArrayList<SentimentOutputModel>();
				for (Sentiment sentiment : sentiments) {
					models.add(SentimentOutputModel.getIdAndNameModel(sentiment));
				}
			}

		} catch (Exception exception) {
			logger.error("Exception in getSentimentNameList for title type : " + type);
			logger.info("Exiting SentimentSerivce getSentimentNameList method");
		}

		if (null != models) {
			logger.debug("Matching sentiment list with type : " + type);
			logger.info("Exiting SentimentSerivce getSentimentNameList method");
			return models;
		}
		logger.info("Exiting SentimentSerivce getSentimentNameList method");
		return null;

	}
}
