package com.ohmuk.folitics.service.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.service.interfaces.UserAdminAuthenticateService;

@Component("userAdminAuthenticateService")
public class UserAdminAuthenticateServiceImpl implements UserAdminAuthenticateService {

	private static Logger logger = LoggerFactory.getLogger(UserAdminAuthenticateServiceImpl.class);
	@Autowired
	private volatile IUserService userService;

	@Override
	public User authenticateUser(String userName, String password) {

		User user = null;
		try {
			user = userService.findByUsername(userName);
			logger.info("user : " + user);

			if (null != user) {
				if (user.getPassword().equals(password) && user.getUsername().equals(userName) && user.getRole() != null
						&& user.getRole().getUserRole() != null
						&& user.getRole().getUserRole().equalsIgnoreCase("ADMIN")) {
					logger.info("ADMIN" + user.getUsername());
					return user;
				}
			}
		} catch (Exception e) {
			logger.error("ERROR: Error occur while calling method findByUsername" + e);
		}
		return null;

	}
}
