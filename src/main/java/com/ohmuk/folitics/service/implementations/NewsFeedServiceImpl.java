package com.ohmuk.folitics.service.implementations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ohmuk.folitics.component.newsfeed.RSSFeedParser;
import com.ohmuk.folitics.dao.interfaces.NewsFeedDao;
import com.ohmuk.folitics.dto.RSSChannelLink;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.service.interfaces.NewsFeedService;
import com.ohmuk.folitics.xml.dto.RSS;

@Service("newsFeedService")
public class NewsFeedServiceImpl implements NewsFeedService {
	@Value("${rss.news.file}")
	private String excelFilePath;

	@Value("${file.sheet.number}")
	private int excelSheetNumber;

	@Autowired
	private RSSFeedParser parser;

	@Autowired
	private NewsFeedDao newsFeedDao;

	List<RSSChannelLink> rss = new ArrayList<RSSChannelLink>();

	private static Logger logger = LoggerFactory.getLogger(NewsFeedServiceImpl.class);

	@PostConstruct
	public void initLoadNews() {
		InputStream inputStream = null;
		Workbook workbook = null;
		try {
			try {
				logger.info("excelFilePath :: " + excelFilePath);
				inputStream = new FileInputStream(new File(excelFilePath));
				workbook = new XSSFWorkbook(inputStream);
			} catch (IOException e) {
				try {

					logger.error("Error occur while reading excel. Please check the file and location : " + e);
					logger.error("Trying with dummy data on classpath and file as sampleforNews.xlsx");
					// Reading dummy data
					inputStream = this.getClass().getClassLoader().getResourceAsStream("sampleforNews.xlsx");
					workbook = new XSSFWorkbook(inputStream);

				} catch (Exception e1) {
					logger.error("Error occur while reading excel: " + e1);
					e1.printStackTrace();
				}
			}
			Sheet firstSheet = workbook.getSheetAt(excelSheetNumber);
			Iterator<Row> iterator = firstSheet.iterator();

			List<String> list = new ArrayList<String>();

			while (iterator.hasNext()) {

				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Map<String, String> links = new HashMap<String, String>();
				RSSChannelLink rssLink = new RSSChannelLink();

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					if (cell.getRowIndex() == 0) {
						list.add(cell.getStringCellValue());
					} else {
						switch (cell.getColumnIndex()) {
						case 0:
							rssLink.setLanguage(cell.getStringCellValue());
							break;
						case 1:
							rssLink.setChannel(cell.getStringCellValue());
							break;
						default:
							if (cell.getStringCellValue() != null && !cell.getStringCellValue().isEmpty()) {
								links.put(list.get(cell.getColumnIndex()), cell.getStringCellValue());
							}
						}
					}
				}
				if (links.size() != 0) {
					rssLink.setCatagoryLink(links);
					rss.add(rssLink);
				}
			}
			logger.info("rss :: " + rss);

			inputStream.close();
		} catch (Exception e) {
			logger.error("" + e);
			e.printStackTrace();
		}

	}

	@Override
	public RSS getNewsByRSSLink(String rssLink) {
		parser.setUrl(rssLink);
		return parser.readFeed();
	}

	@Override
	public List<RSSChannelLink> loadChannelLinks() {

		if (rss == null) {
			initLoadNews();
		}
		return rss;
	}

	@Override
	public boolean addSentimentNews(List<SentimentNews> sentimentNewsList) {
		try {
			if (sentimentNewsList != null && sentimentNewsList.size() > 0) {
				sentimentNewsList.forEach(sentimentNews -> {
					newsFeedDao.save(sentimentNews);
				});
				return true;
			}
		} catch (Exception e) {
			logger.error("" + e);
			return false;
		}
		return false;
	}

	@Override
	public Boolean updatedSentimentNews(List<SentimentNews> sentimentNewsList) {
		try {
			if (sentimentNewsList != null && sentimentNewsList.size() > 0) {
				sentimentNewsList.forEach(sentimentNews -> {
					if (sentimentNews.getActive().equalsIgnoreCase("Y")) {
						newsFeedDao.save(sentimentNews);
					} else if (sentimentNews.getActive().equalsIgnoreCase("N")) {
						newsFeedDao.delete(sentimentNews);
					}
				});
				return true;
			}
		} catch (Exception e) {
			logger.error("" + e);
			return false;
		}
		return false;
	}

}
