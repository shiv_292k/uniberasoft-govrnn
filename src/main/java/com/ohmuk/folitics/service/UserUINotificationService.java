package com.ohmuk.folitics.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;
import com.ohmuk.folitics.model.ImageModel;

@Service
@Transactional
public class UserUINotificationService implements IUserUINotificationService {

	
	private static Logger logger = LoggerFactory
			.getLogger(SentimentOpinionStatService.class);

	@Autowired
	private SessionFactory _sessionFactory;

	private Session getSession() {
		return _sessionFactory.getCurrentSession();
	}
	
	
	@Override
	public UserUINotification save(UserUINotification userUINotification)
			throws Exception {
		logger.info("Inside UserUINotification save method");
		Long id = (Long) getSession().save(userUINotification);
		return read(id);
	}

	@Override
	public UserUINotification read(Long id)
			throws Exception {
		logger.info("Inside UserUINotificationService read method");
		UserUINotification userUINotification = (UserUINotification) getSession().get(UserUINotification.class, id);
		return userUINotification;
	}
	
	@Override
	public UserUINotification read(User user, String notificationType) throws Exception {
		logger.info("Inside UserUINotificationService read method");
		Criteria criteria = getSession().createCriteria(UserUINotification.class);
		criteria.add(Restrictions.and(Restrictions.eqOrIsNull("user", user), Restrictions.eq("notificationType", notificationType)));
		List<UserUINotification> userUINotifications = criteria.list();
		if(userUINotifications != null && !userUINotifications.isEmpty()){
			for (UserUINotification userUINotification : userUINotifications) {
					return userUINotification;
			}
		}
		logger.info("Exiting from read getByUserId method");
		return null;
	}

	@Override
	public UserUINotification update(UserUINotification userUINotification)
			throws Exception {
		logger.info("Inside UserUINotificationService update method");
		getSession().update(userUINotification);
		return userUINotification;
	}
	
	@Override
	public ImageModel getImageModel(Long entityId, boolean isThumbnail)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
