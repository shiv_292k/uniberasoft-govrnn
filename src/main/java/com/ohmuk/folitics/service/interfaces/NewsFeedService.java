package com.ohmuk.folitics.service.interfaces;

import java.util.List;

import com.ohmuk.folitics.dto.RSSChannelLink;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.xml.dto.RSS;

public interface NewsFeedService {

	public RSS getNewsByRSSLink(String rssLink);

	public List<RSSChannelLink> loadChannelLinks();

	public boolean addSentimentNews(List<SentimentNews> sentimentNews);

	public Boolean updatedSentimentNews(List<SentimentNews> sentimentNews);

}
