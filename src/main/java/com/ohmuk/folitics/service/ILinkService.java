/**
 * 
 */
package com.ohmuk.folitics.service;

import com.ohmuk.folitics.hibernate.entity.Link;

/**
 * @author Kalpana
 *
 */
public interface ILinkService extends IBaseService {
	
	public Link create(Link link) throws Exception;
	
	public Link read(Long id) throws Exception;

}
