/**
 * 
 */
package com.ohmuk.folitics.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.model.ImageModel;

/**
 * @author Kalpana
 *
 */
@Service
@Transactional
public class LinkService implements ILinkService {

	 private static Logger logger = LoggerFactory.getLogger(LinkService.class);

	    @Autowired
	    private SessionFactory _sessionFactory;

	    private Session getSession() {
	        return _sessionFactory.getCurrentSession();
	    }
	
	@Override
	public Link create(Link link) throws Exception {
		logger.info("Inside LinkService create method");
		Long linkId = (Long) getSession().save(link);
		logger.info("Exiting from LinkService create method");
		return read(linkId);
	}

	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.ILinkService#read(java.lang.Long)
	 */
	@Override
	public Link read(Long id) throws Exception {
		logger.info("Inside LinkService read method");
        logger.info("Exiting from LinkService read method");
        return (Link) getSession().get(Link.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.IBaseService#getImageModel(java.lang.Long, boolean)
	 */
	@Override
	public ImageModel getImageModel(Long entityId, boolean isThumbnail)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.IBaseService#getImageModels(java.lang.String, boolean)
	 */
	@Override
	public List<ImageModel> getImageModels(String entityIds, boolean isThumbnail) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.ohmuk.folitics.service.ILinkService#create(com.ohmuk.folitics.hibernate.entity.Link)
	 */
	

}
