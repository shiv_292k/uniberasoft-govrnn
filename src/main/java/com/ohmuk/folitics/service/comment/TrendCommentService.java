package com.ohmuk.folitics.service.comment;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ohmuk.folitics.beans.CommentBean;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.repository.comment.ITrendCommentRepository;

@Service
@Transactional
public class TrendCommentService implements ICommentService<TrendComment> {

	private static Logger logger = LoggerFactory
			.getLogger(TrendCommentService.class);

	@Autowired
	private ITrendCommentRepository trendCommentRepository;

	@Override
	public TrendComment create(CommentBean commentBean)
			throws MessageException, Exception {
		logger.debug("Inside create  TrendComment Service");

		if (commentBean.getComponentId() == null) {
			logger.error("trend mappind id found to be null");
			throw (new MessageException("trend mapping found to be null"));
		}

		if (commentBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId found to be null"));
		}

		// create TrendComment object from comment bean
		TrendComment trendComment = new TrendComment();
		trendComment.setUserId(commentBean.getUserId());
		trendComment.setTrendMappingId(commentBean.getComponentId());
		//trendComment.setComponentType(commentBean.getComponentType());
		trendComment.setComment(commentBean.getComment());

		trendCommentRepository.save(trendComment);

		logger.debug("Exiting create TrendComment service");
		return trendComment;

	}

	@Override
	public TrendComment read(Long id) throws MessageException {
		logger.info("Entered TrendComment service read method");
		if (id == null) {
			logger.error("Id found to be null");
			throw new MessageException("Id found to be null");
		}

		TrendComment trendComment = trendCommentRepository.find(id);

		logger.info("Exiting TrendComment service read method");
		return trendComment;
	}

	@Override
	public List<TrendComment> readAll() {

		logger.info("Entered TrendComment service readAll method");
		logger.debug("Getting all TrendComment");
		logger.info("Exiting TrendComment service readAll method");
		return trendCommentRepository.findAll();
	}

	@Override
	public TrendComment update(CommentBean commentBean)
			throws MessageException {
		
		logger.debug("Inside Update Comment method");

		TrendComment originalData = trendCommentRepository.find(commentBean.getId());
		if (originalData == null) {
			logger.error("No Record found in database for update");
			throw (new MessageException(
					"No Record found in database for update"));
		}
		originalData.setComment((commentBean.getComment()!= null) ? commentBean.getComment():originalData.getComment());
		originalData.setTrendMappingId((commentBean.getComponentId()!=null)?commentBean.getComponentId() :originalData.getTrendMappingId() );
		originalData.setUserId((commentBean.getUserId()!=null)?commentBean.getUserId():originalData.getUserId());

		TrendComment trendComment = trendCommentRepository
				.update(originalData);

		if (trendComment == null) {
			logger.error("Something went wrong, record not updated");
			throw (new MessageException(
					"Something went wrong, record not updated"));
		}

		logger.debug("Updated comment  : " + trendComment.getId());
		logger.debug("Exiting Update");

		return trendComment;
	}

	@Override
	public TrendComment delete(Long id) throws MessageException, Exception {
		logger.info("Inside hard delete comment by ID");

		if (id == null) {
			logger.error("comment ID found to be null");
			throw (new MessageException("Comment ID can't be null"));
		}

		TrendComment trendComment = trendCommentRepository.find(id);
		if (trendComment == null) {
			logger.error("Something went wrong, record not found for given id: "
					+ id + ", Can't delete record.");
			throw (new MessageException(
					"Something went wrong, record not found for given id: "
							+ id + ", Can't delete record."));
		}

		trendCommentRepository.delete(id);

		TrendComment tComment = trendCommentRepository.find(id);

		logger.debug("Deleted Comment :" + tComment);
		logger.debug("Exiting  delete comment by ID");

		return tComment;
	}

	@Override
	public List<TrendComment> getByComponentIdAndUserId(Long componentId,
			Long userId) throws MessageException {
		logger.debug("Entered getByComponentIdAndUserId Comment");

		List<TrendComment> trendComments = null;
		trendComments= trendCommentRepository.getByComponentIdAndUserId(
				componentId, userId);

		logger.debug("Comment fetched: " + trendComments.size());
		logger.debug("Exiting getByComponentIdAndUserId comment");

		return trendComments;
	}

	@Override
	public List<TrendComment> getAllCommentsForComponent(Long componentId)
			throws MessageException {
		logger.debug("Entered getAllCommentsForComponent method");

		List<TrendComment> trendComments = null;
		trendComments = trendCommentRepository
				.getAllCommentsForComponent(componentId);

		logger.debug("Comment fetched: " + trendComments.size());
		logger.debug("Exiting getAllCommentsForComponent method");

		return trendComments;
	}

	@Override
	public List<TrendComment> getAllCommentsForUserId(Long userId)
			throws MessageException {
		logger.debug("Entered getAllCommentsForUserId method");

		List<TrendComment> trendComments = null;
		trendComments = trendCommentRepository
				.getAllCommentsForUserId(userId);

		logger.debug("Comment fetched: " + trendComments.size());
		logger.debug("Exiting getAllCommentsForUserId method");

		return trendComments;
	}

}
