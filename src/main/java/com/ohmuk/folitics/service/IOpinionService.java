package com.ohmuk.folitics.service;

import java.util.LinkedHashSet;
import java.util.List;

import com.ohmuk.folitics.beans.UserOpinionDetailBean;
import com.ohmuk.folitics.charting.beans.OpinionAggregation;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Opinion;

/**
 * Service interface for entity: {@link Opinion}
 * 
 * @author Abhishek
 *
 */
public interface IOpinionService extends IBaseService {

    public final int OpinionMaxResults = 100;
    public final Long OpinionLimit = 10l;

    /**
     * Method to save the com.ohmuk.folitics.jpa.entity.Opinion object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.save
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    public Opinion create(Opinion opinion) throws Exception;

    /**
     * Method to get object of com.ohmuk.folitics.jpa.entity.Opinion using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.find by passing id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    public Opinion read(Long id) throws Exception;

    /**
     * Method to get all opinions using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.findAll
     * 
     * @author Abhishek
     * @return java.util.List<com.ohmuk.folitics.jpa.entity.Opinion>
     */
    public List<Opinion> readAll() throws Exception;

    /**
     * Method to update an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.save
     * 
     * @author Abhishek
     * @param com
     *            .ohmuk.folitics.jpa.entity.Opinion opinion
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    public Opinion update(Opinion opinion) throws Exception;

    /**
     * Method to permanent delete an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.delete by passing id
     * 
     * @author Abhishek
     * @param Long
     *            id
     * @return boolean
     */
    public boolean deleteFromDBById(Long id) throws Exception;

    /**
     * Method to permanent delete an object using
     * com.ohmuk.folitics.jpa.repository.IOpinionRepository.delete by passing
     * object of com.ohmuk.folitics.jpa.entity.Opinion
     * 
     * @author Abhishek
     * @param boolean
     * @return com.ohmuk.folitics.jpa.entity.Opinion
     */
    public boolean deleteFromDB(Opinion opinion) throws Exception;

    /**
     * @param user
     * @return List<Opinion>
     * @throws MessageException
     * @throws Exception
     */
    public List<Opinion> getOpinionByUser(Long userId) throws MessageException, Exception;

    public List<Opinion> getOpinionForSentiment(Long sentimentId) throws Exception;

    public OpinionAggregation getOpinionsAggregate(Long sentimentId) throws Exception;

    /**
     * @param sentimentId
     * @param componentId
     * @param componentType
     * @return
     * @throws Exception
     */
    public List<Opinion> getOpinionComponent(Long sentimentId, Long componentId, String componentType) throws Exception;

    /**
     * 
     * @param count
     * @return
     * @throws Exception
     */
    public List<Opinion> getLatestOpinions(int count) throws Exception;

    /**
     * @param sentimentId
     * @param sentimentType
     * @param count
     * @return
     * @throws Exception
     */
    public List<Opinion> getOpinionByType(Long sentimentId, String opinionType, int count) throws Exception;

    public LinkedHashSet<Opinion> searchOpinion(String searchKeyword);

    /**
     * @param sentimentId
     * @param userId
     * @return
     * @throws Exception
     */
    List<Opinion> getOpinionForSentimentAndUser(Long sentimentId, Long userId) throws Exception;

    List<Opinion> getRelatedOpinions(Long id, String text, int count) throws Exception;

    public LinkedHashSet<Opinion> searchOpinionBySearchKeywordAndSentimentId(String searchKeyword, Long sentimentId);

    List<Opinion> getRelatedOpinions(Long id, String text, int count, List<Long> opinionTrendId) throws Exception;
    
    public List<UserOpinionDetailBean> getOpinionsByUserId(Long userId, String month, String year, String opinionType);

}
