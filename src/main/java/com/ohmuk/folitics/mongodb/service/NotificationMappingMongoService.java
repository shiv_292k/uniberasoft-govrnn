package com.ohmuk.folitics.mongodb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.mongodb.dao.NotificationMappingMongodbDAO;
import com.ohmuk.folitics.mongodb.entity.NotificationMappingMongo;

@Service
@Transactional
public class NotificationMappingMongoService implements INotificationMappingMongoservice {

    @Autowired
    INotificationMappingMongoservice notificationMappingMongoRepository;

    
    @Autowired
	NotificationMappingMongodbDAO mappingMongodbDAO;

    @Override
    public NotificationMappingMongo save(NotificationMappingMongo notificationMapping) {

    	try{
        notificationMapping = notificationMappingMongoRepository.save(notificationMapping);
    	}
    	catch(Exception exception){
    		exception.printStackTrace();
    	}
        return notificationMapping;
    }

	@Override
	public boolean deleteByUserId(Long userId) {
		try {
			return mappingMongodbDAO.deleteByUserId(userId);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
    

}
