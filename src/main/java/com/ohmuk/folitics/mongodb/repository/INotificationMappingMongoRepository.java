package com.ohmuk.folitics.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ohmuk.folitics.mongodb.entity.NotificationMappingMongo;

public interface INotificationMappingMongoRepository extends MongoRepository<NotificationMappingMongo, String> {

	boolean deleteByUserId(Long userId);


}
