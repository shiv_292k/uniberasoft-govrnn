package com.ohmuk.folitics.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class PollDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String questionText;
	private ArrayList<OptionsDto> options;
	private String imgFile;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public ArrayList<OptionsDto> getOptions() {
		return options;
	}

	public void setOptions(ArrayList<OptionsDto> options) {
		this.options = options;
	}

	public String getImgFile() {
		return imgFile;
	}

	public void setImgFile(String imgFile) {
		this.imgFile = imgFile;
	}

	@Override
	public String toString() {
		return "PollDto [questionText=" + questionText + ", options=" + options + ", imgFile=" + imgFile + "]";
	}
}
