package com.ohmuk.folitics.dto;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.SentimentNews;

public class SentimentNewsFeed implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<SentimentNews> sentimentNews;

	public List<SentimentNews> getSentimentNews() {
		return sentimentNews;
	}

	public void setSentimentNews(List<SentimentNews> sentimentNews) {
		this.sentimentNews = sentimentNews;
	}

	@Override
	public String toString() {
		return "SentimentNewsFeed [sentimentNews=" + sentimentNews + "]";
	}

}
