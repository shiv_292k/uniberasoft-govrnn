package com.ohmuk.folitics.dto;

import java.io.Serializable;
import java.util.Map;

public class RSSChannelLink implements Serializable {

	private static final long serialVersionUID = 1L;
	private String language;
	private String channel;
	private Map<String, String> catagoryLink;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Map<String, String> getCatagoryLink() {
		return catagoryLink;
	}

	public void setCatagoryLink(Map<String, String> catagoryLink) {
		this.catagoryLink = catagoryLink;
	}

	@Override
	public String toString() {
		return "RSSChannelLink [language=" + language + ", channel=" + channel + ", catagoryLink=" + catagoryLink + "]";
	}

}
