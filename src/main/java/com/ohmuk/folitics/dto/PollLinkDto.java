package com.ohmuk.folitics.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class PollLinkDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<PollDto> polls;
	private ArrayList<LinkDto> links;

	public ArrayList<PollDto> getPolls() {
		return polls;
	}

	public void setPolls(ArrayList<PollDto> polls) {
		this.polls = polls;
	}

	public ArrayList<LinkDto> getLinks() {
		return links;
	}

	public void setLinks(ArrayList<LinkDto> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "PollLinkDto [polls=" + polls + ", links=" + links + "]";
	}
}
