package com.ohmuk.folitics.dto;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.ohmuk.folitics.hibernate.entity.Sentiment;

@Component
public class SentimentIdDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Sentiment sentiment;

	public Sentiment getSentiment() {
		return sentiment;
	}

	public void setSentiment(Sentiment sentiment) {
		this.sentiment = sentiment;
	}
}
