/**
 * 
 */
package com.ohmuk.folitics.util;

import java.util.ArrayList;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;

/**
 * @author Kalpana
 *
 */
public class PollUtil {
	private static String opinionQuestion = "Do you agree with this opinion?";
	private static String optionOpinion1 = "Yes";
	private static String optionOpinion2 = "No";
	
	private static String responseQuestion = "Do you like this response?";
	private static String optionresponse1 = "Up";
	private static String optionresponse2 = "Down";
	
	public static Poll createOpinionPoll(User user){
		Poll poll = new Poll();
		poll.setCreatedBy(user.getId());
		poll.setQuestion(opinionQuestion);
		List<PollOption> listPollOptions = new ArrayList<PollOption>();
		PollOption option1 = new PollOption();
		option1.setCreatedBy(user.getId());
		option1.setPoll(poll);
		option1.setPollOption(optionOpinion1);
		listPollOptions.add(option1);
		PollOption option2 = new PollOption();
		option2.setCreatedBy(user.getId());
		option2.setPoll(poll);
		option2.setPollOption(optionOpinion2);
		listPollOptions.add(option2);
		poll.setOptions(listPollOptions);
		return poll;
	}
	
	public static Poll createResponsePoll(User user){
		Poll poll = new Poll();
		poll.setCreatedBy(user.getId());
		poll.setQuestion(responseQuestion);
		List<PollOption> listPollOptions = new ArrayList<PollOption>();
		PollOption option1 = new PollOption();
		option1.setCreatedBy(user.getId());
		option1.setPoll(poll);
		option1.setPollOption(optionresponse1);
		listPollOptions.add(option1);
		PollOption option2 = new PollOption();
		option2.setCreatedBy(user.getId());
		option2.setPoll(poll);
		option2.setPollOption(optionresponse2);
		listPollOptions.add(option2);
		poll.setOptions(listPollOptions);
		return poll;
	}

}
