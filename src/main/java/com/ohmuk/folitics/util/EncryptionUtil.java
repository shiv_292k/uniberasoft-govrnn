package com.ohmuk.folitics.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptionUtil {
	protected static final Logger LOGGER = LoggerFactory
			.getLogger(EncryptionUtil.class);

	private static SecretKey secretKey;
	private static Cipher cipher;

	private static byte[] keyValue = new byte[] { '0', '2', '3', '4', '5', '6',
			'7', '8', '9', '1', '2', '3', '4', '5', '6', '7' };// your key

	static {
		try {
			secretKey = new SecretKeySpec(keyValue, "AES");
			System.out.println("secretKey : " + secretKey);
			cipher = Cipher.getInstance("AES");
		} catch (Exception e) {
			e.printStackTrace();
			new Throwable(e.getMessage());
		}
	}

	private static byte[] encryptBytes(String plainText, SecretKey secretKey,
			Cipher cipher) throws Exception {
		byte[] plainTextByte = plainText.getBytes();
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] encryptedByte = cipher.doFinal(plainTextByte);
		return encryptedByte;
	}

	public static String encryptStr(String str) {
		try {
			byte[] utf8 = str.getBytes("UTF8");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] enc = cipher.doFinal(utf8);
			//byte[] bytesEncoded = Base64.getEncoder().encode(enc);
			return new String(enc);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (javax.crypto.BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String encryptBase64(String str) {
		try {
			byte[] utf8 = str.getBytes("UTF8");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			byte[] enc = cipher.doFinal(utf8);
			byte[] bytesEncoded = Base64.getEncoder().encode(enc);
			return new String(bytesEncoded);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (javax.crypto.BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String decryptBase64(String str) {
		try {
			byte[] dec = Base64.getDecoder().decode(str);
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			byte[] utf8 = cipher.doFinal(dec);
			return new String(utf8, "UTF8");
		} catch (javax.crypto.BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static String decrypt(byte[] encryptedTextByte,
			SecretKey secretKey, Cipher cipher) throws Exception {
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
		String decryptedText = new String(decryptedByte);
		return decryptedText;
	}
	
	public static byte[] encrypt(String plainText) throws Exception {
		return encryptBytes(plainText, secretKey, cipher);
	}

	public static String decrypt(String base64EncodedEncryptedText)
			throws Exception {
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] encryptedTextByte = decoder.decode(base64EncodedEncryptedText);
		return decrypt(encryptedTextByte, secretKey, cipher);
	}

	public static final byte[] getEncryptedToken(String plainToken) {
		try {
			return EncryptionUtil.encrypt(plainToken);
		} catch (Exception e) {
			LOGGER.error("Encryption failed for token : " + plainToken);
		}
		return null;
	}

	public static final String getDecryptedToken(String base64EncryptedToken) {
		try {
			return EncryptionUtil.decrypt(base64EncryptedToken);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("decryption failed for string : "
					+ base64EncryptedToken);
		}
		return null;
	}

	public static final void main(String []args){
		/**
		 * YK5B5HQV
		 * 123456 
		 */
		List<String> passwordList =new ArrayList<String>();
		passwordList.add("UIZB2O3W");
		passwordList.add("cancel123");
		passwordList.add("abc123");
		passwordList.add("123456");
		passwordList.add("dishant4");
		passwordList.add("Kavya@1234");
		passwordList.add("horward123");
		passwordList.add("123456");
		passwordList.add("12345678");
		passwordList.add("XKMKCXZG");
		passwordList.add("majid0751");
		passwordList.add("123456789");
		passwordList.add("govrnnsonal4");
		passwordList.add("mad050295");
		passwordList.add("18september");
		passwordList.add("Kshitij@1708");
		passwordList.add("pink9873673940");
		passwordList.add("shapeofyou");
		passwordList.add("Kshitij@1708");
		passwordList.add("test@12");
		passwordList.add("VGFXJ9ZY");
		for(String password:passwordList){
			String encryptedPassword = "VgKlKKU35Rw6Wx4Q8EHq+w==";//encryptBase64(password);
			String decryptedStr  = decryptBase64(encryptedPassword);
			System.out.println("Password : "+password+", encryptedPassword : "+encryptedPassword+", decryptedStr : "+decryptedStr);
			System.out.println("update folitics_prod3.user set password='"+encryptedPassword+"' where password='"+password+"'");
		}
		
	}
}
