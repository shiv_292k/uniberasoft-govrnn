package com.ohmuk.folitics.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.enums.AttachmentType;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.attachment.AttachmentFile;

public class AttachmentUtil {

	protected static final Logger LOGGER = LoggerFactory.getLogger(AttachmentUtil.class);

    public static final String EXTERNAL_FILE_PATH="C:/downloads/image.jpg";
    
	/**
	 * Method is to prepare the attachment object
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static Attachment prepareAttachment(MultipartFile file, Attachment attachment) throws Exception {
		if (file != null) {
			String[] contentType = file.getContentType().split("/");
			String fileType = FileType.getFileType(file.getOriginalFilename().split("\\.")[1]).getValue();
			
			if (contentType[0].equals(AttachmentType.IMAGE.getValue())) {
				//Attachment attachment = new Attachment();
				attachment.setAttachmentType(AttachmentType.IMAGE.getValue());
				attachment.setTitle(file.getName());
				attachment.setFileType(fileType);
				AttachmentFile attachmentFile = new AttachmentFile();
				try {
					attachmentFile.setData(file.getBytes());
				} catch (IOException exception) {
					LOGGER.error("Some exception occured while reading file");
					LOGGER.error("Exception: " + exception);
					throw new Exception("Some exception occured while reading file", exception.getCause());
				}
				attachmentFile.setType(FileType.getFileType(contentType[1]).getValue());
				attachment.setAttachmentFile(attachmentFile);
	
				return attachment;
	
			} else {
				// TODO save audio video in disk somewhere
			}
		}
		return null;
	}
	
	}
