package com.ohmuk.folitics.util;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;

import org.simmetrics.metrics.JaroWinkler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserImage;

//Usage of Simmetrics

// String encrytpedStr = prepareEncryptedToken(userId, emailId);
// System.out.println("decyptedencryptedToken "+encrytpedStr);
//Usage of Apache Commons Lang 3

public class FoliticsUtils {

	public static String SOMETHING_WENT_WRONG_ERROR = "Oops! something went wrong! Please contact us via feedback form.";

	public static final String DEFAULT_IMAGE = "." + File.separator
			+ "testdata" + File.separator + "user" + File.separator
			+ "default.jpg";
	public static final String DEFAULT__REFERENCE_IMAGE = "." + File.separator
			+ "testdata" + File.separator + "sentiment" + File.separator
			+ "reference.png";

	protected static final Logger LOGGER = LoggerFactory
			.getLogger(FoliticsUtils.class);

	public static final String getUserName() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth == null)
			return null;
		return auth.getName();
	}

	public static double getRoundedDouble(double value) {

		DecimalFormat f = new DecimalFormat("##.00");
		return Double.parseDouble(f.format(value));
	}

	public static final String getToken(String userId, String emailId) {
		StringBuffer sb = new StringBuffer();
		sb.append("userId=" + userId).append(",emailId=" + emailId);
		return sb.toString();
	}

	public static final String prepareEncryptedBase64EncodedToken(
			String userId, String emailId) {
		String base64Encoded = EncryptionUtil.encryptBase64(getToken(userId,
				emailId));
		System.out.println("base64Encoded : " + base64Encoded);
		return base64Encoded;
	}

//	public static final String prepareEncryptedToken(String userId,
//			String emailId) {
//		String encryptedStr = EncryptionUtil.encryptStr(getToken(userId,
//				emailId));
//		System.out.println("encryptedStr : " + encryptedStr);
//		return encryptedStr;
//	}

//	public static final byte[] prepareToken(String userId, String emailId) {
//		return EncryptionUtil.getEncryptedToken(getToken(userId, emailId));
//	}

	public static final void main(String[] args) {
		String userId = "jahid123";
		String emailId = "jahidiitr@gmail.com";
		// String encrytpedStr = prepareEncryptedToken(userId, emailId);
		// System.out.println("decyptedencryptedToken "+encrytpedStr);
		String base64EncryptedToken = prepareEncryptedBase64EncodedToken(
				userId, emailId);
		try {
			System.out
					.println("base64EncryptedToken : " + base64EncryptedToken);
			String encodedUrl = URLEncoder
					.encode(base64EncryptedToken, "UTF-8");
			System.out.println("encodedUrl : " + encodedUrl);
			String decodedUrl = URLDecoder.decode(encodedUrl, "UTF-8");
			System.out.println("decodedUrl : " + decodedUrl);
			System.out.println("decyptedencryptedToken : "
					+ EncryptionUtil.decryptBase64(decodedUrl));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final String[] parseBase64EncodedToken(
			String base64EncryptedToken) {
		String plainToken = EncryptionUtil
				.getDecryptedToken(base64EncryptedToken);
		System.out.println(plainToken);
		if (StringUtils.isEmpty(plainToken))
			return null;
		else {
			String[] tokenArray = plainToken.split(",");
			String[] userName = tokenArray[0].split("=");
			String[] email = tokenArray[1].split("=");
			String[] dataArray = new String[2];
			dataArray[0] = userName[1];
			dataArray[1] = email[1];
			return dataArray;
		}
	}

	public static final String sanitizeURL(String url) {
		if (!StringUtils.isEmpty(url)) {
			String urlSmallCase = url.toLowerCase();
			if (urlSmallCase.contains("http://")) {
				return urlSmallCase.replace("http://", "");
			}
			if (urlSmallCase.contains("https://")) {
				return urlSmallCase.replace("https://", "");
			}
			return urlSmallCase;
		}
		return url;
	}

	public static final byte[] getDefaultUserImage() {
		return (byte[]) SingletonCache.getInstance().get(
				FoliticsCache.KEYS.USER_IMAGE);
	}

	public static byte[] getImageBytes(User user) {
		byte[] image = new byte[5248];
		try {
			if (user.getUserImages() != null && !user.getUserImages().isEmpty()) {
				UserImage ui = ImageUtil.getUserImage(
						ImageTypeEnum.USERIMAGE.getImageType(),
						user.getUserImages());
				if (ui != null) {
					image = ThumbnailUtil.getImageThumbnail(ui.getImage(),
							ui.getFileType());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("Error while creating snapshot for user");
			image = FoliticsUtils.getDefaultUserImage();
		}
		return image;
	}

	public static List<SentimentNews> removeDuplicateNews(List<SentimentNews> sentimentNews){
		Iterator<SentimentNews> ite=  sentimentNews.iterator();
		while(ite.hasNext()){
			SentimentNews s1 = ite.next();
			Iterator<SentimentNews> ite2=  sentimentNews.iterator();
			while(ite2.hasNext()){
				SentimentNews s2 = ite2.next();
				double distanceTitle = compareStrings(s1.getTitle(), s2.getTitle());
				double distanceText = compareStrings(s1.getPlainText(), s2.getPlainText());
				//if((distanceText+distanceTitle)>0)
				{
					LOGGER.info("distanceTitle : "+distanceTitle+", distanceText : "+distanceText);
					LOGGER.info("s1.getTitle() : "+s1.getTitle());
					LOGGER.info("s2.getTitle() : "+s2.getTitle());
//					if(s1.getRanking()>s2.getRanking()){
//						ite2.remove();
//					}
//					else{
//						ite.remove();
//					}
				}
			}
		}
		return sentimentNews;
	}
	// Usage of Apache Commons Lang 3
	public static double compareStrings(String stringA, String stringB) {
		return org.apache.commons.lang3.StringUtils.getLevenshteinDistance(
				stringA, stringB);
	}

	public static double compareStringsJaroWinkler(String a, String b) {
		JaroWinkler algorithm = new JaroWinkler();
		return algorithm.compare(a, b);
	}
}
