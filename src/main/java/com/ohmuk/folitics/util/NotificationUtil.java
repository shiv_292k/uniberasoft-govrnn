package com.ohmuk.folitics.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.notification.NotificationMapping;

public class NotificationUtil {

	protected static final Logger LOGGER = LoggerFactory.getLogger(NotificationUtil.class);

	/**
	 * Method is to prepare the NotificationMapping object
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static NotificationMapping prepareOpinionNotification(Opinion opinion) throws Exception {
		NotificationMapping notificationMapping = new NotificationMapping();
		notificationMapping.setUserId(opinion.getUser().getId());
		notificationMapping.setComponentType(ComponentType.OPINION.getValue());
		notificationMapping.setComponentId(opinion.getId());
		return notificationMapping;

	}

	public static NotificationMapping createNotificationMappingForResponse(User user, String componentType,
			Long componentId) throws Exception {
		NotificationMapping notificationMapping = new NotificationMapping();
		notificationMapping.setUserId(user.getId());
		notificationMapping.setComponentType(componentType);
		notificationMapping.setComponentId(componentId);
		notificationMapping.setParentComponent(ComponentType.OPINION.getValue());
		return notificationMapping;

	}

	public static NotificationMapping createGeneralNotificationMapping(User user, String componentType,
			Long componentId) throws Exception {
		NotificationMapping notificationMapping = new NotificationMapping();
		notificationMapping.setUserId(user.getId());
		notificationMapping.setComponentType(componentType);
		notificationMapping.setComponentId(componentId);
		return notificationMapping;

	}

}
