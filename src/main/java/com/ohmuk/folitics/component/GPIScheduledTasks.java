package com.ohmuk.folitics.component;

import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.service.GPIAggregationSchedular;
import com.ohmuk.folitics.util.SchedulerUtil;

/**
 * @author Kalpana
 *
 */

@Component("gpiScheduledTasks")
public class GPIScheduledTasks {
	
	 protected static final Logger LOGGER = LoggerFactory
				.getLogger(GPIAggregationSchedular.class);

		private static final ScheduledExecutorService executorServicePerHour = Executors
				.newSingleThreadScheduledExecutor();
		ScheduledFuture<?> futurePerHour = null;
		private static final ScheduledExecutorService executorServicePerDay = Executors
				.newSingleThreadScheduledExecutor();
		ScheduledFuture<?> futurePerDay = null;

    private static final Logger log = LoggerFactory.getLogger(GPIScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

   /* @Scheduled(fixedRate = 3600000)
    public void reportCurrentTime() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        //Need to call GPI Aggregate method
    }*/
    
   public void getString(){
	   System.out.println("gpi schedule");
   }

	public String startMonitoring(String scheduleType, String aggregateOn,
			int threshold) {
		System.out.println("Schedule type   +   "+scheduleType);
		if (scheduleType.equals(SchedulerUtil.ScheduleType.DAILY)) {
			if (!isRunning(futurePerDay)) {
				futurePerDay = executorServicePerDay.scheduleAtFixedRate(
						() -> {
							// call point calculation here.
						}, 0, 24, TimeUnit.SECONDS);
				return scheduleType + " process has started successfully!";
			}
			return scheduleType + " process is already running!";
		} else if (scheduleType.equals(SchedulerUtil.ScheduleType.HOURLY)) {
			if (!isRunning(futurePerHour)) {
				futurePerHour = executorServicePerHour
						.scheduleAtFixedRate(() -> {
							// blackListedService.findAndBlackListPerHour(aggregateOn,threshold);
							// @Scheduled(fixedRate = 5000)
								System.out
										.println(" ************** Starting process Sucessfully started ************ ");

							}, 0, 1, TimeUnit.SECONDS);
				return scheduleType + " process has started successfully!";
			}
			return scheduleType + " process is already running!";
		} else
			return scheduleType + " process is not found!!";
	}

	public static boolean isRunning(ScheduledFuture<?> future) {
		if (future != null) {
			return future.getDelay(TimeUnit.MILLISECONDS) <= 0;
		} else
			return false;
	}

	public String stopMonitoring(String ScheduleType) {
		if (ScheduleType.equals(SchedulerUtil.ScheduleType.DAILY)) {
			if (futurePerDay != null) {
				LOGGER.info("futurePerDay.isRunning: "
						+ isRunning(futurePerDay));
				while (isRunning(futurePerDay)) {
					LOGGER.debug("The task is still ruuning , waiting for daily task to finish!");
					break;
				}
				if (!executorServicePerDay.isShutdown()
						&& !executorServicePerDay.isTerminated()) {
					executorServicePerDay.shutdown();
					return ScheduleType + " process is stopped successfully!!";
				}

			}
			return ScheduleType + " process was not inaitialzed!!";
		} else if (ScheduleType.equals(SchedulerUtil.ScheduleType.HOURLY)) {
			if (futurePerHour != null) {
				LOGGER.info("futurePerHour.isRunning: "
						+ isRunning(futurePerHour));
				while (isRunning(futurePerHour)) {
					LOGGER.info("The task is still ruuning , waiting for hourly task to finish!");
					break;
				}
				if (!executorServicePerHour.isShutdown()
						&& !executorServicePerHour.isTerminated()) {
					executorServicePerHour.shutdown();
					return ScheduleType + " process is stopped successfully!!";

				}
			}
			return ScheduleType + " process was not inaitialzed!!";
		}
		return ScheduleType + " process is not found!!";
	}

}