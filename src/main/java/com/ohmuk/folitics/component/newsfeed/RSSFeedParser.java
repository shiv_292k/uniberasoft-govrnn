package com.ohmuk.folitics.component.newsfeed;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.xml.dto.RSS;


@Component
public class RSSFeedParser {
	private static Logger logger = LoggerFactory.getLogger(RSSFeedParser.class);
	private URL url;

	public RSS readFeed() {
		RSS news = null;

		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(RSS.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			if (url != null) {
				news = (RSS) jaxbUnmarshaller.unmarshal(url.openStream());
			}
		} catch (JAXBException e) {
			logger.error("ERROR : Error occur while creating jaxbContext object", e);
		} catch (IOException e) {
			logger.error("ERROR : Error occur while creating jaxbContext object", e);
		}

		return news;
	}

	public void setUrl(String url) {

		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

}
