/**
 * 
 */
package com.ohmuk.folitics.component.newsfeed;

/**
 * @author Kalpana
 *
 */
public enum ImageTypeEnum {
	USERIMAGE("userimage"),
	ALBUMIMAGE("albumimage");
	
	private String imageType;
	/**
	 * 
	 */
	private ImageTypeEnum(String imageType) {
		// TODO Auto-generated constructor stub
		this.imageType = imageType;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	
	public static final ImageTypeEnum getImageTypeEnum(String imageType){
		if(USERIMAGE.getImageType().equals(imageType))
			return USERIMAGE;
		else if(ALBUMIMAGE.getImageType().equals(imageType))
			return ALBUMIMAGE;
		return null;
	}
	
}
