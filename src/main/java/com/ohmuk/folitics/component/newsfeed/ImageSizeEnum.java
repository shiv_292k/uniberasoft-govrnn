package com.ohmuk.folitics.component.newsfeed;

public enum ImageSizeEnum {
	THUMBNAIL("thumbnail"),
	STANDARD("standard"),
	SNAPSHOT("snapshot");
	
	private String imageSize;

	private ImageSizeEnum(String imageSize){
		this.imageSize=imageSize;
	}
	
	public String getImageSize() {
		return imageSize;
	}

	public void setImageSize(String imageSize) {
		this.imageSize = imageSize;
	}
	
	public static final ImageSizeEnum getImageSizeEnum(String imageSize){
		if(THUMBNAIL.getImageSize().equals(imageSize))
			return THUMBNAIL;
		else if(STANDARD.getImageSize().equals(imageSize))
			return STANDARD;
		else if(SNAPSHOT.getImageSize().equals(imageSize))
			return SNAPSHOT;
		return null;
	}

}
