package com.ohmuk.folitics.hibernate.entity.rating;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Soumya
 *
 */
@Embeddable

public class RatingId implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    @NotNull(message = "error.RatingId.componentId.notNull")
    private Long componentId;

    @Column(nullable = false)
    @NotNull(message = "error.RatingId.userId.notNull")
    private Long userId;

    @Column(nullable = false)
    @NotNull(message = "error.RatingId.parentId.notNull")
    private Long parentId;

    @Column
    @NotNull(message = "error.RatingId.feelType.notNull")
    private String feelType;

    public Long getComponentId() {
        return componentId;
    }

    public void setComponentId(Long componentId) {
        this.componentId = componentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the feelType
	 */
	public String getFeelType() {
		return feelType;
	}

	/**
	 * @param feelType the feelType to set
	 */
	public void setFeelType(String feelType) {
		this.feelType = feelType;
	}

	
}
