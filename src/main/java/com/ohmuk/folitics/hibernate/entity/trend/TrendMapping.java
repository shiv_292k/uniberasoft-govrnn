package com.ohmuk.folitics.hibernate.entity.trend;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.util.DateUtils;

@Entity
@Table(name = "trendmapping")
public class TrendMapping implements Serializable {
	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(unique = false, nullable = false)
	private Long componentId;

	@Column(unique = false, nullable = false)
	private Long userId;

	@Column(unique = false, nullable = false, columnDefinition = "enum('Opinion','Response')")
	private String componentType;

	@ManyToOne
	// (fetch = FetchType.LAZY)
	@JoinColumn(name = "trendId", referencedColumnName = "id")
	@JsonBackReference
	private Trend trend;

	@Column(nullable = false)
	@NotNull(message = "error.trend.updateTimeStamp.notNull")
	private Timestamp updateTimeStamp;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pollId", referencedColumnName = "id")
    @Cascade(value = CascadeType.ALL)
    private Poll funnyFakePoll;

	public TrendMapping() {
		// setComponentType(ComponentType.SENTIMENT.getValue());
		setUpdateTimeStamp(DateUtils.getSqlTimeStamp());
	}

	public Timestamp getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(Timestamp updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public Trend getTrend() {
		return trend;
	}

	public void setTrend(Trend trend) {
		this.trend = trend;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
