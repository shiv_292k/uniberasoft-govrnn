package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.hibernate.entity.newsfeed.FeedData;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * @author soumya
 * 
 */
@Entity
@Table(name = "sentiment_news")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class SentimentNews implements Serializable, Comparable<SentimentNews> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = true, length = 500)
	private String feedSource;

	@Column(nullable = true)
	private String category;

	@Column(nullable = true, length = 500)
	private String title;

	@Column(nullable = true, length = 2000)
	private String link;

	@Column(nullable = true, length = 1000)
	private String previewImageLink;

	@Lob
	private byte[] previewImagePath;

	@Column(nullable = true, length = 5000)
	private String htmlText;

	@Column(nullable = true, length = 2000)
	private String plainText;

	@Column(nullable = true, length = 2000)
	private String feedURL;

	@Column(nullable = true, length = 500)
	private String feedName;

	@Column(nullable = true, length = 500)
	private String writer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sentimentId", referencedColumnName = "id")
	private Sentiment sentiment;

	@Column(nullable = false)
	@NotNull(message = "error.sentiment.createTime.notNull")
	private Timestamp createTime;

	@Transient
	private int ranking;
	
	@Transient
	private String active;
	
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public String getFormattedAge() {
		return DateUtils.getDateOrTimeAgo(this.createTime);
	}

	public SentimentNews() {
		setCreateTime(DateUtils.getSqlTimeStamp());
	}

	/**
	 * @return the feedSource
	 */
	public String getFeedSource() {
		return feedSource;
	}

	/**
	 * @param feedSource
	 *            the feedSource to set
	 */
	public void setFeedSource(String feedSource) {
		this.feedSource = feedSource;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the htmlText
	 */
	public String getHtmlText() {
		return htmlText;
	}

	/**
	 * @param htmlText
	 *            the htmlText to set
	 */
	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}

	/**
	 * @return the plainText
	 */
	public String getPlainText() {
		return plainText;
	}

	/**
	 * @param plainText
	 *            the plainText to set
	 */
	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

	/**
	 * @return the feedURL
	 */
	public String getFeedURL() {
		return feedURL;
	}

	/**
	 * @param feedURL
	 *            the feedURL to set
	 */
	public void setFeedURL(String feedURL) {
		this.feedURL = feedURL;
	}

	/**
	 * @return the feedName
	 */
	public String getFeedName() {
		return feedName;
	}

	/**
	 * @param feedName
	 *            the feedName to set
	 */
	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	/**
	 * @return the sentiment
	 */
	public Sentiment getSentiment() {
		return sentiment;
	}

	/**
	 * @param sentiment
	 *            the sentiment to set
	 */
	public void setSentiment(Sentiment sentiment) {
		this.sentiment = sentiment;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the writer
	 */
	public String getWriter() {
		return writer;
	}

	/**
	 * @param writer
	 *            the writer to set
	 */
	public void setWriter(String writer) {
		this.writer = writer;
	}

	/**
	 * @return the createTime
	 */
	public Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getPreviewImageLink() {
		return previewImageLink;
	}

	public void setPreviewImageLink(String previewImageLink) {
		this.previewImageLink = previewImageLink;
	}

	/**
	 * @return the previewImagePath
	 */
	public byte[] getPreviewImagePath() {
		return previewImagePath;
	}

	/**
	 * @param previewImagePath
	 *            the previewImagePath to set
	 */
	public void setPreviewImagePath(byte[] previewImagePath) {
		this.previewImagePath = previewImagePath;
	}

	@Override
	public int compareTo(SentimentNews o) {
		if (getCreateTime() == null || o.getCreateTime() == null)
			return 0;
		return getCreateTime().compareTo(o.getCreateTime());
	}

	public static final SentimentNews getSentimentNewsFromFeeData(
			Long sentimentId, FeedData fdData, String feedName, int ranking) {
		SentimentNews sentimentNews = new SentimentNews();
		sentimentNews.setCategory(fdData.getCategory());
		sentimentNews.setFeedName(feedName);
		sentimentNews.setFeedSource(fdData.getFeedChannel().getFeedSource()
				.getName());
		sentimentNews.setFeedURL(fdData.getFeedChannel().getLink());
		sentimentNews.setHtmlText( org.apache.commons.lang3.StringUtils.substring(fdData.getPlainText(), 0, 2000));
		sentimentNews.setLink(fdData.getLink());
		sentimentNews.setPlainText(org.apache.commons.lang3.StringUtils.substring(fdData.getPlainText(), 0, 2000) );
		sentimentNews.setPreviewImageLink(fdData.getImageUrl());
		sentimentNews.setTitle(fdData.getTitle());
		sentimentNews.setWriter(fdData.getAuthor());
		sentimentNews.setRanking(ranking);
		return sentimentNews;
	}

	@Override
	public String toString() {
		return "SentimentNews [id=" + id + ", feedSource=" + feedSource + ", category=" + category + ", title=" + title
				+ ", link=" + link + ", previewImageLink=" + previewImageLink + ", previewImagePath="
				+ Arrays.toString(previewImagePath) + ", htmlText=" + htmlText + ", plainText=" + plainText
				+ ", feedURL=" + feedURL + ", feedName=" + feedName + ", writer=" + writer + ", sentiment=" + sentiment
				+ ", createTime=" + createTime + ", ranking=" + ranking + ", active=" + active + "]";
	}
	
	
}
