package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

@Entity
@Table(name = "newsHeading")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class NewsHeading implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NewsHeadingId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String title;

	@Column
	private String link;

	@Column(columnDefinition = "enum('Sentiment','BreakingNews','Both')")
	private String newsFeedType;

	@Column
	private String imageUrl;

	@Column
	private byte[] image;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getNewsFeedType() {
		return newsFeedType;
	}

	public void setNewsFeedType(String newsFeedType) {
		this.newsFeedType = newsFeedType;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "NewsHeading [id=" + id + ", title=" + title + ", link=" + link + ", newsFeedType=" + newsFeedType
				+ ", imageUrl=" + imageUrl + ", image=" + Arrays.toString(image) + "]";
	}

}
