package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: UserUINotification
 * 
 * @author Mayank Sharma
 *
 */

@Entity
@Table(name = "User_UI_Notification")
public class UserUINotification implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(nullable = true, columnDefinition = "enum('General','Opinion')")
	//@Size(min = 1, max = 128, message = "error.UserUINotification.componentType.size")
	private String notificationType; //Opinion, General

	@Column(nullable = false)
	private Boolean enabled = true;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId", referencedColumnName = "id")
	@NotNull(message = "error.UserUINotification.user.notNull")
	private User user;
	

	public UserUINotification() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UserUINotification(String notificationType, Boolean enabled) {
		this.notificationType = notificationType;
		this.enabled = enabled;
		// TODO Auto-generated constructor stub
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	
	/*public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public Boolean getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(Boolean notificationType) {
		this.notificationType = notificationType;
	}*/

	/**
	 * @return the notificationType
	 */
	public String getNotificationType() {
		return notificationType;
	}

	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @return the enabled
	 */
	public Boolean getEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
