package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * Entity implementation class for Entity: Component
 * 
 * @author Abhishek
 */
@Entity
@Table(name = "component")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
@Inheritance(strategy = InheritanceType.JOINED)
public class Component implements Serializable, Comparable<Component> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, columnDefinition = "enum('Sentiment', 'Opinion', 'Response', 'Link', 'GPI', 'GA', 'GlobalVerdict', 'LocalVerdict','Chart','Trend')")
	@NotNull(message = "error.component.componentType.notNull")
	private String componentType;

	@Column(nullable = false)
	@NotNull(message = "error.component.editTime.notNull")
	private Timestamp editTime;

	@Column(nullable = true)
	private Long editedBy;

	@Column(nullable = false)
	@NotNull(message = "error.component.createTime.notNull")
	private Timestamp createTime;

	@Column(nullable = false)
	@NotNull(message = "error.component.createdBy.notNull")
	private Long created_By;

	@Column(nullable = true)
	private String formattedAge;

	@Column(nullable = true)
	private Long views;

	public Component() {

	}

	public String getFormattedAge() {
		return DateUtils.getDateOrTimeAgo(this.createTime);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the views
	 */
	public Long getViews() {
		return views;
	}

	/**
	 * @param views
	 *            the views to set
	 */
	public void setViews(Long views) {
		this.views = views;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	/**
	 * @return the editTime
	 */
	public Timestamp getEditTime() {
		return editTime;
	}

	/**
	 * @param editTime
	 *            the editTime to set
	 */
	public void setEditTime(Timestamp editTime) {
		this.editTime = editTime;
	}

	/**
	 * @return the editedBy
	 */
	public Long getEditedBy() {
		return editedBy;
	}

	/**
	 * @param editedBy
	 *            the editedBy to set
	 */
	public void setEditedBy(Long editedBy) {
		this.editedBy = editedBy;
	}

	/**
	 * @return the createTime
	 */
	public Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the created_By
	 */
	public Long getCreated_By() {
		return created_By;
	}

	/**
	 * @param created_By
	 *            the created_By to set
	 */
	public void setCreated_By(Long created_By) {
		this.created_By = created_By;
	}

	@Override
	public int compareTo(Component o) {
		if (getCreateTime() == null || o.getCreateTime() == null)
			return 0;
		return o.getCreateTime().compareTo(getCreateTime());
	}
}
