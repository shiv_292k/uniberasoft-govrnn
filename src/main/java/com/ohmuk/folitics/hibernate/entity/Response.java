package com.ohmuk.folitics.hibernate.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.util.DateUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * Entity implementation class for Entity: Response
 * 
 * @author Abhishek Patel
 */
@Entity
@Table(name = "response")
@PrimaryKeyJoinColumn(name = "id")
@JsonIdentityInfo(generator = JSOGGenerator.class, property = "@id")
public class Response extends Component implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 128, columnDefinition = "enum ('Agree','Disagree')")
    private String flag;

    @Column(nullable = false)
    private Timestamp edited;

    @Column(nullable = true, columnDefinition = "enum ('Active','Deleted','Updated')")
    private String state;

    @ManyToOne(fetch = FetchType.EAGER)
    //@Cascade(value = CascadeType.ALL)
    @JoinTable(name = "userresponse", joinColumns = { @JoinColumn(name = "responseId", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "userId", referencedColumnName = "id") })
    private User _user;

    @Column(nullable = false, length = 512)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "opinionId", referencedColumnName = "id")
    //@Cascade(value = CascadeType.ALL)
    private Opinion opinion;

    @ManyToOne(fetch = FetchType.EAGER)
    //@Cascade(value = CascadeType.ALL)
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "parentResponse")
    @JsonManagedReference
    @Cascade(value = CascadeType.ALL)
    private List<Response> childResponses;

    @ManyToOne
    @JsonBackReference
    private Response parentResponse;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pollId", referencedColumnName = "id")
    @Cascade(value = CascadeType.ALL)
    private Poll upDownVote;

    @Transient
    private List<Long> userIds;
    
    @Lob
    @Column(name="attachment")
    private byte[] responseAttachment;
    
    @Column(name="positiveFeel")
    private String positiveFeel;
    
    @Column(name="negativeFeel")
    private String negativeFeel;
    
    public Response() {
        setCreateTime(DateUtils.getSqlTimeStamp());
        setEditTime(DateUtils.getSqlTimeStamp());
        setComponentType(ComponentType.RESPONSE.getValue());
        setState(ComponentState.ACTIVE.getValue());
    }

    public String getFlag() {
        return flag;
    }

    public String getState() {
        return state;
    }

    public void setState(String status) {
        this.state = status;
    }

    public Opinion getOpinion() {
        return opinion;
    }

    public void setOpinion(Opinion opinion) {
        this.opinion = opinion;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Timestamp getEdited() {
        return this.edited;
    }

    public void setEdited(Timestamp edited) {
        this.edited = edited;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the upDownVote
     */
    public Poll getUpDownVote() {
        return upDownVote;
    }

    /**
     * @param upDownVote
     *            the upDownVote to set
     */
    public void setUpDownVote(Poll upDownVote) {
        this.upDownVote = upDownVote;
    }

    /**
     * @return the childResponses
     */
    public List<Response> getChildResponses() {
        return childResponses;
    }

    /**
     * @param childResponses
     *            the childResponses to set
     */
    public void setChildResponses(List<Response> childResponses) {
        this.childResponses = childResponses;
    }

    /**
     * @return the parentResponse
     */
    public Response getParentResponse() {
        return parentResponse;
    }

    /**
     * @param parentResponse
     *            the parentResponse to set
     */
    public void setParentResponse(Response parentResponse) {
        this.parentResponse = parentResponse;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

	public byte[] getResponseAttachment() {
		return responseAttachment;
	}

	public void setResponseAttachment(byte[] responseAttachment) {
		this.responseAttachment = responseAttachment;
	}

	public String getPositiveFeel() {
		return positiveFeel;
	}

	public void setPositiveFeel(String positiveFeel) {
		this.positiveFeel = positiveFeel;
	}

	public String getNegativeFeel() {
		return negativeFeel;
	}

	public void setNegativeFeel(String negativeFeel) {
		this.negativeFeel = negativeFeel;
	}

}
