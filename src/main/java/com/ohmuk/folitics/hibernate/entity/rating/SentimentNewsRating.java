package com.ohmuk.folitics.hibernate.entity.rating;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.util.DateUtils;

/**
 * Entity for like on entity: {@link Sentiment news}
 * @author soumya
 *
 */

@Entity
@Table(name = "sentimentNewsRating")
public class SentimentNewsRating implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private RatingId ratingId;    
   
    @Column(nullable = false)
    @NotNull(message = "error.RatingId.ratingId.notNull")
    private Long rating;

    @Column(nullable = false)
    @NotNull(message = "error.componentLike.editTime.notNull")
    private Timestamp editTime;

    @Column(nullable = false)
    @NotNull(message = "error.componentLike.createTime.notNull")
    private Timestamp createTime;

    public SentimentNewsRating() {

        setCreateTime(DateUtils.getSqlTimeStamp());
        setEditTime(DateUtils.getSqlTimeStamp());
    }

	/**
	 * @return the rating
	 */
	public Long getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(Long rating) {
		this.rating = rating;
	}

	/**
	 * @return the ratingId
	 */
	public RatingId getRatingId() {
		return ratingId;
	}



	/**
	 * @param ratingId the ratingId to set
	 */
	public void setRatingId(RatingId ratingId) {
		this.ratingId = ratingId;
	}



	public Timestamp getEditTime() {
        return editTime;
    }

    public void setEditTime(Timestamp editTime) {
        this.editTime = editTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

}
