package com.ohmuk.folitics.hibernate.repository.rating;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserMonetizationBusinessDeligate;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.like.SentimentLike;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.ReferenceRating;
import com.ohmuk.folitics.util.DateUtils;

/**
 * Repository implementation for entity: {@link SentimentLike}
 * 
 * @author soumya
 *
 */
@Component
@Repository
public class ReferenceRatingRepositoryImplementation implements IReferenceRatingRepository {

	private static Logger logger = LoggerFactory.getLogger(ReferenceRatingRepositoryImplementation.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private volatile IUserMonetizationBusinessDeligate userMonetizationBussinessDeligate;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public ReferenceRating save(ReferenceRating referenceRating) {

		logger.info("Entered SentimentLike save method");
		logger.debug("Saving like for sentiment id = " + referenceRating.getRatingId().getComponentId()
				+ " and user id = " + referenceRating.getRatingId().getUserId());

		RatingId id = (RatingId) getSession().save(referenceRating);
		referenceRating = (ReferenceRating) getSession().get(ReferenceRating.class, id);

		logger.info("SentimentLike saved. Exiting save method");
		return referenceRating;
	}

	@Override
	public ReferenceRating update(ReferenceRating referenceRating) {
		logger.info("Entered SentimentReferenceRating update method");
		logger.debug("Updating rating for sentiment id = " + referenceRating.getRatingId().getComponentId()
				+ " and user id = " + referenceRating.getRatingId().getUserId());

		referenceRating = (ReferenceRating) getSession().merge(referenceRating);
		referenceRating.setCreateTime(DateUtils.getSqlTimeStamp());
		getSession().update(referenceRating);

		referenceRating = (ReferenceRating) getSession().get(ReferenceRating.class, referenceRating.getRatingId());

		logger.info("Updated SentimentReferenceRating. Exiting update method");
		return referenceRating;

	}

	@Override
	public ReferenceRating find(RatingId ratingId) {
		logger.info("Entered SentimentReferenceRating find method");
		logger.debug("Getting rating for sentiment id = " + ratingId.getComponentId() + " and user id = "
				+ ratingId.getUserId());

		Criteria selectCriteria = getSession().createCriteria(ReferenceRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.parentId", ratingId.getParentId()))
				.add(Restrictions.eq("ratingId.userId", ratingId.getUserId()));

		ReferenceRating referenceRating = (ReferenceRating) selectCriteria.uniqueResult();

		logger.info("Returning SentimentReferenceRating. Exiting find method");
		return referenceRating;

	}

	@Override
	public List<ReferenceRating> findAll() {

		logger.info("Entered ReferenceRating findAll method");

		Criteria selectCriteria = getSession().createCriteria(ReferenceRating.class);
		List<ReferenceRating> ratings = selectCriteria.list();

		logger.info("Exiting ReferenceRating findAll method");
		return ratings;
	}

	@Override
	public boolean delete(RatingId id) {
		// TODO Auto-generated method stub
		 logger.info("Inside ReferenceRating service delete method");
		 ReferenceRating referenceRating = find(id);
	     getSession().delete(referenceRating);
	     logger.info("Exiting from ReferenceRating service delete");
	     return true;
	}

	@Override
	public void addMonetizationPoints(RatingDataBean ratingDataBean, String action) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ReferenceRating> findByParentIdAndUserId(Long parentId, Long userId) {


		logger.info("Entered ReferenceRating findByParentIdAndUserId method");

		logger.debug("Getting like for parentId id = " + parentId + " and user id = " + userId);

		Criteria selectCriteria = getSession().createCriteria(ReferenceRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.parentId", parentId))
				.add(Restrictions.eq("ratingId.userId", userId));
		 List<ReferenceRating> referenceRatings = selectCriteria.list();

		logger.info("Returning ReferenceRating. Exiting findByParentIdAndUserId method");
		return referenceRatings;

	
	}

	@Override
	public ReferenceRating findByComponentIdAndUserId(Long componentId, Long userId) {


		logger.info("Entered ReferenceRating findByComponentIdAndUserId method");

		logger.debug("Getting like for componentId id = " + componentId + " and user id = " + userId);

		Criteria selectCriteria = getSession().createCriteria(ReferenceRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.componentId", componentId))
				.add(Restrictions.eq("ratingId.userId", userId));
		ReferenceRating referenceRating = (ReferenceRating) selectCriteria.uniqueResult();

		logger.info("Returning ReferenceRating. Exiting findByComponentIdAndUserId method");
		return referenceRating;

	
	}

}