package com.ohmuk.folitics.hibernate.repository.rating;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;

/**
 * Interface for entity: {@link sentiment reference} repository
 * @author soumya
 *
 */
public interface ISentimentNewsRatingRepository extends IRatingHibernateRepository<SentimentNewsRating> {

	SentimentNewsRating save(SentimentNewsRating newsRating);

	SentimentNewsRating update(SentimentNewsRating newsRating);
	
	SentimentNewsRating find(RatingId ratingId);
	
	SentimentNewsRating findByComponentIdAndUserId(Long componentId, Long userId,String feelType);
	
	List<SentimentNewsRating> findAll();	
	
	List<SentimentNewsRating> findByParentIdAndUserId(Long parentId, Long userId);

	List<SentimentNewsRating> findByComponentId(Long componentId);

	List<SentimentNewsRating> findByComponentIdByDate(Long componentId);
	
}
