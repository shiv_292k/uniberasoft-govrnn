package com.ohmuk.folitics.hibernate.repository.rating;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserMonetizationBusinessDeligate;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.ReferenceRating;
import com.ohmuk.folitics.hibernate.entity.rating.SentimentNewsRating;

/**
 * Repository implementation for entity: {@link SentimentNews}
 * 
 * @author soumya
 *
 */
@Component
@Repository
public class SentimentNewsRatingRepositoryImplementation implements ISentimentNewsRatingRepository {

	private static Logger logger = LoggerFactory.getLogger(SentimentNewsRatingRepositoryImplementation.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private volatile IUserMonetizationBusinessDeligate userMonetizationBussinessDeligate;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public SentimentNewsRating save(SentimentNewsRating newsRating) {

		logger.info("Entered SentimentLike save method");
		logger.debug("Saving like for sentiment id = " + newsRating.getRatingId().getComponentId() + " and user id = "
				+ newsRating.getRatingId().getUserId());

		RatingId id = (RatingId) getSession().save(newsRating);
		newsRating = (SentimentNewsRating) getSession().get(SentimentNewsRating.class, id);

		logger.info("SentimentLike saved. Exiting save method");
		return newsRating;
	}

	@Override
	@Transactional(readOnly = false)
	public SentimentNewsRating update(SentimentNewsRating newsRating) {
		logger.info("Entered SentimentSentimentNewsRating update method");
		logger.debug("Updating rating for sentiment id = "
				+ newsRating.getRatingId().getComponentId()
				+ " and user id = " + newsRating.getRatingId().getUserId());
		//RatingId ratingId = (RatingId) getSession().merge(newsRating.getRatingId());
		Transaction tx = getSession().beginTransaction();
		 
		newsRating  = (SentimentNewsRating) getSession().merge(newsRating);
		getSession().flush();
		//newsRating = (SentimentNewsRating) getSession().get(SentimentNewsRating.class,
		if(!tx.getStatus().equals(TransactionStatus.ACTIVE) ) { 
		    tx.commit();
		}

		logger.info("Updated SentimentSentimentNewsRating. Exiting update method");
		return newsRating;

	}

	@Override
	public SentimentNewsRating find(RatingId ratingId) {
		logger.info("Entered SentimentSentimentNewsRating find method");
		logger.debug("Getting rating for sentiment id = "
				+ ratingId.getComponentId() + " and user id = "
				+ ratingId.getUserId());

		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.parentId", ratingId.getParentId()))
				.add(Restrictions.eq("ratingId.userId", ratingId.getUserId()));

		SentimentNewsRating newsRating = (SentimentNewsRating) selectCriteria.uniqueResult();

		logger.info("Returning SentimentSentimentNewsRating. Exiting find method");
		return newsRating;

	}

	@Override
	public List<SentimentNewsRating> findAll() {

		logger.info("Entered ReferenceRating findAll method");
		
		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		List<SentimentNewsRating> ratings = selectCriteria.list();
		
		logger.info("Exiting ReferenceRating findAll method");
		return ratings;
	
	}

	@Override
	public boolean delete(RatingId id) {
		// TODO Auto-generated method stub
		 logger.info("Inside ReferenceRating service delete method");
		 SentimentNewsRating sentimentNewsRating = find(id);
	     getSession().delete(sentimentNewsRating);
	     logger.info("Exiting from ReferenceRating service delete");
	     return true;// TODO Auto-generated method stub
		
	}

	@Override
	public void addMonetizationPoints(RatingDataBean ratingDataBean, String action) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public SentimentNewsRating findByComponentIdAndUserId(Long componentId, Long userId,String feelType) {


		logger.info("Entered SentimentNewsRating findByComponentIdAndUserId method");

		logger.debug("Getting like for sentiment id = " + componentId + " and user id = " + userId);

		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.componentId", componentId)).add(Restrictions.eq("ratingId.userId", userId))
		.add(Restrictions.eq("ratingId.feelType", feelType));
		SentimentNewsRating newsRating = (SentimentNewsRating) selectCriteria.uniqueResult();

		logger.info("Returning SentimentNewsRating. Exiting findByComponentIdAndUserId method");
		return newsRating;
 
	
	}

	@Override
	public List<SentimentNewsRating> findByParentIdAndUserId(Long parentId, Long userId) {



		logger.info("Entered SentimentNewsRating findByParentIdAndUserId method");

		logger.debug("Getting like for parentId id = " + parentId + " and user id = " + userId);

		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.parentId", parentId)).add(Restrictions.eq("ratingId.userId", userId));
		List<SentimentNewsRating> newsRatings = selectCriteria.list();

		logger.info("Returning SentimentNewsRating. Exiting findByParentIdAndUserId method");
		return newsRatings;
 
	
		}
	
	@Override
	public List<SentimentNewsRating> findByComponentId(Long componentId) {
		logger.info("Entered SentimentSentimentNewsRating findByComponentId method");
		logger.debug("Getting rating for sentiment id = "
				+ componentId);

		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.componentId", componentId));

		List<SentimentNewsRating> newsRating = selectCriteria.list();

		logger.info("Returning SentimentSentimentNewsRating. Exiting findByComponentId method");
		return newsRating;

	}

	@Override
	public List<SentimentNewsRating> findByComponentIdByDate(Long componentId) {
		logger.info("Entered SentimentSentimentNewsRating findByComponentIdByDate method");
		logger.debug("Getting rating for sentiment id = "
				+ componentId);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -7);
		
		Criteria selectCriteria = getSession().createCriteria(SentimentNewsRating.class);
		selectCriteria.add(Restrictions.eq("ratingId.componentId", componentId));
		selectCriteria.add(Restrictions.ge("editTime",c.getTime()));
		List<SentimentNewsRating> newsRating = selectCriteria.list();

		logger.info("Returning SentimentSentimentNewsRating. Exiting findByComponentIdByDate method");
		return newsRating;

	}

}