package com.ohmuk.folitics.hibernate.repository.rating;

import java.util.List;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;

/**
 * Hibernate repository interface for like
 * @author Soumya
 *
 * @param <T>
 */
public interface IRatingHibernateRepository<T> {

    /** 
     * This method will save the object t in the database and then will return the object
     * 
     * @author Soumya
     * @param T t
     * @return T
     */
    public T save(T t);

    /**
     * This method will update the given object in the database and then will return the updated object
     * 
     * @author Soumya
     * @param T t
     * @return T
     */
    public T update(T t);

    /**
     * This method will find the record in database by the given id
     * 
     * @param com.ohmuk.folitics.jpa.entity.like.LikeId id
     * @return T
     */
    public T find(RatingId id);

    /**
     * This method will return all the object present in table and will return them in list
     * 
     * @author Soumya
     * @return java.util.List<T>
     */
    public List<T> findAll();

    /**
     * This method will delete the record from database where id is equals to the given id
     * 
     * @author Soumya
     * @param com.ohmuk.folitics.jpa.entity.like.LikeId id
     * @return 
     */
    public boolean delete(RatingId id);

    /**
     * This method will added user points, after like on any component
     * 
     * @author Harish
     * @param javacom.ohmuk.folitics.beans.LikeDataBean
     * 
     * @return void 
     */
    
	public void addMonetizationPoints(RatingDataBean ratingDataBean ,String action) throws Exception;

	
    
    


}
