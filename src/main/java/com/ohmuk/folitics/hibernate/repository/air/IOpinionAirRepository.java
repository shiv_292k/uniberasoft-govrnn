package com.ohmuk.folitics.hibernate.repository.air;

import com.ohmuk.folitics.hibernate.entity.air.OpinionAir;

/**
 * Interface for entity: {@link OpinionAir} repository
 * @author Harish
 *
 */
public interface IOpinionAirRepository extends IAirHibernateRepository<OpinionAir>{

}
