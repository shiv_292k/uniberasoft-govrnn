package com.ohmuk.folitics.hibernate.repository.comment;

import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;

/**
 * Interface for entity: {@link TrendComment} repository
 * 
 * @author soumya
 *
 */
public interface ITrendCommentRepository extends
		ICommentHibernateRepository<TrendComment> {

}
