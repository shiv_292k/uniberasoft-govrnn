package com.ohmuk.folitics.hibernate.repository.comment;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.ohmuk.folitics.beans.CommentBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserMonetizationBusinessDeligate;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Module;
import com.ohmuk.folitics.hibernate.entity.UserMonetization;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;

/**
 * Repository implementation for entity: {@link TrendComment}
 * 
 * @author Soumya
 *
 */

@Component
@Repository
public class TrendCommentRepositoryImplementation implements
		ITrendCommentRepository {

	private static Logger logger = LoggerFactory
			.getLogger(TrendCommentRepositoryImplementation.class);

	@Autowired
	private SessionFactory sessionFactory;

	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public TrendComment save(TrendComment trendComment) {
		logger.info("Entered  TrendComment repository  save method");
		logger.debug("Saving Comment for trend mapping id = "
				+ trendComment.getTrendMappingId() + " and user id = "
				+ trendComment.getUserId());

		Long id = (Long) getSession().save(trendComment);
		trendComment = (TrendComment) getSession().get(
				TrendComment.class, id);

		logger.info("TrendComment saved. Exiting save method");
		return trendComment;
	}

	@Override
	public TrendComment update(TrendComment trendComment) {
		logger.info("Entered TrendComment repository update method");
		logger.debug("Updating comment for trendmapping id = "
				+ trendComment.getTrendMappingId() + " and user id = "
				+ trendComment.getUserId());

		trendComment = (TrendComment) getSession().merge(trendComment);
		getSession().update(trendComment);

		trendComment = (TrendComment) getSession().get(
				TrendComment.class, trendComment.getId());

		logger.info("Updated TrendComment. Exiting update method");
		return trendComment;
	}

	@Override
	public List<TrendComment> findAll() {
		logger.info("Entered TrendComment findAll method");
		logger.debug("Getting all likes");

		Criteria selectAllCriteria = getSession().createCriteria(
				TrendComment.class);
		@SuppressWarnings("unchecked")
		List<TrendComment> responseComments = selectAllCriteria.list();

		logger.info("Returning all TrendComment. Exiting findAll method");
		return responseComments;
	}

	@Override
	public void delete(Long id) {
		logger.info("Entered TrendComment delete method");

		TrendComment trendComment = (TrendComment) getSession().get(
				TrendComment.class, id);

		logger.info("Deleted TrendComment. Exiting delete method");
		getSession().delete(trendComment);

	}

	@Override
	public List<TrendComment> getByComponentIdAndUserId(Long trendMappingId,
			Long userId) throws MessageException {
		logger.info("Entered TrendComment getByComponentIdAndUserId method");
		Query query = getSession()
				.createSQLQuery(
						"SELECT * FROM  trendComment t WHERE t.trendMappingId = :trendMappingId AND t.userId = :userId")
				.addEntity(TrendComment.class)
				.setParameter("trendMappingId", trendMappingId)
				.setParameter("userId", userId);

		@SuppressWarnings("unchecked")
		List<TrendComment> opinionComments = (List<TrendComment>) query
				.list();

		if (opinionComments.size() == 0) {
			logger.error("No record found for these trendMappingId :"
					+ trendMappingId + "and userId: " + userId);
			throw new MessageException(
					"No record found for these componentId :" + trendMappingId
							+ "and userId: " + userId);
		}

		logger.info("Exiting TrendComment getByComponentIdAndUserId method");

		return opinionComments;
	}

	@Override
	public List<TrendComment> getAllCommentsForComponent(Long trendMappingId)
			throws MessageException {
		logger.info("Entered TrendComment getAllCommentsForComponent method");

		Criteria criteria = getSession().createCriteria(TrendComment.class);
		criteria.add(Restrictions.eq("trendMappingId", trendMappingId));

		@SuppressWarnings("unchecked")
		List<TrendComment> opinionComments = criteria.list();

		if (opinionComments.size() == 0) {
			logger.error("comments does not exist for these componentId :"
					+ trendMappingId);
		}
		logger.info("Exiting TrendComment getAllCommentsForComponent method");
		return opinionComments;
	}

	@Override
	public List<TrendComment> getAllCommentsForUserId(Long userId)
			throws MessageException {
		logger.info("Entered TrendComment getAllCommentsForUserId method");

		Criteria criteria = getSession().createCriteria(TrendComment.class);
		criteria.add(Restrictions.eq("userId", userId));

		@SuppressWarnings("unchecked")
		List<TrendComment> responseComments = criteria.list();

		if (responseComments.size() == 0) {
			logger.error(" trend comments does not exist for these userId :" + userId);
			throw new MessageException(
					" trend comments does not exist for these UserId :" + userId);
		}
		logger.info("Exiting TrendComment getAllCommentsForUserId method");
		return responseComments;
	}

	@Override
	public TrendComment find(Long id) {
		logger.info("Entered TrendComment find method");

		TrendComment trendComment = (TrendComment) getSession().get(
				TrendComment.class, id);

		logger.info("Returning TrendComment. Exiting find method");
		return trendComment;
	}

	@Override
	public void addMonetizationPoints(CommentBean commentBean, String action)
			throws Exception {}

}
