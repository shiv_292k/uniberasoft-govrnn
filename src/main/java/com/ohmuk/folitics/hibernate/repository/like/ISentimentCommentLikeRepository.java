package com.ohmuk.folitics.hibernate.repository.like;

import com.ohmuk.folitics.hibernate.entity.like.SentimentCommentLike;

/**
 * Interface for entity: {@link TaskCommentLike} repository
 * 
 * @author Harish
 *
 */
public interface ISentimentCommentLikeRepository extends
		ILikeHibernateRepository<SentimentCommentLike> {

}
