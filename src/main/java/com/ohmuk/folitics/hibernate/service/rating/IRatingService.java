package com.ohmuk.folitics.hibernate.service.rating;

import java.util.List;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.exception.MessageException;

/**
 * Service interface for Like
 * 
 * @author soumya
 *
 * @param <T>
 */
public interface IRatingService<T> {

	/**
	 * This method calls the repository save method to save the object t in
	 * database
	 * 
	 * @author soumya
	 * @param T
	 *            t
	 * @return T
	 * @throws MessageException
	 */
	public T create(T rating) throws MessageException,Exception;

	/**
	 * This method calls the repository find method to return object where id is
	 * equals to the given id
	 * 
	 * @author soumya
	 * @param com
	 *            .ohmuk.folitics.jpa.entity.like.LikeId likeId
	 * @return T
	 * @throws MessageException
	 */
	public List<T> read(T ratingDataBean) throws MessageException;

	/**
	 * This method calls the repository finaAll method to return all the objects
	 * of T present in database table
	 * 
	 * @author soumya
	 * @return java.util.List<T>
	 */
	public List<T> readAll();

	/**
	 * This method calls the repository update method to update the object t in
	 * database and then return the updated object
	 * 
	 * @author soumya
	 * @param T
	 *            t
	 * @return T
	 * @throws MessageException
	 */
	public T update(T ratingDataBean) throws MessageException;

	/**
	 * This method calls the repository delete method to delete the object t
	 * from database table
	 * 
	 * @author soumya
	 * @param T
	 *            t
	 * @return T
	 * @throws MessageException
	 */
	public RatingDataBean delete(T ratingDataBean) throws MessageException;

	/**
	 * This method returns the object which have component id and user id equals
	 * to the given component id and user id
	 * 
	 * @author soumya
	 * @param java
	 *            .lang.Long componentId
	 * @param java
	 *            .lang.Long userId
	 * @return T
	 * @throws MessageException
	 */
	
	 public T getByComponentIdAndUserId(T ratingBean) throws MessageException;

	 /**
		 * This method returns the object which have parent id and user id equals
		 * to the given component id and user id
		 * 
		 * @author soumya,Jahid
		 * @param java
		 *            .lang.Long componentId
		 * @param java
		 *            .lang.Long userId
		 * @return T
		 * @throws MessageException
		 */
	 public List<T> getByParentIdAndUserId(T ratingBean) throws MessageException;

	List<RatingDataBean> getByComponentId(RatingDataBean ratingDataBean) throws MessageException;

	List<RatingDataBean> getByComponentIdByDate(RatingDataBean ratingDataBean) throws MessageException;

}
