package com.ohmuk.folitics.hibernate.service.rating;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.hibernate.entity.rating.ReferenceRating;
import com.ohmuk.folitics.hibernate.repository.rating.IReferenceRatingRepository;

/**
 * @author Soumya
 *
 */

@Service
@Transactional
public class ReferenceRatingService implements IRatingService<RatingDataBean> {

	private static Logger logger = LoggerFactory.getLogger(ReferenceRatingService.class);

	@Autowired
	private IReferenceRatingRepository repository;

	@Override
	public RatingDataBean create(RatingDataBean ratingDataBean) throws MessageException, Exception {
		logger.info("Entered ReferenceRating service create method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}

		// check whether the entry for same component id and user id already
		// exists in database

		RatingDataBean ratingDataBeanExisting = getByComponentIdAndUserId(ratingDataBean);

		// if no entry exist for rating for reference of sentiment in database
		if (ratingDataBeanExisting == null) {
			RatingId ratingId = new RatingId();
			ratingId.setComponentId(ratingDataBean.getComponentId());
			ratingId.setUserId(ratingDataBean.getUserId());
			ratingId.setParentId(ratingDataBean.getParentId());
			ReferenceRating sentimentReferencerating = new ReferenceRating();
			sentimentReferencerating.setRatingId(ratingId);
			sentimentReferencerating.setRating(ratingDataBean.getRating());
			// save the new entry for like event
			sentimentReferencerating = repository.save(sentimentReferencerating);
			return RatingDataBean.getRatingDataBean(sentimentReferencerating, ratingDataBean.getComponentType());
		} else {
			return update(ratingDataBean);

		}
	}

	@Override
	public List<RatingDataBean> read(RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered ReferenceRating service update method");
		List<RatingDataBean>  ratingDataBeans = new ArrayList<>();
		if (ratingDataBean.getParentId() == null ) {
			logger.error("parentId found to be null");
			throw (new MessageException("getParentId can't be null"));
		}		
		if (ratingDataBean.getUserId() == null ) {
			logger.error("UserId found to be null");
			throw (new MessageException("UserId can't be null"));
		}		
		
		List<ReferenceRating> ratings =  repository.findByParentIdAndUserId(ratingDataBean.getParentId(), ratingDataBean.getUserId());
		for(ReferenceRating rating:ratings){
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating, ratingDataBean.getComponentType()));
		}
		return ratingDataBeans; 

	}

	@Override
	public List<RatingDataBean> readAll() {
		logger.info("Entered ReferenceRating service readAll method");
		List<RatingDataBean>  ratingDataBeans = new ArrayList<>();
		List<ReferenceRating> ratings =  repository.findAll();
		for(ReferenceRating rating:ratings){
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating, "Reference"));
		}
		return ratingDataBeans;
	}

	@Override
	public RatingDataBean update(RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered ReferenceRating service update method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}
		ReferenceRating originalreferenceRating = repository.findByComponentIdAndUserId(ratingDataBean.getComponentId(), ratingDataBean.getUserId());
		if (originalreferenceRating != null) {
					logger.debug("Updating SentimentLike with sentiment id = " + originalreferenceRating.getRatingId().getComponentId()
							+ " and user id = " + originalreferenceRating.getRatingId().getUserId());

			originalreferenceRating.setRating(ratingDataBean.getRating());
			ReferenceRating rating =  repository.update(originalreferenceRating);
			return  RatingDataBean.getRatingDataBean(rating, ratingDataBean.getComponentType());
		}
		return null;
	}

	@Override
	public RatingDataBean delete(RatingDataBean ratingBean) throws MessageException {
		// TODO Auto-generated method stub
		ReferenceRating rating  = repository.findByComponentIdAndUserId(ratingBean.getComponentId(),ratingBean.getUserId());
		RatingDataBean rdb =null;
		if(rating!=null){
			rdb =  RatingDataBean.getRatingDataBean(rating ,ratingBean.getComponentType());
			repository.delete(rating.getRatingId());
		}
		return rdb;
	}

	@Override
	public RatingDataBean getByComponentIdAndUserId(RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered ReferenceRating service getByComponentIdAndUserId method");
		if (ratingDataBean.getComponentId() == null) {
			logger.error("componentId found to be null");
			throw (new MessageException("componentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}
		logger.debug("Getting ReferenceRating with sentiment id = " + ratingDataBean.getComponentId() + " and user id = " + ratingDataBean.getUserId());
		return RatingDataBean.getRatingDataBean(	repository.findByComponentIdAndUserId(ratingDataBean.getComponentId(), ratingDataBean.getUserId()), ratingDataBean.getComponentType()); 
	}

	
	@Override
	public List<RatingDataBean> getByParentIdAndUserId(RatingDataBean ratingDataBean) throws MessageException {
		logger.info("Entered getByParentIdAndUserId service readAll method");
		logger.info("Entered ReferenceRating service getByParentIdAndUserId method");
		if (ratingDataBean.getParentId() == null) {
			logger.error("parentId found to be null");
			throw (new MessageException("parentId can't be null"));
		}
		if (ratingDataBean.getUserId() == null) {
			logger.error("userId found to be null");
			throw (new MessageException("userId can't be null"));
		}
		
		List<RatingDataBean>  ratingDataBeans = new ArrayList<>();
		List<ReferenceRating> ratings =  repository.findByParentIdAndUserId(ratingDataBean.getParentId(), ratingDataBean.getUserId());
		for(ReferenceRating rating:ratings){
			ratingDataBeans.add(RatingDataBean.getRatingDataBean(rating, ratingDataBean.getComponentType()));
		}
		return ratingDataBeans;
	}

	@Override
	public List<RatingDataBean> getByComponentId(RatingDataBean ratingDataBean) throws MessageException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RatingDataBean> getByComponentIdByDate(RatingDataBean ratingDataBean) throws MessageException {
		// TODO Auto-generated method stub
		return null;
	}

}
