package com.ohmuk.folitics.xml.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rss")
public class RSS implements Serializable {

	private static final long serialVersionUID = 1L;
	private Channel channel;

	@XmlElement(name = "channel")
	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

}
