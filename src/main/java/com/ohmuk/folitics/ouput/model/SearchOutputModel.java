package com.ohmuk.folitics.ouput.model;

import java.sql.Timestamp;

public class SearchOutputModel {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private String componentType;

    byte[] image;

    private String imageUrl;

    private String url;
    private Timestamp formattedAge;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Timestamp getFormattedAge() {
        return formattedAge;
    }

    public void setFormattedAge(Timestamp formattedAge) {
        this.formattedAge = formattedAge;
    }

}
