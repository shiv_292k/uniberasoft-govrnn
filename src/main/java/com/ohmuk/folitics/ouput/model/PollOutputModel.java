/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;

/**
 * @author Kalpana
 *
 */
public class PollOutputModel extends BaseOutputModel<String> implements Serializable{
	
	 private static final long serialVersionUID = 1L;
	 private String question;
	 private List<PollOptionOutputModel> pollOptionOutputModels = new ArrayList<PollOptionOutputModel>();
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	
	/**
	 * @return the pollOptionOutputModels
	 */
	public List<PollOptionOutputModel> getPollOptionOutputModels() {
		return pollOptionOutputModels;
	}
	/**
	 * @param pollOptionOutputModels the pollOptionOutputModels to set
	 */
	public void setPollOptionOutputModels(
			List<PollOptionOutputModel> pollOptionOutputModels) {
		this.pollOptionOutputModels = pollOptionOutputModels;
	}
	
	public void addOption(PollOptionOutputModel option) {
        if (option != null) {
        	pollOptionOutputModels.add(option);
        }
    }

    public void removeOption(PollOptionOutputModel option) {
        if (option != null) {
        	pollOptionOutputModels.remove(option);
        }
    }
    
	public PollOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}

	public static final PollOutputModel getModel(Poll entity) {
		PollOutputModel model = new PollOutputModel(entity.getId(),
				null, "Poll", null, null, null);
		model.setQuestion(entity.getQuestion());
		for(PollOption pollOption:entity.getOptions()){
			model.addOption(PollOptionOutputModel.getModel(pollOption));
		}
		return model;
	}
	
}
