/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

/**
 * @author Kalpana
 *
 */
public class PollOptionsCount {
	private Long pollOptionId;
	private String pollOption;
	private int count;
	private float percentage;
	/**
	 * @return the pollOption
	 */
	public String getPollOption() {
		return pollOption;
	}
	/**
	 * @param pollOption the pollOption to set
	 */
	public void setPollOption(String pollOption) {
		this.pollOption = pollOption;
	}
	
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the pollOptionId
	 */
	public Long getPollOptionId() {
		return pollOptionId;
	}
	/**
	 * @param pollOptionId the pollOptionId to set
	 */
	public void setPollOptionId(Long pollOptionId) {
		this.pollOptionId = pollOptionId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "PollOptionsCount[pollOption=" + pollOption + ", count=" + count + "]";
	}
	/**
	 * @return the percentCount
	 */
	/**
	 * @return the percentage
	 */
	public float getPercentage() {
		return percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	
}
