package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;

import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;

public class UserTrendOutputModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private int trendMappingsCount;
	private TrendOutputModel trend;
	private UserOutputModel user;
	private Long trendId;
	private String trendName;

	public int getTrendMappingsCount() {
		return trendMappingsCount;
	}

	public void setTrendMappingsCount(int trendMappingsCount) {
		this.trendMappingsCount = trendMappingsCount;
	}

	public UserTrendOutputModel(User user, Trend entity) {
		this.trend = TrendOutputModel.getModel(entity);
		this.user = UserOutputModel.getModel(user);
	}
	
	public UserTrendOutputModel(User user) {
		this.user = UserOutputModel.getModel(user);
	}
	
	public UserTrendOutputModel() {
		
	}

	public TrendOutputModel getTrend() {
		return trend;
	}

	public void setTrend(TrendOutputModel trend) {
		this.trend = trend;
	}

	public UserOutputModel getUser() {
		return user;
	}

	public void setUser(UserOutputModel user) {
		this.user = user;
	}

	public Long getTrendId() {
		return trendId;
	}

	public void setTrendId(Long trendId) {
		this.trendId = trendId;
	}

	public String getTrendName() {
		return trendName;
	}

	public void setTrendName(String trendName) {
		this.trendName = trendName;
	}
	
}
