package com.ohmuk.folitics.ouput.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ThumbnailUtil;

  public class CommentOutputModel extends BaseOutputModel<String> implements
      Serializable {
         /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
	    private String comment;
	    private Long trendMappingId;
	    private UserOutputModel userOutputModel;
	    
	    public CommentOutputModel(Long id, String title, String type,
				UIMetrics uiMetrics, List otherAttributes,
				String formattedAge) {
			super(id, title, type, uiMetrics, otherAttributes, formattedAge);
			// TODO Auto-generated constructor stub
		}
		
	    public static final CommentOutputModel getModel(TrendComment entity ) {
	    	CommentOutputModel model = new CommentOutputModel(entity.getId(),
					 null,null, null, null,null);
			model.setComment(entity.getComment());
			model.setTrendMappingId(entity.getTrendMappingId());
			model.setFormattedAge(DateUtils.getDateOrTimeAgo(entity.getCreateTime()));
			return model;
			
			
		}

		/**
		 * @return the comment
		 */
		public String getComment() {
			return comment;
		}

		/**
		 * @param comment the comment to set
		 */
		public void setComment(String comment) {
			this.comment = comment;
		}

		/**
		 * @return the trendMappingId
		 */
		public Long getTrendMappingId() {
			return trendMappingId;
		}

		/**
		 * @param trendMappingId the trendMappingId to set
		 */
		public void setTrendMappingId(Long trendMappingId) {
			this.trendMappingId = trendMappingId;
		}

		/**
		 * @return the userOutputModel
		 */
		public UserOutputModel getUserOutputModel() {
			return userOutputModel;
		}

		/**
		 * @param userOutputModel the userOutputModel to set
		 */
		public void setUserOutputModel(UserOutputModel userOutputModel) {
			this.userOutputModel = userOutputModel;
		}
		
	}


