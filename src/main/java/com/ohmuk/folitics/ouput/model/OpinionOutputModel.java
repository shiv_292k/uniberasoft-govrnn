package com.ohmuk.folitics.ouput.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.springframework.util.StringUtils;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.ThumbnailUtil;

public class OpinionOutputModel extends BaseOutputModel<Opinion> implements
		Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PollOutputModel agreeDisagrePoll;
	
	private String sentimentName;
	
	private Long sentimentId;
	
	private String sentimentCategory;
	
	private byte[] image;
	
	private String imageUrl;
	
	private String text;
	
	private LinkOutputModel link;
	
	private UserOutputModel user;
	//TODO : refactor this cod to use from useroutPut model.
	//private Long userId;
	//private String userName;
	
	
	/*public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}*/

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
	
	/**
	 * @return the sentimentName
	 */
	public String getSentimentName() {
		return sentimentName;
	}

	/**
	 * @param sentimentName the sentimentName to set
	 */
	public void setSentimentName(String sentimentName) {
		this.sentimentName = sentimentName;
	}

	/**
	 * @return the sentimentId
	 */
	public Long getSentimentId() {
		return sentimentId;
	}

	/**
	 * @param sentimentId the sentimentId to set
	 */
	public void setSentimentId(Long sentimentId) {
		this.sentimentId = sentimentId;
	}
	/**
	 * @return the sentimentCategory
	 */
	public String getSentimentCategory() {
		return sentimentCategory;
	}

	/**
	 * @param sentimentCategory the sentimentCategory to set
	 */
	public void setSentimentCategory(String sentimentCategory) {
		this.sentimentCategory = sentimentCategory;
	}

	
	public OpinionOutputModel(){
		super();
	}
	public OpinionOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<Opinion> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		// TODO Auto-generated constructor stub
	}

	

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public UserOutputModel getUser() {
		return user;
	}

	public void setUser(UserOutputModel user) {
		this.user = user;
	}
	
	/**
	 * @return the link
	 */
	public LinkOutputModel getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(LinkOutputModel link) {
		this.link = link;
	}

	public static final OpinionOutputModel getModel(Opinion entity) {
		OpinionOutputModel model = new OpinionOutputModel(entity.getId(),
				entity.getSubject(), entity.getType(),null, null, DateUtils.getDateOrTimeAgo(entity.getCreateTime()));
		model.setSentimentId(entity.getSentiment().getId());
		model.setSentimentName(entity.getSentiment().getSubject());		
		model.setSentimentCategory(entity.getSentiment().getType());
		model.setText(entity.getText());
		model.setAgreeDisagrePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		model.setUser(UserOutputModel.getModel(entity.getUser()));
		//model.setUserId(entity.getUser().getId());
		//model.setUserName(entity.getUser().getName());
		model.setLink(LinkOutputModel.getModel(entity.getLink()));
		model.setImageUrl(entity.getImageUrl());
		if(StringUtils.isEmpty(entity.getImageUrl())){
			try {
				model.setImage( ThumbnailUtil.getSentimentImageThumbnail(entity.getSentiment().getImage(), entity.getSentiment().getImageType()));
				model.setImageFileType(entity.getSentiment().getImageType());
			} catch (IOException e) {
				model.setImage(entity.getSentiment().getImage());
				model.setImageFileType(entity.getSentiment().getImageType());
				e.printStackTrace();
			}
		}
		return model;
	}

	/**
	 * @return the agreeDisagrePoll
	 */
	public PollOutputModel getAgreeDisagrePoll() {
		return agreeDisagrePoll;
	}

	/**
	 * @param agreeDisagrePoll the agreeDisagrePoll to set
	 */
	public void setAgreeDisagrePoll(PollOutputModel agreeDisagrePoll) {
		this.agreeDisagrePoll = agreeDisagrePoll;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
