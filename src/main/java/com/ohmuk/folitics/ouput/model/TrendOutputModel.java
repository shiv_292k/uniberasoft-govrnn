package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.trend.Trend;

public class TrendOutputModel extends BaseOutputModel<Trend> implements
		Serializable {
	private static final long serialVersionUID = 1L;
	String url;
	private boolean adminTrend;
	int trendMappingsCount;
	
	public String getUrl() {
		return "/#/trends?id="+getId();
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getTrendMappingsCount() {
		return trendMappingsCount;
	}

	public void setTrendMappingsCount(int trendMappingsCount) {
		this.trendMappingsCount = trendMappingsCount;
	}

	public boolean isAdminTrend() {
		return adminTrend;
	}

	public void setAdminTrend(boolean adminTrend) {
		this.adminTrend = adminTrend;
	}

	public TrendOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<Trend> otherAttributes,
			String fromattedAge, int trendMappingsCount) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
		this.trendMappingsCount = trendMappingsCount;
	}

	public static final TrendOutputModel getModel(Trend entity) {
		TrendOutputModel model = new TrendOutputModel(entity.getId(),
				entity.getName(), null, null, null, null,
				entity.getTrendMappings() != null ? entity.getTrendMappings()
						.size() : 0);
		model.setAdminTrend(entity.getAdminTrend());
		return model;
	}
}
