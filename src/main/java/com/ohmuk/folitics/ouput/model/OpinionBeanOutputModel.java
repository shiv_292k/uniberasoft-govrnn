/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.StringUtils;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;

/**
 * @author Kalpana
 *
 */
public class OpinionBeanOutputModel extends BaseOutputModel<String> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String subject;
	private String state;
	private String text;
	private Attachment attachment;
	private String imageUrl;
	private SentimentOutputModel sentimentOutputModel;
	private UserOutputModel userOutputModel;
	private PollOutputModel agreeDisagreePoll;
	private LinkOutputModel link;
	
	public LinkOutputModel getLink() {
		return link;
	}

	public void setLink(LinkOutputModel link) {
		this.link = link;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the attachment
	 */
	public Attachment getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the sentimentOutputModel
	 */
	public SentimentOutputModel getSentimentOutputModel() {
		return sentimentOutputModel;
	}

	/**
	 * @param sentimentOutputModel the sentimentOutputModel to set
	 */
	public void setSentimentOutputModel(SentimentOutputModel sentimentOutputModel) {
		this.sentimentOutputModel = sentimentOutputModel;
	}

	/**
	 * @return the userOutputModel
	 */
	public UserOutputModel getUserOutputModel() {
		return userOutputModel;
	}

	/**
	 * @param userOutputModel the userOutputModel to set
	 */
	public void setUserOutputModel(UserOutputModel userOutputModel) {
		this.userOutputModel = userOutputModel;
	}

	/**
	 * @return the agreeDisagreePoll
	 */
	public PollOutputModel getAgreeDisagreePoll() {
		return agreeDisagreePoll;
	}

	/**
	 * @param agreeDisagreePoll the agreeDisagreePoll to set
	 */
	public void setAgreeDisagreePoll(PollOutputModel agreeDisagreePoll) {
		this.agreeDisagreePoll = agreeDisagreePoll;
	}
	
	public OpinionBeanOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String fromattedAge) {
		super(id, title, type, uiMetrics, otherAttributes, fromattedAge);
	}
	
	public static final OpinionBeanOutputModel getModel(Opinion entity) {
		OpinionBeanOutputModel model = new OpinionBeanOutputModel(entity.getId(),
				null, entity.getType(), null, null, null);
		model.setSubject(entity.getSubject());
		model.setState(entity.getState());
		model.setText(entity.getText());
		model.setAttachment(entity.getAttachment());
		model.setSentimentOutputModel(SentimentOutputModel.getModel(entity.getSentiment()));
		model.getSentimentOutputModel().setImage(null);
		model.setUserOutputModel(UserOutputModel.getModel(entity.getUser()));
		model.setAgreeDisagreePoll(PollOutputModel.getModel(entity.getAgreeDisagrePoll()));
		model.setImageUrl(entity.getImageUrl());
		model.setLink(LinkOutputModel.getModel(entity.getLink()));
		if(!StringUtils.isEmpty(model.getImageUrl())){
			model.getSentimentOutputModel().setImage(null);
		}
		return model;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OpinionBeanOutputModel [subject=" + subject + ", state="
				+ state + ", text=" + text + ", attachment=" + attachment
				+ ", sentimentOutputModel=" + sentimentOutputModel
				+ ", userOutputModel=" + userOutputModel
				+ ", agreeDisagreePoll=" + agreeDisagreePoll + "]";
	}
	
	
}
