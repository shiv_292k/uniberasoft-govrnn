package com.ohmuk.folitics.ouput.model;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.util.StringUtils;

import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.util.DateUtils;

public class AttachmentOutputModel extends BaseOutputModel<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String attachmentType;// "link", "image", "video", "audio"
	private String link;

	private String iconUrl;
	private String description;
	private String fileType;

	private String title;

	private String provider;
	private long providerId;
	private long opinionId;
	private String opinionTitle;
	private byte[] data;

	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public AttachmentOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<String> otherAttributes,
			String formattedAge, Timestamp createTime) {
		super(id, title, type, uiMetrics, otherAttributes, formattedAge, createTime);
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public long getOpinionId() {
		return opinionId;
	}

	public void setOpinionId(long opinionId) {
		this.opinionId = opinionId;
	}

	public String getOpinionTitle() {
		return opinionTitle;
	}

	public void setOpinionTitle(String opinionTitle) {
		this.opinionTitle = opinionTitle;
	}

	public static final AttachmentOutputModel getModel(Attachment entity,
			String provider, long providerId) {
		AttachmentOutputModel model = new AttachmentOutputModel(entity.getId(),
				entity.getTitle(), entity.getAttachmentType(), null, null,
				DateUtils.getDateOrTimeAgo(entity.getCreateTime()),entity.getCreateTime());
		if (entity.getAttachmentFile() != null) {
			model.setData(entity.getAttachmentFile().getData());
			model.setFileType(entity.getAttachmentFile().getType());
		} else {
			model.setFileType(entity.getFileType());
		}
		model.setAttachmentType(entity.getAttachmentType());
		model.setDescription(entity.getDescription());
		if (entity.getFilePath() != null) {
			model.setAttachmentType("link");
			model.setLink(entity.getFilePath());
			model.setIconUrl(getIconUrlForFile(entity.getFilePath()));
		}
		model.setProvider(provider);
		model.setProviderId(providerId);
		return model;
	}
	

	public static final AttachmentOutputModel getModel(Opinion entity) {
		if (isValidUrl(entity.getLink().getLink())) {
			AttachmentOutputModel model = new AttachmentOutputModel(entity
					.getLink().getId(), clearString(entity.getLink().getDescription()),
					"Link", null, null, DateUtils.getDateOrTimeAgo(entity
							.getCreateTime()),entity.getCreateTime());
			if (!StringUtils.isEmpty(entity.getLink().getDescription()))
				model.setDescription(clearString(entity.getLink().getDescription()));
			else
				model.setDescription("Reference attached by "
						+ entity.getUser().getName());
			model.setAttachmentType("link");
			model.setLink(massageUrl(entity.getLink().getLink()));
			model.setIconUrl(getIconUrlForFile(entity.getLink().getLink()));
			model.setProvider(entity.getUser().getName());
			model.setProviderId(entity.getUser().getId());
			model.setOpinionTitle(entity.getSubject());
			model.setOpinionId(entity.getId());
			return model;
		} else
			return null;
	}
}
