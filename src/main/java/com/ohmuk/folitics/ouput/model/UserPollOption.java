/**
 * 
 */
package com.ohmuk.folitics.ouput.model;

/**
 * @author Kalpana
 *
 */
public class UserPollOption {
	private Long pollId;
	private Long pollOptionId;
	public Long getPollId() {
		return pollId;
	}
	public void setPollId(Long pollId) {
		this.pollId = pollId;
	}
	public Long getPollOptionId() {
		return pollOptionId;
	}
	public void setPollOptionId(Long pollOptionId) {
		this.pollOptionId = pollOptionId;
	}
}
