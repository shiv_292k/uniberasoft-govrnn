package com.ohmuk.folitics.ouput.model;

import java.util.List;

public class UIMetrics {
	List<Metric> metrics;

	public List<Metric> getMetrics() {
		return metrics;
	}

	public void setMetrics(List<Metric> metrics) {
		this.metrics = metrics;
	}

	@Override
	public String toString() {
		return "UIMetrics [metrics=" + metrics + "]";
	}
	
	
}
