package com.ohmuk.folitics.ouput.model;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;

public class BaseOutputModel<T> implements Serializable, Comparable<Object>  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String LINK="link";
	protected static final Map<String, String> ICON_URL_MAP = new HashMap<String, String>();
	static {
		ICON_URL_MAP.put("doc", "/images/word-icon.png");
		ICON_URL_MAP.put("docx", "/images/word-icon.png");
		ICON_URL_MAP.put("pdf", "/images/pdf-icon.png");
		ICON_URL_MAP.put("txt", "/images/attachment-icon.png");
		ICON_URL_MAP.put("attachment", "/images/attachment-icon.png");
		ICON_URL_MAP.put("html", "/images/link-icon.png");
		ICON_URL_MAP.put("png", "/images/link-icon.png");
		ICON_URL_MAP.put("jpg", "/images/link-icon.png");
		ICON_URL_MAP.put("jpeg", "/images/link-icon.png");
		ICON_URL_MAP.put("svg", "/images/link-icon.png");
		ICON_URL_MAP.put("avi", "/images/video-icon.png");
		ICON_URL_MAP.put("flv", "/images/video-icon.png");
		ICON_URL_MAP.put("mpv", "/images/video-icon.png");
		ICON_URL_MAP.put("mp4", "/images/video-icon.png");
		ICON_URL_MAP.put("wmv", "/images/video-icon.png");
		ICON_URL_MAP.put(LINK, "/images/link-icon.png");

	}
	private Long id;
	private String title;
	private String type;
	private UIMetrics uiMetrics;
	private List<T> otherAttributes;
	private String formattedAge;
	private Long views;
	private String imageFileType;
	private String fileType = "attachment";

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getImageFileType() {
		return imageFileType;
	}

	public void setImageFileType(String imageFileType) {
		this.imageFileType = imageFileType;
	}

	/**
	 * @return the views
	 */
	public Long getViews() {
		return views;
	}

	/**
	 * @param views
	 *            the views to set
	 */
	public void setViews(Long views) {
		this.views = views;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UIMetrics getUiMetrics() {
		return uiMetrics;
	}

	public void setUiMetrics(UIMetrics uiMetrics) {
		this.uiMetrics = uiMetrics;
	}

	public List<T> getOtherAttributes() {
		return otherAttributes;
	}

	public void setOtherAttributes(List<T> otherAttributes) {
		this.otherAttributes = otherAttributes;
	}

	public String getFormattedAge() {
		return formattedAge;
	}

	public void setFormattedAge(String formattedAge) {
		this.formattedAge = formattedAge;
	}

	public BaseOutputModel() {
		super();
	}

	public BaseOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<T> otherAttributes, String formattedAge) {
		setId(id);
		setTitle(title);
		setType(type);
		setUiMetrics(uiMetrics);
		setOtherAttributes(otherAttributes);
		setFormattedAge(formattedAge);
	}
	public BaseOutputModel(Long id, String title, String type,
			UIMetrics uiMetrics, List<T> otherAttributes, String formattedAge, Timestamp createTime) {
		setId(id);
		setTitle(title);
		setType(type);
		setUiMetrics(uiMetrics);
		setOtherAttributes(otherAttributes);
		setFormattedAge(formattedAge);
		setCreateTime(createTime);
	}

	public static String getIconUrlForFile(String filePath) {
		String linkType = getFileType(filePath);
		return getIconUrl(linkType);
	}
	
	public static String getIconUrl(String linkType) {
		String iconUrl = ICON_URL_MAP.get(linkType);
		if (StringUtils.isEmpty(iconUrl)){
			iconUrl = ICON_URL_MAP.get(LINK);
		}
		return iconUrl;
	}

	public static String getFileType(String link) {
		URL url;
		try {
			url = new URL(link);
			return FilenameUtils.getExtension(url.getPath());
		} catch (MalformedURLException e) {
			//e.printStackTrace();
		}
		return LINK;
	}

	public static final String massageUrl(String str){
		if(StringUtils.isEmpty(str)){
			return "";
		}
		if(str.toLowerCase().contains("www.") && !str.toLowerCase().contains("http")){
			return "http://"+str;
		}
		return str;
	}
	
	public static boolean isValidUrl(String url) {
		try {
			new URL(massageUrl(url));
			return true;
		} catch (MalformedURLException e) {
			// e.printStackTrace();
		}
		return false;
	}

	public static final String clearString(String str){
		if(StringUtils.isEmpty(str)){
			return "";
		}
		return StringUtils.trimWhitespace(str).replace("\"", "");
	}
	
	private Timestamp createTime;

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public int compareTo(Object o) {
		BaseOutputModel<T> o1 = (BaseOutputModel<T>)o;
		if (getCreateTime() == null || o1.getCreateTime() == null)
			return 0;
		return o1.getCreateTime().compareTo(getCreateTime());
	}

	
}
