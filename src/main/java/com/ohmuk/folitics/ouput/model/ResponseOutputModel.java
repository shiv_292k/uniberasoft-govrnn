package com.ohmuk.folitics.ouput.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.enums.FileType;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.util.ThumbnailUtil;

  public class ResponseOutputModel extends BaseOutputModel<Response> implements
      Serializable {
         /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
		private byte[] userImage;
	    private String  userName;
	    private Long userId;
	    private String flag;
	    private String content;
	    private Timestamp edited;
	    private Long pollId;
	    private List<PollOptionsCount> pollOptionsCounts = new ArrayList<PollOptionsCount>();
	    private List<ResponseOutputModel> childResponses = new ArrayList<ResponseOutputModel>();
		private long opinionId;
		private byte[] responseAttachment;
		private String positiveFeel;
		private String negativeFeel;
		private long positiveFeelCount;
		private long negativeFeelCount;
		
		
	    public ResponseOutputModel(Long id, String title, String type,
				UIMetrics uiMetrics, List<Response> otherAttributes,
				String formattedAge) {
			super(id, title, type, uiMetrics, otherAttributes, formattedAge);
			// TODO Auto-generated constructor stub
		}
		
	    public static final ResponseOutputModel getModel(Response entity ) {
	    	ResponseOutputModel model = new ResponseOutputModel(entity.getId(),
					 null,null, null, null,null);
			//model.setImage(entity.getUser().get);
			model.setUserName(entity.getUser().getName());
			model.setUserId(entity.getUser().getId());
			try {
				for (UserImage userImage : entity.getUser().getUserImages()) {
					if(userImage.getImageType().equals(ImageTypeEnum.USERIMAGE.getImageType())){
							model.setUserImage(ThumbnailUtil.getImageThumbnail(userImage.getImage(),FileType.JPEG.getValue()));
					}
				}
				if(model.getUserImage()==null){
					model.setUserImage((byte[])SingletonCache.getInstance().get(FoliticsCache.KEYS.USER_IMAGE));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			model.setOpinionId(entity.getOpinion().getId());
			model.setFlag(entity.getFlag());
			model.setContent(entity.getContent());
			model.setEdited(entity.getEdited());
			model.setPollId(entity.getUpDownVote().getId());
			model.setResponseAttachment(entity.getResponseAttachment());
			   
			for (PollOption pollOption : entity.getUpDownVote().getOptions()) {
				PollOptionsCount pollOptionsCount = new PollOptionsCount();
				pollOptionsCount.setPollOptionId(pollOption.getId());
				pollOptionsCount.setPollOption(pollOption.getPollOption());
				if(pollOption.getUsers() != null)
					pollOptionsCount.setCount(pollOption.getUsers().size());
				model.getPollOptionsCounts().add(pollOptionsCount);
			}
			return model;
			
			
		}

		public byte[] getUserImage() {
			return userImage;
		}

		public void setUserImage(byte[] userImage) {
			this.userImage = userImage;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getFlag() {
			return flag;
		}

		public void setFlag(String flag) {
			this.flag = flag;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public Timestamp getEdited() {
			return edited;
		}

		public void setEdited(Timestamp edited) {
			this.edited = edited;
		}

		/**
		 * @return the pollOptionsCounts
		 */
		public List<PollOptionsCount> getPollOptionsCounts() {
			return pollOptionsCounts;
		}

		/**
		 * @param pollOptionsCounts the pollOptionsCounts to set
		 */
		public void setPollOptionsCounts(List<PollOptionsCount> pollOptionsCounts) {
			this.pollOptionsCounts = pollOptionsCounts;
		}

		/**
		 * @return the childResponses
		 */
		public List<ResponseOutputModel> getChildResponses() {
			return childResponses;
		}

		/**
		 * @param childResponses the childResponses to set
		 */
		public void setChildResponses(List<ResponseOutputModel> childResponses) {
			this.childResponses = childResponses;
		}

		/**
		 * @return the pollId
		 */
		public Long getPollId() {
			return pollId;
		}

		/**
		 * @param pollId the pollId to set
		 */
		public void setPollId(Long pollId) {
			this.pollId = pollId;
		}

		/**
		 * @return the userId
		 */
		public Long getUserId() {
			return userId;
		}

		/**
		 * @param userId the userId to set
		 */
		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public long getOpinionId() {
			return opinionId;
		}

		public void setOpinionId(long opinionId) {
			this.opinionId = opinionId;
		}

		public byte[] getResponseAttachment() {
			return responseAttachment;
		}

		public void setResponseAttachment(byte[] responseAttachment) {
			this.responseAttachment = responseAttachment;
		}

		public String getPositiveFeel() {
			return positiveFeel;
		}

		public void setPositiveFeel(String positiveFeel) {
			this.positiveFeel = positiveFeel;
		}

		public String getNegativeFeel() {
			return negativeFeel;
		}

		public void setNegativeFeel(String negativeFeel) {
			this.negativeFeel = negativeFeel;
		}

		public long getPositiveFeelCount() {
			return positiveFeelCount;
		}

		public void setPositiveFeelCount(long positiveFeelCount) {
			this.positiveFeelCount = positiveFeelCount;
		}

		public long getNegativeFeelCount() {
			return negativeFeelCount;
		}

		public void setNegativeFeelCount(long negativeFeelCount) {
			this.negativeFeelCount = negativeFeelCount;
		}
		
	}