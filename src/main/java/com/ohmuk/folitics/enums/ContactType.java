package com.ohmuk.folitics.enums;

public enum ContactType {

	EMAIL("Email"),MOBILE("Mobile"),FACEBOOK("Facebook"),TWITTER("Twitter"),GOOGLE_PLUS("Google+");
	
	private String vaue;
	
	private ContactType(String value){
		this.vaue = value;
	}

	public static final ContactType getContactType(String value){
		
		if(EMAIL.getVaue().equals(value)){
			return EMAIL;
		}
		else if(MOBILE.getVaue().equals(value)){
			return MOBILE;
		}
		else if(FACEBOOK.getVaue().equals(value)){
			return FACEBOOK;
		}
		else if(TWITTER.getVaue().equals(value)){
			return TWITTER;
		}
		else if (GOOGLE_PLUS.getVaue().equals(value)){
			return GOOGLE_PLUS;
		}
		return null;
	}
	
	/**
	 * @return the vaue
	 */
	public String getVaue() {
		return vaue;
	}

	/**
	 * @param vaue the vaue to set
	 */
	public void setVaue(String vaue) {
		this.vaue = vaue;
	}
	
}
