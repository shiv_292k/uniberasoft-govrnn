package com.ohmuk.folitics.enums;

public enum UserUINotificationTypes {

	OPINION("Opinion"), GENERAL("General");

	private String value;

	private UserUINotificationTypes(String value) {

		setValue(value);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static final UserUINotificationTypes getUserUINotificationTypes(
			String value) {

		if (OPINION.getValue().equals(value)) {
			return OPINION;
		}
		if (GENERAL.getValue().equals(value)) {
			return GENERAL;
		}

		return null;
	}
}
