package com.ohmuk.folitics.enums;

import org.bouncycastle.crypto.RuntimeCryptoException;

public enum RatingType {

	SAD(0L), BAISED(1L), ADVERT(2L), PLAGRISM(3L), HIDING_FACTS(4L), POSITIVE(
			5L), WELL_FORMAT(6l), INTERESTING(7l), INFORMATIVE(8l), ATTRACTIVE(
					9l), ETHICAL(10l);

	private Long value;

	private RatingType(Long value) {
		this.setValue(value);
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public static final RatingType getRatingType(Long value) {
		if (SAD.getValue().equals(value)) {
			return SAD;
		}
		if (BAISED.getValue().equals(value)) {
			return BAISED;
		}
		if (ADVERT.getValue().equals(value)) {
			return ADVERT;
		}
		if (PLAGRISM.getValue().equals(value)) {
			return PLAGRISM;
		}
		if (HIDING_FACTS.getValue().equals(value)) {
			return HIDING_FACTS;
		}
		if (POSITIVE.getValue().equals(value)) {
			return POSITIVE;
		}
		if (WELL_FORMAT.getValue().equals(value)) {
			return WELL_FORMAT;
		}
		if (INTERESTING.getValue().equals(value)) {
			return INTERESTING;
		}
		if (INFORMATIVE.getValue().equals(value)) {
			return INFORMATIVE;
		}
		if (ATTRACTIVE.getValue().equals(value)) {
			return ATTRACTIVE;
		}
		if (ETHICAL.getValue().equals(value)) {
			return ETHICAL;
		}
		throw new RuntimeCryptoException("Rating type not defined : "+value);
	}
}
