package com.ohmuk.folitics.notification;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.enums.NotificationReadStatus;
import com.ohmuk.folitics.hibernate.entity.NotificationLookUp;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserConnection;
import com.ohmuk.folitics.hibernate.entity.comment.OpinionComment;
import com.ohmuk.folitics.hibernate.entity.comment.SentimentComment;
import com.ohmuk.folitics.hibernate.entity.follow.OpinionFollow;
import com.ohmuk.folitics.hibernate.entity.follow.SentimentFollow;
import com.ohmuk.folitics.hibernate.entity.like.OpinionLike;
import com.ohmuk.folitics.hibernate.entity.like.SentimentLike;
import com.ohmuk.folitics.mongodb.entity.NotificationMappingMongo;
import com.ohmuk.folitics.mongodb.entity.NotificationMongo;
import com.ohmuk.folitics.mongodb.repository.INotificationMappingMongoRepository;
import com.ohmuk.folitics.mongodb.service.INotificationMongoService;
import com.ohmuk.folitics.service.IOpinionService;
import com.ohmuk.folitics.service.IResponseService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.DateUtils;

/**
 * @author Harish
 *
 */
@Service
@Transactional
public class NotificationServiceImplementation implements InterfaceNotificationService {

    @Autowired
    private IUserService userService;

    @Autowired
    private volatile IOpinionService opinionService;

    @Autowired
    private volatile IResponseService responseService;

    @Autowired
    private INotificationMongoService notificationMongoService;

    @Autowired
    private INotificationRepository notificationRepository;

    private static Logger logger = LoggerFactory.getLogger(NotificationServiceImplementation.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private INotificationMappingMongoRepository notificationMappingMongoRepository;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void followNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        Criteria criteria1 = getSession().createCriteria(NotificationLookUp.class);
        criteria1.add(Restrictions.eqOrIsNull("action", "follow"));
        @SuppressWarnings("unchecked")
        List<NotificationLookUp> notificationLookUps = criteria1.list();
        for (NotificationLookUp notificationLookUp : notificationLookUps) {
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("followers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                    criteria.add(Restrictions.eqOrIsNull("followId.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                            Projections.property("followId.userId"));
                    @SuppressWarnings("unchecked")
                    Set<Long> list = (Set<Long>) criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                        Criteria criteria = getSession().createCriteria(OpinionFollow.class);
                        criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                Projections.property("followId.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    }
                }
                if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Likers")) {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        // notificationMapping.setSendingUsers(list);
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                            Criteria criteria = getSession().createCriteria(SentimentLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        } else {
                            if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                                Criteria criteria = getSession().createCriteria(OpinionLike.class);
                                criteria.add(Restrictions.eqOrIsNull("id.componentId",
                                        notificationMapping.getComponentId()));
                                criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                        Projections.property("id.userId"));
                                @SuppressWarnings("unchecked")
                                List<Long> list = criteria.list();
                                Set<Long> userSet = new HashSet<Long>(list);
                                notificationMapping.setSendingUsers(userSet);
                                sendNotificationToForMongo.addAll(userSet);
                            }
                        }
                    }
                }
                if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Commentors")) {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                        Criteria criteria = getSession().createCriteria(SentimentComment.class);
                        criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                .setProjection(Projections.property("userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        // notificationMapping.setSendingUsers(list);
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                            Criteria criteria = getSession().createCriteria(SentimentComment.class);
                            criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                    .setProjection(Projections.property("userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        } else {
                            if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                                Criteria criteria = getSession().createCriteria(OpinionComment.class);
                                criteria.add(
                                        Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                        .setProjection(Projections.property("userId"));
                                @SuppressWarnings("unchecked")
                                List<Long> list = criteria.list();
                                Set<Long> userSet = new HashSet<Long>(list);
                                notificationMapping.setSendingUsers(userSet);
                                sendNotificationToForMongo.addAll(userSet);
                            }
                        }
                    }
                }
            }

            User user = userService.findUserById(notificationMapping.getUserId());
            String msg = user.getUsername() + " follow " + notificationMapping.getComponentType()
                    + " which you have already followed";
            notificationMapping.setMessage(msg);
            // call for redis
            Notification notification = new Notification();
            notification.setMessage(msg);
            notification.setNotificationType("general");
            notification.setTimeStamp(DateUtils.getSqlTimeStamp());
            notification.setSendingUsers(sendNotificationToForMongo);
            NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
            notificationMappingMongoRepository.save(notificationMappingMongo);

            // NotificationRepository.createNotification(notificationMapping);
            // notificationRepository.createNotificationWithType(notification);
            // NotificationRepository.createNotification(notificationMapping);
            for (Long sendToUser : sendNotificationToForMongo) {
                NotificationMongo notificationMongo = new NotificationMongo();
                notificationMongo.setComponentId(notificationMapping.getComponentId());
                notificationMongo.setComponentType(notificationMapping.getComponentType());
                notificationMongo.setUserId(notificationMapping.getUserId());
                notificationMongo.setAction("Follow");
                notificationMongo.setMessage(msg);
                notificationMongo.setSendToUser(sendToUser);
                notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
                notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
                notificationMongo.setNotificationType("General");

                NotificationMongo notificationMongo2 = notificationMongoService.save(notificationMongo);
            }
        }
    }

    private NotificationMappingMongo convertNotificationMappingToNotificationMappingMongo(
            NotificationMapping notificationMapping) {
        NotificationMappingMongo notificationMappingMongo = new NotificationMappingMongo();
        notificationMappingMongo.setAction(notificationMapping.getAction());
        notificationMappingMongo.setComponentId(notificationMapping.getComponentId());
        notificationMappingMongo.setComponentType(notificationMapping.getComponentType());
        notificationMappingMongo.setMessage(notificationMapping.getMessage());
        notificationMappingMongo.setNotificationFrom(notificationMapping.getNotificationFrom());
        notificationMappingMongo.setNotificationTo(notificationMapping.getNotificationTo());
        notificationMappingMongo.setNotificationType(notificationMapping.getNotificationType());
        notificationMappingMongo.setSendingUsers(notificationMapping.getSendingUsers());
        notificationMappingMongo.setUserId(notificationMapping.getUserId());
        return notificationMappingMongo;
    }

    @Override
    public void likeNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        Criteria criteria1 = getSession().createCriteria(NotificationLookUp.class);
        criteria1.add(Restrictions.eqOrIsNull("action", "Like"));

        @SuppressWarnings("unchecked")
        List<NotificationLookUp> notificationLookUps = criteria1.list();

        for (NotificationLookUp notificationLookUp : notificationLookUps) {

            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("followers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                    criteria.add(Restrictions.eqOrIsNull("followId.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                            Projections.property("followId.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                        criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                Projections.property("followId.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionFollow.class);
                            criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                    notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                    Projections.property("followId.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Likers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentLike.class);
                    criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                            Projections.property("id.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Commentors")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentComment.class);
                    criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                            .setProjection(Projections.property("userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentComment.class);
                        criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                .setProjection(Projections.property("userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionComment.class);
                            criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                    .setProjection(Projections.property("userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
        }
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getUsername() + " Likes " + notificationMapping.getComponentType()
                + " which you have already like";
        notificationMapping.setMessage(msg);
        Notification notification = new Notification();
        notification.setMessage(msg);
        notification.setNotificationType("general");
        notification.setTimeStamp(DateUtils.getSqlTimeStamp());
        notification.setSendingUsers(sendNotificationToForMongo);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // NotificationRepository.createNotification(notificationMapping);
        // notificationRepository.createNotificationWithType(notification);

        for (Long sendToUser : sendNotificationToForMongo) {
            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("Like");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setUrl("/" + notificationMapping.getComponentType() + "/"
                    + notificationMapping.getComponentId());
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("general");
            notificationMongoService.save(notificationMongo);
        }
    }

    @Override
    public void airNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        Criteria criteria1 = getSession().createCriteria(NotificationLookUp.class);
        criteria1.add(Restrictions.eqOrIsNull("action", "Air"));

        @SuppressWarnings("unchecked")
        List<NotificationLookUp> notificationLookUps = criteria1.list();

        for (NotificationLookUp notificationLookUp : notificationLookUps) {
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("followers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                    criteria.add(Restrictions.eqOrIsNull("followId.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                            Projections.property("followId.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                        criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                Projections.property("followId.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionFollow.class);
                            criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                    notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                    Projections.property("followId.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Likers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentLike.class);
                    criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                            Projections.property("id.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Commentors")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentComment.class);
                    criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                            .setProjection(Projections.property("userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentComment.class);
                        criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                .setProjection(Projections.property("userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionComment.class);
                            criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                    .setProjection(Projections.property("userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
        }
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getUsername() + " Likes " + notificationMapping.getComponentType()
                + "which you have already like";
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {

            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("Like");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("general");
            notificationMongoService.save(notificationMongo);
        }
    }

    @Override
    public void shareNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();

        Criteria criteria1 = getSession().createCriteria(NotificationLookUp.class);
        criteria1.add(Restrictions.eqOrIsNull("action", "Share"));

        @SuppressWarnings("unchecked")
        List<NotificationLookUp> notificationLookUps = criteria1.list();
        for (NotificationLookUp notificationLookUp : notificationLookUps) {
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("followers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                    criteria.add(Restrictions.eqOrIsNull("followId.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                            Projections.property("followId.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                        criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                Projections.property("followId.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionFollow.class);
                            criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                    notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                    Projections.property("followId.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Likers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentLike.class);
                    criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                            Projections.property("id.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Commentors")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentComment.class);
                    criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                            .setProjection(Projections.property("userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentComment.class);
                        criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                .setProjection(Projections.property("userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionComment.class);
                            criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                    .setProjection(Projections.property("userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
        }
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getUsername() + " share " + notificationMapping.getComponentType()
                + "which you have already shared";
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);
        for (Long sendToUser : sendNotificationToForMongo) {
            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("Share");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("general");
            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public void responseNotification(NotificationMapping notificationMapping, Response response) throws Exception {/*
                                                                                                                    * Set
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * sendNotificationToForMongo
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * HashSet
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * Criteria
                                                                                                                    * criteria1
                                                                                                                    * =
                                                                                                                    * getSession
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * createCriteria
                                                                                                                    * (
                                                                                                                    * NotificationLookUp
                                                                                                                    * .
                                                                                                                    * class
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria1
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "action"
                                                                                                                    * ,
                                                                                                                    * "Response"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * @
                                                                                                                    * SuppressWarnings
                                                                                                                    * (
                                                                                                                    * "unchecked"
                                                                                                                    * )
                                                                                                                    * List
                                                                                                                    * <
                                                                                                                    * NotificationLookUp
                                                                                                                    * >
                                                                                                                    * notificationLookUps
                                                                                                                    * =
                                                                                                                    * criteria1
                                                                                                                    * .
                                                                                                                    * list
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * for
                                                                                                                    * (
                                                                                                                    * NotificationLookUp
                                                                                                                    * notificationLookUp
                                                                                                                    * :
                                                                                                                    * notificationLookUps
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * if
                                                                                                                    * (
                                                                                                                    * notificationLookUp
                                                                                                                    * .
                                                                                                                    * getNotificationTo
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * equalsIgnoreCase
                                                                                                                    * (
                                                                                                                    * "followers"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * if
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getComponentType
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * equalsIgnoreCase
                                                                                                                    * (
                                                                                                                    * "response"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * Criteria
                                                                                                                    * criteria
                                                                                                                    * =
                                                                                                                    * getSession
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * createCriteria
                                                                                                                    * (
                                                                                                                    * SentimentFollow
                                                                                                                    * .
                                                                                                                    * class
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "followId.componentId"
                                                                                                                    * ,
                                                                                                                    * response
                                                                                                                    * .
                                                                                                                    * getOpinion
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * getSentiment
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * getId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "isFollowing"
                                                                                                                    * ,
                                                                                                                    * true
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * setProjection
                                                                                                                    * (
                                                                                                                    * Projections
                                                                                                                    * .
                                                                                                                    * property
                                                                                                                    * (
                                                                                                                    * "followId.userId"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * @
                                                                                                                    * SuppressWarnings
                                                                                                                    * (
                                                                                                                    * "unchecked"
                                                                                                                    * )
                                                                                                                    * List
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * list
                                                                                                                    * =
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * list
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * /
                                                                                                                    * /
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * list
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * Set
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * userSet
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * HashSet
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * (
                                                                                                                    * list
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * sendNotificationToForMongo
                                                                                                                    * .
                                                                                                                    * addAll
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * }
                                                                                                                    * }
                                                                                                                    * if
                                                                                                                    * (
                                                                                                                    * notificationLookUp
                                                                                                                    * .
                                                                                                                    * getNotificationTo
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * equalsIgnoreCase
                                                                                                                    * (
                                                                                                                    * "Likers"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * Criteria
                                                                                                                    * criteria
                                                                                                                    * =
                                                                                                                    * getSession
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * createCriteria
                                                                                                                    * (
                                                                                                                    * SentimentLike
                                                                                                                    * .
                                                                                                                    * class
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "id.componentId"
                                                                                                                    * ,
                                                                                                                    * response
                                                                                                                    * .
                                                                                                                    * getOpinion
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * getSentiment
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * getId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "likeFlag"
                                                                                                                    * ,
                                                                                                                    * true
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * setProjection
                                                                                                                    * (
                                                                                                                    * Projections
                                                                                                                    * .
                                                                                                                    * property
                                                                                                                    * (
                                                                                                                    * "id.userId"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * @
                                                                                                                    * SuppressWarnings
                                                                                                                    * (
                                                                                                                    * "unchecked"
                                                                                                                    * )
                                                                                                                    * List
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * list1
                                                                                                                    * =
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * list
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * /
                                                                                                                    * /
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * list
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * Set
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * userSet
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * HashSet
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * (
                                                                                                                    * list1
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * Criteria
                                                                                                                    * criteria2
                                                                                                                    * =
                                                                                                                    * getSession
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * createCriteria
                                                                                                                    * (
                                                                                                                    * OpinionLike
                                                                                                                    * .
                                                                                                                    * class
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria2
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "id.componentId"
                                                                                                                    * ,
                                                                                                                    * response
                                                                                                                    * .
                                                                                                                    * getOpinion
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * getId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria2
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "likeFlag"
                                                                                                                    * ,
                                                                                                                    * true
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * setProjection
                                                                                                                    * (
                                                                                                                    * Projections
                                                                                                                    * .
                                                                                                                    * property
                                                                                                                    * (
                                                                                                                    * "id.userId"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * @
                                                                                                                    * SuppressWarnings
                                                                                                                    * (
                                                                                                                    * "unchecked"
                                                                                                                    * )
                                                                                                                    * List
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * list
                                                                                                                    * =
                                                                                                                    * criteria2
                                                                                                                    * .
                                                                                                                    * list
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * userSet
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * HashSet
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * (
                                                                                                                    * list
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * sendNotificationToForMongo
                                                                                                                    * .
                                                                                                                    * addAll
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * }
                                                                                                                    * if
                                                                                                                    * (
                                                                                                                    * notificationLookUp
                                                                                                                    * .
                                                                                                                    * getNotificationTo
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * equalsIgnoreCase
                                                                                                                    * (
                                                                                                                    * "response"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * Criteria
                                                                                                                    * criteria
                                                                                                                    * =
                                                                                                                    * getSession
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * createCriteria
                                                                                                                    * (
                                                                                                                    * Response
                                                                                                                    * .
                                                                                                                    * class
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * add
                                                                                                                    * (
                                                                                                                    * Restrictions
                                                                                                                    * .
                                                                                                                    * eqOrIsNull
                                                                                                                    * (
                                                                                                                    * "opinion"
                                                                                                                    * ,
                                                                                                                    * response
                                                                                                                    * .
                                                                                                                    * getOpinion
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * .
                                                                                                                    * setProjection
                                                                                                                    * (
                                                                                                                    * Projections
                                                                                                                    * .
                                                                                                                    * property
                                                                                                                    * (
                                                                                                                    * "user"
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * @
                                                                                                                    * SuppressWarnings
                                                                                                                    * (
                                                                                                                    * "unchecked"
                                                                                                                    * )
                                                                                                                    * List
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * list1
                                                                                                                    * =
                                                                                                                    * criteria
                                                                                                                    * .
                                                                                                                    * list
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * /
                                                                                                                    * /
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * list
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * Set
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * userSet
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * HashSet
                                                                                                                    * <
                                                                                                                    * Long
                                                                                                                    * >
                                                                                                                    * (
                                                                                                                    * list1
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setSendingUsers
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * sendNotificationToForMongo
                                                                                                                    * .
                                                                                                                    * addAll
                                                                                                                    * (
                                                                                                                    * userSet
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * }
                                                                                                                    * }
                                                                                                                    * User
                                                                                                                    * user
                                                                                                                    * =
                                                                                                                    * userService
                                                                                                                    * .
                                                                                                                    * findUserById
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getUserId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * String
                                                                                                                    * msg
                                                                                                                    * =
                                                                                                                    * user
                                                                                                                    * .
                                                                                                                    * getUsername
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * +
                                                                                                                    * " share "
                                                                                                                    * +
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getComponentType
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * +
                                                                                                                    * "which you have already shared"
                                                                                                                    * ;
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * setMessage
                                                                                                                    * (
                                                                                                                    * msg
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationRepository
                                                                                                                    * .
                                                                                                                    * createNotification
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * 
                                                                                                                    * for
                                                                                                                    * (
                                                                                                                    * Long
                                                                                                                    * sendToUser
                                                                                                                    * :
                                                                                                                    * sendNotificationToForMongo
                                                                                                                    * )
                                                                                                                    * {
                                                                                                                    * 
                                                                                                                    * NotificationMongo
                                                                                                                    * notificationMongo
                                                                                                                    * =
                                                                                                                    * new
                                                                                                                    * NotificationMongo
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setComponentId
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getComponentId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setComponentType
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getComponentType
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setUserId
                                                                                                                    * (
                                                                                                                    * notificationMapping
                                                                                                                    * .
                                                                                                                    * getUserId
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setAction
                                                                                                                    * (
                                                                                                                    * "Share"
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setMessage
                                                                                                                    * (
                                                                                                                    * msg
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setSendToUser
                                                                                                                    * (
                                                                                                                    * sendToUser
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setReadStatus
                                                                                                                    * (
                                                                                                                    * NotificationReadStatus
                                                                                                                    * .
                                                                                                                    * UNREAD
                                                                                                                    * .
                                                                                                                    * getValue
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setCreatedAt
                                                                                                                    * (
                                                                                                                    * new
                                                                                                                    * Date
                                                                                                                    * (
                                                                                                                    * System
                                                                                                                    * .
                                                                                                                    * currentTimeMillis
                                                                                                                    * (
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongo
                                                                                                                    * .
                                                                                                                    * setNotificationType
                                                                                                                    * (
                                                                                                                    * "opinion"
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * notificationMongoService
                                                                                                                    * .
                                                                                                                    * save
                                                                                                                    * (
                                                                                                                    * notificationMongo
                                                                                                                    * )
                                                                                                                    * ;
                                                                                                                    * }
                                                                                                                    */

        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        List<User> allUsers = userService.readAll();
        logger.info("All users in system : " + allUsers.size());
        Set<Long> allUsersIdsExceptCurrentUser = new HashSet<Long>();
        for (User user : allUsers) {
            logger.info("UserId : " + user.getId());
            if (!user.getId().equals(notificationMapping.getUserId())) {
                allUsersIdsExceptCurrentUser.add(user.getId());
            }
        }
        notificationMapping.setSendingUsers(allUsersIdsExceptCurrentUser);
        sendNotificationToForMongo.addAll(allUsersIdsExceptCurrentUser);
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getName() + " created a " + notificationMapping.getComponentType() + " on the opinion ";
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {
            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("create");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setUrl("#/viewOpinion?id=" + notificationMapping.getComponentId());
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("opinion");
            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public void dislikeNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();

        Criteria criteria1 = getSession().createCriteria(NotificationLookUp.class);
        criteria1.add(Restrictions.eqOrIsNull("action", "Dislike"));

        @SuppressWarnings("unchecked")
        List<NotificationLookUp> notificationLookUps = criteria1.list();

        for (NotificationLookUp notificationLookUp : notificationLookUps) {
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("followers")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                    criteria.add(Restrictions.eqOrIsNull("followId.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                            Projections.property("followId.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);
                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentFollow.class);
                        criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                Projections.property("followId.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);
                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionFollow.class);
                            criteria.add(Restrictions.eqOrIsNull("followId.componentId",
                                    notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("isFollowing", true)).setProjection(
                                    Projections.property("followId.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);
                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Likers")) {

                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentLike.class);
                    criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                            Projections.property("id.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);

                    sendNotificationToForMongo.addAll(userSet);

                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);

                        sendNotificationToForMongo.addAll(userSet);

                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("likeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);

                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }

            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Dislikers")) {

                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentLike.class);
                    criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                    criteria.add(Restrictions.eqOrIsNull("dislikeFlag", true)).setProjection(
                            Projections.property("id.userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);

                    sendNotificationToForMongo.addAll(userSet);

                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentLike.class);
                        criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                        criteria.add(Restrictions.eqOrIsNull("dislikeFlag", true)).setProjection(
                                Projections.property("id.userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);

                        sendNotificationToForMongo.addAll(userSet);

                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionLike.class);
                            criteria.add(Restrictions.eqOrIsNull("id.componentId", notificationMapping.getComponentId()));
                            criteria.add(Restrictions.eqOrIsNull("dislikeFlag", true)).setProjection(
                                    Projections.property("id.userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);

                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }
            }
            if (notificationLookUp.getNotificationTo().equalsIgnoreCase("Commentors")) {
                if (notificationMapping.getComponentType().equalsIgnoreCase("sentiment")) {
                    Criteria criteria = getSession().createCriteria(SentimentComment.class);
                    criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                            .setProjection(Projections.property("userId"));
                    @SuppressWarnings("unchecked")
                    List<Long> list = criteria.list();
                    // notificationMapping.setSendingUsers(list);
                    Set<Long> userSet = new HashSet<Long>(list);
                    notificationMapping.setSendingUsers(userSet);
                    sendNotificationToForMongo.addAll(userSet);

                } else {
                    if (notificationMapping.getComponentType().equalsIgnoreCase("task")) {
                        Criteria criteria = getSession().createCriteria(SentimentComment.class);
                        criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                .setProjection(Projections.property("userId"));
                        @SuppressWarnings("unchecked")
                        List<Long> list = criteria.list();
                        Set<Long> userSet = new HashSet<Long>(list);
                        notificationMapping.setSendingUsers(userSet);
                        sendNotificationToForMongo.addAll(userSet);

                    } else {
                        if (notificationMapping.getComponentType().equalsIgnoreCase("opinion")) {
                            Criteria criteria = getSession().createCriteria(OpinionComment.class);
                            criteria.add(Restrictions.eqOrIsNull("componentId", notificationMapping.getComponentId()))
                                    .setProjection(Projections.property("userId"));
                            @SuppressWarnings("unchecked")
                            List<Long> list = criteria.list();
                            Set<Long> userSet = new HashSet<Long>(list);
                            notificationMapping.setSendingUsers(userSet);

                            sendNotificationToForMongo.addAll(userSet);
                        }
                    }
                }

            }
        }
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getUsername() + " dislike " + notificationMapping.getComponentType()
                + "which you have already dislike";
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {

            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("Dislike");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("general");

            notificationMongoService.save(notificationMongo);
        }
    }

    @Override
    public void taskNotification(NotificationMapping notificationMapping) throws Exception {

        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        sendNotificationToForMongo.addAll(notificationMapping.getSendingUsers());
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getName() + " invited you to participate in his problem ";

        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {

            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("task");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setUrl("/problem/" + notificationMapping.getComponentId());
            notificationMongo.setNotificationType("general");
            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public void monetizationNotification(NotificationMapping notificationMapping) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void opinionNotification(NotificationMapping notificationMapping, Long sentimentId1) throws Exception {

        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        /*
         * Criteria criteria1 = getSession().createCriteria(
         * NotificationLookUp.class);
         * criteria1.add(Restrictions.eqOrIsNull("action", "opinion"));
         * 
         * @SuppressWarnings("unchecked") List<NotificationLookUp>
         * notificationLookUps = criteria1.list();
         * 
         * for (NotificationLookUp notificationLookUp : notificationLookUps) {
         * if (notificationLookUp.getNotificationTo().equalsIgnoreCase(
         * "followers")) { if
         * (notificationMapping.getComponentType().equalsIgnoreCase( "opinion"))
         * { Criteria criteria = getSession().createCriteria(
         * SentimentFollow.class); criteria.add(Restrictions.eqOrIsNull(
         * "followId.componentId", sentimentId));
         * criteria.add(Restrictions.eqOrIsNull("isFollowing", true))
         * .setProjection( Projections.property("followId.userId"));
         * 
         * @SuppressWarnings("unchecked") List<Long> list = criteria.list(); //
         * notificationMapping.setSendingUsers(list); Set<Long> userSet = new
         * HashSet<Long>(list); notificationMapping.setSendingUsers(userSet);
         * sendNotificationToForMongo.addAll(userSet); } } if
         * (notificationLookUp.getNotificationTo().equalsIgnoreCase( "Likers"))
         * { if (notificationMapping.getComponentType().equalsIgnoreCase(
         * "opinion")) { Criteria criteria = getSession().createCriteria(
         * SentimentLike.class);
         * criteria.add(Restrictions.eqOrIsNull("id.componentId", sentimentId));
         * criteria.add(Restrictions.eqOrIsNull("likeFlag", true))
         * .setProjection(Projections.property("id.userId"));
         * 
         * @SuppressWarnings("unchecked") List<Long> list = criteria.list(); //
         * notificationMapping.setSendingUsers(list); Set<Long> userSet = new
         * HashSet<Long>(list); notificationMapping.setSendingUsers(userSet);
         * sendNotificationToForMongo.addAll(userSet); } } if
         * (notificationLookUp.getNotificationTo().equalsIgnoreCase(
         * "connections")) { if
         * (notificationMapping.getComponentType().equalsIgnoreCase( "opinion"))
         * { Criteria criteria = getSession().createCriteria(
         * UserConnection.class);
         * criteria.add(Restrictions.eqOrIsNull("connectionStatus",
         * "Accepted")); criteria.add( Restrictions.eqOrIsNull("userId",
         * notificationMapping.getUserId()))
         * .setProjection(Projections.property("connectionId"));
         * 
         * @SuppressWarnings("unchecked") List<Long> list = criteria.list(); //
         * notificationMapping.setSendingUsers(list); Set<Long> userSet = new
         * HashSet<Long>(list); notificationMapping.setSendingUsers(userSet);
         * 
         * sendNotificationToForMongo.addAll(userSet);
         * 
         * } } }
         */
        List<User> allUsers = userService.readAll();
        logger.info("All users in system : " + allUsers.size());
        Set<Long> allUsersIdsExceptCurrentUser = new HashSet<Long>();
        for (User user : allUsers) {
            logger.info("UserId : " + user.getId());
            if (!user.getId().equals(notificationMapping.getUserId())) {
                allUsersIdsExceptCurrentUser.add(user.getId());
            }
        }
        notificationMapping.setSendingUsers(allUsersIdsExceptCurrentUser);
        sendNotificationToForMongo.addAll(allUsersIdsExceptCurrentUser);
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getName() + " created a " + notificationMapping.getComponentType() + " on the sentiment ";
        notificationMapping.setMessage(msg);
        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);
        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {
            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("create");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setUrl("#/viewOpinion?id=" + notificationMapping.getComponentId());
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("Opinion");
            notificationMongoService.save(notificationMongo);
        }
    }

    @Override
    public void generalNotification(NotificationMapping notificationMapping, Response responseObject,
            String pollOption, Long responseId, Long userID) throws Exception {
        logger.info("Entering NotificationServiceImplementation generalNotification::");
        logger.info("Componenet type :::::" + notificationMapping.getComponentType());
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        Long opinionOwner = null;
        Long responseOwner = null;
        String msg = "";
        Long opinionId = null;
        Set<Long> sendNotificationTo = new HashSet<Long>();
        User user = userService.findUserById(notificationMapping.getUserId());
        if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.RESPONSE.getValue())
                && notificationMapping.getParentComponent() != null) {

            Response response = responseService.getResponseById(notificationMapping.getComponentId());
            if (response != null && response.getOpinion() != null && response.getOpinion().getUser() != null) {
                opinionOwner = response.getOpinion().getUser().getId();
                opinionId = response.getOpinion().getId();
                sendNotificationTo.add(responseOwner);
                responseOwner = response.getUser().getId();
                sendNotificationTo.add(opinionOwner);
                if (responseObject != null) {
                    if (responseObject.getFlag() == null) {

                    } else {
                        if (responseObject.getFlag().equals(Constants.AGREE)) {
                            msg = user.getName() + "$ Agreed to Your $" + "Opinion";
                        }
                        if (responseObject.getFlag().equals(Constants.DISAGREE)) {
                            msg = user.getName() + "$ Disagree to Your $" + "Opinion";
                        }
                    }
                }
            }
        } else if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.RESPONSE.getValue())) {

            Response response = responseService.getResponseById(notificationMapping.getComponentId());
            if (response != null && response.getOpinion() != null && response.getOpinion().getUser() != null) {
                opinionOwner = response.getOpinion().getUser().getId();
                opinionId = response.getOpinion().getId();
                sendNotificationTo.add(responseOwner);
                responseOwner = response.getUser().getId();
                sendNotificationTo.add(opinionOwner);

                if (responseObject != null) {
                    if (responseObject.getFlag() == null) {

                    } else {
                        if (responseObject.getFlag().equals(Constants.AGREE)) {
                            msg = user.getName() + "$ Agreed to Your $" + "Opinion";
                        }
                        if (responseObject.getFlag().equals(Constants.DISAGREE)) {
                            msg = user.getName() + "$ Disagree to Your $" + "Opinion" + "";
                        }
                    }
                }

            }
        } else if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.OPINION.getValue())) {
            Opinion opinion = opinionService.read(notificationMapping.getComponentId());
            opinionOwner = opinion.getUser().getId();
            opinionId = opinion.getId();
            sendNotificationTo.add(opinionOwner);
            msg = user.getName() + "$ commented on your $" + notificationMapping.getComponentType();
        }
        List<NotificationMongo> notifications = getAllNotifications();
        for (NotificationMongo notification : notifications) {
            if (notification.getComponentId() != null
                    && (notification.getComponentId().longValue() == notificationMapping.getComponentId().longValue())) {
                if (notification.getComponentType() != null
                        && (notification.getComponentType().equalsIgnoreCase(notificationMapping.getComponentType()))) {
                    if (notification.getUserId() != null
                            && (notification.getUserId().longValue() == notificationMapping.getUserId().longValue())) {
                        notificationMongoService.delete(notification);
                    }
                }
            }
        }
        notificationMapping.setSendingUsers(sendNotificationTo);
        sendNotificationToForMongo.addAll(sendNotificationTo);

        if (responseObject != null) {

            if (responseObject.getId() != null) {
                if (pollOption != null) {
                    if (pollOption.equals(Constants.UP)) {
                        msg = user.getName() + "$ like your $" + notificationMapping.getComponentType();
                    }
                    if (pollOption.equals(Constants.DOWN)) {
                        msg = user.getName() + "$ dislike your $" + notificationMapping.getComponentType();
                    }
                    sendNotificationToForMongo.add(responseObject.getUser().getId());
                }
                if (responseId == null) {

                } else {
                    msg = user.getName() + "$ commented your $" + notificationMapping.getComponentType();
                    Response dbResponse = responseService.getResponseById(responseId);
                    if (dbResponse != null) {
                        sendNotificationToForMongo.add(dbResponse.getUser().getId());
                    }
                }
                if (responseObject.getParentResponse() == null) {
                    sendNotificationToForMongo.add(responseObject.getUser().getId());
                } else {
                    if (responseObject.getParentResponse().getId() != null && pollOption == null) {
                        msg = user.getName() + "$ commented your $" + notificationMapping.getComponentType();
                    }
                }
            }
        }

        if (responseObject != null) {
            if (responseObject.getFlag() == null && pollOption == null) {

            } else {
                if (pollOption != null) {

                } else {
                    sendNotificationToForMongo.remove(responseObject.getUser().getId());
                }

            }
        }

        if (userID == null) {

        } else {
            sendNotificationToForMongo.remove(userID);
        }
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        for (Long sendToUser : sendNotificationToForMongo) {
            if (sendToUser == null) {

            } else {
                NotificationMongo notificationMongo = new NotificationMongo();

                if (responseObject.getFlag() == null) {
                    notificationMongo.setComponentId(notificationMapping.getComponentId());
                    notificationMongo.setComponentType(notificationMapping.getComponentType());
                } else {
                    notificationMongo.setComponentId(responseObject.getOpinion().getId());
                    notificationMongo.setComponentType("Opinion");
                }

                notificationMongo.setUserId(notificationMapping.getUserId());
                notificationMongo.setAction("action");
                notificationMongo.setMessage(msg);
                notificationMongo.setSendToUser(sendToUser);
                if (responseObject != null) {
                    if (responseObject.getId() != null) {
                        notificationMongo.setUrl("#/viewOpinion?id=" + responseObject.getOpinion().getId()
                                + "&responseId=" + responseObject.getId());
                    }
                } else {
                    notificationMongo.setUrl("#/viewOpinion?id=" + opinionId);
                }
                notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
                notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
                notificationMongo.setNotificationType("General");
                notificationMongoService.save(notificationMongo);
            }

        }
    }

    @Override
    public void connectionNotification(NotificationMapping notificationMapping) throws Exception {

        Set<Long> sendNotificationToForMongo = new HashSet<Long>();

        Criteria criteria = getSession().createCriteria(UserConnection.class);
        criteria.add(Restrictions.eqOrIsNull("userId", notificationMapping.getUserId()));
        criteria.add(Restrictions.eqOrIsNull("connectionStatus", "Pending")).setProjection(
                Projections.property("connectionId"));

        @SuppressWarnings("unchecked")
        List<Long> list = criteria.list();
        Set<Long> userSet = new HashSet<Long>(list);

        notificationMapping.setSendingUsers(userSet);

        sendNotificationToForMongo.addAll(notificationMapping.getSendingUsers());

        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getName() + " sent a friend request to you ";

        notificationMapping.setMessage(msg);

        Notification notification = new Notification();
        notification.setMessage(msg);
        notification.setNotificationType("connection");
        notification.setTimeStamp(DateUtils.getSqlTimeStamp());
        notification.setSendingUsers(userSet);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotificationWithType(notification);

        for (Long sendToUser : sendNotificationToForMongo) {

            NotificationMongo notificationMongo = new NotificationMongo();

            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("connection");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setUrl("/requesteduser/"

            + sendToUser);
            notificationMongo.setNotificationType("connection");

            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public List<NotificationMongo> getAllNotifications() throws Exception {
        return notificationMongoService.getAll();
    }

    @Override
    public void participateNotification(NotificationMapping notificationMapping) throws Exception {
        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        sendNotificationToForMongo.addAll(notificationMapping.getSendingUsers());
        User user = userService.findUserById(notificationMapping.getUserId());
        String msg = user.getName() + " wants to participate in your problem ";

        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {

            NotificationMongo notificationMongo = new NotificationMongo();

            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("participate");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setUrl("/problem/" + notificationMapping.getComponentId());
            notificationMongo.setNotificationType("General");

            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public void sendTagNotification(NotificationMapping notificationMapping, List<Long> userIds) throws Exception {
        logger.info("Entering NotificationServiceImplementation generalNotification");

        Set<Long> sendNotificationToForMongo = new HashSet<Long>();
        /*
         * Long opinionOwner = null; Long responseOwner = null;
         */
        String msg = "";
        Set<Long> sendNotificationTo = new HashSet<Long>();
        User user = userService.findUserById(notificationMapping.getUserId());
        if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.RESPONSE.getValue())
                && notificationMapping.getParentComponent() != null) {

            Response response = responseService.getResponseById(notificationMapping.getComponentId());
            if (response != null && response.getOpinion() != null && response.getOpinion().getUser() != null) {
                /*
                 * opinionOwner = response.getOpinion().getUser().getId();
                 * sendNotificationTo.add(responseOwner); responseOwner =
                 * response.getUser().getId();
                 * sendNotificationTo.add(opinionOwner);
                 */
                msg = user.getName() + "$ Tagged you in $" + notificationMapping.getComponentType();
            }
        } else if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.RESPONSE.getValue())) {

            Response response = responseService.getResponseById(notificationMapping.getComponentId());
            if (response != null && response.getOpinion() != null && response.getOpinion().getUser() != null) {
                /*
                 * opinionOwner = response.getOpinion().getUser().getId();
                 * sendNotificationTo.add(responseOwner); responseOwner =
                 * response.getUser().getId();
                 * sendNotificationTo.add(opinionOwner);
                 */
                msg = user.getName() + "$ Tagged you in $" + notificationMapping.getComponentType();
            }
        } else if (notificationMapping.getComponentType().equalsIgnoreCase(ComponentType.OPINION.getValue())) {
            Opinion opinion = opinionService.read(notificationMapping.getComponentId());
            /*
             * opinionOwner = opinion.getUser().getId();
             * sendNotificationTo.add(opinionOwner);
             */
            msg = user.getName() + "$ Tagged you in $" + notificationMapping.getComponentType();
        }
        List<NotificationMongo> notifications = getAllNotifications();

        sendNotificationTo.addAll(userIds);
        notificationMapping.setSendingUsers(sendNotificationTo);
        sendNotificationToForMongo.addAll(userIds);
        notificationMapping.setMessage(msg);

        // insert a new notificationMapping object in mongoDB;
        NotificationMappingMongo notificationMappingMongo = convertNotificationMappingToNotificationMappingMongo(notificationMapping);
        notificationMappingMongoRepository.save(notificationMappingMongo);

        System.out.println("component Id ::" + notificationMapping.getComponentId());
        // notificationRepository.createNotification(notificationMapping);

        for (Long sendToUser : sendNotificationToForMongo) {
            NotificationMongo notificationMongo = new NotificationMongo();
            notificationMongo.setComponentId(notificationMapping.getComponentId());
            notificationMongo.setComponentType(notificationMapping.getComponentType());
            notificationMongo.setUserId(notificationMapping.getUserId());
            notificationMongo.setAction("action");
            notificationMongo.setMessage(msg);
            notificationMongo.setSendToUser(sendToUser);
            notificationMongo.setUrl("#/viewOpinion?id=" + notificationMapping.getComponentId() + "&responseId="
                    + notificationMapping.getComponentId());
            notificationMongo.setReadStatus(NotificationReadStatus.UNREAD.getValue());
            notificationMongo.setCreatedAt(new Date(System.currentTimeMillis()));
            notificationMongo.setNotificationType("General");
            notificationMongoService.save(notificationMongo);
        }

    }

    @Override
    public boolean deleteNotificationByUserId(Long userId) throws Exception {
        return notificationMongoService.deleteByUserId(userId);
    }

    @Override
    public boolean deleteNotificationByComponentIdAndType(Long componentId, String componentType) throws Exception {
        return notificationMongoService.deleteByComponentIdAndType(componentId, componentType);
    }

    @Override
    public boolean deleteNotificationMappingsByUserId(Long userId) throws Exception {
        return notificationMappingMongoRepository.deleteByUserId(userId);
    }

}
