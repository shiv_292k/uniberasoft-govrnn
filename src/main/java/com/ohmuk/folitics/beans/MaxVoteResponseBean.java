package com.ohmuk.folitics.beans;

public class MaxVoteResponseBean implements Comparable<MaxVoteResponseBean> {

	private Long responseId;
	private int  voteCount;


	public Long getResponseId() {
		return responseId;
	}
	
	public void setResponseId(Long responseId) {
		this.responseId = responseId;
	}
	
	public int getUpVoteResponseCount() {
		return voteCount;
	}
	
	public void setUpVoteResponseCount(int upVoteResponseCount) {
		this.voteCount = upVoteResponseCount;
	}

	@Override
	public int compareTo(MaxVoteResponseBean resUpvoteBean) {
		if(this.voteCount == resUpvoteBean.voteCount){
			return 0;
		}
		else{
			return  resUpvoteBean.voteCount - this.voteCount;
		}
	}
}

