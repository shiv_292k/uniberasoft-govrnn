/**
 * 
 */
package com.ohmuk.folitics.model;

import java.util.List;

/**
 * @author Kalpana
 *
 */

public class TrendMappingMultiple {
	
	private Long componentId;
	private Long userId;
	private String componentType;
	private List<Long> trendIds; 
	/**
	 * @return the componentId
	 */
	public Long getComponentId() {
		return componentId;
	}
	/**
	 * @param componentId the componentId to set
	 */
	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}
	/**
	 * @return the componentType
	 */
	public String getComponentType() {
		return componentType;
	}
	/**
	 * @param componentType the componentType to set
	 */
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}
	/**
	 * @return the trendIds
	 */
	public List<Long> getTrendIds() {
		return trendIds;
	}
	/**
	 * @param trendIds the trendIds to set
	 */
	public void setTrendIds(List<Long> trendIds) {
		this.trendIds = trendIds;
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "TrendMappingMultiple [componentId=" + componentId + ", userId="
				+ userId + ", componentType=" + componentType + ", trendIds="
				+ trendIds + "]";
	}
	
	
}


