/**
 * 
 */
package com.ohmuk.folitics.model;

/**
 * @author Kalpana
 *
 */
public class NotificationModel {
		private String notificationType;
		private Boolean enabled = true;
		
		/**
		 * 
		 */
		public NotificationModel() {
			// TODO Auto-generated constructor stub
		}
		
		/**
		 * 
		 */
		public NotificationModel(String notificationType, Boolean enabled) {
			// TODO Auto-generated constructor stub
			this.notificationType = notificationType;
			this.enabled = enabled;
		}
		/**
		 * @return the notificationType
		 */
		public String getNotificationType() {
			return notificationType;
		}
		/**
		 * @param notificationType the notificationType to set
		 */
		public void setNotificationType(String notificationType) {
			this.notificationType = notificationType;
		}
		/**
		 * @return the enabled
		 */
		public Boolean getEnabled() {
			return enabled;
		}
		/**
		 * @param enabled the enabled to set
		 */
		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}
		
}
