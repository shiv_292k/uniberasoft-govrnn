/**
 * 
 */
package com.ohmuk.folitics.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ohmuk.folitics.hibernate.entity.Opinion;

/**
 * @author Kalpana
 *
 */
public class OpinionModel {
	
	private Opinion opinion;
	@JsonProperty("userIds")
	private List<Long> userIds;
	/**
	 * @return the opinion
	 */
	public Opinion getOpinion() {
		return opinion;
	}
	/**
	 * @param opinion the opinion to set
	 */
	public void setOpinion(Opinion opinion) {
		this.opinion = opinion;
	}
	/**
	 * @return the userIds
	 */
	public List<Long> getUserIds() {
		return userIds;
	}
	/**
	 * @param userIds the userIds to set
	 */
	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}
	
	

}
