package com.ohmuk.folitics.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.ohmuk.folitics.hibernate.entity.Achievement;
import com.ohmuk.folitics.hibernate.entity.ContactDetails;
import com.ohmuk.folitics.hibernate.entity.Leader;
import com.ohmuk.folitics.hibernate.entity.PoliticalView;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserAssociation;
import com.ohmuk.folitics.hibernate.entity.UserEducation;
import com.ohmuk.folitics.hibernate.entity.UserEmailNotificationSettings;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.UserRole;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;
import com.ohmuk.folitics.util.FoliticsUtils;

public class UserOutputModelFull {

	private Long id;

	private String username;

	private String password;

	private String emailId;

	private String name;

	private String gender;

	private String status;

	private Timestamp dob;

	private String maritalStatus;

	private String state;

	private String religion;

	private Timestamp createTime;

	private String summary;

	private Set<UserAssociation> userAssociation;

	private Set<UserEducation> userEducation;

	private Set<UserEmailNotificationSettings> userEmailNotificationSettings;

	private List<ContactDetails> contactDetails;

	private Set<UserUINotification> userUINotification;

	private List<UserImage> userImages;

	private Set<Achievement> achievements;

	private Set<Leader> leaders;

	private Set<PoliticalView> politicalViews;

	private String googlePlusEmail;

	private String faceBookEmail;

	private String twitterEmail;

	private String website;

	private Double points;

	private String badge;

	private Long inclinationAggregation;

	private String caste;

	private String qualification;

	private Long mobileNumber;

	private String hobbies;

	private String city;

	private String country;

	private String currentLocation;

	private String nationality;

	private String occupation;

	private String motherTongue;

	private String address;

	private UserRole role;

	private String emePersonName;

	private String emeContactNo;
	
	private String nativeCity;

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Set<Achievement> getAchievements() {
		return achievements;
	}

	public void setAchievements(Set<Achievement> achievements) {
		this.achievements = achievements;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */

	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the userAssociation
	 */
	public Set<UserAssociation> getUserAssociation() {
		return userAssociation;
	}

	/**
	 * @param userAssociation
	 *            the userAssociation to set
	 */
	public void setUserAssociation(Set<UserAssociation> userAssociation) {
		this.userAssociation = userAssociation;
	}

	/**
	 * @return the userEducation
	 */
	public Set<UserEducation> getUserEducation() {
		return userEducation;
	}

	/**
	 * @param userEducation
	 *            the userEducation to set
	 */
	public void setUserEducation(Set<UserEducation> userEducation) {
		this.userEducation = userEducation;
	}

	/**
	 * @return the userEmailNotificationSettings
	 */
	public Set<UserEmailNotificationSettings> getUserEmailNotificationSettings() {
		return userEmailNotificationSettings;
	}

	/**
	 * @param userEmailNotificationSettings
	 *            the userEmailNotificationSettings to set
	 */
	public void setUserEmailNotificationSettings(
			Set<UserEmailNotificationSettings> userEmailNotificationSettings) {
		this.userEmailNotificationSettings = userEmailNotificationSettings;
	}

	/**
	 * @return the userUINotification
	 */
	public Set<UserUINotification> getUserUINotification() {
		return userUINotification;
	}

	/**
	 * @param userUINotification
	 *            the userUINotification to set
	 */
	public void setUserUINotification(Set<UserUINotification> userUINotification) {
		this.userUINotification = userUINotification;
	}

	/**
	 * @return the userImages
	 */
	public List<UserImage> getUserImages() {
		return userImages;
	}

	/**
	 * @param userImage
	 *            the userImage to set
	 */
	public void setUserImages(List<UserImage> userImages) {
		this.userImages = userImages;
	}

	/*    *//**
	 * @return the userPrivacySettings
	 */
	/*
	 * public Set<UserPrivacyData> getUserPrivacySettings() { return
	 * userPrivacySettings; }
	 *//**
	 * @param userPrivacySettings
	 *            the userPrivacySettings to set
	 */
	/*
	 * public void setUserPrivacySettings(Set<UserPrivacyData>
	 * userPrivacySettings) { this.userPrivacySettings = userPrivacySettings; }
	 */

	/**
	 * @return the points
	 */
	public Double getPoints() {
		return points;
	}

	/**
	 * @param points
	 *            the points to set
	 */
	public void setPoints(Double points) {
		this.points = points;
	}

	/**
	 * @return the badge
	 */
	public String getBadge() {
		return badge;
	}

	/**
	 * @param badge
	 *            the badge to set
	 */
	public void setBadge(String badge) {
		this.badge = badge;
	}

	/**
	 * @return the inclinationAggregation
	 */
	public Long getInclinationAggregation() {
		return inclinationAggregation;
	}

	/**
	 * @param inclinationAggregation
	 *            the inclinationAggregation to set
	 */
	public void setInclinationAggregation(Long inclinationAggregation) {
		this.inclinationAggregation = inclinationAggregation;
	}

	/**
	 * @return the role
	 */
	public UserRole getRole() {
		return role;
	}

	/**
	 * @return the caste
	 */
	public String getCaste() {
		return caste;
	}

	/**
	 * @param caste
	 *            the caste to set
	 */
	public void setCaste(String caste) {
		this.caste = caste;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(UserRole role) {
		this.role = role;
	}

	/**
	 * @return the dob
	 */
	public Timestamp getDob() {
		return dob;
	}

	/**
	 * @param dob
	 *            the dob to set
	 */
	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	/**
	 * @return the maritalStatus
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * @param maritalStatus
	 *            the maritalStatus to set
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the religion
	 */
	public String getReligion() {
		return religion;
	}

	/**
	 * @param religion
	 *            the religion to set
	 */
	public void setReligion(String religion) {
		this.religion = religion;
	}

	/**
	 * @return the leaders
	 */
	public Set<Leader> getLeaders() {
		return leaders;
	}

	/**
	 * @param leaders
	 *            the leaders to set
	 */
	public void setLeaders(Set<Leader> leaders) {
		this.leaders = leaders;
	}

	/**
	 * @return the politicalViews
	 */
	public Set<PoliticalView> getPoliticalViews() {
		return politicalViews;
	}

	/**
	 * @param politicalViews
	 *            the politicalViews to set
	 */
	public void setPoliticalViews(Set<PoliticalView> politicalViews) {
		this.politicalViews = politicalViews;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		if (StringUtils.isEmpty(this.qualification))
			return "-";
		return qualification;
	}

	/**
	 * @param qualification
	 *            the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the mobileNumber
	 */
	public Long getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the hobbies
	 */
	public String getHobbies() {
		return hobbies;
	}

	/**
	 * @param hobbies
	 *            the hobbies to set
	 */
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the currentLocation
	 */
	public String getCurrentLocation() {
		return currentLocation;
	}

	/**
	 * @param currentLocation
	 *            the currentLocation to set
	 */
	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	/**
	 * @return the nationality
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation
	 *            the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the motherTongue
	 */
	public String getMotherTongue() {
		return motherTongue;
	}

	/**
	 * @param motherTongue
	 *            the motherTongue to set
	 */
	public void setMotherTongue(String motherTongue) {
		this.motherTongue = motherTongue;
	}

	/**
	 * @return the contactDetails
	 */
	public List<ContactDetails> getContactDetails() {
		return contactDetails;
	}

	/**
	 * @param contactDetails
	 *            the contactDetails to set
	 */
	public void setContactDetails(List<ContactDetails> contactDetails) {
		this.contactDetails = contactDetails;
	}

	/**
	 * @return the Summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param Summary
	 *            the Summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getEmePersonName() {
		return emePersonName;
	}

	public void setEmePersonName(String emePersonName) {
		this.emePersonName = emePersonName;
	}

	public String getEmeContactNo() {
		return emeContactNo;
	}

	public void setEmeContactNo(String emeContactNo) {
		this.emeContactNo = emeContactNo;
	}

	/**
	 * @return the googlePlusEmail
	 */
	public String getGooglePlusEmail() {
		return FoliticsUtils.sanitizeURL(googlePlusEmail);
	}

	/**
	 * @param googlePlusEmail
	 *            the googlePlusEmail to set
	 */
	public void setGooglePlusEmail(String googlePlusEmail) {
		this.googlePlusEmail = googlePlusEmail;
	}

	/**
	 * @return the faceBookEmail
	 */
	public String getFaceBookEmail() {
		return FoliticsUtils.sanitizeURL(faceBookEmail);
	}

	/**
	 * @param faceBookEmail
	 *            the faceBookEmail to set
	 */
	public void setFaceBookEmail(String faceBookEmail) {
		this.faceBookEmail = faceBookEmail;
	}

	/**
	 * @return the twitterEmail
	 */
	public String getTwitterEmail() {
		return FoliticsUtils.sanitizeURL(twitterEmail);
	}

	/**
	 * @param twitterEmail
	 *            the twitterEmail to set
	 */
	public void setTwitterEmail(String twitterEmail) {
		this.twitterEmail = twitterEmail;
	}

	public String getWebsite() {
		return FoliticsUtils.sanitizeURL(website);
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public String getNativeCity() {
		return nativeCity;
	}

	public void setNativeCity(String nativeCity) {
		this.nativeCity = nativeCity;
	}

	public static final UserOutputModelFull getModel(User user) {
		if (user != null) {
			UserOutputModelFull userOutputModel = new UserOutputModelFull();
			userOutputModel.setAchievements(user.getAchievements());
			userOutputModel.setAddress(user.getAddress());
			userOutputModel.setBadge(user.getBadge());
			userOutputModel.setCaste(user.getCaste());
			userOutputModel.setCity(user.getCity());
			userOutputModel.setContactDetails(user.getContactDetails());
			userOutputModel.setCountry(user.getCountry());
			userOutputModel.setCreateTime(user.getCreateTime());
			userOutputModel.setCurrentLocation(user.getCurrentLocation());
			userOutputModel.setDob(user.getDob());
			userOutputModel.setEmailId(user.getEmailId());
			userOutputModel.setEmeContactNo(user.getEmeContactNo());
			userOutputModel.setEmePersonName(user.getEmePersonName());
			userOutputModel.setFaceBookEmail(user.getFaceBookEmail());
			userOutputModel.setGender(user.getGender());
			userOutputModel.setGooglePlusEmail(user.getGooglePlusEmail());
			userOutputModel.setHobbies(user.getHobbies());
			userOutputModel.setId(user.getId());
			userOutputModel.setInclinationAggregation(user
					.getInclinationAggregation());
			userOutputModel.setLeaders(user.getLeaders());
			userOutputModel.setMaritalStatus(user.getMaritalStatus());
			userOutputModel.setMobileNumber(user.getMobileNumber());
			userOutputModel.setMotherTongue(user.getMotherTongue());
			userOutputModel.setName(user.getName());
			userOutputModel.setNationality(user.getNationality());
			userOutputModel.setOccupation(user.getOccupation());
			userOutputModel.setPoints(user.getPoints());
			userOutputModel.setPoliticalViews(user.getPoliticalViews());
			userOutputModel.setQualification(user.getQualification());
			userOutputModel.setReligion(user.getReligion());
			userOutputModel.setRole(user.getRole());
			userOutputModel.setState(user.getState());
			userOutputModel.setSummary(user.getSummary());
			userOutputModel.setTwitterEmail(user.getTwitterEmail());
			userOutputModel.setUserAssociation(user.getUserAssociation());
			userOutputModel.setUserEducation(user.getUserEducation());
			userOutputModel.setUserEmailNotificationSettings(user
					.getUserEmailNotificationSettings());
			userOutputModel.setUserImages(user.getUserImages());
			userOutputModel.setUsername(user.getUsername());
			userOutputModel.setUserUINotification(user.getUserUINotification());
			userOutputModel.setWebsite(user.getWebsite());
			userOutputModel.setNativeCity(user.getNativeCity());
			return userOutputModel;
		}
		return null;
	}

}
