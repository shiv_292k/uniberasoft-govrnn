package com.ohmuk.folitics.businessDelegate.interfaces;

import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.ouput.model.ResponseOutputModel;

public interface IResponseBusinessDelegate {
    /**
     * Method is to add {@link Response} by id
     * 
     * @param response
     * @param pollId 
     * @return {@link Response}
     * @throws Exception
     */
    public Response create(Response response, Long pollId) throws Exception;

    /**
     * Method is to get {@link Response} by id
     * 
     * @param id
     * @return
     * @throws Exception
     */
    public Response getResponseById(Long id) throws Exception;

    /**
     * Method is to get all {@link Response}
     * 
     * @return {@link Response}
     * @throws Exception
     */
    public List<Response> readAll() throws Exception;

    /**
     * Method is to update {@link Response}
     * 
     * @param response
     * @return {@link Response}
     * @throws Exception
     */
    public Response update(Response response) throws Exception;

    /**
     * Method is to get response{@link Response} by opinionId
     * 
     * @param id
     * @return boolean
     * @throws Exception
     */
    public List<Response> getByOpinionId(Long id) throws Exception;

    /**
     * Method is to get response {@link Response} by userId
     * 
     * @param id
     * @return boolean
     * @throws Exception
     */
    public List<Response> getByUserId(Long id) throws Exception;

    /**
     * Method is to delete {@link Response} by id
     * 
     * @param id
     * @return boolean
     * @throws Exception
     */
    public boolean delete(Long id) throws Exception;

    /**
     * Method is to delete {@link Response}
     * 
     * @param response
     * @return boolean
     * @throws Exception
     */
    public boolean delete(Response response) throws Exception;

    /**
     * Method is to hard delete {@link Response} by id
     * 
     * @param id
     * @return boolean
     * @throws Exception
     */
    public boolean deleteFromDBById(Long id) throws Exception;

    /**
     * Method is to hard delete {@link Response}
     * 
     * @param response
     * @return boolean
     * @throws Exception
     */
    public boolean deleteFromDB(Response response) throws Exception;

    /**
     * @param id
     * @return
     * @throws Exception
     */
    public List<Response> getResponseForOpinion(Long id) throws Exception;

    public List<ResponseOutputModel> getAllResponseById(long opinionId) throws Exception;

	/**
	 * @param opinionId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
    public List<Response> getByOpinionAndUser(Long opinionId, Long userId) throws Exception;

	Integer getCountByUserId(Long id) throws Exception;

}
