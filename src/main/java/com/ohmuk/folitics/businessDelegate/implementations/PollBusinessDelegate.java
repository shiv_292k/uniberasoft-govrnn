package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ohmuk.folitics.businessDelegate.interfaces.IPollBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IResponseBusinessDelegate;
import com.ohmuk.folitics.charting.beans.PollResultBean;
import com.ohmuk.folitics.enums.ComponentState;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.hibernate.entity.poll.PollOptionAnswer;
import com.ohmuk.folitics.notification.InterfaceNotificationService;
import com.ohmuk.folitics.notification.NotificationMapping;
import com.ohmuk.folitics.ouput.model.PollOptionsCount;
import com.ohmuk.folitics.service.IPollResult;
import com.ohmuk.folitics.service.IPollService;
import com.ohmuk.folitics.service.ISentimentService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.NotificationUtil;

@Component
@Transactional
public class PollBusinessDelegate implements IPollBusinessDelegate {

    private static Logger logger = LoggerFactory.getLogger(PollBusinessDelegate.class);

    @Autowired
    private volatile IPollService pollService;
    
    @Autowired
    private volatile IUserService userService;
    
    @Autowired
    private volatile ISentimentService sentimentService;
    
    @Autowired
    private volatile Map<String,IPollResult> pollResultService;

    @Autowired
    private volatile IResponseBusinessDelegate responseBusinessDelagate;

	@Autowired
	private volatile InterfaceNotificationService notificationService;
    
	@Override
    public Poll create(Poll poll) throws Exception {
            logger.info("Inside  create method in business delegate");
        List<PollOption> list = poll.getOptions();
        for (PollOption options : list) {
            options.setPoll(poll);
        }
        Poll pollData = pollService.create(poll);
        logger.info("Exiting create method in business delegate");
        return pollData;
    }
    
    @Override
    public Poll getPollById(Long id) throws Exception {
        logger.info("Inside getPollById method in business delegate");
        Poll pollData = pollService.getPollById(id);
        logger.info("Exiting getPollById method in business delegate");
        return pollData;
    }

    @Override
    public List<Poll> readAll() throws Exception {
        logger.info("Inside readAll method in business delegate");
        List<Poll> pollData = pollService.readAll();
        logger.info("Exiting readAll method in business delegate");
        return pollData;
    }

    @Override
    public List<Poll> readAllActivePoll() throws Exception {
        logger.info("Inside readAllActivePoll method in business delegate");
        List<Poll> pollData = pollService.readAllActivePoll();
        logger.info("Exiting readAllActivePoll method in business delegate");
        return pollData;
    }

    @Override
    public Poll update(Poll poll) throws Exception {
        logger.info("Inside update method in business delegate ");
        /*
         * Poll originalPoll = getPollById(poll.getId()); if (originalPoll == null) return null;
         */
        poll.setEditTime(DateUtils.getSqlTimeStamp());
        List<PollOption> list = poll.getOptions();
        for (PollOption options : list) {
            options.setPoll(poll);
        }
        Poll pollData = pollService.update(poll);
        logger.info("Exiting  update method in business delegate ");
        return pollData;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        logger.info("Inside delete method in business delegate");
        boolean poll = pollService.delete(id);
        logger.info("Exiting delete method in business delegate");
        return poll;
    }

    @Override
    public boolean delete(Poll poll) throws Exception {
        logger.info("Inside  delete method in business delegate ");
        poll = getPollById(poll.getId());
        poll.setState(ComponentState.DELETED.getValue());
        List<PollOption> pollOptions = poll.getOptions();
        for (PollOption pollOption : pollOptions) {
            pollOption.setState(ComponentState.DELETED.getValue());
        }
        boolean success = pollService.delete(poll);
        logger.info("Exiting  delete method in business delegate ");
        return success;
    }

    @Override
    public boolean deleteFromDB(Long id) throws Exception {
        logger.info("Inside deleteFromDB method in business delegate");
        boolean sucess = pollService.deleteFromDB(id);
        logger.info("Exiting deleteFromDB method in business delegate");
        return sucess;
    }

    @Override
    public boolean deleteFromDB(Poll poll) throws Exception {
        logger.info("Inside deleteFromDB method in business delegate");
        boolean sucess = pollService.deleteFromDB(poll);
        logger.info("Exiting deleteFromDB method in business delegate");
        return sucess;
    }

    @Override
    public List<Poll> getPollsForSentiment(Long sentimentId) throws Exception {
        logger.info("Inside getPollsForSentiment method in business delegate");
        Sentiment sentiment = sentimentService.read(sentimentId);
        //List<Poll> poll = pollService.getPollsForSentiment(sentimentId);
        logger.info("Exiting getPollsForSentiment method in business delegate");
        return sentiment.getPolls();
    }

    @Override
    public List<Poll> getIsolatedPolls() throws Exception {
        logger.info("Inside getIsolatedPolls method in business delegate");
        List<Poll> poll = pollService.getIsolatedPolls();
        logger.info("Exiting getIsolatedPolls method in business delegate");
        return poll;
    }

    @Override
    public Poll save(Poll sessionPoll) throws Exception {
        logger.info("Inside save method in business delegate");
        Poll poll = pollService.save(sessionPoll);
        logger.info("Exiting save method in business delegate");
        return poll;
    }

    @Override
    public Poll saveAndFlush(Poll sessionPoll) throws Exception {
        logger.info("Inside saveAndFlush method in business delegate");
        Poll poll = pollService.saveAndFlush(sessionPoll);
        logger.info("Exiting saveAndFlush method in business delegate");
        return poll;
    }

    @Override
    public PollOption answerPoll(PollOptionAnswer pollOption) throws Exception {
        logger.info("Inside answerPoll method in business delegate");
        PollOption pollOptionData = pollService.answerPoll(pollOption);
        logger.info("Exiting answerPoll method in business delegate");
        return pollOptionData;
    }

    @Override
    public  List<List<PollResultBean>> getPollResult(Long pollId,String filter) throws Exception {
        Poll poll = pollService.getPollById(pollId);
        IPollResult pollResult = pollResultService.get(filter);
        return pollResult.getPollResult(poll);
    }

	@Override
	public Long createPollOptionAnswer(PollOptionAnswer answer) throws Exception {
        logger.info("Inside   createPollOptionAnswer in business delegate");
        Long id = pollService.createPollOptionAnswer(answer);
        logger.info("Exiting createPollOptionAnswer method in business delegate");
        return id;
	}
	
	@Override
	public Poll selectVote(Long pollOptionId, Long userId, String componentType,Long componentId,Long parentOwnerId) throws Exception {
		logger.info("Inside select vote in business delegate");
		PollOption pollOption = pollService.getPollOptionById(pollOptionId);
		User user = userService.findUserById(userId);
		List<PollOption> pollOptions = pollOption.getPoll().getOptions();
		for (PollOption pollOption2 : pollOptions) {
			if(pollOption2.existUser(user)){
				unSelectVote(pollOption2.getId(), user.getId());
			}
		}
		pollOption = pollService.selectVote(pollOption, user);
		 Response response = new Response() ;
		if (pollOption != null) {
			// component type and component id are required to send the
			// notification
			if (componentType != null && (componentType.equalsIgnoreCase(ComponentType.RESPONSE.getValue())
					|| componentType.equalsIgnoreCase(ComponentType.OPINION.getValue()))) {

				NotificationMapping notificationMapping = NotificationUtil.createGeneralNotificationMapping(user,
						componentType,componentId);
				if(parentOwnerId!=null){
				     response = responseBusinessDelagate.getResponseById(parentOwnerId);
				    
				}
				notificationService.generalNotification(notificationMapping,response,pollOption.getPollOption(),null,userId);
			}
		}
		
		//PollOption newPollOption = pollService.getPollOptionById(pollOptionId);
        /*Hibernate.initialize(poll.getSentiment().getCategories());
		Hibernate.initialize(poll.getSentiment().getSentimentNews());
		Hibernate.initialize(poll.getSentiment().getRelatedSentiments());*/
        logger.info("Exiting select vote method in business delegate");
        return pollOption.getPoll();
	}
	
	@Override
	public Poll unSelectVote(Long pollOptionId, Long userId) throws Exception {
		logger.info("Inside unselect vote in business delegate");
		PollOption pollOption = pollService.getPollOptionById(pollOptionId);
		User user = userService.findUserById(userId);
		Poll poll = null;
		if(pollOption.existUser(user)){
			poll = pollService.unSelectVote(pollOption, user);
		}
        logger.info("Exiting unselect vote method in business delegate");
        return poll;
	}
    
    @Override
	public Long getPollOption(Long pollId, Long userid) throws Exception {
		logger.info("Inside getPollOptions method in business delegate");
		Poll pollData = pollService.getPollById(pollId);
		if (null != pollData.getOptions() && !pollData.getOptions().isEmpty()) {
			for (PollOption option : pollData.getOptions()) {
				List<User> users = option.getUsers();
				if (users != null && !users.isEmpty()) {
					for (User user : users) {
						if (userid.compareTo(user.getId()) == 0) {
							return option.getId();
						}
					}
				}
			}
		}
		logger.info("Exiting getPollOptions method in business delegate");
		return null;
	}
	@Override
	public Map<Long, Long> getAllPollOptionBySentiment(Long sentimentId, Long userId)
			throws Exception {
		logger.info("Inside getAllPollOptionBySentiment method in business delegate");
		Map<Long, Long> sentimentPolls = new HashMap<Long, Long>();
		List<Poll> polls = getPollsForSentiment(sentimentId);
		   if(polls !=null && !polls.isEmpty()){
			   for (Poll poll : polls) {
				   	Long pollOptionId = getPollOption(poll.getId(), userId);
				   	sentimentPolls.put(poll.getId(), pollOptionId);
		   }
	   }
		return sentimentPolls;
	}
	
	@Override
	public List<PollOptionsCount> getPollAggregationPercentage(Long pollId) throws Exception {
		List<PollOptionsCount> pollOptionsCounts = new ArrayList<PollOptionsCount>();
		Poll poll = getPollById(pollId);
		Long sumOfVotes = 0l;
		if (poll != null) {
			logger.debug("Poll with id: " + poll.getId() + " is found");
			// Map<Long, Long> pollAggregation = new HashMap<>();
			List<PollOption> options = poll.getOptions();
			if (null != options && !options.isEmpty()) {
				for (PollOption option : options) {
					// not usre why we are using..
					Long votes = (option.getUsers() != null) ? new Long(option.getUsers().size()) : null;
					sumOfVotes = sumOfVotes + votes;
				}
				
				for (PollOption option : options) {
					// not sure why we are using..
					Long votes = (option.getUsers() != null) ? new Long(option.getUsers().size()) : null;
					PollOptionsCount pollOptionsCount = new PollOptionsCount();
					if (votes != null){
						pollOptionsCount.setCount(votes.intValue());
					}
					else{
						pollOptionsCount.setCount(0);
					}
					pollOptionsCount.setPollOptionId(option.getId());
					if(sumOfVotes!=0){
						pollOptionsCount.setPercentage((votes*100)/sumOfVotes);
					}
					else{
						pollOptionsCount.setPercentage(0);
					}
					pollOptionsCounts.add(pollOptionsCount);
				}
			}
		}
		return pollOptionsCounts;
	}
}
