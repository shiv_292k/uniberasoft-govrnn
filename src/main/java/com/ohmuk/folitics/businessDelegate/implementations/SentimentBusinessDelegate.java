package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.elasticsearch.service.IESService;
import com.ohmuk.folitics.enums.SentimentState;
import com.ohmuk.folitics.enums.SentimentType;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.SentimentNews;
import com.ohmuk.folitics.hibernate.entity.SentimentOpinionStat;
import com.ohmuk.folitics.mongodb.service.INewsFeedMongodbService;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.service.IPollService;
import com.ohmuk.folitics.util.ElasticSearchUtils;
import com.ohmuk.folitics.service.ISentimentService;
@Component
@Transactional
public class SentimentBusinessDelegate implements ISentimentBusinessDelegate {

	private static Logger logger = LoggerFactory
			.getLogger(SentimentBusinessDelegate.class);

	@Autowired
	private volatile ISentimentService sentimentService;

	@Autowired
	private volatile IESService eSService;

	@Autowired
	IPollService pollService;

	@Autowired
	private INewsFeedMongodbService newsFeedService;

	@Override
	public Sentiment save(Sentiment sentiment) throws Exception {
		logger.info("Inside save method in business delegate");
		List<SentimentNews> list = sentiment.getSentimentNews();
		if (list != null) {
			for (SentimentNews options : list) {
				options.setSentiment(sentiment);
			}
		}
		Sentiment sentimentData = sentimentService.save(sentiment);
		logger.info("Exiting save method in business delegate");
		return sentimentData;
	}

	@Override
	public Sentiment read(Long id) throws Exception {
		logger.info("Inside read method in business delegate");
		Sentiment sentimentData = sentimentService.read(id);
		logger.info("Exiting read method in business delegate");
		return sentimentData;
	}

	@Override
	public List<Sentiment> readAll() throws Exception {
		logger.info("Inside readAll method in business delegate");
		List<Sentiment> sentimentData = sentimentService.readAll();
		logger.info("Exiting readAll method in business delegate");
		return sentimentData;
	}

	@Override
	public boolean changeSentimentState(String subject, String sentimentType, SentimentState state )
			throws Exception {
		logger.info("Inside changeSentimentState method in business delegate");
		List<Sentiment> sentiments = sentimentService.findByName(subject);
		try {
			for (Sentiment sentiment : sentiments) {
				if(sentiment.getType().equals(SentimentType.getSentimentType(sentimentType).getValue())){
					sentiment.setState(state.getValue());
					sentimentService.update(sentiment);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		logger.info("Exiting changeSentimentState method in business delegate");
		return true;
	}

	@Override
	public Sentiment update(Sentiment sentiment) throws Exception {
		logger.info("Inside update method in business delegate");
		Sentiment sentimentData = sentimentService.update(sentiment);
		logger.info("Exiting update method in business delegate");
		return sentimentData;
	}

	@Override
	public boolean delete(Long id) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = sentimentService.delete(id);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean delete(Sentiment sentiment) throws Exception {
		logger.info("Inside delete method in business delegate");
		boolean sucess = sentimentService.delete(sentiment);
		logger.info("Exiting delete method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(Long id) throws Exception {
		logger.info("Inside deleteFromDBById method in business delegate");
		Sentiment sentiment = read(id);
		/*
		 * for (Poll poll : sentiment.getPolls()) { poll.setSentiment(null);
		 * pollService.update(poll); }
		 */
		if (sentiment != null && sentiment.getRelatedToSentiments() != null) {
			for (Sentiment related : sentiment.getRelatedToSentiments()) {
				boolean sucess = sentimentService.deleteFromDB(related);
			}

		}
		if (sentiment != null && sentiment.getRelatedSentiments() != null) {
			for (Sentiment related : sentiment.getRelatedSentiments()) {
				boolean sucess = sentimentService.deleteFromDB(related);
			}

		}

		sentiment.setRelatedToSentiments(null);
		sentiment.setRelatedSentiments(null);
		boolean sucess = sentimentService.deleteFromDB(sentiment);
		logger.info("Exiting deleteFromDBById method in business delegate");
		return sucess;
	}

	@Override
	public boolean deleteFromDB(Sentiment sentiment) throws Exception {
		logger.info("Inside deleteFromDB method in business delegate");
		if (sentimentService.read(sentiment.getId()) != null) {
			boolean sucess = sentimentService.deleteFromDB(sentiment);
			logger.info("Exiting deleteFromDB method in business delegate");
			return sucess;
		}
		return true;
	}

	@Override
	public boolean updateSentimentStatus(Long id, String status)
			throws Exception {
		logger.info("Inside updateSentimentStatus method in business delegate");
		boolean sucess = sentimentService.updateSentimentStatus(id, status);
		logger.info("Exiting updateSentimentStatus method in business delegate");
		return sucess;
	}

	@Override
	public Sentiment clone(Sentiment sentiment) throws Exception {
		logger.info("Inside clone method in business delegate");
		sentiment.setId(null);
		sentiment.setState("Active");
		Sentiment sentimentData = sentimentService.clone(sentiment);
		logger.info("Exiting clone method in business delegate");
		return sentimentData;
	}

	@Override
	public List<Sentiment> getAllSentimentNotIn(Set<Long> sentimentIds)
			throws Exception {
		logger.info("Inside getAllSentimentNotIn method in business delegate");
		List<Sentiment> sentimentData = sentimentService
				.getAllSentimentNotIn(sentimentIds);
		logger.info("Exiting getAllSentimentNotIn method in business delegate");
		return sentimentData;
	}

	@Override
	public List<Link> getAllSourcesForSentiment(Sentiment sentiment)
			throws Exception {
		logger.info("Inside getAllSourcesForSentiment method in business delegate");
		List<Link> links = sentimentService
				.getAllSourcesForSentiment(sentiment);
		logger.info("Exiting getAllSourcesForSentiment method in business delegate");
		return links;
	}

	@Override
	public Set<Category> getAllIndicator(Long sentimentId) throws Exception {
		logger.info("Inside SentimentBusinessDelegate getAllIndicator method");
		Sentiment sentiment = sentimentService.read(sentimentId);
		if (null != sentiment) {
			Set<Category> indicators = new HashSet<Category>();

			for (Category subCategory : sentiment.getCategories()) {
				for (Category childCategory : subCategory.getChilds()) {
					Hibernate.initialize(subCategory.getChilds());
					indicators.add(childCategory);
				}
			}
			logger.info("Existing from SentimentBusinessDelegate getAllIndicator method");
			return indicators;
		} else {
			logger.info("Existing from SentimentBusinessDelegate getAllIndicator method");
			return null;
		}
	}

	@Override
	public List<Sentiment> findByType(String type) throws Exception {
		logger.info("Inside find by type method in business delegate");
		List<Sentiment> sentimentData = sentimentService.findByType(type);
		logger.info("Exiting findbytype method in business delegate");
		return sentimentData;
	}

	@Override
	public Set<Sentiment> getRelatedSentiment(Long sentimentId)
			throws Exception {
		// TODO Auto-generated method stub
		return sentimentService.getRelatedSentiment(sentimentId);
	}

	@Override
	public String getSentimentSupport(Long sentimentId) throws Exception {
		SentimentOpinionStat opinionStat = sentimentService
				.getSentimentOpinionStat(sentimentId);
		if (null != opinionStat) {
			if (opinionStat.getAgainstPoints() > opinionStat.getFavorPoints()) {
				return "anti";
			}
			if (opinionStat.getFavorPoints() > opinionStat.getAgainstPoints()) {
				return "pro";
			}
		}
		return "nutral";
	}

	@Override
	public void saveNewsFeed() {

		newsFeedService.saveNewsFeed();

	}

	@Override
	public List<SentimentOutputModel> getAllSentimentByMatch(String title)
			throws Exception {
		logger.info("Inside getAllSentimentByMatch method in business delegate");
		List<SentimentOutputModel> sentiments = sentimentService
				.getAllSentimentByMatch(title);
		logger.info("Exiting getAllSentimentByMatch method in business delegate");
		return sentiments;

	}

	@Override
	public List<Sentiment> getAllSentimentByExactMatch(String subject)
			throws Exception {
		logger.info("Inside getAllSentimentByMatch method in business delegate");
		List<Sentiment> sentiments = sentimentService
				.getAllSentimentByExactMatch(subject);
		logger.info("Exiting getAllSentimentByMatch method in business delegate");
		return sentiments;

	}

	@Override
	public List<SentimentOutputModel> getAllSentimentByElasticMatch(String title)
			throws Exception {
		logger.info("Inside getAllSentimentByElasticMatch method in business delegate");
		SearchResponse searchResponse = eSService.search(
				ElasticSearchUtils.INDEX, ElasticSearchUtils.TYPE_SENTIMENT,
				title);

		List<SentimentOutputModel> sentimentOutputModels = new ArrayList<SentimentOutputModel>();
		for (SearchHit hit : searchResponse.getHits()) {
			// senList.add(new ObjectMapper().readValue(hit.getSourceAsString(),
			// Sentiment.class));
			Sentiment sentiment = new Sentiment();
			String sourceAsString = hit.getSourceAsString();
			if (sourceAsString != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
				sentiment = mapper.readValue(sourceAsString, Sentiment.class);
			}
			sentimentOutputModels.add(SentimentOutputModel.getModel(sentiment));
		}

		logger.info("Exiting getAllSentimentByMatch method in business delegate");
		return sentimentOutputModels;

	}

	@Override
	public List<SentimentOutputModel> getAllSentimentByType(String type)
			throws Exception {
		logger.info("Inside getAllSentimentByType method in business delegate");
		List<SentimentOutputModel> sentimentModels = sentimentService
				.getAllSentimentsByType(type);
		logger.info("Exiting getAllSentimentByType method in business delegate");
		return sentimentModels;

	}

	@Override
	public List<SentimentOutputModel> getSentimentNameList(String type)
			throws Exception {
		logger.info("Inside getSentimentNameList method in business delegate");
		List<SentimentOutputModel> sentimentModels = sentimentService
				.getSentimentNameList(type);
		logger.info("Exiting getSentimentNameList method in business delegate");
		return sentimentModels;

	}

	@Override
	public List<Sentiment> searchSentiment(String searchKeyword) {
		List<Sentiment> sentimentList = sentimentService
				.searchSentiment(searchKeyword);
		return sentimentList;
	}

	@Override
	public List<Sentiment> findByName(String subject) throws Exception {
		logger.info("Inside find by name method in business delegate");
		List<Sentiment> sentiments = sentimentService.findByName(subject);
		logger.info("Exiting find by name method in business delegate");
		return sentiments;
	}
	
	@Override
	public Sentiment findById(Long id) throws Exception {
		logger.info("Inside find by id method in business delegate");
		Sentiment sentimentOutput = sentimentService.read(id);
		logger.info("Exiting find by id method in business delegate");
		return sentimentOutput;
	}

	@Override
	public List<SentimentOutputModel> getPaginatedSentimentByType(String type,
			int from, int to) throws Exception {
		logger.info("Inside getSentimentSetByType method in business delegate");
		List<SentimentOutputModel> sentimentModels = sentimentService
				.getPaginatedSentimentByType(type, from, to);
		logger.info("Exiting getSentimentSetByType method in business delegate");
		return sentimentModels;

	}

}
