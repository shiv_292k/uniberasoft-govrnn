/**
 * 
 */
package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.List;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.businessDelegate.interfaces.ISourceRatingGraphBusinessDelegate;
import com.ohmuk.folitics.hibernate.entity.rating.RatingId;
import com.ohmuk.folitics.ouput.model.SourceRatingGraphOutputModel;
import com.ohmuk.folitics.service.ISourceRatingGraphService;


/**
 * @author Deewan
 *
 */
@Component
public class SourceRatingGraphBusinessDeligate implements ISourceRatingGraphBusinessDelegate{

	
	private static Logger logger = Logger
			.getLogger(SourceRatingGraphBusinessDeligate.class);
	@Autowired
	private volatile ISourceRatingGraphService soureRatingGraphService;
	@Override
	public List<SourceRatingGraphOutputModel> top50PercentRatingType(String feedName) {
		// TODO Auto-generated method stub
			logger.info("Inside top50PercentRatingType method in business delegate");
			List<SourceRatingGraphOutputModel> outputModelsForTop50PercentRatingType = soureRatingGraphService.top50PercentRatingType(feedName);
			logger.info("Exiting top50PercentRatingType method in business delegate");
			return outputModelsForTop50PercentRatingType;
	}
	@Override
	public List<SourceRatingGraphOutputModel> ratingTypePercentage(
			String feedName) {
		// TODO Auto-generated method stub
		
		logger.info("Inside ratingTypePercentage method in business delegate");
		List<SourceRatingGraphOutputModel> outputModelsForRatingTypePercentage = soureRatingGraphService.ratingTypePercentage(feedName);
		logger.info("Exiting ratingTypePercentage method in business delegate");
		return outputModelsForRatingTypePercentage;
	}
	@Override
	public List<SourceRatingGraphOutputModel> ratingTypeBarGraph(String feedName) {
		// TODO Auto-generated method stub
		
		logger.info("Inside ratingTypeBarGraph method in business delegate");
		List<SourceRatingGraphOutputModel> outputModelsForRatingTypeBarGraph = soureRatingGraphService.ratingTypeBarGraph(feedName);
		logger.info("Exiting ratingTypeBarGraph method in business delegate");
		return outputModelsForRatingTypeBarGraph;
	}

	
}
