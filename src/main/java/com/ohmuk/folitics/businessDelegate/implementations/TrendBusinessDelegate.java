/**
 * 
 */
package com.ohmuk.folitics.businessDelegate.implementations;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ohmuk.folitics.businessDelegate.interfaces.ITrendBusinessDelegate;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.comment.TrendComment;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.ouput.model.UserTrendOutputModel;
import com.ohmuk.folitics.service.ITrendService;

/**
 * @author Deewan
 *
 */
@Component
public class TrendBusinessDelegate implements ITrendBusinessDelegate {

	private static Logger logger = LoggerFactory
			.getLogger(TrendBusinessDelegate.class);
	@Autowired
	private volatile ITrendService trendService;

	@Override
	public List<Trend> displayTrend(Trend trend) throws Exception {
		logger.info("Inside displayTrend method in business delegate");
		List<Trend> trendData = trendService.displayTrend(trend);
		logger.info("Exiting displayTrend method in business delegate");
		return trendData;
	}

	@Override
	public boolean verifyIfTrendNameExist(Trend trend) throws Exception {
		logger.info("Inside verifyIfTrendnameExist method in business delegate");
		boolean result = trendService.verifyIfTrendnameExist(trend);
		logger.info("Exiting verifyIfTrendnameExist method in business delegate");
		return result;
	}

	@Override
	public Trend createTrend(Trend trend) throws Exception {
		logger.info("Inside TrendBusinessDelegate createTrend method");
		if (trend.getId() == null) {
			Trend existingTrend = trendService.getTrendByName(trend.getName());
			if(existingTrend==null){
				return trendService.saveTrend(trend);	
			}
			logger.info("Trend alreadt exists : "+trend);
		}
		logger.info("Exiting TrendBusinessDelegate createTrend method");
		return null;
	}

	@Override
	public Trend findTrend(Long id) throws Exception {
		logger.info("Inside TrendBusinessDelegate findTrend method");
		logger.info("Exiting TrendBusinessDelegate findTrend method");
		return trendService.readTrend(id);
	}

	@Override
	public Trend getTrendByName(String name) throws Exception {
		logger.info("Inside TrendBusinessDelegate getTrendByIdorName method");
		Trend tr =trendService.getTrendByName(name);
		logger.info("Exiting TrendBusinessDelegate getTrendByIdorName method");
		return tr; 
	}

	@Override
	public Trend update(Trend trend) throws Exception {
		logger.info("Inside Trend Service update method");
		Trend tr = trendService.update(trend);
		logger.info("Existing from TrendService update method");
		return tr;
	}
		
	@Override
	public List<User> top5User(String name) throws Exception {
		logger.info("Inside top5User method in business delegate");
		List<User> user = trendService.top5User(name);
		logger.info("Exiting saveTrend method in business delegate");
		return user;
	}

	@Override
	public Long saveTrendMapping(Trend trend) throws Exception {
		long id = trendService.saveTrendMapping(trend);
		return id;
	}

	@Override
	public Long createTrendMapping(TrendMapping trendMapping) {
		logger.info("Inside createTrendMapping method in business delegate");
		try {
			Trend trend = trendService.readTrend(trendMapping.getTrend()
					.getId());
			trendMapping.setTrend(trend);

			Long id = trendService.createTrendMapping(trendMapping);
			logger.info("Exiting createTrendMapping method in business delegate");
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Inside createTrendMapping method in business delegate error setting trend");
		}
		return -1l;
	}

	@Override
	public List<Trend> searchExactTrend(Trend trend) {
		logger.info("Inside displayTrend method in business delegate");
		List<Trend> trendData = trendService.searchExactTrend(trend);
		logger.info("Exiting displayTrend method in business delegate");
		return trendData;
	}

	@Override
	public List<Opinion> searchTrendOpinion(Long id) {
		logger.info("Inside searchTrendOpinion method in business delegate");
		List<Opinion> opns = trendService.searchTrendOpinion(id);
		logger.info("Exiting searchTrendOpinion method in business delegate");
		return opns;
	}


	@Override
	public List<Response> searchTrendResponse(Long id) {
		logger.info("Inside searchTrendResponse method in business delegate");
		List<Response> responses = trendService.searchTrendResponse(id);
		logger.info("Exiting searchTrendResponse method in business delegate");
		return responses;
	}

	@Override
	public Long addToTrendMapping(TrendMapping trendMapping) {
		return null;
	}

	@Override
	public List<TrendOutputModel> matchingTrend(String name) {
		logger.info("Inside matchingTrend method in business delegate");
		List<TrendOutputModel> outputModels = trendService.matchingTrend(name);
		logger.info("Exiting matchingTrend method in business delegate");
		return outputModels;
	}

	@Override
	public List<TrendOutputModel> getTopTrends(int count) {
		logger.info("Inside getTopTrends method in business delegate");
		List<TrendOutputModel> outputModels = trendService.getTopTrends(count,false);
		logger.info("Exiting getTopTrends method in business delegate");
		return outputModels;
	}

	@Override
	public List<UserTrendOutputModel> getTopTrendUsers(int count) {
		logger.info("Inside getTopTrendUsers method in business delegate");
		List<UserTrendOutputModel> outputModels = trendService
				.getTopTrendUsers(count);
		logger.info("Exiting getTopTrendUsers method in business delegate");
		return outputModels;
	}

	@Override
	public Boolean deleteTrendById(Long id) throws Exception {
		logger.info("Inside TrendBusinessDelegate findTrend method");
		Trend trend = trendService.readTrend(id);
		trendService.deleteTrend(trend);
		trend = trendService.readTrend(id);
		logger.info("Exiting TrendBusinessDelegate findTrend method");
		if(trend == null){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public Boolean deleteTrendCommentsById(Long trendMappingId) throws Exception {
		logger.info("Inside TrendBusinessDelegate deleteTrendCommentsById method"); 
		TrendComment comment = trendService.readTrendComments(trendMappingId);
		trendService.deleteTrendComments(comment);
		comment = trendService.readTrendComments(trendMappingId);
		logger.info("Exiting TrendBusinessDelegate deleteTrendCommentsById method");
		if(comment == null){
			return true;
		}else{
			return false;
		}
		
	}
	
	@Override
	public Boolean manageTrend(List<String> trendNames, String action) {
		Boolean status = false;
		for (String trendName : trendNames) {
			Trend trend = new Trend();
			trend.setName(trendName);
			try {
				trend = trendService.findTrendByTrendName(trend);
				if (action.equals(ITrendService.ADD)) {
					trend.setTrending(true);
				} else if (action.equals(ITrendService.REMOVE)) {
					trend.setTrending(false);
				}
				trend = trendService.update(trend);
				if (trend != null)
					status = true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				status = false;
				logger.info("Inside manageTrend method in business delegate error setting trend "
						+ e.getMessage());
			}
		}
		return status;
	}
	@Override
	public List<TrendOutputModel> getTopAdminTrends(int count) {
		logger.info("Inside getTopAdminTrends method in business delegate");
		List<TrendOutputModel> outputModels = trendService.getTopTrends(count,true);
		logger.info("Exiting getTopAdminTrends method in business delegate");
		return outputModels;
	}

	
	

}
