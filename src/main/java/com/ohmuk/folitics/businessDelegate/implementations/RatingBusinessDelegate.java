package com.ohmuk.folitics.businessDelegate.implementations;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.util.CharArrayMap.EntrySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.util.collection.Int2ObjectHashMap.KeySet;
import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IRatingsBusinessDelegate;
import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.enums.RatingType;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.service.rating.IRatingService;

/**
 * @author soumya
 *
 */

@Component
public class RatingBusinessDelegate implements IRatingsBusinessDelegate {

	private static Logger logger = LoggerFactory
			.getLogger(RatingBusinessDelegate.class);

	@SuppressWarnings("rawtypes")
	@Autowired
	private volatile Map<String, IRatingService> ratingServiceMap;

	@Override
	public RatingDataBean rate(RatingDataBean ratingDataBean)
			throws MessageException, Exception {
		logger.info("Entered RatingBusinessDelegate rate method");
		RatingDataBean rating;
		logger.info("+++++++++++++++++++++++++++++");
		@SuppressWarnings("unchecked")
		IRatingService<RatingDataBean> service = ratingServiceMap
				.get(ratingDataBean.getComponentType()
						+ Constants.RATING_SERVICE_SUFFIX);
		logger.info("+++++++++++++++++++++++++++++"+ratingServiceMap);
		logger.info("Entered RatingBusinessDelegate rate method : "+service);
		
		ratingDataBean.setRating(RatingType.getRatingType(
				ratingDataBean.getRating()).getValue());

		rating = service.create(ratingDataBean);

		return rating;
	}

	@Override
	public RatingDataBean unrate(RatingDataBean ratingDataBean)
			throws MessageException, Exception {
		logger.info("Entered RatingBusinessDelegate rate method ");
		RatingDataBean rating;
		@SuppressWarnings("unchecked")
		IRatingService<RatingDataBean> service = ratingServiceMap
				.get(ratingDataBean.getComponentType()
						+ Constants.RATING_SERVICE_SUFFIX);
		ratingDataBean.setRating(RatingType.getRatingType(
				ratingDataBean.getRating()).getValue());

		rating = service.delete(ratingDataBean);

		return rating;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<RatingDataBean> getAllRatings(RatingDataBean ratingDataBean)
			throws MessageException, Exception {

		logger.info("Entered RatingBusinessDelegate getAllRatings method ");
		List<RatingDataBean> ratings = null;
		if (ratingDataBean != null && ratingDataBean.getComponentType() != null) {
			@SuppressWarnings("unchecked")
			IRatingService<RatingDataBean> service = ratingServiceMap
					.get(ratingDataBean.getComponentType()
							+ Constants.RATING_SERVICE_SUFFIX);
			ratings = service.readAll();
		}
		return ratings;
	}

	@Override
	public List<RatingDataBean> getRatingForComponentByUser(
			RatingDataBean ratingDataBean) throws MessageException, Exception {

		logger.info("Entered RatingBusinessDelegate getAllRatings method ");
		List<RatingDataBean> ratingBeans = null;
		if (ratingDataBean != null && ratingDataBean.getComponentType() != null) {
			@SuppressWarnings("unchecked")
			IRatingService<RatingDataBean> service = ratingServiceMap
					.get(ratingDataBean.getComponentType()
							+ Constants.RATING_SERVICE_SUFFIX);
			ratingBeans = service.getByParentIdAndUserId(ratingDataBean);
		}
		return ratingBeans;
	}

	@Override
	public RatingDataBean getRatingAggregation(RatingDataBean ratingDataBean)
			throws MessageException, Exception {

		logger.info("Entered RatingBusinessDelegate getAllRatings method ");
		Map<String,Map<Long,Integer> > ratingAggregation = new HashMap<>();
		Map<Long,Long> feelAggregation = new HashMap<>();
		
		Map<Long,Long> rateTotalMap = new HashMap<>();
		
		if (ratingDataBean != null && ratingDataBean.getComponentType() != null) {
			@SuppressWarnings("unchecked")
			IRatingService<RatingDataBean> service = ratingServiceMap
					.get(ratingDataBean.getComponentType()
							+ Constants.RATING_SERVICE_SUFFIX);
			List<RatingDataBean> ratings = service.getByComponentId(ratingDataBean);
			
			List<RatingDataBean> weekOldRatings = service.getByComponentIdByDate(ratingDataBean);
			
			List<RatingDataBean> positiveRatings = new ArrayList<RatingDataBean>();
			List<RatingDataBean> negativeRatings = new ArrayList<RatingDataBean>();
			Map<Long,List<RatingDataBean>> negativeRateMap = new HashMap<Long,List<RatingDataBean>>();
			Map<Long,List<RatingDataBean>> positiveRateMap = new HashMap<Long,List<RatingDataBean>>();
			
			if(weekOldRatings != null && !weekOldRatings.isEmpty()){
				
				for(RatingDataBean dataBean : ratings){

					if(dataBean.getFeelType().equalsIgnoreCase("negative")){
						negativeRatings.add(dataBean);
					}
					else{
						positiveRatings.add(dataBean);
					}
				}
			
			}
			// prepare feel map by month
			
			if(negativeRatings != null && !negativeRatings.isEmpty()){

				for(RatingDataBean bean : negativeRatings){
					
					 Date date = new Date(bean.getEditTime().getTime());
	                    String day = (new SimpleDateFormat("dd")).format(date);
	                    Calendar cal = Calendar.getInstance();
	                    cal.setTime(date);
	                    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
	                    
	                    if(feelAggregation.isEmpty()){
	                    	feelAggregation.put((long) dayOfWeek, -1l);
	                    }else{
	                    	
	                    	if(feelAggregation.containsKey((long)dayOfWeek)){
	                    		Long count = feelAggregation.get((long)dayOfWeek);  
	                    		feelAggregation.put((long) dayOfWeek, count-1 );
	                    	}else{
	                    		feelAggregation.put((long) dayOfWeek, -1l);
	                    	}
	                    	
	                    }
				}
				
			}
			if(positiveRatings != null && !positiveRatings.isEmpty()){


				for(RatingDataBean bean : positiveRatings){
					

					 Date date = new Date(bean.getEditTime().getTime());
	                    String day = (new SimpleDateFormat("dd")).format(date);
	                    Calendar cal = Calendar.getInstance();
	                    cal.setTime(date);
	                    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
	                   
	                    if(feelAggregation.isEmpty()){
	                    	feelAggregation.put((long) dayOfWeek, 1l);
	                    }else{
	                    	
	                    	if(feelAggregation.containsKey((long) dayOfWeek)){
	                    		Long count = feelAggregation.get((long) dayOfWeek); 
	                    		feelAggregation.put((long) dayOfWeek, count+1 );
	                    	}else{
	                    		feelAggregation.put((long) dayOfWeek, 1l);
	                    	}
	                    	
	                    }
				}
				
			
			}
			 positiveRatings = new ArrayList<RatingDataBean>();
			 negativeRatings = new ArrayList<RatingDataBean>();
			
			if(ratings != null && !ratings.isEmpty()){
							
							for(RatingDataBean dataBean : ratings){
			
								if(dataBean.getFeelType().equalsIgnoreCase("negative")){
									negativeRatings.add(dataBean);
								}
								else{
									positiveRatings.add(dataBean);
								}
							}
						
						}

			
				// negative ratings are from 0 to 4
				if(negativeRatings != null && !negativeRatings.isEmpty()){
					List<RatingDataBean> negativeRateList = new ArrayList<RatingDataBean>();
					for(RatingDataBean bean : negativeRatings){
						
						if(negativeRateMap.isEmpty()){
							negativeRateList.add(bean);
							negativeRateMap.put(bean.getRating(),negativeRateList );
						}else{
							//has key
							if(negativeRateMap.get(bean.getRating()) != null){
								List<RatingDataBean> valueList = negativeRateMap.get(bean.getRating());
								valueList.add(bean);
								negativeRateMap.put(bean.getRating(), valueList);
							}
							else{
								negativeRateList = new ArrayList<RatingDataBean>();
								negativeRateList.add(bean);
								negativeRateMap.put(bean.getRating(),negativeRateList );								
							}
						}
					}
				}
				
				//positive are from 5 to 9
				
				if(positiveRatings != null && !positiveRatings.isEmpty()){
					List<RatingDataBean> positiveRateList = null;
					for(RatingDataBean bean : positiveRatings){						
						if(positiveRateMap.isEmpty()){
							positiveRateList = new ArrayList<RatingDataBean>();
							positiveRateList.add(bean);
							positiveRateMap.put(bean.getRating(),positiveRateList );
						}else{
							//has key
							if(positiveRateMap.get(bean.getRating()) != null){
								List<RatingDataBean> valueList = positiveRateMap.get(bean.getRating());
								valueList.add(bean);
								positiveRateMap.put(bean.getRating(), valueList);
							}
							else{
								positiveRateList = new ArrayList<RatingDataBean>();
								positiveRateList.add(bean);
								positiveRateMap.put(bean.getRating(),positiveRateList );								
							}
						}
					}
				
				}
				
				//perform the aggregation
				
				Map<Long,Integer> negativeAggregate = new HashMap<>();
				Map<Long,Integer> positiveAggregate = new HashMap<>();
				
				
				if(negativeRateMap != null && !negativeRateMap.isEmpty()){					
					
					for(Long key : negativeRateMap.keySet()){
						rateTotalMap.put(key, (long) negativeRateMap.get(key).size());
						negativeAggregate.put(key, (((negativeRateMap.get(key).size()*100)/negativeRatings.size())));
					}
					ratingAggregation.put("negative", negativeAggregate);
				}
				if(positiveRateMap != null && !positiveRateMap.isEmpty()){					
					for(Long key : positiveRateMap.keySet()){
						rateTotalMap.put(key, (long) positiveRateMap.get(key).size());
						positiveAggregate.put(key, (((positiveRateMap.get(key).size()*100)/positiveRatings.size())));
					}
					ratingAggregation.put("positive", positiveAggregate);
				}
				
			
		}
		ratingDataBean.setRateAggregaionMap(ratingAggregation);
		ratingDataBean.setFeelAggregation(feelAggregation);
		ratingDataBean.setRateTotalMap(rateTotalMap);
		return ratingDataBean;
	}


}
