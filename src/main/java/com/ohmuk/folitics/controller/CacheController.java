package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.daily.news.input.DailyNewsInputReader;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.ouput.model.DailyNewsOutPutModal;
import com.ohmuk.folitics.service.ISentimentService;
import com.ohmuk.folitics.util.FileLoadingUtils;

/**
 * @author soumya
 * 
 */
@Controller
@RequestMapping("/cache")
public class CacheController {

	@Autowired
	private DailyNewsInputReader dailyNewsInputReader;

	private static Logger logger = LoggerFactory
			.getLogger(CacheController.class);

	@Autowired
    private volatile  ISentimentService sentimentService;
	
	@RequestMapping(value = "/refreshDailyNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<List<DailyNewsOutPutModal>> refreshDailyNews() {
		logger.info("Entering CacheController refreshDailyNews method");
		try {
			List<DailyNewsOutPutModal> newsData = dailyNewsInputReader
							.getDailyNewsOutpuModal(dailyNewsInputReader.addDailyNewsToCache());
			logger.info("Exit CacheController refreshDailyNews method");
			return new ResponseDto<List<DailyNewsOutPutModal>>(true,
							newsData);
		} catch (Exception exception) {
			logger.error("Error laoding daily News", exception);
			logger.info("Exiting from CacheController refreshDailyNews method");
			return new ResponseDto<List<DailyNewsOutPutModal>>(null);
		}
	}

	@RequestMapping(value = "/refreshSentimentImageUrls", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Map<String, List<String>>> refreshSentimentImageUrls() {
		logger.info("Entering CacheController refreshDailyNews method");
		try {
			Map<String, List<String>> sentimentImaeUrlMap = FileLoadingUtils
					.addSentimentImageUrlsToCache();
			if (sentimentImaeUrlMap != null && !sentimentImaeUrlMap.isEmpty()) {
				return new ResponseDto<Map<String, List<String>>>(true,
						sentimentImaeUrlMap);
			} else {
				logger.info("Sentiment Images do not exist");
				return new ResponseDto<Map<String, List<String>>>(null);
			}
		} catch (Exception exception) {
			logger.error("Error while refreshing Sentiment Images Urls cache",
					exception);
			logger.info("Exiting from Cache Controller refreshSentimentImageUrls method");
			return new ResponseDto<Map<String, List<String>>>(null);
		}
	}
	
	@RequestMapping(value = "/searchImage", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<String>> searchImageUrls(@RequestParam ("searchkeyword") String searchkeyword,@RequestParam ("sentimentId") Long sentimentId) {
        logger.info("Entering CacheController refreshDailyNews method");
        List<String> sentimentImaeUrlMap = new ArrayList<String>();
        try {
            Sentiment sentiment  = sentimentService.read(sentimentId);
            
             sentimentImaeUrlMap = FileLoadingUtils
                    .searchSentimentImage(searchkeyword,sentiment);
            if (sentimentImaeUrlMap != null && !sentimentImaeUrlMap.isEmpty()) {
                return new ResponseDto<List<String>>(true,
                        sentimentImaeUrlMap);
            } else {
                logger.info("Sentiment Images do not exist");
                return new ResponseDto<List<String>>(false,sentimentImaeUrlMap);
            }
        } catch (Exception exception) {
            logger.error("Error while refreshing Sentiment Images Urls cache",
                    exception);
            logger.info("Exiting from Cache Controller refreshSentimentImageUrls method");
            return new ResponseDto<List<String>>(false,sentimentImaeUrlMap);
        }
    }
	
}
