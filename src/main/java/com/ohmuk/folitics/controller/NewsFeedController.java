package com.ohmuk.folitics.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.dto.RSSChannelLink;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.dto.SentimentNewsFeed;
import com.ohmuk.folitics.service.interfaces.NewsFeedService;
import com.ohmuk.folitics.xml.dto.RSS;

@Controller

@RequestMapping("/admin/news")
public class NewsFeedController {
	private static Logger logger = LoggerFactory.getLogger(NewsFeedController.class);

	@Autowired
	private NewsFeedService newsFeedService;

	@RequestMapping(value = "/rss", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<RSS> getNews(@RequestParam(value = "rssLink", required = true) String rssLink) {

		logger.info("Inside getNews method");
		RSS rss = null;
		try {
			rss = newsFeedService.getNewsByRSSLink(rssLink);
		} catch (Exception exception) {
			logger.error("Exception: " + exception);
			return new ResponseDto<RSS>(false);
		}
		if (rss != null) {
			logger.debug("getNews Success");
			return new ResponseDto<RSS>(true, rss);
		}
		logger.debug("getNews Un-Successfully");
		return new ResponseDto<RSS>(false);
	}

	@RequestMapping(value = "/preLoadNewsData", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<RSSChannelLink>> loadChannelLinks() {
		logger.info("Inside loadChannelLinks method");
		List<RSSChannelLink> channelList = null;
		try {
			channelList = newsFeedService.loadChannelLinks();
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (channelList != null) {
			logger.debug("loadChannelLinks list Success");
			return new ResponseDto<List<RSSChannelLink>>(true, channelList);
		}
		return new ResponseDto<List<RSSChannelLink>>(false, channelList);
	}

	@RequestMapping(value = "/addSentimentNews", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> addSentimentNews(@RequestBody SentimentNewsFeed sentimentNewsFeed) {
		logger.info("Inside addNewsFeed method");
		Boolean successMessage = null;
		try {
			logger.info("Inside addSentimentNews" + sentimentNewsFeed);
			successMessage = newsFeedService.addSentimentNews(sentimentNewsFeed.getSentimentNews());
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (successMessage != null && successMessage == true) {
			logger.info("addSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}

	@RequestMapping(value = "/updatedSentimentNews", method = RequestMethod.PUT)
	public @ResponseBody ResponseDto<Boolean> updatedSentimentNews(@RequestBody SentimentNewsFeed sentimentNewsFeed) {
		logger.info("Inside updatedSentimentNews method");
		Boolean successMessage = null;
		try {
			logger.info("Inside updatedSentimentNews" + sentimentNewsFeed);
			successMessage = newsFeedService.updatedSentimentNews(sentimentNewsFeed.getSentimentNews());
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}

		if (successMessage != null && successMessage == true) {
			logger.info("updatedSentimentNews Success");
			return new ResponseDto<Boolean>(true, successMessage);
		}
		return new ResponseDto<Boolean>(false, successMessage);
	}
}
