package com.ohmuk.folitics.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.daily.news.input.DailyNewsInputPutModal;
import com.ohmuk.folitics.daily.news.input.DailyNewsInputReader;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.ouput.model.DailyNewsOutPutModal;

/**
 * @author Soumya
 * 
 */
@Controller
@RequestMapping("/dailyNews")
public class DailyNewsController {

	@Autowired
	DailyNewsInputReader dailyNewsInputReader;

	private static Logger logger = LoggerFactory.getLogger(DailyNewsController.class);

	@RequestMapping
	public String getDailyNewsPage() {

		return "DailyNews-page";
	}

	/**
	 * The file should be under /testdata/DailyNews *
	 * 
	 * @param fileName
	 * @return
	 */
	@RequestMapping(value = "/getDailyNews", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<DailyNewsOutPutModal>> getDailyNews() {
		logger.info("Inside DailyNewsController loadNews method");
		try {
			
			List<DailyNewsInputPutModal> dailyNewsInputPutModals = (List<DailyNewsInputPutModal>) SingletonCache.getInstance().get(FoliticsCache.KEYS.DAILY_NEWS);
			List<DailyNewsOutPutModal> newsData = dailyNewsInputReader.getDailyNewsOutpuModal(dailyNewsInputPutModals);
			if (newsData == null) {
				logger.error("Daily news is null");
				return new ResponseDto<List<DailyNewsOutPutModal>>(null);
			}
			return new ResponseDto<List<DailyNewsOutPutModal>>(true, newsData);
		} catch (Exception exception) {
			logger.error("Error laoding daily News", exception);
			logger.info("Exiting from DailyNewsController loadNews method");
			return new ResponseDto<List<DailyNewsOutPutModal>>(null);
		}
	}
}
