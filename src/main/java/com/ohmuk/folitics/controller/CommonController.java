/**
 * 
 */
package com.ohmuk.folitics.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.ICommonBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.Opinion;

/**
 * @author Kalpana
 *
 */
@Controller
@RequestMapping("/common")
public class CommonController {
	
	 final static Logger logger = LoggerFactory.getLogger(OpinionController.class);
	 
	 @Autowired
	 private ICommonBusinessDelegate  businessDelegate;
	//incrementViewCount(componentId, componentType)
	 
	 @RequestMapping(value = "/incrementViewCount", method = RequestMethod.POST)
     public @ResponseBody ResponseDto<Opinion> incrementViewCount(@RequestParam Long opinionId) {
		try {
			logger.info("incrementViewCount +===========  ");
			boolean status = businessDelegate.incrementView(opinionId);
			if (status == true) {
				return new ResponseDto<Opinion>(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseDto<Opinion>(false);
    }
}
