
package com.ohmuk.folitics.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.ouput.model.UserOutputModel;

/**
 * 
 * @author Soumya
 *
 */
@Controller
@RequestMapping("/tag")
public class TagController {
	@Autowired
	private volatile IUserBusinessDelegate userBusinessDelegate;
	@Autowired 
	private volatile ISentimentBusinessDelegate sentimentBusinessDelegate;

	private static Logger logger = LoggerFactory
			.getLogger(TagController.class);

	/**
	 * Web service is to for tagging Users
	 * 
	 * @param id
	 * @return ResponseDto<User>
	 */
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<UserOutputModel>> tagUser(
			@RequestParam(value = "match", required = true) String match) {

		logger.info("Inside TagController tagUser method");
		List<UserOutputModel> users = null;
		try {
			 users = userBusinessDelegate.getUserByMatch(match);
		} catch (Exception exception) {
			logger.error("Exception in tagUser with userName : "
					+ match);
			logger.error("Exception: " + exception);
			logger.info("Exiting from TagController tagUser method");
			return new ResponseDto<List<UserOutputModel>>(false);
		}
		if (null != users) {
			logger.debug("User tagUser Success");
			logger.info("Exiting from TagController tagUser method");
			return new ResponseDto<List<UserOutputModel>>(true,users);
		}
		logger.debug("User tagUser Un-Success");
		logger.info("Exiting from TagController tagUser method");
		return new ResponseDto<List<UserOutputModel>>(false);
	}
	
	/**
	 * Web service is to for tagging sentiments
	 * 
	 * @param id
	 * @return ResponseDto<User>
	 */
	@RequestMapping(value = "/sentiment", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModel> tagSentiment(
			@RequestParam(value = "match", required = true) String match) {

		logger.info("Inside TagController tagSentiment method");
		List<SentimentOutputModel> sentiments = null;
		try {
			sentiments = sentimentBusinessDelegate.getAllSentimentByMatch(match);
		} catch (Exception exception) {
			logger.error("Exception in tagSentiment with title : "
					+ match);
			logger.error("Exception: " + exception);
			logger.info("Exiting from TagController tagSentiment method");
			return new ResponseDto<SentimentOutputModel>(false);
		}
		if (null != sentiments) {
			logger.debug("User tagSentiment Success");
			logger.info("Exiting from TagController tagSentiment method");
			return new ResponseDto<SentimentOutputModel>(true,sentiments);
		}
		logger.debug("Sentiment tagSentiment Un-Success");
		logger.info("Exiting from TagController tagSentiment method");
		return new ResponseDto<SentimentOutputModel>(false);
	}
	

	
}
