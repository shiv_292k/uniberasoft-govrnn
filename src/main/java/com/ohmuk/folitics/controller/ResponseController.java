package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.ohmuk.folitics.beans.MaxVoteResponseBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IResponseBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.ResponseType;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.ouput.model.OpinionFeelModel;
import com.ohmuk.folitics.ouput.model.OpinionOutputModel;
import com.ohmuk.folitics.ouput.model.PollOptionsCount;
import com.ohmuk.folitics.ouput.model.ResponseOutputModel;
import com.ohmuk.folitics.service.IResponseService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FoliticsUtils;
import com.ohmuk.folitics.util.PollUtil;

/**
 * @author Abhishek
 *
 */
@Controller
@RequestMapping("/response")
public class ResponseController {

	@Autowired
	private volatile IResponseBusinessDelegate businessDelegate;
	
	@Autowired
	private IResponseService responseService;

	private static Logger logger = LoggerFactory.getLogger(ResponseController.class);

	private static final String AGREE_RESPONSE = "Agree";
	private static final String DISAGREE_RESPONSE = "Disagree";
	private static final String UP_RESPONSE = "Up";
	private static final String DOWN_RESPONSE = "Down";
	
	@RequestMapping
	public String getResponsePage() {
		return "response-page";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> getAdd() {
		List<Response> responses = new ArrayList<>();
		responses.add(getTestResponse());
		return new ResponseDto<Response>(true, responses);
	}

	/**
	 * Web service is to add {@link Response}
	 * 
	 * @param response
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<ResponseOutputModel> add(@RequestBody Response response ) {
		logger.info("Inside ResponseController add method ::");
		Long responseId = null;
		try {
            if (response.getId() != null) {
                responseId = response.getId();
            }

		    response.setEdited(DateUtils.getSqlTimeStamp());
			response.setCreateTime(DateUtils.getSqlTimeStamp());
			response.setUpDownVote(PollUtil.createResponsePoll(response.getUser()));
            List<Response> responses = businessDelegate.getByOpinionAndUser(response.getOpinion().getId(), response
                    .getUser().getId());
			if(responses != null && responses.size() < IResponseService.ResponseLimit){
                response = businessDelegate.create(response, responseId);
            }else{
                return new ResponseDto<ResponseOutputModel>("You reached the maximum limit of Create Response on this Opinion",
                        false);
           }
			
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in adding response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController add method");
			return new ResponseDto<ResponseOutputModel>(false);
		}
		if (response != null) {
			logger.debug("Response is save");
			logger.info("Exiting from ResponseController add method");
			ResponseOutputModel rom = ResponseOutputModel.getModel(response);
			OpinionFeelModel positiveFeelResponse = responseService.calculatePositiveFeel(responseId);
			rom.setPositiveFeel(positiveFeelResponse.getPositiveFeel());
			rom.setPositiveFeelCount(positiveFeelResponse.getPositiveFeelCount());
			OpinionFeelModel negativeFeelResponse = responseService.calculateNegativeFeel(responseId);
			rom.setNegativeFeel(negativeFeelResponse.getNegativeFeel());
			rom.setNegativeFeelCount(negativeFeelResponse.getNegativeFeelCount());
			
			return new ResponseDto<ResponseOutputModel>(true, rom);
		}
		logger.debug("Response is not saved");
		logger.info("Exiting from ResponseController add method");
		return new ResponseDto<ResponseOutputModel>(false);
	}

	/**
	 * Web service is to update {@link Response}
	 * 
	 * @param response
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Response> edit(@RequestBody Response response) {
		logger.info("Inside ResponseController edit method");
		try {
			response = businessDelegate.update(response);
		} catch (Exception exception) {
			logger.error("Exception in updating response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController edit method");
			return new ResponseDto<Response>(false);
		}
		if (response != null) {
			logger.debug("Response with id: " + response.getId() + " is updated");
			logger.info("Exiting from ResponseController edit method");
			return new ResponseDto<Response>(true, response);
		}
		logger.debug("Response is not update");
		logger.info("Exiting from ResponseController edit method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web service is to get response{@link Response} by opinionId
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/getByRId", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> getByOpinionId(Long id) {
		logger.info("Inside ResponseController getByOpinionId method");
		try {
			List<Response> responses = businessDelegate.getByOpinionId(id);
			if (responses != null) {
				logger.debug("Response with id: " + id + " has been found");
				logger.info("Exiting from ResponseController getByOpinionId method");
				return new ResponseDto<Response>(true,responses,"Successfully fetched the records");
			}
		} catch (Exception exception) {
			logger.error("Exception while fetching response by opinionId");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getByOpinionId method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response with id: " + id + " has not found");
		logger.info("Exiting from ResponseController getByOpinionId method");
		return new ResponseDto<Response>(false);
	}
	
	/**
	 * Web service is to get response{@link Response} by userId
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/getByUserId", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> getByUserId(Long id) {
		logger.info("Inside ResponseController getByTaskId method");
		try {
			List<Response> responses = businessDelegate.getByUserId(id);
			if (responses != null) {
				logger.debug("Response with id: " + id + " has been found");
				logger.info("Exiting from ResponseController getByUserId method");
				return new ResponseDto<Response>(true,responses,"Successfully fetched the records");
			}
		} catch (Exception exception) {
			logger.error("Exception while fetching response by userId");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getByUserId method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response with id: " + id + " has not found");
		logger.info("Exiting from ResponseController getByUserId method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web service is to soft delete {@link Response} by id
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/deleteById", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Response> deleteById(Long id) {
		logger.info("Inside ResponseController add method");
		try {
			if (businessDelegate.delete(id)) {
				logger.debug("Response with id: " + id + " is soft deleted");
				logger.info("Exiting from ResponseController deleteById method");
				return new ResponseDto<Response>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in soft delete response by id");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController deleteById method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response with id: " + id + " is not delete");
		logger.info("Exiting from ResponseController deleteById method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web service is to soft deletd {@link Response}
	 * 
	 * @param response
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Response> delete(@RequestBody Response response) {
		logger.info("Inside ResponseController add method");
		try {
			if (businessDelegate.delete(response)) {
				logger.debug("Response with id: " + response.getId() + " is deleted");
				logger.info("Exiting from ResponseController delete method");
				return new ResponseDto<Response>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in soft response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController delete method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response is not delete");
		logger.info("Exiting from ResponseController delete method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web service is to hard delete {@link Response} by id
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/deleteFromDBByid", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> deleteFromDB(Long id) {
		logger.info("Inside ResponseController add method");
		try {
			if (businessDelegate.deleteFromDBById(id)) {
				logger.debug("Response with id: " + id + " is hard deleted");
				logger.info("Exiting from ResponseController deleteFromDB method");
				return new ResponseDto<Response>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in hard delete response by id");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController deleteFromDB method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response is not delete");
		logger.info("Exiting from ResponseController deleteFromDB method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web businessDelegate is to hard delete {@link Response}
	 * 
	 * @param response
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/deleteFromDB", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Response> deleteFromDB(@RequestBody Response response) {
		logger.info("Inside ResponseController add method");
		try {
			if (businessDelegate.deleteFromDB(response)) {
				logger.debug("Response with id: " + response.getId() + " is hard deleted");
				logger.info("Exiting from ResponseController deleteFromDB method");
				return new ResponseDto<Response>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in hard delete response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController deleteFromDB method");
			return new ResponseDto<Response>(false);
		}
		logger.debug("Response is not delete");
		logger.info("Exiting from ResponseController deleteFromDB method");
		return new ResponseDto<Response>(false);
	}

	/**
	 * Web service is to get all {@link Response}
	 * 
	 * @return {@link Response}
	 */
	//@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<Response>> getAll() {
		logger.info("Inside ResponseController getAll method");
		List<Response> responses = null;
		try {
			responses = businessDelegate.readAll();
		} catch (Exception exception) {
			logger.error("Exception in getting all response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getAll method");
			return new ResponseDto<List<Response>>(false);
		}
		if (responses != null) {
			logger.debug(responses.size() + " response is found");
			logger.info("Exiting from ResponseController getAll method");
			return new ResponseDto<List<Response>>(true, responses);
		}
		logger.debug("No response is found");
		logger.info("Exiting from ResponseController getAll method");
		return new ResponseDto<List<Response>>(false);
	}

	/**
	 * Web service is to get {@link Response} by id
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> find(Long id) {
		logger.info("Inside ResponseController find method");
		Response response = null;
		try {
			response = businessDelegate.getResponseById(id);
		} catch (Exception exception) {
			logger.error("Exception in finding response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController find method");
			return new ResponseDto<Response>(false);
		}
		if (response != null) {
			logger.debug("Response is found with id: " + id);
			logger.info("Exiting from ResponseController find method");
			return new ResponseDto<Response>(true, response);
		}
		logger.debug("Response with id: " + id + " is not found");
		logger.info("Exiting from ResponseController find method");
		return new ResponseDto<Response>(false);
	}

	@RequestMapping(value = "/getTestResponse", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Response getTestResponse() {
		return getDummyResponse();

	}
	
	@RequestMapping(value = "/getResponsesForOpinion", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Response> getResponsesForOpinion( Long id) {
		logger.info("Inside SentimentController getOpinions method");
		List<Response> responses = null;
		try {
			responses = businessDelegate.getResponseForOpinion(id);
		} catch (Exception exception) {
			logger.error("Exception in get Responses for Opinion");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getResponsesForOpinion method");

			return new ResponseDto<Response>(false);
		}
		if (responses != null) {
			logger.debug(responses.size() + " opinions exsits");
			logger.info("Exiting from ResponseController getResponsesForOpinion method");
			return new ResponseDto<Response>(true, responses);
		}
		logger.debug("No sentiment is found");
		logger.info("Exiting from ResponseController getResponsesForOpinion method");
		return new ResponseDto<Response>(false);
	}
	
	private Response getDummyResponse() {
		Response response = new Response();
		// response.setId((new Random()).nextLong());
		// response.setSubject("Response Subject");
		// response.setText("Response Text");
		response.setFlag(ResponseType.AGREE.getValue());
		return response;
	}
	
	@RequestMapping(value = "/getAllResponseById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<ResponseOutputModel> getAllResponseById(long opinionId) {
		logger.info("Inside ResponseController getAllResponseById method");
		List<ResponseOutputModel> responseModels = null;
		try {
			responseModels = businessDelegate.getAllResponseById(opinionId);
		} catch (Exception exception) {
			logger.error("Exception in updating Response");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getAllResponseById method");

			return new ResponseDto<ResponseOutputModel>(false);
		}
		if (responseModels != null) {
			logger.debug(responseModels.size() + " response is found");
			logger.info("Exiting from responseController getAllResponseById method");
			return new ResponseDto<ResponseOutputModel>(true, responseModels);
		}
		logger.debug("No response is found");
		logger.info("Exiting from responseController getAllResponseById method");
		return new ResponseDto<ResponseOutputModel>(false);
	}

    @RequestMapping(value = "/updateResponseText", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<ResponseOutputModel> updateResponseText(@RequestBody Response editResponse) {
        logger.info("Inside ResponseController updateResponseText method");
        Response response;
        ResponseOutputModel responseModel = null;
        try {
        	response = businessDelegate.getResponseById(editResponse.getId());
        	if(response != null){
        		response.setContent(editResponse.getContent());
        		response = businessDelegate.update(response);
        		responseModel = ResponseOutputModel.getModel(response);
        		logger.info("Response content updated to "+ editResponse.getContent() + "for response id : "+ editResponse.getId());
        		return new ResponseDto<ResponseOutputModel>(true,responseModel);
        	}        	
        } catch (Exception exception) {
            logger.error("Exception in updating Response");
            logger.error("Exception: " + exception);
            logger.info("Exiting from ResponseController updateResponseText method");
            return new ResponseDto<ResponseOutputModel>(false);
        }
        logger.debug("No response is found");
        logger.info("Exiting from responseController updateResponseText method");
        return new ResponseDto<ResponseOutputModel>(false);
    }

    /**
	 * Web service is to get responses count {@link Response} by userId
	 * 
	 * @param id
	 * @return {@link Response}
	 */
	@RequestMapping(value = "/getCountByUserId", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Integer> getCountByUserId(Long id) {
		logger.info("Inside ResponseController getCountByUserId method");
		Integer count = null;
		try {
			count = businessDelegate.getCountByUserId(id);
			
			if(count != null){
				return new ResponseDto<Integer>(true,count);
			}
			
		} catch (Exception exception) {
			logger.error("Exception while fetching response by userId");
			logger.error("Exception: " + exception);
			logger.info("Exiting from ResponseController getCountByUserId method");
			return new ResponseDto<Integer>(false);
		}
		logger.debug("Response with id: " + id + " has not found");
		logger.info("Exiting from ResponseController getCountByUserId method");
		return new ResponseDto<Integer>(false,count);
	}
	
	
	@RequestMapping(value = "/allMaxUpvotedResponseById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<ResponseOutputModel>> getAllResponseOrderByMaxUpvoted(long opinionId) {
		logger.info("Inside ResponseController getAllResponseOrderByMaxUpvoted method");
		List<ResponseOutputModel> responseModels = null;
		Map<Long, ResponseOutputModel> agreeResponsesMap = new HashMap<>();
		Map<Long, ResponseOutputModel> disAgreeResponsesMap = new HashMap<>();
		List<MaxVoteResponseBean> agreeUpvoteResponsesList = new ArrayList<>();
		List<MaxVoteResponseBean> disAgreeResponsesList = new ArrayList<>();
		List<ResponseOutputModel> orderedUpvotedResponseList = new ArrayList<>();
		try {
			responseModels = businessDelegate.getAllResponseById(opinionId);
			if (null == responseModels || responseModels.isEmpty()) {
				logger.debug(responseModels.size() + " No record found");
				logger.info("Exiting from responseController getAllResponseOrderByMaxUpvoted method");
				return new ResponseDto<List<ResponseOutputModel>>(" No Record Found ", false);
			}
			
			if(null != responseModels && !responseModels.isEmpty()){
				for(ResponseOutputModel model : responseModels){
					if(AGREE_RESPONSE.equalsIgnoreCase(model.getFlag())){
						List<PollOptionsCount> pollOptionsCounts = model.getPollOptionsCounts();
						if(null != pollOptionsCounts && !pollOptionsCounts.isEmpty()){
							for(PollOptionsCount pollOption : pollOptionsCounts){
								if(UP_RESPONSE.equalsIgnoreCase(pollOption.getPollOption())){
									agreeResponsesMap.put(model.getId(), model);
									MaxVoteResponseBean agreeUpvoteBean = new MaxVoteResponseBean();
									agreeUpvoteBean.setResponseId(model.getId());
									agreeUpvoteBean.setUpVoteResponseCount(pollOption.getCount());
									agreeUpvoteResponsesList.add(agreeUpvoteBean);
								}
							}
						}
					}
					
					if(DISAGREE_RESPONSE.equalsIgnoreCase(model.getFlag())){
						List<PollOptionsCount> pollOptionsCounts = model.getPollOptionsCounts();
						if(null != pollOptionsCounts && !pollOptionsCounts.isEmpty()){
							for(PollOptionsCount pollOption : pollOptionsCounts){
								if(UP_RESPONSE.equalsIgnoreCase(pollOption.getPollOption())){
									disAgreeResponsesMap.put(model.getId(), model);
									MaxVoteResponseBean disAgreeUpvoteBean = new MaxVoteResponseBean();
									disAgreeUpvoteBean.setResponseId(model.getId());
									disAgreeUpvoteBean.setUpVoteResponseCount(pollOption.getCount());
									disAgreeResponsesList.add(disAgreeUpvoteBean);
								}
							}
						}
					}
				}
				
				Collections.sort(agreeUpvoteResponsesList);
				for(MaxVoteResponseBean orderBean : agreeUpvoteResponsesList){
					orderedUpvotedResponseList.add(agreeResponsesMap.get(orderBean.getResponseId()));
				}
				Collections.sort(disAgreeResponsesList);
				for(MaxVoteResponseBean orderBean : disAgreeResponsesList){
					orderedUpvotedResponseList.add(disAgreeResponsesMap.get(orderBean.getResponseId()));
				}
			}
		} catch (Exception exception) {
			logger.error("Exception Occurs" + exception);
			exception.printStackTrace();
			return new ResponseDto<List<ResponseOutputModel>>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR,false);
		}
		
		logger.info("Exiting from responseController getAllResponseOrderByMaxUpvoted method");
		return new ResponseDto<List<ResponseOutputModel>>(true, orderedUpvotedResponseList);
	}
	
	
	@RequestMapping(value = "/allMaxDownVotedResponseById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<ResponseOutputModel>> getAllResponseMaxDownUpvotedOrder(long opinionId) {
		logger.info("Inside ResponseController getAllResponseMaxDownUpvotedOrder method");
		List<ResponseOutputModel> responseModels = null;
		Map<Long, ResponseOutputModel> agreeResponsesMap = new HashMap<>();
		Map<Long, ResponseOutputModel> disAgreeResponsesMap = new HashMap<>();
		List<MaxVoteResponseBean> agreeDownVoteResponsesList = new ArrayList<>();
		List<MaxVoteResponseBean> disAgreeDownVoteResponsesList = new ArrayList<>();
		List<ResponseOutputModel> orderedDownVotedResponseList = new ArrayList<>();
		try {
			responseModels = businessDelegate.getAllResponseById(opinionId);
			if (null == responseModels || responseModels.isEmpty()) {
				logger.debug(responseModels.size() + " no record found");
				logger.info("Exiting from responseController getAllResponseMaxDownUpvotedOrder method");
				return new ResponseDto<List<ResponseOutputModel>>(" No Record Found ", false);
			}
			
			if(null != responseModels && !responseModels.isEmpty()){
				for(ResponseOutputModel model : responseModels){
					if(AGREE_RESPONSE.equalsIgnoreCase(model.getFlag())){
						List<PollOptionsCount> pollOptionsCounts = model.getPollOptionsCounts();
						if(null != pollOptionsCounts && !pollOptionsCounts.isEmpty()){
							for(PollOptionsCount pollOption : pollOptionsCounts){
								if(DOWN_RESPONSE.equalsIgnoreCase(pollOption.getPollOption())){
									agreeResponsesMap.put(model.getId(), model);
									MaxVoteResponseBean agreeUpvoteBean = new MaxVoteResponseBean();
									agreeUpvoteBean.setResponseId(model.getId());
									agreeUpvoteBean.setUpVoteResponseCount(pollOption.getCount());
									agreeDownVoteResponsesList.add(agreeUpvoteBean);
								}
							}
						}
					}
					
					if(DISAGREE_RESPONSE.equalsIgnoreCase(model.getFlag())){
						List<PollOptionsCount> pollOptionsCounts = model.getPollOptionsCounts();
						if(null != pollOptionsCounts && !pollOptionsCounts.isEmpty()){
							for(PollOptionsCount pollOption : pollOptionsCounts){
								if(DOWN_RESPONSE.equalsIgnoreCase(pollOption.getPollOption())){
									disAgreeResponsesMap.put(model.getId(), model);
									MaxVoteResponseBean disAgreeUpvoteBean = new MaxVoteResponseBean();
									disAgreeUpvoteBean.setResponseId(model.getId());
									disAgreeUpvoteBean.setUpVoteResponseCount(pollOption.getCount());
									disAgreeDownVoteResponsesList.add(disAgreeUpvoteBean);
								}
							}
						}
					}
				}
				
				Collections.sort(agreeDownVoteResponsesList);
				for(MaxVoteResponseBean orderBean : agreeDownVoteResponsesList){
					orderedDownVotedResponseList.add(agreeResponsesMap.get(orderBean.getResponseId()));
				}
				Collections.sort(disAgreeDownVoteResponsesList);
				for(MaxVoteResponseBean orderBean : disAgreeDownVoteResponsesList){
					orderedDownVotedResponseList.add(disAgreeResponsesMap.get(orderBean.getResponseId()));
				}
			}
		} catch (Exception exception) {
			logger.error("Exception occurs "+ exception);
			exception.printStackTrace();
			return new ResponseDto<List<ResponseOutputModel>>(FoliticsUtils.SOMETHING_WENT_WRONG_ERROR,false);
		}
		logger.info("Exiting from responseController getAllResponseMaxDownUpvotedOrder method");
		return new ResponseDto<List<ResponseOutputModel>>(true, orderedDownVotedResponseList);
	}
}
