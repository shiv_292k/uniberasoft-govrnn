package com.ohmuk.folitics.controller;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.implementations.SearchOutputModel;
import com.ohmuk.folitics.businessDelegate.interfaces.ISearchBusinessDeligate;
import com.ohmuk.folitics.constants.Constants;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.elasticsearch.ElasticSearchClient;

@Controller
@RequestMapping("/search")
public class SearchController {

    static final Logger logger = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    private volatile ISearchBusinessDeligate searchBusinessDeligate;

    @Autowired
    private ElasticSearchClient client;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public @ResponseBody Map<Integer, Object> searchDocument(String index, String type, String text)
            throws UnknownHostException {

        int i = 0;

        logger.info("Inside search controller");

        SearchResponse response = searchBusinessDeligate.searchDocument(index, type, text);

        SearchHit[] results = response.getHits().getHits();

        logger.info("Exiting search controller");
        System.out.println("result" + response.getHits().getHits());
        Map<Integer, Object> result = new HashMap<Integer, Object>();

        for (SearchHit hit : results) {
            System.out.println(hit.getSource());
            logger.info(hit.getId());// prints out the id of the document

            i += 1;
            result.put(i, hit.getSource()); // the retrieved document
        }
        return result;
    }

    @RequestMapping(value = "/searchresult", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<SearchOutputModel> searchresult(String searchKeyword, String componentType,
            @RequestParam(value = "userId", required = false) Long userId, HttpServletRequest request) throws Exception {
        if (StringUtils.isEmpty(componentType))
            componentType = Constants.ALL;
        logger.info("Inside SearchController searchresult method : componentType : ");

        List<SearchOutputModel> searchOutputModels;
        searchOutputModels = searchBusinessDeligate.searchByComponent(searchKeyword, componentType, userId,request);
        logger.info("Exiting SearchController searchresult method +++++");
        return new ResponseDto<SearchOutputModel>(true, searchOutputModels);
    }

}
