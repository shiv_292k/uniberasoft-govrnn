package com.ohmuk.folitics.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.IAttachmentBusinessDeligate;
import com.ohmuk.folitics.businessDelegate.interfaces.IOpinionBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IPollBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.ISentimentBusinessDelegate;
import com.ohmuk.folitics.dto.LinkDto;
import com.ohmuk.folitics.dto.OptionsDto;
import com.ohmuk.folitics.dto.PollDto;
import com.ohmuk.folitics.dto.PollLinkDto;
import com.ohmuk.folitics.dto.RelatedSentimentsDto;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.dto.SentimentIdDto;
import com.ohmuk.folitics.enums.AttachmentType;
import com.ohmuk.folitics.enums.CategoryType;
import com.ohmuk.folitics.enums.SentimentState;
import com.ohmuk.folitics.hibernate.entity.Category;
import com.ohmuk.folitics.hibernate.entity.Link;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Sentiment;
import com.ohmuk.folitics.hibernate.entity.attachment.Attachment;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.ouput.model.SentimentOutputModel;
import com.ohmuk.folitics.ouput.model.SentimentOutputModelFull;
import com.ohmuk.folitics.sentiment.input.AdminSentimentInputModel;
import com.ohmuk.folitics.sentiment.input.ISentimentInputReader;
import com.ohmuk.folitics.service.newsfeed.ISentimentNewsLoaderService;
import com.ohmuk.folitics.util.DateUtils;
import com.ohmuk.folitics.util.FileLoadingUtils;

/**
 * @author HariJi
 * 
 */
@Controller
@RequestMapping("/admin/sentiment")
public class AdminSentimentController {

	private static Logger LOGGER = LoggerFactory.getLogger(AdminSentimentController.class);

	@Autowired
	private volatile ISentimentBusinessDelegate sentimentBusinessDelegate;

	@Autowired
	private volatile IPollBusinessDelegate pollBusinessDelegate;

	@Autowired
	private IOpinionBusinessDelegate opinionBusinessDelegate;

	@Autowired
	private ISentimentNewsLoaderService sentimentNewsLoaderService;

	@Autowired
	ISentimentInputReader sentimentInputReader;

	@Autowired
	private IAttachmentBusinessDeligate attachmentBusinessDeligate;

	@Autowired
	private SentimentIdDto sentimentIdDto;

	@RequestMapping
	public String getSentimentPage() {

		return "sentiment-page";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> getAdd() {

		List<Sentiment> sentiments = new ArrayList<>();
		return new ResponseDto<Sentiment>(true, sentiments);
	}

	/**
	 * This method is used to add sentiment. This is a multipart spring web
	 * service that requires image and json for sentiment.
	 * 
	 * @author HariJi
	 * @param image
	 * @param sentiment
	 * @return ResponseDto<Sentiment>
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Sentiment> add(@RequestBody AdminSentimentInputModel sentimentInput) {

		LOGGER.info("Inside AdminSentimentController add method");
		// HttpSession session = request.getSession(false);

		try {
			byte[] imageInBytes = Base64.decodeBase64(sentimentInput.getImageByte());
			Sentiment sentiment = new Sentiment();
			sentiment.setSubject(sentimentInput.getSubject());
			sentiment.setDescription(sentimentInput.getDescription());
			sentiment.setType(sentimentInput.getType());
			sentiment.setKeywords(sentimentInput.getKeywords());
			LOGGER.debug("Sentiment Categories: " + sentimentInput.getCategories());
			sentiment.setStartTime(DateUtils.getSqlTimeStamp());
			sentiment.setEndTime(DateUtils.addDays(3, DateUtils.getSqlTimeStamp()));
			sentiment.setId(null);
			sentiment.setState(SentimentState.ALIVE.getValue());
			sentiment.setCreated_By(1L);
			sentiment.setImage(imageInBytes);
			String str[] = sentimentInput.getImageType().trim().split("/");
			LOGGER.debug("Image Type: " + str[1]);
			sentiment.setImageType(str[1]);

			StringBuilder sb = new StringBuilder();
			sb.append(FileLoadingUtils.BASE_IMAGE_PATH_SENTIMENT_LIST);
			sb.append(File.separator);
			sb.append(sentiment.getSubject().replaceAll(" ", ""));
			sb.append(".");
			sb.append(str[1]);
			String fullFilePath = FileLoadingUtils.BASE_IMAGE_PATH + sb.toString();
			FileOutputStream fos = new FileOutputStream(fullFilePath);
			fos.write(imageInBytes);
			fos.close();
			LOGGER.debug("File write at the location: " + fullFilePath);

			Set<Category> categories = new HashSet<Category>();

			LOGGER.debug("The Categories are : " + sentimentInput.getCategories());

			if (sentimentInput.getCategories() != null) {
				Iterator<Category> itCat = sentimentInput.getCategories().iterator();
				while (itCat.hasNext()) {
					Category cat = itCat.next();
					cat.setId(null);
					cat.setCreatedBy(sentiment.getCreated_By());
					cat.setDescription(cat.getName());
					cat.setType(CategoryType.CATEGORY.getValue());
					categories.add(cat);
				}
			} else {
				Category category = new Category();
				category.setCreatedBy(sentiment.getCreated_By());
				category.setDescription(sentiment.getSubject());
				category.setType(CategoryType.CATEGORY.getValue());
				category.setName(sentiment.getSubject());
				categories.add(category);
			}
			sentiment.setCategories(categories);
			
			ArrayList<RelatedSentimentsDto> relatedSentimentsDtos = sentimentInput.getRelatedSentimentsId();
			HashSet<Sentiment> sentimentSet = new HashSet<Sentiment>();
			Iterator<RelatedSentimentsDto> it = relatedSentimentsDtos.iterator();
			Sentiment tempSenti = null;
			while (it.hasNext()) {
				RelatedSentimentsDto relatedSentimentsDto = it.next();
				tempSenti = new Sentiment();
				tempSenti.setId(relatedSentimentsDto.getId());
				sentimentSet.add(tempSenti);
			}
			sentiment.setRelatedSentiments(sentimentSet);
			sentiment = sentimentBusinessDelegate.save(sentiment);
			sentimentIdDto.setSentiment(sentiment);
			return new ResponseDto<Sentiment>(true, sentiment);

		} catch (Exception exception) {
			LOGGER.error("Error saving sentiment", exception);
			LOGGER.info("Exiting from AdminSentimentController add method");
			return new ResponseDto<Sentiment>(false);
		}
	}

	@RequestMapping(value = "/getAllSentiments", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModel> getAll(@RequestParam("sentimentType") String sentimentType) {
		LOGGER.info("Inside AdminSentimentController getAll method");
		List<SentimentOutputModel> sentimentOutputModels = null;
		try {
			sentimentOutputModels = sentimentBusinessDelegate.getAllSentimentByType(sentimentType);
		} catch (Exception exception) {
			LOGGER.error("Exception in getAll AdminSentiment Controller");
			LOGGER.info("Exiting from AdminSentimentController getAll method");

			return new ResponseDto<SentimentOutputModel>(false);
		}
		if (sentimentOutputModels != null) {
			LOGGER.debug(sentimentOutputModels.size() + " sentiment is found");
			LOGGER.info("Exiting from AdminSentimentController getAll method");
			return new ResponseDto<SentimentOutputModel>(true, sentimentOutputModels);
		}
		LOGGER.debug("No sentiment is found");
		LOGGER.info("Exiting from AdminSentimentController getAll method");
		return new ResponseDto<SentimentOutputModel>(false);
	}

	@RequestMapping(value = "/getSentimentById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SentimentOutputModelFull> findSentiment(Long id) {
		LOGGER.info("Inside SentimentController findSentiment method");
		Sentiment sentiment = null;
		try {
			sentiment = sentimentBusinessDelegate.read(id);
			if (sentiment != null) {
				LOGGER.debug("Sentiment is found for id: " + id);
				LOGGER.info("Exiting from SentimentController find method");
				SentimentOutputModelFull model = SentimentOutputModelFull.getModel(sentiment);
				model.prepareReferences(sentiment.getAttachments());
				sentiment.setAttachments(null);
				List<Opinion> opinions = opinionBusinessDelegate.getOpinionForSentiment(id);
				if (opinions != null) {
					Collections.sort(opinions);
					for (Opinion entity : opinions) {
						if (entity.getLink() != null) {
							model.addReferences(entity);
						}
					}
				}
				return new ResponseDto<SentimentOutputModelFull>(true, model);
			}
		} catch (Exception exception) {
			LOGGER.error("Exception in finding Sentiment");
			LOGGER.error("Exception: " + exception);
			LOGGER.info("Exiting from SentimentController find method");

			return new ResponseDto<SentimentOutputModelFull>(false);
		}
		LOGGER.debug("Sentiment is not found for id: " + id);
		LOGGER.info("Exiting from SentimentController find method");
		return new ResponseDto<SentimentOutputModelFull>(false);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.PUT)
	public @ResponseBody ResponseDto<Sentiment> edit(@RequestBody AdminSentimentInputModel sentimentInput) {
		LOGGER.info("Inside SentimentController edit method");
		Sentiment sentiment = null;
		LOGGER.info("sentimentInput in edit method: " + sentimentInput);
		try {
			byte[] imageInBytes = Base64.decodeBase64(sentimentInput.getImageByte());
			sentiment = new Sentiment();
			sentiment.setSubject(sentimentInput.getSubject());
			sentiment.setDescription(sentimentInput.getDescription());
			sentiment.setType(sentimentInput.getType());
			sentiment.setKeywords(sentimentInput.getKeywords());
			LOGGER.debug("Sentiment Categories: " + sentimentInput.getCategories());
			sentiment.setStartTime(DateUtils.getSqlTimeStamp());
			sentiment.setEndTime(DateUtils.addDays(3, DateUtils.getSqlTimeStamp()));
			sentiment.setId(null);
			sentiment.setState(SentimentState.ALIVE.getValue());
			sentiment.setCreated_By(1L);
			sentiment.setImage(imageInBytes);
			// String str[] = sentimentInput.getImageType().trim().split("/");
			// LOGGER.debug("Str: " + str[1]);
			// sentiment.setImageType(str[1]);

			StringBuilder sb = new StringBuilder();
			sb.append(FileLoadingUtils.BASE_IMAGE_PATH_SENTIMENT_LIST);
			sb.append(File.separator);
			sb.append(sentiment.getSubject());
			sb.append(".");
			sb.append(sentimentInput.getImageType());
			String fullFilePath = FileLoadingUtils.BASE_IMAGE_PATH + sb.toString();
			FileOutputStream fos = new FileOutputStream(fullFilePath);
			fos.write(imageInBytes);
			fos.close();
			LOGGER.debug("File write at the location: " + fullFilePath);

			// Category category = new Category();
			Set<Category> categories = new HashSet<Category>();

			if (sentimentInput.getCategories() != null && sentimentInput.getCategories().size() > 0) {
				Iterator<Category> itCat = sentimentInput.getCategories().iterator();
				while (itCat.hasNext()) {
					Category cat = itCat.next();
					cat.setCreatedBy(sentiment.getCreated_By());
					cat.setDescription(cat.getName());
					cat.setType(CategoryType.CATEGORY.getValue());
					categories.add(cat);
				}
			} else {
				Category category = new Category();
				category.setId(119L);
				category.setCreatedBy(sentiment.getCreated_By());
				category.setDescription(sentiment.getSubject());
				category.setType(CategoryType.CATEGORY.getValue());
				category.setName(sentiment.getSubject());
				categories.add(category);
			}
			sentiment.setCategories(categories);
			
			ArrayList<RelatedSentimentsDto> relatedSentimentsDtos = sentimentInput.getRelatedSentimentsId();
			HashSet<Sentiment> sentimentSet = new HashSet<Sentiment>();
			Iterator<RelatedSentimentsDto> it = relatedSentimentsDtos.iterator();
			Sentiment tempSenti = null;
			while (it.hasNext()) {
				RelatedSentimentsDto relatedSentimentsDto = it.next();
				tempSenti = new Sentiment();
				tempSenti.setId(relatedSentimentsDto.getId());
				sentimentSet.add(tempSenti);
			}
			sentiment.setRelatedSentiments(sentimentSet);
			
			sentiment = sentimentBusinessDelegate.update(sentiment);
			if (sentiment != null) {
				LOGGER.debug("Sentiment with id: " + sentiment.getId() + " is update");
				LOGGER.info("Exiting from AdminSentimentController edit method");
				return new ResponseDto<Sentiment>(true, sentiment);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.info("Exiting from AdminSentimentController edit method");
			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment is not update");
		LOGGER.info("Exiting from AdminSentimentController edit method");
		return new ResponseDto<Sentiment>(false);
	}

	@RequestMapping(value = "/disableEnableSentiment", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Sentiment> disableEnableSentiment(@RequestParam("state") String state,
			@RequestParam("id") Long id) {

		try {
			AdminSentimentInputModel sentimentInput = new AdminSentimentInputModel();
			sentimentInput.setId(id);
			sentimentInput.setState(state);
			boolean status = sentimentBusinessDelegate.updateSentimentStatus(sentimentInput.getId(),
					sentimentInput.getState());
			Sentiment sentiment = new Sentiment();
			sentiment.setId(sentimentInput.getId());
			sentiment.setState(sentimentInput.getState());

			if (status) {
				LOGGER.debug("Sentiment with id: " + sentimentInput.getId() + " is update");
				LOGGER.info("Exiting from AdminSentimentController disableEnableSentiment method");
				return new ResponseDto<Sentiment>(true, sentiment);
			}
		} catch (Exception e) {
			LOGGER.error("Exception in updating Sentiment");
			LOGGER.info("Exiting from AdminSentimentController disableEnableSentiment method");
			return new ResponseDto<Sentiment>(false);
		}
		LOGGER.debug("Sentiment is not update");
		LOGGER.info("Exiting from AdminSentimentController disableEnableSentiment method");
		return new ResponseDto<Sentiment>(false);
	}

	@RequestMapping(value = "/addPoll", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<PollDto> addPoll(@RequestBody PollLinkDto pollLinkDto) {

		try {
			LOGGER.debug("pollLinkDto: " + pollLinkDto);
			if (pollLinkDto.getPolls() != null && pollLinkDto.getPolls().size() > 0) {
				Poll poll = null;
				ArrayList<Poll> polls = new ArrayList<Poll>();
				List<PollDto> pollList = pollLinkDto.getPolls();
				ArrayList<PollOption> pollOptions = null;
				PollOption pollOption = null;
				Iterator<PollDto> itPoll = pollList.iterator();
				while (itPoll.hasNext()) {
					PollDto pollDto = itPoll.next();
					poll = new Poll();
					poll.setQuestion(pollDto.getQuestionText());
					poll.setImage(Base64.decodeBase64(pollDto.getImgFile()));
					pollOptions = new ArrayList<PollOption>();
					Iterator<OptionsDto> optionDto = pollDto.getOptions().iterator();
					while (optionDto.hasNext()) {
						OptionsDto optionsDto = optionDto.next();
						pollOption = new PollOption();
						String optText = optionsDto.getOptionText();
						if (optText != null && optText.length() > 0) {
							pollOption.setPollOption(optText);
							pollOption.setCreatedBy(1L);
							pollOptions.add(pollOption);
						}
					}
					poll.setOptions(pollOptions);
					poll.setCreatedBy(1L);
					LOGGER.debug("sentimentIdDto.getSentiment: [" + sentimentIdDto.getSentiment() + "]");
					if (sentimentIdDto.getSentiment() != null) {
						poll = pollBusinessDelegate.create(poll);
						polls.add(poll);
					} else {
						throw new NullPointerException("Sentiment not added for this poll question.");
					}
					LOGGER.info("poll added with Id: " + poll.getId());
				}
				sentimentIdDto.getSentiment().setPolls(polls);
			}
			if (pollLinkDto.getLinks() != null && pollLinkDto.getLinks().size() > 0) {
				Attachment attachment = null;
				ArrayList<Attachment> attachmentsList = new ArrayList<Attachment>();
				List<LinkDto> linkList = pollLinkDto.getLinks();
				Iterator<LinkDto> itLink = linkList.iterator();
				while (itLink.hasNext()) {
					attachment = new Attachment();
					LinkDto linkDto = itLink.next();
					attachment.setFilePath(linkDto.getLinkText());
					attachment.setDescription(linkDto.getTitleText());
					attachment.setCreated_By(1L);
					attachment.setAttachmentType(AttachmentType.FILE.getValue());
					attachment.setFileType("website");
					attachment.setTitle(linkDto.getTitleText());
					attachmentsList.add(attachment);
					attachment = attachmentBusinessDeligate.create(attachment);
					LOGGER.debug("Link added with Id: " + attachment.getId());
				}
				sentimentIdDto.getSentiment().setAttachments(attachmentsList);
			}
			sentimentBusinessDelegate.update(sentimentIdDto.getSentiment());
			return new ResponseDto<PollDto>(true);
		} catch (Exception e) {
			LOGGER.error("Exception in Adding poll question");
			LOGGER.info("Exiting from AdminSentimentController addPoll method");
			return new ResponseDto<PollDto>(false);
		}
	}

	@RequestMapping(value = "/editPoll", method = RequestMethod.PUT)
	public @ResponseBody ResponseDto<PollDto> editPoll(@RequestBody PollLinkDto pollLinkDto) {

		try {
			LOGGER.info("pollLinkDto: " + pollLinkDto);
			if (pollLinkDto.getPolls() != null && pollLinkDto.getPolls().size() > 0) {
				Poll poll = null;
				List<PollDto> pollList = pollLinkDto.getPolls();
				ArrayList<PollOption> pollOptions = null;
				PollOption pollOption = null;
				Iterator<PollDto> itPoll = pollList.iterator();
				while (itPoll.hasNext()) {
					PollDto pollDto = itPoll.next();
					poll = new Poll();
					poll.setId(pollDto.getId());
					poll.setQuestion(pollDto.getQuestionText());
					poll.setImage(Base64.decodeBase64(pollDto.getImgFile()));
					poll.setEditedBy(1l);
					pollOptions = new ArrayList<PollOption>();
					Iterator<OptionsDto> optionDto = pollDto.getOptions().iterator();
					while (optionDto.hasNext()) {
						OptionsDto optionsDto = optionDto.next();
						pollOption = new PollOption();
						pollOption.setPollOption(optionsDto.getOptionText());
						pollOption.setEditedBy(1l);
						pollOptions.add(pollOption);
					}
					poll.setOptions(pollOptions);
					poll.setCreatedBy(1L);
					poll = pollBusinessDelegate.update(poll);
					LOGGER.info("poll updated with Id: " + poll.getId());
				}
			}
			if (pollLinkDto.getLinks() != null && pollLinkDto.getLinks().size() > 0) {
				Attachment attachment = null;
				List<LinkDto> linkList = pollLinkDto.getLinks();
				Iterator<LinkDto> itLink = linkList.iterator();
				while (itLink.hasNext()) {
					attachment = new Attachment();
					
					LinkDto linkDto = itLink.next();
					attachment.setId(linkDto.getId());
					attachment.setFilePath(linkDto.getLinkText());
					attachment.setDescription(linkDto.getTitleText());
					attachment.setEditedBy(1L);
					attachment.setTitle(linkDto.getTitleText());
					attachment = attachmentBusinessDeligate.update(attachment);
					LOGGER.debug("Link updated with Id: " + attachment.getId());
				}
			}
			return new ResponseDto<PollDto>(true);
		} catch (Exception e) {
			LOGGER.error("Exception in updating poll question");
			LOGGER.info("Exiting from AdminSentimentController editPoll method");
			return new ResponseDto<PollDto>(false);
		}
	}
}
