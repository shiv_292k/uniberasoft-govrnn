package com.ohmuk.folitics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.businessDelegate.interfaces.IPollBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IResponseBusinessDelegate;
import com.ohmuk.folitics.charting.beans.PollResultBean;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.hibernate.entity.poll.Poll;
import com.ohmuk.folitics.hibernate.entity.poll.PollOption;
import com.ohmuk.folitics.ouput.model.PollOptionsCount;
import com.ohmuk.folitics.ouput.model.UserPollOption;

/**
 * @author Abhishek
 *
 */
@Controller
@RequestMapping("/poll")
public class PollController {

    @Autowired
    private volatile IPollBusinessDelegate businessDelegate;

    @Autowired
    volatile IResponseBusinessDelegate responseBusinessDelegate;

    private static Logger logger = LoggerFactory.getLogger(PollController.class);

    @RequestMapping
    public String getPollPage() {

        return "poll-page";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> getAdd() {

        List<Poll> polls = new ArrayList<>();
        polls.add(getTestPoll());
        return new ResponseDto<Poll>(true, polls);
    }

    /**
     * Web service is to add {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<Poll> add(@Validated @RequestPart(value = "poll") Poll poll,
            @RequestPart(value = "file") MultipartFile image) {
        logger.info("Inside PollController add method");
        try {
            if (image != null) {
                logger.info("Image " + image.getOriginalFilename() + "Received " + "Image size " + image.getSize());
                poll.setImage(image.getBytes());
            }
            poll = businessDelegate.create(poll);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in adding poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController add method");
            return new ResponseDto<Poll>(false);
        }
        if (poll != null) {
            logger.debug("Poll is save");
            logger.info("Exiting from PollController add method");
            return new ResponseDto<Poll>(true, poll);
        }
        logger.debug("Poll is not save");
        logger.info("Exiting from PollController add method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service is to update {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Poll> edit(@RequestBody Poll poll) {
        logger.info("Inside PollController edit method");
        try {
            poll = businessDelegate.update(poll);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in updating poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController edit method");
            return new ResponseDto<Poll>(false);
        }
        if (poll != null) {
            logger.debug("Poll is updated: " + poll.getId());
            logger.info("Exiting from PollController edit method");
            return new ResponseDto<Poll>(true, poll);
        }
        logger.debug("Poll is not update");
        logger.info("Exiting from PollController edit method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to delete {@link Poll} by id
     * 
     * @param id
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/deleteById", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> delete(Long id) {
        logger.info("Inside PollController delete method");
        try {
            if (businessDelegate.delete(id)) {
                logger.debug("Poll with id: " + id + " is delete");
                logger.info("Exiting from PollController delete method");
                return new ResponseDto<Poll>(true);
            }
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in soft deleting poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController delete method");
            return new ResponseDto<Poll>(false);
        }
        logger.debug("Poll with id: " + id + " is not delete");
        logger.info("Exiting from PollController delete method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to delete {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Poll> delete(@Validated @RequestBody Poll poll) {
        logger.info("Inside PollController delete method");
        try {
            if (businessDelegate.delete(poll)) {
                logger.debug("Poll with id: " + poll.getId() + " is delete");
                logger.info("Exiting from PollController delete method");
                return new ResponseDto<Poll>(true);
            }
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in soft deleting poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController delete method");
            return new ResponseDto<Poll>(false);
        }
        logger.debug("Poll with id: " + poll.getId() + " is not delete");
        logger.info("Exiting from PollController delete method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to delete hard {@link Poll} by id
     * 
     * @param id
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/deleteFromDbById", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> deleteFromDB(Long id) {
        logger.info("Inside PollController deleteFromDB method");
        try {
            Boolean test = businessDelegate.deleteFromDB(id);

            if (test) {
                logger.debug("Poll with id: " + id + " is delete");
                logger.info("Exiting from PollController deleteFromDB method");
                return new ResponseDto<Poll>(true);
            }
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in hard deleting poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController deleteFromDB method");
            return new ResponseDto<Poll>(false);
        }
        logger.debug("Poll with id: " + id + " is not delete");
        logger.info("Exiting from PollController deleteFromDB method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * web service to hard delete {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/deleteFromDb", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseDto<Poll> deleteFromDB(@Validated @RequestBody Poll poll) {
        logger.info("Inside PollController add method");
        try {
            if (businessDelegate.deleteFromDB(poll)) {
                logger.debug("Poll with id: " + poll.getId() + " is delete");
                logger.info("Exiting from PollController deleteFromDB method");
                return new ResponseDto<Poll>(true);
            }
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in hard deleting poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController deleteFromDB method");
            return new ResponseDto<Poll>(false);
        }
        logger.debug("Poll with id: " + poll.getId() + " is not delete");
        logger.info("Exiting from PollController deleteFromDB method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to get all {@link Poll}
     * 
     * @return ResponseDto<Poll>
     */
    //@RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> getall() {
        logger.info("Inside PollController getall method");
        List<Poll> polls = null;
        try {
            polls = businessDelegate.readAll();
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in getting getall poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getall method");
            return new ResponseDto<Poll>(false);
        }
        if (polls != null) {
            logger.debug(polls.size() + " polls is found");
            logger.info("Exiting from PollController getall method");
            return new ResponseDto<Poll>(true, polls);
        }
        logger.debug("No poll is found");
        logger.info("Exiting from PollController getall method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to get all active {@link Poll}
     * 
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/getActivePolls", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> getActivePolls() {
        logger.info("Inside PollController getActivePolls method");
        List<Poll> polls = null;
        try {
            polls = businessDelegate.readAllActivePoll();
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in getting all active poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getActivePolls method");
            return new ResponseDto<Poll>(false);
        }
        if (polls != null) {
            logger.debug(polls.size() + " polls is found");
            logger.info("Exiting from PollController getActivePolls method");
            return new ResponseDto<Poll>(true, polls);
        }
        logger.debug("No poll is found");
        logger.info("Exiting from PollController getActivePolls method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to get {@link Poll} by id
     * 
     * @param id
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> find(Long id) {
        logger.info("Inside PollController add method");
        try {
            Poll poll = businessDelegate.getPollById(id);
            logger.debug("Poll with id: " + poll.getId() + " is found");
            logger.info("Exiting from PollController find method");
            logger.info("Exiting from PollController find method");
            return new ResponseDto<Poll>(true, poll);
        } catch (Exception exception) {
            exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController find method");
            return new ResponseDto<Poll>(false);
        }
    }

    /**
     * Web service to get {@link Poll} for
     * {@link com.ohmuk.folitics.hibernate.entity.Sentiment}
     * 
     * @param id
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/getPollForSentiment", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> getPollForSentiment(Long id) {
        logger.info("Inside PollController add method");
        List<Poll> polls = null;
        try {
            polls = businessDelegate.getPollsForSentiment(id);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in getting poll getPollForSentiment sentiment");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getPollForSentiment method");
            return new ResponseDto<Poll>(false);
        }
        if (polls != null) {
            logger.debug(polls.size() + " polls is found for setiment id: " + id);
            logger.info("Exiting from PollController getPollForSentiment method");
            return new ResponseDto<Poll>(true, polls);
        }
        logger.debug("No poll is found for sentiment id: " + id);
        logger.info("Exiting from PollController getPollForSentiment method");
        return new ResponseDto<Poll>(false);
    }

    /**
     * Web service to get {@link Poll} which are not attached with any
     * {@link com.ohmuk.folitics.hibernate.entity.Sentiment}
     * 
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/getIsolatedPolls", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Poll> getIsolatedPolls() {
        logger.info("Inside PollController add method");
        List<Poll> polls = null;
        try {
            polls = businessDelegate.getIsolatedPolls();
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in getting getIsolatedPolls poll");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getIsolatedPolls method");
            return new ResponseDto<Poll>(false);
        }
        if (polls != null) {
            logger.debug(polls.size() + " isolated polls is found");
            logger.info("Exiting from PollController getIsolatedPolls method");
            return new ResponseDto<Poll>(true, polls);
        }
        logger.debug("No isolated poll is found");
        logger.info("Exiting from PollController getIsolatedPolls method");
        return new ResponseDto<Poll>(false);
    }

    // /**
    // * Web service to save {@link PollOption}
    // * @param pollOption
    // * @return ResponseDto<PollOption>
    // */
    // @RequestMapping(value = "/answerPoll", method = RequestMethod.GET)
    // public @ResponseBody ResponseDto<Object> answerPoll(Long pollOptionId,
    // Long userId) {
    //
    // try {
    // return new
    // ResponseDto<Object>(distributionBusinessDelegate.answerPoll(pollOptionId,
    // userId));
    //
    // } catch (Exception exception) {
    //
    // logger.error("Exception while answering poll " + exception);
    // return new ResponseDto<Object>(false);
    // }
    // }

    /**
     * Web service to get {@link PollOption} by {@link Poll} id
     * 
     * @param id
     * @return ResponseDto<Set<PollOption>>
     */
    @RequestMapping(value = "/getPollResult", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody ResponseDto<List<PollResultBean>> getPollResult(Long id, String filter) {
        logger.info("Inside PollController getPollResult method");
        List<List<PollResultBean>> pollResults = null;
        try {
            pollResults = businessDelegate.getPollResult(id, filter);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in getting getPollResult result");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getPollResult method");
            return new ResponseDto<List<PollResultBean>>(false);
        }
        if (pollResults != null) {
            logger.debug(pollResults + " Poll result is found");
            logger.info("Exiting from PollController getPollResult method");
            return new ResponseDto<List<PollResultBean>>(true, pollResults);
        }
        logger.debug("No poll result is found");
        logger.info("Exiting from PollController getPollResult method");
        return new ResponseDto<List<PollResultBean>>(false);
    }

    /**
     * Web service to get {@link Poll} by id
     * 
     * @param id
     * @return ResponseDto<Poll>
     */
    /*
     * @RequestMapping(value = "/getPollOptionCount", method =
     * RequestMethod.GET) public @ResponseBody ResponseDto<Long>
     * getPollOptionCount(Long id) {
     * logger.info("Inside PollController add method"); try { return new
     * ResponseDto<Long>(true, businessDelegate.getPollOptionCount(id)); } catch
     * (Exception exception) { logger.error("Exception in finding poll by id");
     * logger.error("Exception: " + exception);
     * logger.info("Exiting from PollController find method"); return new
     * ResponseDto<Long>(false); } }
     */

    @RequestMapping(value = "/getTestPoll", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Poll getTestPoll() {

        return getDummyPoll();

    }

    private Poll getDummyPoll() {
        Poll poll = new Poll();
        // poll.setId((new Random()).nextLong());
        PollOption option1 = new PollOption();
        option1.setPollOption("this is option 1");
        PollOption option2 = new PollOption();
        option2.setPollOption("this is option 2");
        List<PollOption> pollOptions = new ArrayList<PollOption>();
        pollOptions.add(option1);
        pollOptions.add(option2);
        poll.setQuestion("What is the qustion?");
        poll.setOptions(pollOptions);
        return poll;
    }

    @RequestMapping(value = "/selectVote", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<Object> selectVote(@RequestParam Long pollOptionId, @RequestParam Long userId,
            @RequestParam String componentType, @RequestParam Long componentId,
            @RequestParam(value = "parentOwnerId", required = false) Long parentOwnerId) {
        logger.info("Inside PollController select vote method");
        Poll poll = null;
        try {
            poll = businessDelegate.selectVote(pollOptionId, userId, componentType, componentId,parentOwnerId);

        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in selectVote");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController selectVote method");
            return new ResponseDto<Object>(false);
        }
        if (poll != null) {
            logger.debug("Poll is save");
            logger.info("Exiting from PollController selectVote method");
            return new ResponseDto<Object>(true, poll);
        }
        logger.debug("Poll is not save");
        logger.info("Exiting from PollController selectVote method");
        return new ResponseDto<Object>(false);
    }

    @RequestMapping(value = "/unSelectVote", method = RequestMethod.POST)
    public @ResponseBody ResponseDto<Object> unSelectVote(Long pollOptionId, Long userId) {
        logger.info("Inside PollController unSelectVote method");
        Poll poll = null;
        try {
            poll = businessDelegate.unSelectVote(pollOptionId, userId);

        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in unSelectVote");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController unSelectVote method");
            return new ResponseDto<Object>(false);
        }
        if (poll != null) {
            logger.debug("Poll is save");
            logger.info("Exiting from PollController unSelectVote method");
            return new ResponseDto<Object>(true, poll);
        }
        logger.debug("Poll is not save");
        logger.info("Exiting from PollController unSelectVote method");
        return new ResponseDto<Object>(false);
    }

    /**
     * Web service is to get poll aggregation {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/getPollAggregation", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Map<Long, Long>> getPollAggregation(Long pollId) {
        logger.info("Inside PollController getPollAggregation method");
        try {
            Poll poll = businessDelegate.getPollById(pollId);
            logger.debug("Poll with id: " + poll.getId() + " is found");
            Map<Long, Long> pollAggregation = new HashMap<>();
            List<PollOption> options = poll.getOptions();
            if (null != options && !options.isEmpty()) {
                for (PollOption option : options) {
                    // not usre why we are using..
                    Long votes = (option.getUsers() != null) ? new Long(option.getUsers().size()) : null;
                    pollAggregation.put(option.getId(), votes);
                }
            }

            logger.info("Exiting from PollController getPollAggregation method");
            return new ResponseDto<Map<Long, Long>>(true, pollAggregation);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getPollAggregation method");
            return new ResponseDto<Map<Long, Long>>(false);
        }
    }

    /**
     * Web service is to get poll aggregation {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Poll>
     */
    @RequestMapping(value = "/getPollAggregationBean", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<PollOptionsCount>> getPollAggregationBean(Long pollId) {
        logger.info("Inside PollController getPollAggregation method");
        try {
            List<PollOptionsCount> pollOptionsCounts = businessDelegate.getPollAggregationPercentage(pollId);

            logger.info("Exiting from PollController getPollAggregationBean method");
            return new ResponseDto<List<PollOptionsCount>>(true, pollOptionsCounts);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getPollAggregationBean method");
        }
        return new ResponseDto<List<PollOptionsCount>>(false);
    }

    /**
     * Web service is to get poll options {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Long>
     */
    @RequestMapping(value = "/getPollOption", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Long> getPollOption(Long pollId, Long userId) {
        logger.info("Inside PollController getPollOptions method");
        try {
            Long pollOptionId = businessDelegate.getPollOption(pollId, userId);
            logger.debug("Poll option with  poll id: " + pollId + " and user Id : " + userId + " are found");
            logger.info("Exiting from PollController getPollOptions method");
            return new ResponseDto<Long>(true, pollOptionId);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getPollOptions method");
            return new ResponseDto<Long>(false);
        }
    }

    /**
     * Web service is to get poll options {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Long> Map<Long,Long>= <pollId1,
     *         pollOptionId3><pollId2, pollOptionId1><pollId3, pollOptionId2>
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/getAllPollOptionBySentiment", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<Map<Long, Long>> getAllPollOptionBySentiment(Long sentimentId, Long userId) {
        logger.info("Inside PollController getAllPollOptionBySentiment method");
        try {
            Map<Long, Long> sentimentPolls = businessDelegate.getAllPollOptionBySentiment(sentimentId, userId);
            logger.debug("sentiment polls with  sentiment= id: " + sentimentId + " and user Id : " + userId
                    + " are found");
            logger.info("Exiting from PollController getAllPollOptionBySentiment method");
            return new ResponseDto<Map<Long, Long>>(true, sentimentPolls);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getAllPollOptionBySentiment method");
            return new ResponseDto<Map<Long, Long>>(false);
        }
    }

    /**
     * Web service is to get poll options {@link Poll}
     * 
     * @param poll
     * @return ResponseDto<Long> Map<Long,Long>= <pollId1,
     *         pollOptionId3><pollId2, pollOptionId1><pollId3, pollOptionId2>
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/getAllPollOptionBySentimentBean", method = RequestMethod.GET)
    public @ResponseBody ResponseDto<List<UserPollOption>> getAllPollOptionBySentimentBean(Long sentimentId, Long userId) {
        logger.info("Inside PollController getAllPollOptionBySentiment method");
        try {
            List<UserPollOption> pollOptions = new ArrayList<UserPollOption>();
            Map<Long, Long> sentimentPolls = businessDelegate.getAllPollOptionBySentiment(sentimentId, userId);
            Set<Long> keys = sentimentPolls.keySet();
            Iterator<Long> pollIds = keys.iterator();
            while (pollIds.hasNext()) {
                Long pollId = pollIds.next();
                Long pollOptionId = sentimentPolls.get(pollId);
                UserPollOption po = new UserPollOption();
                po.setPollId(pollId);
                po.setPollOptionId(pollOptionId);
                pollOptions.add(po);
            }
            logger.debug("sentiment polls with  sentiment= id: " + sentimentId + " and user Id : " + userId
                    + " are found");
            logger.info("Exiting from PollController getAllPollOptionBySentiment method");
            return new ResponseDto<List<UserPollOption>>(true, pollOptions);
        } catch (Exception exception) {
        	exception.printStackTrace();
            logger.error("Exception in finding poll by id");
            logger.error("Exception: " + exception);
            logger.info("Exiting from PollController getAllPollOptionBySentiment method");
            return new ResponseDto<List<UserPollOption>>(false);
        }
    }
}