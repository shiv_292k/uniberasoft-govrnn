package com.ohmuk.folitics.controller.trend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.businessDelegate.interfaces.ICommentBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.ITrendBusinessDelegate;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.ComponentType;
import com.ohmuk.folitics.hibernate.entity.Opinion;
import com.ohmuk.folitics.hibernate.entity.Response;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.trend.Trend;
import com.ohmuk.folitics.hibernate.entity.trend.TrendMapping;
import com.ohmuk.folitics.model.TrendMappingMultiple;
import com.ohmuk.folitics.ouput.model.TrendMappingOutputModel;
import com.ohmuk.folitics.ouput.model.TrendOutputModel;
import com.ohmuk.folitics.ouput.model.UserTrendOutputModel;
import com.ohmuk.folitics.service.ITrendService;
import com.ohmuk.folitics.util.DateUtils;

/**
 * 
 * @author Deewan
 *
 */
@Controller
@RequestMapping("/trend")
public class TrendController {

	private static Logger logger = LoggerFactory.getLogger(TrendController.class);
	
	@Autowired
	private volatile IUserBusinessDelegate userBusinessDelegate;
	
	@Autowired
	private volatile ITrendBusinessDelegate trendBusinessDelegate;
	
	@Autowired
	private volatile ICommentBusinessDelegate commentBusinessDelegate;
	
	@Autowired
	private ITrendService trendService;

	// Below method is displaying similar trend and similar user
	@RequestMapping(value = "/displaytrend", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> displayTrend(
			@RequestBody Trend trend) {
		logger.info("Inside TrendController displaytrend method");
		List<Trend> top5Trend = null;
		List<User> top5User = null;
		try {
			top5Trend = trendBusinessDelegate.displayTrend(trend);
			if (null != top5Trend) {
				for (int i = 0; i < top5Trend.size(); i++) {
					System.out.println("Displaying Top  5 Trend ["
							+ top5Trend.get(i).getId() + "] Trend "
							+ top5Trend.get(i).getName() + "] Created Date ["
							+ top5Trend.get(i).getCreateTime() + "]");
				}
			}
			top5User = trendBusinessDelegate.top5User(trend.getName());
			if (null != top5User) {
				for (int i = 0; i < top5User.size(); i++) {
					System.out.println("Displaying Top  5 User ["
							+ top5User.get(i).getName() + "]");
				}
			}
		} catch (Exception exception) {
			logger.error("Exception in displaytrend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController displaytrend method");
			exception.printStackTrace();
			return new ResponseDto<Trend>(false, trend);
		}
		return new ResponseDto<Trend>(true, trend);
	}

	/*
	 * { "name":"#TrendFoliticsIndore3", "trendMappings": [ ] }
	 */
	@RequestMapping(value = "/createTrend", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> createTrend(@RequestBody Trend trend) {
		logger.info("Inside TrendController createTrend method");
		try {
			trend.setCreatedBy("user");
			trend.setAdminTrend(false);
			trend.setEnabled(true);;
			Trend trendObject = trendBusinessDelegate.createTrend(trend);
			if (null != trendObject) {
				return new ResponseDto<Trend>(true, trendObject);
			}
			else{
				trendObject = trendBusinessDelegate.getTrendByName(trend.getName());
				return new ResponseDto<Trend>(true, trendObject);
			}
		} catch (Exception exception) {
			logger.error("Exception in saveTrend");
			logger.error("Exception: " + exception.getMessage());
			logger.info("Exiting from TrendController createTrend method");
			//exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
	}
	
	@RequestMapping(value = "/createTrendByAdmin", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> createTrendByAdmin(@RequestBody Trend trend) {
		logger.info("Inside TrendController createTrend method");
		try {
			trend.setCreatedBy("admin");
			trend.setAdminTrend(true);
			trend.setEnabled(true);;
			Trend trendObject = trendBusinessDelegate.createTrend(trend);
			if (null != trendObject) {
				return new ResponseDto<Trend>(true, trendObject);
			}
			else{
				return new ResponseDto<Trend>(false, trendObject ,"Trend already exists with same name. Try to make existing trend as admin trend");
			}
		} catch (Exception exception) {
			logger.error("Exception in saveTrend");
			logger.error("Exception: " + exception.getMessage());
			logger.info("Exiting from TrendController createTrend method");
			//exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
	}
	@RequestMapping(value = "/makeAdminTrend", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> makeTrendByAdmin(String name) {
		logger.info("Inside TrendController createTrend method");
		try {
			Trend trendObject = trendBusinessDelegate.getTrendByName(name);
			if (null != trendObject) {
				trendObject.setAdminTrend(true);
				trendObject.setCreatedBy("admin");
				trendObject.setTrending(true);
				trendObject.setEditTime(DateUtils.getSqlTimeStamp());
				trendObject = trendBusinessDelegate.update(trendObject);
				trendObject.setTrendMappings(null);
				return new ResponseDto<Trend>(true, trendObject);
			}
		} catch (Exception exception) {
			logger.error("Exception in saveTrend");
			logger.error("Exception: " + exception.getMessage());
			logger.info("Exiting from TrendController createTrend method");
			exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
		return new ResponseDto<Trend>(false);
	}

	@RequestMapping(value = "/removeAdminTrend", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> removeTrendByAdmin(String name) {
		logger.info("Inside TrendController createTrend method");
		try {
			Trend trendObject = trendBusinessDelegate.getTrendByName(name);
			if (null != trendObject) {
				trendObject.setAdminTrend(false);
				trendObject.setEditTime(DateUtils.getSqlTimeStamp());
				trendObject.setCreatedBy("user");
				trendObject = trendBusinessDelegate.update(trendObject);
				trendObject.setTrendMappings(null);
				return new ResponseDto<Trend>(true, trendObject);
			}
		} catch (Exception exception) {
			logger.error("Exception in saveTrend");
			logger.error("Exception: " + exception.getMessage());
			logger.info("Exiting from TrendController createTrend method");
			//exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
		return new ResponseDto<Trend>(false);
	}

	@RequestMapping(value = "/manageTrend", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Trend> manageTrend(@RequestParam List<String> trendNames, @RequestParam String action) {
		logger.info("Inside TrendController createTrend method");
		Boolean status = false;
		try {
			status = trendBusinessDelegate.manageTrend(trendNames, action);
			if (status == true) {
				return new ResponseDto<Trend>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in saveTrend");
			logger.error("Exception: " + exception.getMessage());
			logger.info("Exiting from TrendController createTrend method");
			//exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
		return new ResponseDto<Trend>(false);
	}


	@RequestMapping(value = "/createTrendMapping", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Long> createTrendMapping(
			@RequestBody TrendMapping trendMapping) {
		logger.info("Inside TrendController createTrendMapping method");
		Long id = null;
		try {
			logger.info("Adding new trendMapping in addToTrendMapping method");
			// get the trend by id (as we pass only id for trend object)-- then
			// get trend and add into trend mapping
			id = trendBusinessDelegate.createTrendMapping(trendMapping);
			logger.info("Added trendMapping with ID " + id + "");
		} catch (Exception exception) {
			logger.error("Exception in addToTrendMapping");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController createTrendMapping method");
			exception.printStackTrace();
			new ResponseDto<Long>("some exception occured", false);
		}
		return new ResponseDto<Long>(true, id, "success");
	}

	@RequestMapping(value = "/createMultipleTrendMapping", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<List<Long>> createMultipleTrendMapping(
			@RequestBody TrendMappingMultiple trendMappingMultiple) {
		logger.info("Inside TrendController createTrendMapping method : "+trendMappingMultiple);
		List<Long> ids = new ArrayList<Long>();
		try {
			if (trendMappingMultiple.getTrendIds() != null
					&& !trendMappingMultiple.getTrendIds().isEmpty()) {
				logger.info("Adding new trendMapping in addToTrendMapping method");
				TrendMapping trendMapping = new TrendMapping();
				trendMapping.setComponentId(trendMappingMultiple
						.getComponentId());
				trendMapping.setComponentType(trendMappingMultiple
						.getComponentType());
				trendMapping.setUserId(trendMappingMultiple.getUserId());
				for (int i = 0; i < trendMappingMultiple.getTrendIds().size(); i++) {
					Trend trend = new Trend();
					trend.setId(trendMappingMultiple.getTrendIds().get(i));
					trendMapping.setTrend(trend);
					Long id = trendBusinessDelegate.createTrendMapping(trendMapping);
					logger.info("Added trendMapping with ID " + id + "");
					ids.add(id);
				}
				return new ResponseDto<List<Long>>(true,ids,"Trending mappings created");
			}
		} catch (Exception exception) {
			logger.error("Exception in addToTrendMapping");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController createTrendMapping method");
			exception.printStackTrace();
			return new ResponseDto<List<Long>>(false, ids, "Some exception has occured");
		}
		return  new ResponseDto<List<Long>>(false, ids);
	}

	// Below method is displaying exact trend to user while typing #and trend
	// name
	@RequestMapping(value = "/searchexacttrend", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Trend> searchExactTrend(
			@RequestBody Trend trend) {
		logger.info("Inside TrendController searchExactTrend method");
		List<Trend> similarTrend = null;
		try {
			similarTrend = trendBusinessDelegate.searchExactTrend(trend);
			if (null != similarTrend) {
				for (int i = 0; i < similarTrend.size(); i++) {
					System.out.println("Displaying Exact Trend ["
							+ similarTrend.get(i).getId() + "] Trend "
							+ similarTrend.get(i).getName()
							+ "] Created Date ["
							+ similarTrend.get(i).getCreateTime() + "]");
				}
			}
		} catch (Exception exception) {
			logger.error("Exception in searchExactTrend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController searchExactTrend method");
			exception.printStackTrace();
			return new ResponseDto<Trend>(false, trend);
		}
		return new ResponseDto<Trend>(true, trend);
	}

	@RequestMapping(value = "/matchingTrends", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<TrendOutputModel>> matchingTrends(
			String name) {
		logger.info("Inside TrendController matchTrend method");
		List<TrendOutputModel> outputModels = null;
		try {
			outputModels = trendBusinessDelegate.matchingTrend(name);

			if (outputModels != null) {
				return new ResponseDto<List<TrendOutputModel>>(true,
						outputModels);
			}
		} catch (Exception exception) {
			logger.error("Exception in matchingTrends");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController matchingTrends method");
			exception.printStackTrace();
			return new ResponseDto<List<TrendOutputModel>>(false);
		}
		return new ResponseDto<List<TrendOutputModel>>(false);
	}

	@RequestMapping(value = "/getOpinionsByTrendId", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<TrendMappingOutputModel>> getOpinionsByTrendId(
			Long trendId, int count) {
		logger.info("Inside getOpinionsByTrend getOpinions method");
		List<Opinion> opinions = null;
		List<TrendMappingOutputModel> trendMappingOutputModels = new ArrayList<TrendMappingOutputModel>();
		try {
			opinions = trendBusinessDelegate.searchTrendOpinion(trendId);
			Trend trend = trendBusinessDelegate.findTrend(trendId);
			List<TrendMapping> trendMappings = trend.getTrendMappings();
			Map<Long, Long> trendMappingToOpinionMap = new HashMap<Long, Long>();
			for (TrendMapping trendMapping : trendMappings) {
				trendMappingToOpinionMap.put(trendMapping.getComponentId(), trendMapping.getId());
			}
			
			for(Opinion opinion: opinions){
				Long trendMappingid = trendMappingToOpinionMap.get(opinion.getId());
				TrendMappingOutputModel model = TrendMappingOutputModel.getModelFromOpinion(opinion, trendId, trendMappingid); 
		//				new TrendMappingOutputModel(opinion,trendId,trendMappingid);
				trendMappingOutputModels.add(model);
			}
			
		} catch (Exception exception) {
			logger.error("Exception in getOpinionsByTrend for trend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getOpinions method");
			exception.printStackTrace();
			return new ResponseDto<List<TrendMappingOutputModel>>(false,trendMappingOutputModels);
		}
		
		if (trendMappingOutputModels != null) {
			logger.debug(trendMappingOutputModels.size() + " opinions exsits");
			logger.info("Exiting from TrendController getOpinionsByTrend method");
			return new ResponseDto<List<TrendMappingOutputModel>>(true,
					trendMappingOutputModels);
		}
		logger.debug("No sentiment is found");
		logger.info("Exiting from TrendController getOpinionsByTrend method");
		return new ResponseDto<List<TrendMappingOutputModel>>(false);
	}


	@RequestMapping(value = "/getResponsesByTrendId", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<TrendMappingOutputModel>> getResponsesByTrendId(
			Long trendId, int count) {
		logger.info("Inside getResponsesByTrendId getResponsesByTrendId method");
		List<Response> responses = null;
		List<TrendMappingOutputModel> trendMappingOutputModels = new ArrayList<TrendMappingOutputModel>();
		try {
			responses = trendBusinessDelegate.searchTrendResponse(trendId);
			Trend trend = trendBusinessDelegate.findTrend(trendId);
			List<TrendMapping> trendMappings = trend.getTrendMappings();
			Map<Long, Long> trendMappingToOpinionMap = new HashMap<Long, Long>();
			for (TrendMapping trendMapping : trendMappings) {
				trendMappingToOpinionMap.put(trendMapping.getComponentId(), trendMapping.getId());
			}
			
			for(Response response: responses){
				Long trendMappingid = trendMappingToOpinionMap.get(response.getId());
				TrendMappingOutputModel model = TrendMappingOutputModel.getModelFromResponse(response, trendId, trendMappingid); 
		//				new TrendMappingOutputModel(opinion,trendId,trendMappingid);
				trendMappingOutputModels.add(model);
			}
			
		} catch (Exception exception) {
			logger.error("Exception in getResponsesByTrendId for trend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getResponsesByTrendId method");
			exception.printStackTrace();
			return new ResponseDto<List<TrendMappingOutputModel>>(false,trendMappingOutputModels);
		}
		
		if (trendMappingOutputModels != null) {
			logger.debug(trendMappingOutputModels.size() + " Responses exsits");
			logger.info("Exiting from TrendController getResponsesByTrendId method");
			return new ResponseDto<List<TrendMappingOutputModel>>(true,
					trendMappingOutputModels);
		}
		logger.debug("No Response is found");
		logger.info("Exiting from TrendController getResponsesByTrendId method");
		return new ResponseDto<List<TrendMappingOutputModel>>(false);
	}

	@RequestMapping(value = "/getTopTrends", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<TrendOutputModel>> getTopTrends(int count) {
		logger.info("Inside getTopTrends method");
		List<TrendOutputModel> trends = new ArrayList<TrendOutputModel>();
		try {
			trends = trendBusinessDelegate.getTopTrends(count);
			
		} catch (Exception exception) {
			logger.error("Exception in getTopTrends for trend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTopTrends method");
			exception.printStackTrace();
			return new ResponseDto<List<TrendOutputModel>>(false,trends);
		}
		if (trends != null && !trends.isEmpty()) {
			logger.info("Exiting from TrendController getTopTrends method");
			return new ResponseDto<List<TrendOutputModel>>(true, trends);
		}
		logger.debug("No sentiment is found");
		logger.info("Exiting from TrendController getTopTrends method");
		return new ResponseDto<List<TrendOutputModel>>(false);
	}

	@RequestMapping(value = "/getTopTrendUsers", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<UserTrendOutputModel>> getTopTrendUsers(
			int count) {
		logger.info("Inside getTopTrends method");
		List<UserTrendOutputModel> trends = new ArrayList<UserTrendOutputModel>();
		try {
			trends = trendBusinessDelegate.getTopTrendUsers(count);
		} catch (Exception exception) {
			logger.error("Exception in getTopTrends for trend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTopTrends method");
			exception.printStackTrace();
			return new ResponseDto<List<UserTrendOutputModel>>(false);
		}
		if (trends != null) {
			logger.info("Exiting from TrendController getTopTrends method");
			return new ResponseDto<List<UserTrendOutputModel>>(true, trends);
		}
		logger.debug("No sentiment is found");
		logger.info("Exiting from TrendController getTopTrends method"); 
		return new ResponseDto<List<UserTrendOutputModel>>(false);
	}
	
	@RequestMapping(value = "/getTrend", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Trend> getTrend(
			@RequestParam String id) {
		logger.info("Inside TrendController getTrend method");
		Trend trend = null;
		try {
			trend = trendBusinessDelegate.getTrendByName(id);

			if (trend != null) {
				return new ResponseDto<Trend>(true,
						trend);
			}
		} catch (Exception exception) {
			logger.error("Exception in getTrend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTrend method");
			exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
		return new ResponseDto<Trend>(false);
	}


	@RequestMapping(value = "/deleteTrendById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<String> deleteTrendById(
			Long id) {
		logger.info("Inside TrendController deleteTrendById method");
		Boolean success = null;
		try {
			success = trendBusinessDelegate.deleteTrendById(id);

			if (success) {
				return new ResponseDto<String>(true,
						"deleted");
			}
		} catch (Exception exception) {
			logger.error("Exception in deleteTrendById");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController deleteTrendById method");
			exception.printStackTrace();
			return new ResponseDto<String>(false);
		}
		return new ResponseDto<String>(false);
	}


	@RequestMapping(value = "/deleteTrendComments", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<String> deleteTrendCommentsById(
			Long trendMappingId) {
		logger.info("Inside TrendController deleteTrendCommentsById method");
		Boolean success = null;
		try {
			success = trendBusinessDelegate.deleteTrendCommentsById(trendMappingId);

			if (success) {
				return new ResponseDto<String>(true,
						"comments deleted");
			}
		} catch (Exception exception) {
			logger.error("Exception in deleteTrendCommentsById");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController deleteTrendCommentsById method");
			exception.printStackTrace();
			return new ResponseDto<String>(false);
		}
		return new ResponseDto<String>(false);
	}

	@RequestMapping(value = "/getTrendById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Trend> getTrendById(
			 Long id) {
		logger.info("Inside TrendController getTrendById method");
		Trend trend = null;
		try {
			trend = trendBusinessDelegate.findTrend(id);

			if (trend != null) {
				return new ResponseDto<Trend>(true,
						trend);
			}
		} catch (Exception exception) {
			logger.error("Exception in getTrendById");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTrendById method");
			exception.printStackTrace();
			return new ResponseDto<Trend>(false);
		}
		return new ResponseDto<Trend>(false);
	}

	

	@RequestMapping(value = "/getTrendCommentCount", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Map> getTrendCommentCount(
			@RequestParam Long id) {
		logger.info("Inside TrendController getTrendById method");
		Trend trend = null;
		Long totalCommnets = 0l;
		Map<Long, Long> trendCommentCount = null;
		try {
			trend = trendBusinessDelegate.findTrend(id);

			if (trend != null) {
				
				if(trend.getTrendMappings() != null && !trend.getTrendMappings().isEmpty()){
					
					trendCommentCount = new HashMap<>();
					for(TrendMapping mapping : trend.getTrendMappings()){
						
					List<Object> comments =	commentBusinessDelegate.getAllCommentsForComponent(mapping.getId(), ComponentType.TREND.getValue());
					
					if(comments!= null && !comments.isEmpty()){
						 totalCommnets = (long) (totalCommnets.intValue() + comments.size());
					}
					
					 
					}
				trendCommentCount.put(trend.getId(), totalCommnets);
				}
				return new ResponseDto<Map>(true,
						trendCommentCount);
			}
		} catch (Exception exception) {
			logger.error("Exception in getTrendCommentCount");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTrendCommentCount method");
			exception.printStackTrace();
			return new ResponseDto<Map>(false);
		}
		return new ResponseDto<Map>(false);
	}
	
	@RequestMapping(value = "/getTopAdminTrends", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<TrendOutputModel>> getTopAdminTrends(int count) {
		logger.info("Inside getTopAdminTrends method");
		List<TrendOutputModel> trends = new ArrayList<TrendOutputModel>();
		try {
			trends = trendBusinessDelegate.getTopAdminTrends(count);
			
		} catch (Exception exception) {
			logger.error("Exception in getTopAdminTrends for trend");
			logger.error("Exception: " + exception);
			logger.info("Exiting from TrendController getTopAdminTrends method");
			exception.printStackTrace();
			return new ResponseDto<List<TrendOutputModel>>(false,trends);
		}
		if (trends != null && !trends.isEmpty()) {
			logger.info("Exiting from TrendController getTopAdminTrends method");
			return new ResponseDto<List<TrendOutputModel>>(true, trends);
		}
		logger.info("Exiting from TrendController getTopAdminTrends method");
		return new ResponseDto<List<TrendOutputModel>>(false);
	}
	
	@RequestMapping(value = "/getAllTrendsByUser", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<UserTrendOutputModel>> getAllTrendsByUserId(@RequestParam Long userId) {
		logger.info("Inside TrendController getAllTrendsByUserId method");
		List<UserTrendOutputModel> userTrendsList = null;
		try {
			if(userId != null){
				userTrendsList = trendService.fetchAllTrendsByUserId(userId);
			} else {
				return new ResponseDto<List<UserTrendOutputModel>>("Please provide appropriate UserId", false );
			}
		} catch (Exception exception) {
			logger.error("Exception occurs in getAllTrendsByUserId: " + exception);
			exception.printStackTrace();
			return new ResponseDto<List<UserTrendOutputModel>>(false);
		}
		
		return new ResponseDto<List<UserTrendOutputModel>>(true, userTrendsList);
	}
}
