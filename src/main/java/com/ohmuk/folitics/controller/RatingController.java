package com.ohmuk.folitics.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ohmuk.folitics.beans.RatingDataBean;
import com.ohmuk.folitics.businessDelegate.interfaces.IRatingsBusinessDelegate;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.exception.MessageException;

/**
 * Controller for Like
 * 
 * @author Soumya
 *
 */

@Controller
@RequestMapping("/rating")
public class RatingController {

	private static Logger logger = LoggerFactory.getLogger(RatingController.class);

	@Autowired
	private IRatingsBusinessDelegate businessDelegate;

	/**
	 * Spring web service(POST) for rating on the component with id and
	 * component type for the userid
	 * 
	 * @author Soumya
	 * @param java
	 *            .lang.String componentType
	 * @param java
	 *            .lang.Long componentId
	 * @param java
	 *            .lang.Long userId
	 * @return com.ohmuk.folitics.dto.ResponseDto<java.lang.Object>
	 */
	@RequestMapping(value = "/rate", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<RatingDataBean> rate(
			@RequestBody RatingDataBean bean) {

		logger.info("Inside RatingController rate method");
		logger.info("Input componentType = " + bean.getComponentType()
				+ ", componentId = " + bean.getComponentId() + " and userId = "
				+ bean.getUserId() +", Rating : "+bean.getRating());

		RatingDataBean rateObject = new RatingDataBean();
		try {

			logger.info("Trying to save rating. Entering RatingsBusinessDelegate rate method");
			rateObject = businessDelegate.rate(bean);

		} catch (MessageException e) {
e.printStackTrace();
			logger.error("Error while saving rating for componentType = "
					+ bean.getComponentType() + ", componentId = "
					+ bean.getComponentId() + " and userId = "
					+ bean.getUserId());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController rate method due to error");

			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());

		} catch (Exception e) {

			logger.error("Error while saving rating for componentType = "
					+ bean.getComponentType() + ", componentId = "
					+ bean.getComponentId() + " and userId = "
					+ bean.getUserId());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController rate method due to error");
			 e.printStackTrace();

			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());
		}
		if (rateObject != null) {
			logger.info("Saved rating. Exiting RatingController rate method");
			return new ResponseDto<RatingDataBean>(true, rateObject);
		}
		logger.info("Rate object after saving is null. Exiting RatingController rate method");
		return new ResponseDto<RatingDataBean>(false, rateObject);
	}

	@RequestMapping(value = "/unrate", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<RatingDataBean> unrate(
			@RequestBody RatingDataBean bean) {

		logger.info("Inside RatingController unrate method");
		logger.debug("Input componentType = " + bean.getComponentType()
				+ ", componentId = " + bean.getComponentId() + " and userId = "
				+ bean.getUserId());

		RatingDataBean rateObject = new RatingDataBean();
		try {
			logger.info("Trying to deleting rating. Entering RatingsBusinessDelegate rate method");
			rateObject = businessDelegate.unrate(bean);
		} catch (MessageException e) {
			logger.error("Error while deleting rating for componentType = "
					+ bean.getComponentType() + ", componentId = "
					+ bean.getComponentId() + " and userId = "
					+ bean.getUserId());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController unrate method due to error");
			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());

		} catch (Exception e) {
			logger.error("Error while deleting rating for componentType = "
					+ bean.getComponentType() + ", componentId = "
					+ bean.getComponentId() + " and userId = "
					+ bean.getUserId());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController unrate method due to error");
			// e.printStackTrace();
			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());
		}
		if (rateObject != null) {
			logger.info("Delete rating. Exiting RatingController unrate method");
			return new ResponseDto<RatingDataBean>(true, rateObject);
		}
		logger.info("unRate object after saving is null. Exiting RatingController unrate method");
		return new ResponseDto<RatingDataBean>(false, rateObject);
	}

	/**
	 * Spring web service(POST) for getting all rating on the componentType with
	 * parentId id
	 * 
	 * @param ratingDataBean
	 * @return
	 */
	@RequestMapping(value = "/getRatingAggregation", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<RatingDataBean> getRatingAggregation(
			@RequestParam Long componentId,@RequestParam String componentType) {

		logger.info("Inside RatingController getRatingAggregation method");

		RatingDataBean ratingDataBean = new RatingDataBean();
		ratingDataBean.setComponentId(componentId);
		ratingDataBean.setComponentType(componentType);
		RatingDataBean rateObject = null;
		try {

			logger.info("Trying to get all ratings. Entering RatingsBusinessDelegate getRatingAggregation method");
			rateObject = businessDelegate.getRatingAggregation(ratingDataBean);

		} catch (MessageException e) {

			logger.error("Error while saving getting all ratings aggregation for componentType = "
					+ ratingDataBean.getComponentType());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController getRatingAggregation method due to error");

			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());

		} catch (Exception e) {

			logger.error("Error while saving getting rating aggregation  for componentType = "
					+ ratingDataBean.getComponentType());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController getRatingAggregation method due to error");
			// e.printStackTrace();

			return new ResponseDto<RatingDataBean>(false, rateObject,
					e.getMessage());
		}

		if (rateObject != null) {

			logger.info("Saved rating. Exiting RatingController getRatingAggregation method");
			return new ResponseDto<RatingDataBean>(true, rateObject);
		}

		logger.info("Exiting RatingController getRatingAggregation method");
		return new ResponseDto<RatingDataBean>(false, rateObject);
	}

	/**
	 * Spring web service(POST) for rating on the componentType with parentId id
	 * and user Id
	 * 
	 * @param ratingDataBean
	 * @return
	 */
	@RequestMapping(value = "/getRating", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<List<RatingDataBean>> getRating(
			@RequestBody RatingDataBean ratingDataBean) {

		logger.info("Inside RatingController getRating method");

		List<RatingDataBean> rating = null;
		try {

			logger.info("Trying to get a getRating. Entering RatingsBusinessDelegate getRatings method");
			rating = businessDelegate.getRatingForComponentByUser(ratingDataBean);

		} catch (MessageException e) {

			logger.error("Error while saving getting ratings  for componentType = "
					+ ratingDataBean.getComponentType()
					+ " parentId = "
					+ ratingDataBean.getParentId()
					+ " and userId = "
					+ ratingDataBean.getUserId());

			logger.error("Error is ", e);
			logger.info("Exiting RatingController getRating method due to error");

			return new ResponseDto<List<RatingDataBean>>(false, rating,
					e.getMessage());

		} catch (Exception e) {
			logger.error("Error while saving getting ratings  for componentType = "
					+ ratingDataBean.getComponentType()
					+ " parentId = "
					+ ratingDataBean.getParentId()
					+ " and userId = "
					+ ratingDataBean.getUserId());
			logger.error("Error is ", e);
			logger.info("Exiting RatingController getRating method due to error");
			// e.printStackTrace();
			return new ResponseDto<List<RatingDataBean>>(false, rating,
					e.getMessage());
		}

		if (rating != null) {
			logger.info("Saved rating. Exiting RatingController getRating method");
			return new ResponseDto<List<RatingDataBean>>(true, rating);
		}
		logger.info("Exiting RatingController getRating method");
		return new ResponseDto<List<RatingDataBean>>(false);
	}

}
