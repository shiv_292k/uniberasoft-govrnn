package com.ohmuk.folitics.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ohmuk.folitics.beans.UserOpinionDetailBean;
import com.ohmuk.folitics.businessDelegate.implementations.SearchOutputModel;
import com.ohmuk.folitics.businessDelegate.interfaces.IUserBusinessDelegate;
import com.ohmuk.folitics.cache.FoliticsCache;
import com.ohmuk.folitics.cache.SingletonCache;
import com.ohmuk.folitics.component.newsfeed.ImageSizeEnum;
import com.ohmuk.folitics.component.newsfeed.ImageTypeEnum;
import com.ohmuk.folitics.dto.ResponseDto;
import com.ohmuk.folitics.enums.UserStatus;
import com.ohmuk.folitics.exception.MessageException;
import com.ohmuk.folitics.hibernate.entity.Achievement;
import com.ohmuk.folitics.hibernate.entity.ContactDetails;
import com.ohmuk.folitics.hibernate.entity.Leader;
import com.ohmuk.folitics.hibernate.entity.PoliticalView;
import com.ohmuk.folitics.hibernate.entity.User;
import com.ohmuk.folitics.hibernate.entity.UserConnection;
import com.ohmuk.folitics.hibernate.entity.UserEmailNotificationSettings;
import com.ohmuk.folitics.hibernate.entity.UserImage;
import com.ohmuk.folitics.hibernate.entity.UserPrivacyData;
import com.ohmuk.folitics.hibernate.entity.UserProfile;
import com.ohmuk.folitics.hibernate.entity.UserUINotification;
import com.ohmuk.folitics.hibernate.entity.result.lookup.MaritalStatus;
import com.ohmuk.folitics.hibernate.entity.result.lookup.Qualification;
import com.ohmuk.folitics.hibernate.entity.result.lookup.RegionState;
import com.ohmuk.folitics.hibernate.entity.result.lookup.Religion;
import com.ohmuk.folitics.model.UserChangePassword;
import com.ohmuk.folitics.model.UserOutputModelFull;
import com.ohmuk.folitics.model.UserPersonalDetail;
import com.ohmuk.folitics.model.UserUINotificationModel;
import com.ohmuk.folitics.ouput.model.UserOutputModel;
import com.ohmuk.folitics.service.IOpinionService;
import com.ohmuk.folitics.service.IUserService;
import com.ohmuk.folitics.util.EncryptionUtil;
import com.ohmuk.folitics.util.FoliticsUtils;
import com.ohmuk.folitics.util.ThumbnailUtil;

/**
 * 
 * @author Mayank Sharma
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private volatile IUserBusinessDelegate businessDelegate;

	private static Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IOpinionService opinionService;

	@RequestMapping
	public String getUserPage() {
		return "user-page";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getAdd() {
		List<UserOutputModelFull> users = new ArrayList<>();
		users.add(UserOutputModelFull.getModel(getTestUser()));
		return new ResponseDto<UserOutputModelFull>(true, users);
	}

	/**
	 * Web service to add user
	 * 
	 * @param user
	 * @param userImageMultipart
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/addUserMultipart", headers = "content-type=multipart/*", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> add(
			@RequestPart(value = "user") User user,
			@RequestParam(value = "userImage", required = false) MultipartFile userImageMultipart,
			@RequestParam(value = "albumImage", required = false) MultipartFile albumImageMultipart) {
		logger.info("Inside UserController add method");

		User userFromDB = null;
		UserOutputModelFull userOutputModel = null;
		/**
		 * Check for user with that username
		 */
		try {
			userFromDB = businessDelegate.findByUsername(user.getUsername());
			logger.debug("User found with username: " + user.getUsername());
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in finding user with username: "
					+ user.getUsername());
			logger.info("Exiting from UserController add method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (null != userFromDB) {
			logger.debug("Username already exist with username: "
					+ user.getUsername());
			logger.info("Exiting from UserController add method");
			return new ResponseDto<UserOutputModelFull>(false, userOutputModel,
					"Username already existwith username: "
							+ user.getUsername());
		}

		user.setStatus(UserStatus.PENDING_VERIFICATION.getValue());
		// TODO: Logic based on ImageType --
		try {
			String password = user.getPassword();
			logger.info("addUserMultipart password : " + password);
			user.setPassword(EncryptionUtil.encryptBase64(password));
			user = businessDelegate.save(user);
			if (null != user) {

				List<UserImage> userImages = new ArrayList<UserImage>();
				if (userImageMultipart != null) {
					UserImage userImage = new UserImage();
					userImage.setImageType(ImageTypeEnum.USERIMAGE
							.getImageType());
					userImage.setUser(user);
					String ext = FilenameUtils.getExtension(userImageMultipart
							.getOriginalFilename());
					logger.info("File extension : " + ext);
					try {
						userImage.setImage(ThumbnailUtil.getUserImage(
								userImageMultipart.getBytes(), ext));
					} catch (Exception e) {
						// if exception then set original
						userImage.setImage(userImageMultipart.getBytes());
					}
					userImage.setFileType(ext);
					userImages.add(userImage);
				} else {
					UserImage userImage = new UserImage();
					userImage.setImage((byte[]) SingletonCache.getInstance()
							.get(FoliticsCache.KEYS.USER_IMAGE));
					userImage.setImageType(ImageTypeEnum.USERIMAGE
							.getImageType());
					userImage.setUser(user);
					String ext = "jpg";
					logger.info("File extension : " + ext);
					userImage.setFileType(ext);
					userImages.add(userImage);
				}
				if (albumImageMultipart != null) {
					UserImage albumImage = new UserImage();
					albumImage.setImage(albumImageMultipart.getBytes());
					albumImage.setImageType(ImageTypeEnum.ALBUMIMAGE
							.getImageType());
					albumImage.setUser(user);
					String ext = FilenameUtils.getExtension(userImageMultipart
							.getOriginalFilename());
					logger.info("File extension : " + ext);
					albumImage.setFileType(ext);
					try {
						albumImage.setImage(ThumbnailUtil.getImageThumbnail(
								albumImageMultipart.getBytes(), ext, 900, 300));
					} catch (Exception e) {
						// if exception then set original
						albumImage.setImage(albumImageMultipart.getBytes());
					}
					userImages.add(albumImage);
				}
				user.setUserImages(userImages);
				user = businessDelegate.update(user);
				logger.debug("set user image and album image to user");
				// sending welcome email
				try {
					String base64EncryptedToken = FoliticsUtils
							.prepareEncryptedBase64EncodedToken(
									user.getUsername(), user.getEmailId());
					StringBuilder sb = new StringBuilder();
					String encodedUrl = URLEncoder.encode(base64EncryptedToken,
							"UTF-8");
					logger.info("encodedUrl : " + encodedUrl);
					 sb.append("http://localhost:8080/user/verifyemail?token=").append(encodedUrl);
					//sb.append("http://www.govrnn.com/user/verifyemail?token=")
					//		.append(encodedUrl);
					// sb.append("http://104.154.86.37/user/verifyemail?token=").append(encodedUrl);
					logger.info("Email verification url : " + sb);
					businessDelegate.sentWelcomeEmail(user, sb.toString());
				} catch (Exception e) {
					logger.error("Error sending welcome email for user : "
							+ user);
				}
				userOutputModel = UserOutputModelFull.getModel(user);
				return new ResponseDto<UserOutputModelFull>(true,
						userOutputModel);
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
			logger.error("Exception in while add image in user");
			logger.error("Exception: " + ioException);
			logger.info("Exiting from UserController add method");
			return new ResponseDto<UserOutputModelFull>(false);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in save user with username: "
					+ user.getUsername());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController add method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

		logger.info("Exiting from UserController add method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web service is to add {@link User}
	 * 
	 * @param user
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> addUser(
			@RequestBody User user) {
		logger.info("Inside UserController addUser method");
		UserOutputModelFull userOutputModelFull = null;
		try {
			User userSaved = businessDelegate.save(user);
			userOutputModelFull = UserOutputModelFull.getModel(userSaved);
			logger.debug("User added with username: " + user.getUsername());
		} catch (Exception exception) {
			logger.error("Exception in adding user with username: "
					+ user.getUsername());
			exception.printStackTrace();
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController addUser method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		logger.info("Exiting from UserController addUser method");
		return new ResponseDto<UserOutputModelFull>(true, userOutputModelFull);
	}

	/**
	 * Web service to update user image
	 * 
	 * @param file
	 * @param userId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/uploadimage", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> uploadImage(
			@RequestParam("image") MultipartFile file,
			@RequestParam(value = "imageType") String imageType,
			@RequestParam("userId") Long userId) {
		logger.info("Inside UserController uploadImage method");
		User user = null;
		UserOutputModelFull userOutputModelFull = null;
		try {
			user = businessDelegate.uploadImage(file, userId, imageType);
			userOutputModelFull = UserOutputModelFull.getModel(user);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in upload user image with user id: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController uploadImage method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("User image is uploaded with user id: " + userId);
			logger.info("Exiting from UserController uploadImage method");
			return new ResponseDto<UserOutputModelFull>(true,
					userOutputModelFull);
		}
		logger.info("Exiting from UserController uploadImage method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web service to edit user
	 * 
	 * @author Mayank Sharma
	 * @param user
	 * @return new ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> edit(
			@RequestBody User user) {
		logger.info("Inside UserController edit method");
		UserOutputModelFull userOutputModelFull = null;
		try {
			User userExisting = businessDelegate.update(user);
			userOutputModelFull = UserOutputModelFull.getModel(userExisting);
			logger.debug("User updated with username: " + user.getUsername());
		} catch (Exception exception) {
			logger.error("Exception in updating user with username: "
					+ user.getUsername());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController edit method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		logger.info("Exiting from UserController edit method");
		return new ResponseDto<UserOutputModelFull>(true, userOutputModelFull);
	}

	/**
	 * @param userPersonalDetail
	 * @return
	 */
	@RequestMapping(value = "/updatePersonalDetails", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> updatePersonalDetails(
			@RequestBody UserPersonalDetail userPersonalDetail) {
		logger.info("Inside UserController edit method");
		UserOutputModelFull userOutputModelFull = null;
		try {
			User userExisting = businessDelegate
					.updatePersonalDetail(userPersonalDetail);
			userOutputModelFull = UserOutputModelFull.getModel(userExisting);
			logger.debug("User updated with username: "
					+ userPersonalDetail.getName());
		} catch (Exception exception) {
			logger.error("Exception in updating user with username: "
					+ userPersonalDetail.getName());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController edit method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		logger.info("Exiting from UserController edit method");
		return new ResponseDto<UserOutputModelFull>(true);
	}

	/**
	 * @param userChangePassword
	 * @return
	 */
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Boolean> changePassword(
			@RequestBody UserChangePassword userChangePassword) {
		logger.info("Inside UserController edit method");
		try {
			if (businessDelegate.changePassword(userChangePassword))
				return new ResponseDto<Boolean>(true);
			logger.debug("User updated with username: "
					+ userChangePassword.getId());
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in updating user with username: "
					+ userChangePassword.getId());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController edit method");
			return new ResponseDto<Boolean>(false);
		}
		logger.info("Exiting from UserController edit method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * @param file
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/updateUserImage", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> updateUserImage(
			@RequestParam("image") MultipartFile file,
			@RequestParam("imageType") String imageType,
			@RequestParam("userId") Long userId) {

		logger.info("Inside UserController updateImage method");
		User user = null;
		UserOutputModelFull userOutputModelFull = null;
		try {
			// Need to add delegate method
			user = businessDelegate.findUserById(userId);
			// if(imageType.equalsIgnoreCase(ImageTypeEnum.getImageTypeEnum(imageType).toString())){
			List<UserImage> userImages = new ArrayList<UserImage>();
			if (user.getUserImages() != null) {
				for (UserImage image : user.getUserImages()) {
					if (image.getImageType().equalsIgnoreCase(
							ImageTypeEnum.ALBUMIMAGE.toString())) {
						user.getUserImages().remove(image);
						UserImage albumImage = new UserImage();
						albumImage.setImage(file.getBytes());
						albumImage.setImageType(ImageTypeEnum.ALBUMIMAGE
								.getImageType());
						albumImage.setUser(user);
						userImages.add(albumImage);
					} else if (image.getImageType().equalsIgnoreCase(
							ImageTypeEnum.USERIMAGE.toString())) {
						user.getUserImages().remove(image);
						UserImage userImage = new UserImage();
						userImage.setImage(file.getBytes());
						userImage.setImageType(ImageTypeEnum.USERIMAGE
								.getImageType());
						userImage.setUser(user);
						userImages.add(userImage);
					}
				}

			}
			user.setUserImages(userImages);

			businessDelegate.update(user);
			// }

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in update user image with user id: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateImage method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("User image is updated with user id: " + userId);
			logger.info("Exiting from UserController updateImage method");
			userOutputModelFull = UserOutputModelFull.getModel(user);
			return new ResponseDto<UserOutputModelFull>(true,
					userOutputModelFull);
		} else {
			logger.info("Exiting from UserController updateImage method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/downloadUserImage", method = RequestMethod.GET)
	public @ResponseBody byte[] downloadUserImage(
			@RequestParam("userId") Long userId,
			@RequestParam("imageType") String imageType,
			@RequestParam("imageSize") String imageSize) {

		logger.info("Inside UserController downloadUserImage method");
		User user = null;
		try {
			// Need to add delegate method
			user = businessDelegate.findUserById(userId);
			if (user == null) {
				logger.debug("User not found user id: " + userId);
				logger.info("Exiting from UserController downloadUserImage method");
				return null;
			}
			byte[] bytes = null;
			String fileType = "jpg";
			System.out.println("userIamges ImageSize : " + imageSize
					+ ", imageType : " + imageType);
			if (user.getUserImages() == null
					|| user.getUserImages().size() <= 0) {
				fileType = "jpg";
				bytes = (byte[]) SingletonCache.getInstance().get(
						FoliticsCache.KEYS.USER_IMAGE);
			} else {
				for (UserImage image : user.getUserImages()) {
					if (image.getImageType().equals(
							ImageTypeEnum.getImageTypeEnum(imageType)
									.getImageType())) {
						fileType = image.getFileType();
						bytes = image.getImage();
					}
				}
			}
			if (fileType == null)
				fileType = "jpg";
			logger.info("fileType : " + fileType);
			if (bytes == null
					&& imageType.equals(ImageTypeEnum.USERIMAGE.getImageType()))
				return (byte[]) SingletonCache.getInstance().get(
						FoliticsCache.KEYS.USER_IMAGE);

			if (imageSize.equals(ImageSizeEnum.THUMBNAIL.getImageSize())) {
				return ThumbnailUtil.getImageThumbnail(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.STANDARD.getImageSize())) {
				return ThumbnailUtil.getImageOriginal(bytes, fileType);
			} else if (imageSize.equals(ImageSizeEnum.SNAPSHOT.getImageSize())) {
				return ThumbnailUtil.getImageSnapshot(bytes, fileType);
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in downloadUserImage with user id: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController downloadUserImage method");
			return null;
		}
		logger.info("Exiting from UserController downloadUserImage method");
		return null;
	}

	/**
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/deactivateUserAccount", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> deactivateUserAccount(
			@RequestParam("userId") Long userId) {
		logger.info("Inside UserController deactivateUserAccount method");
		User user = null;
		UserOutputModelFull userOutputModelFull = null;
		try {
			user = businessDelegate.deactivateUserAccount(userId);
			userOutputModelFull = UserOutputModelFull.getModel(user);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in upload deactivateUserAccount with user id: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController deactivateUserAccount method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("User deactivateUserAccount is updated with user id: "
					+ userId);
			logger.info("Exiting from UserController deactivateUserAccount method");
			return new ResponseDto<UserOutputModelFull>(true,
					userOutputModelFull);
		}
		logger.info("Exiting from UserController deactivateUserAccount method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * @param userUINotificationModel
	 * @return
	 */
	@RequestMapping(value = "/updateUserUINotificationSettings", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Boolean> updateUserUINotificationSettings(
			@RequestBody UserUINotificationModel userUINotificationModel) {
		logger.info("Inside UserController updateUserUINotificationSettings method");
		boolean status = false;
		try {
			status = businessDelegate
					.updateUserUINotificationSettings(userUINotificationModel);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in upload updateUserUINotificationSettings with user id: "
					+ userUINotificationModel.getUserId());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserUINotificationSettings method");
			return new ResponseDto<Boolean>(false);
		}
		if (status == true) {
			logger.debug("User updateUserUINotificationSettings is updated with user id: "
					+ userUINotificationModel.getUserId());
			logger.info("Exiting from UserController updateUserUINotificationSettings method");
			return new ResponseDto<Boolean>(true);
		}
		logger.info("Exiting from UserController updateUserUINotificationSettings method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * Web service to soft delete user by user id
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/deleteById", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Boolean> delete(Long id) {
		logger.info("Indide UserController delete method");
		try {
			if (businessDelegate.delete(id)) {
				logger.debug("User is soft deleted");
				return new ResponseDto<Boolean>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in deleting user with username: " + id);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController delete method");
			return new ResponseDto<Boolean>(false);
		}

		logger.debug("User is not deleted");
		logger.info("Exiting from UserController delete method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * Web service to soft delete user
	 * 
	 * @param user
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Boolean> delete(@RequestBody User user) {
		logger.info("Inside UserController delete method");
		try {
			if (businessDelegate.delete(user)) {
				logger.debug("User is soft deleted");
				logger.info("Exiting from UserController delete method");
				return new ResponseDto<Boolean>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in deleting user with username: "
					+ user.getName());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController delete method");
			return new ResponseDto<Boolean>(false);
		}
		logger.debug("User is not deleted");
		logger.info("Exiting from UserController delete method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * Web service to hard delete user by user id
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/deleteFromDbById", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Boolean> deleteFromDB(Long id) {
		logger.info("Inside UserController deleteFromDbById method");
		try {
			if (businessDelegate.deleteFromDB(id)) {
				logger.debug("User is hard deleted");
				return new ResponseDto<Boolean>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in deleting user with user id: " + id);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController deleteFromDbById method");
			return new ResponseDto<Boolean>(false);
		}

		logger.debug("User is not deleted");
		logger.info("Exiting from UserController deleteFromDbById method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * Web service to hard delete user
	 * 
	 * @param user
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/deleteFromDb", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<Boolean> deleteFromDB(
			@RequestBody User user) {
		logger.info("Inside UserController deleteFromDB method");
		try {
			if (businessDelegate.deleteFromDB(user)) {
				logger.debug("User is hard deleted");
				logger.info("Exiting from UserController deleteFromDB method");
				return new ResponseDto<Boolean>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in deleting user with username: "
					+ user.getName());
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController deleteFromDB method");
			return new ResponseDto<Boolean>(false);
		}

		logger.debug("User is not deleted");
		logger.info("Exiting from UserController deleteFromDB method");
		return new ResponseDto<Boolean>(false);
	}

	/**
	 * Web service to get all user
	 * 
	 * @return ResponseDto<UserOutputModelFull>
	 */
	// @RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getAll() {
		logger.info("Inside UserController getAll method");
		List<UserOutputModelFull> usersOM = null;
		List<User> users = null;
		try {
			users = businessDelegate.readAll();
		} catch (Exception exception) {
			logger.error("Exception in reading getAll user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAll method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (users != null) {
			return new ResponseDto<UserOutputModelFull>(true, usersOM);
		}
		logger.debug("No user found");
		logger.info("Exiting from UserController add method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	// @RequestMapping(value = "/getAllRemainingUsers", method =
	// RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getAllRemainingUsers(
			Long id) {
		logger.info("Inside UserController getAllRemainingUsers method");
		List<UserOutputModelFull> usersOM = null;
		List<User> users = null;

		try {
			users = businessDelegate.getAllRemainingUsers(id);
		} catch (Exception exception) {
			logger.error("Exception in reading getAllRemainingUsers user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAllRemainingUsers method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (users != null) {
			return new ResponseDto<UserOutputModelFull>(true, usersOM);
		}
		logger.debug("No user found");
		logger.info("Exiting from UserController add method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web businessDelegate to find user by user id
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/find", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseDto<UserOutputModelFull> find(Long id) {
		logger.info("Inside UserController find method");
		User user = null;
		UserOutputModelFull userOutputModelFull = null;
		try {
			user = businessDelegate.findUserById(id);
			userOutputModelFull = UserOutputModelFull.getModel(user);
		} catch (Exception exception) {
			logger.error("Exception in finding user with user id: " + id);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAll method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.info("Exiting from UserController getAll method");
			ResponseDto<UserOutputModelFull> responseDto = new ResponseDto<UserOutputModelFull>(
					true, userOutputModelFull);
			return responseDto;
		} else {
			logger.info("Exiting from UserController getAll method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Web businessDelegate to find user by user id
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/findByUsername", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> findByUsername(
			String username) {
		logger.info("Inside UserController findByUsername method");
		UserOutputModelFull user = null;
		try {
			user = UserOutputModelFull.getModel(businessDelegate
					.findByUsername(username));
		} catch (Exception exception) {
			logger.error("Exception in finding user with user name: "
					+ username);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController findByUsername method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.info("Exiting from UserController findByUsername method");
			ResponseDto<UserOutputModelFull> responseDto = new ResponseDto<UserOutputModelFull>(
					true, user);
			return responseDto;
		} else {
			logger.info("Exiting from UserController findByUsername method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Web service is to reset {@link User} password
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> resetPassword(Long id) {
		logger.info("Inside UserController resetPassword method");
		UserOutputModelFull user = null;
		try {
			user = UserOutputModelFull.getModel(businessDelegate
					.resetPassword(id));
		} catch (Exception exception) {
			logger.error("Exception in reseting user password with user id: "
					+ id);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController resetPassword method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("Password is reset");
			logger.info("Exiting from UserController resetPassword method");
			return new ResponseDto<UserOutputModelFull>(true, user);
		}
		logger.debug("Password is not reset");
		logger.info("Exiting from UserController resetPassword method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web service to find {@link User} from session
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/getUserFromSession", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getUserFromSession(
			HttpServletRequest request) {
		logger.info("Inside UserController getUserFromSession method");
		UserOutputModelFull user = null;
		try {
			user = UserOutputModelFull.getModel(businessDelegate
					.getUserFromSession(request));
		} catch (Exception exception) {
			logger.error("Exception in finding user from session ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getUserFromSession method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.info("Exiting from UserController getUserFromSession method");
			return new ResponseDto<UserOutputModelFull>(true, user);
		}
		logger.info("Exiting from UserController getUserFromSession method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web service is to get user privacy details by viewer
	 * 
	 * @param viewerId
	 * @param UserId
	 * @return ResponseDto<UserPrivacyData>
	 */
	@RequestMapping(value = "/viewUserProfile", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserPrivacyData> viewUserProfile(
			Long viewerId, Long userId) {
		logger.info("Inside UserController viewUserProfile method");
		try {
			List<UserPrivacyData> userPrivacyDatas = businessDelegate
					.viewUserPrivacyData(viewerId, userId);
			return new ResponseDto<UserPrivacyData>(true, userPrivacyDatas);
		} catch (Exception exception) {
			logger.error("Exception in getting user details");
			logger.error("Exception: " + exception);
			logger.info("Exiting from viewUserProfile method");
			return new ResponseDto<UserPrivacyData>(false);
		}
	}

	/**
	 * Web service is to add user connection
	 * 
	 * @param userId
	 * @param connectionId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/addConnection", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> addConnection(
			Long userId, Long connectionId) {
		logger.info("Inside UserController addConnection method");
		try {
			Long userConnectionId = businessDelegate.addConnection(userId,
					connectionId);
			if (null != userConnectionId) {
				logger.debug("User connection is added");
				logger.info("Exiting from UserController addConnection method");
				return new ResponseDto<UserOutputModelFull>(true);
			} else {
				logger.info("Exiting from UserController addConnection method");
				return new ResponseDto<UserOutputModelFull>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in adding user connection with userId: "
					+ userId + "and connectionId:" + connectionId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController addConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

	}

	@RequestMapping(value = "/getAllReligion", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Object> getAllReligion() {

		List<Religion> religions;
		try {
			religions = businessDelegate.getAllReligion();
		} catch (Exception e) {
			logger.error("CustomException while fetching the religions " + e);
			return new ResponseDto<Object>(false, null, e.getMessage());
		}
		return new ResponseDto<Object>(true, religions,
				"Sucessfully Fetched list of all religions");
	}

	@RequestMapping(value = "/getAllState", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Object> getAllState() {

		List<RegionState> states;
		try {
			states = businessDelegate.getAllState();
		} catch (Exception e) {
			logger.error("CustomException while fetching the states " + e);
			return new ResponseDto<Object>(false, null, e.getMessage());
		}
		return new ResponseDto<Object>(true, states,
				"Sucessfully Fetched list of all states");
	}

	@RequestMapping(value = "/getAllMaritalStatus", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Object> getAllMaritalStatus() {

		List<MaritalStatus> maritalstatus;
		try {
			maritalstatus = businessDelegate.getAllMaritalStatus();
		} catch (Exception e) {
			logger.error("CustomException while fetching the maritalstatus "
					+ e);
			return new ResponseDto<Object>(false, null, e.getMessage());
		}
		return new ResponseDto<Object>(true, maritalstatus,
				"Sucessfully Fetched list of all maritalstatus");
	}

	@RequestMapping(value = "/getAllQualification", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Object> getAllQualification() {

		List<Qualification> qualification;
		try {
			qualification = businessDelegate.getAllQualification();
		} catch (Exception e) {
			logger.error("CustomException while fetching the qualification "
					+ e);
			return new ResponseDto<Object>(false, null, e.getMessage());
		}
		return new ResponseDto<Object>(true, qualification,
				"Sucessfully Fetched list of all states");
	}

	/**
	 * Web service is to delete user connection
	 * 
	 * @param userId
	 * @param connectionId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/deleteConnection", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> deleteConnection(
			Long userId, Long connectionId) {
		logger.info("Inside UserController deleteConnection method");
		try {
			if (businessDelegate.deleteConnection(userId, connectionId)) {
				logger.debug("User connection is added");
				logger.info("Exiting from UserController deleteConnection method");
				return new ResponseDto<UserOutputModelFull>(true);
			} else {
				logger.info("Exiting from UserController deleteConnection method");
				return new ResponseDto<UserOutputModelFull>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in updating user connection with userId: "
					+ userId + "and connectionId:" + connectionId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController deleteConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

	}

	/**
	 * Web service is to update
	 * {@link com.ohmuk.folitics.hibernate.entity.UserConnection} status
	 * 
	 * @param userId
	 * @param connectionId
	 * @param connectionStatus
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/updateConnectionStatus", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> updateConnectionStatus(
			Long userId, Long connectionId, String connectionStatus) {
		logger.info("Inside UserController updateConnectionStatus method");
		try {
			if (businessDelegate.updateConnectionStatus(userId, connectionId,
					connectionStatus)) {
				return new ResponseDto<UserOutputModelFull>(true);
			}
			return new ResponseDto<UserOutputModelFull>(false);
		} catch (Exception exception) {
			logger.error("Exception in updating user connection status with userId: "
					+ userId + "and connectionId:" + connectionId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateConnectionStatus method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

	}

	/**
	 * Method is to get all {@link User} connection
	 * 
	 * @RequestMapping(value = "/getRequestConnection", method =
	 *                       RequestMethod.GET) public @ResponseBody
	 *                       ResponseDto<UserOutputModelFull>
	 *                       getRequestConnection(Long connectionId,String
	 *                       status) {
	 *                       logger.info("Inside UserController getUserConnection method"
	 *                       ); try { List<User>connections =
	 *                       businessDelegate.getRequestConnection
	 *                       (connectionId,status); if (null != connections) {
	 *                       logger.info(
	 *                       "Exiting from UserController getUserConnection method"
	 *                       ); return new
	 *                       ResponseDto<UserOutputModelFull>(true,
	 *                       connections); }
	 *                       logger.info("Exiting from UserController getUserConnection method"
	 *                       ); return new
	 *                       ResponseDto<UserOutputModelFull>(false); } catch
	 *                       (Exception exception) {
	 *                       logger.error("Exception in getting user connection with connectionId:"
	 *                       + connectionId); logger.error("Exception: " +
	 *                       exception);
	 *                       logger.info("Exiting from UserController getRequesteConnection method"
	 *                       ); return new
	 *                       ResponseDto<UserOutputModelFull>(false); } }
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/getUserConnection", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getUserConnection(
			Long userId) {
		logger.info("Inside UserController getUserConnection method");
		try {
			List<UserOutputModelFull> connections = null;
			;// businessDelegate.getAllConnection(userId);
			if (null != connections) {
				logger.info("Exiting from UserController getUserConnection method");
				return new ResponseDto<UserOutputModelFull>(true, connections);
			}
			logger.info("Exiting from UserController getUserConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		} catch (Exception exception) {
			logger.error("Exception in updating user connection status with userId:"
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getUserConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Method is to block {@link User}
	 * 
	 * @param userId
	 * @param blockUserId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/blockUser", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> blockUser(
			Long userId, Long blockUserId) {
		logger.info("Inside UserController blockUser method");
		try {
			if (businessDelegate.blockUser(userId, blockUserId)) {
				logger.debug("User connection is added");
				logger.info("Exiting from UserController blockUser method");
				return new ResponseDto<UserOutputModelFull>(true);
			} else {
				logger.info("Exiting from UserController blockUser method");
				return new ResponseDto<UserOutputModelFull>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in blocking user with userId: "
					+ blockUserId + "and by user with id:" + userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController blockUser method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Web service is to unblock {@link User}
	 *
	 * @param userId
	 * @param blockUserId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/unBlockUser", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> unBlockUser(
			Long userId, Long blockUserId) {
		logger.info("Inside UserController unBlockUser method");
		try {
			if (businessDelegate.unBlockUser(userId, blockUserId)) {
				logger.debug("User connection is added");
				logger.info("Exiting from UserController unBlockUser method");
				return new ResponseDto<UserOutputModelFull>(true);
			} else {
				logger.info("Exiting from UserController unBlockUser method");
				return new ResponseDto<UserOutputModelFull>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in unblocking user with userId: "
					+ blockUserId + "and by user with id:" + userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController unBlockUser method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Method is to save {@link UserUINotification} by userId
	 * 
	 * @param userUINotification
	 * @param userId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/saveUserUINotification", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> saveUserUINOtification(
			@RequestBody UserUINotification userUINotification,
			@RequestParam Long userId) {
		logger.info("Inside UserController saveUserUINOtification method");
		try {
			Long uINotificationId = businessDelegate.saveUserUINotification(
					userId, userUINotification);
			if (null != uINotificationId) {
				logger.info("Exiting from UserController saveUserUINOtification method");
				return new ResponseDto<Object>(true, uINotificationId);
			} else {
				return new ResponseDto<Object>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in saving UserUINotifiaction in userId: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController saveUserUINOtification method");
			return new ResponseDto<Object>(false);
		}
	}

	/**
	 * Method is to save {@link UserEmailNotificationSettings} by userId
	 * 
	 * @param userEmailNotificationSettings
	 * @param userId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/saveUserEmailNotificationSettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> saveUserEmailNotificationSettings(
			@RequestBody UserEmailNotificationSettings userEmailNotificationSettings,
			@RequestParam Long userId) {
		logger.info("Inside UserController saveUserEmailNotificationSettings method");
		try {
			Long userENId = businessDelegate.saveUserEmailNotificationSettings(
					userId, userEmailNotificationSettings);
			if (null != userENId) {
				logger.info("Exiting from UserController UserEmailNotificationSettings method");
				return new ResponseDto<Object>(true, userENId);
			} else {
				return new ResponseDto<Object>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception while saving UserEmailNotificationSettings in userId: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController UserEmailNotificationSettings method");
			return new ResponseDto<Object>(false);
		}
	}

	/**
	 * Method is to update {@link UserUINotification}
	 * 
	 * @param userUINotification
	 * @return ResponseDto<UserUINotification>
	 */
	@RequestMapping(value = "/updateUserUINotification", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserUINotification> updateUserUINotification(
			@RequestBody UserUINotification userUINotification) {
		logger.info("Inside UserController updateUserUINotification method");
		try {
			if (businessDelegate.updateUserUINotification(userUINotification)) {
				logger.info("Exiting from UserController updateUserUINotification method");
				return new ResponseDto<UserUINotification>(true);
			} else {
				logger.info("Exiting from UserController updateUserUINotification method");
				return new ResponseDto<UserUINotification>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all user UI notification user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserUINotification method");
			return new ResponseDto<UserUINotification>(false);
		}
	}

	/**
	 * Method is to update {@link UserEmailNotificationSettings}
	 * 
	 * @param userEmailNotificationSettings
	 * @return ResponseDto<UserEmailNotificationSettings>
	 */
	@RequestMapping(value = "/updateUserEmailNotificationSettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserEmailNotificationSettings> updateUserEmailNotificationSettings(
			@RequestBody UserEmailNotificationSettings userEmailNotificationSettings) {
		logger.info("Inside UserController updateUserEmailNotificationSettings method");
		try {
			if (businessDelegate
					.updateUserEmailNotificationSettings(userEmailNotificationSettings)) {
				logger.info("Exiting from UserController updateUserEmailNotificationSettings method");
				return new ResponseDto<UserEmailNotificationSettings>(true);
			} else {
				logger.info("Exiting from UserController updateUserEmailNotificationSettings method");
				return new ResponseDto<UserEmailNotificationSettings>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all user UI notification user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserEmailNotificationSettings method");
			return new ResponseDto<UserEmailNotificationSettings>(false);
		}
	}

	/**
	 * Web service is to get all {@link UserUINotification} by userId
	 * 
	 * @param userId
	 * @return ResponseDto<UserUINotification>
	 */
	@RequestMapping(value = "/getAllUserUINotification", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserUINotification> getAllUserUINotification(
			Long userId) {
		logger.info("Inside UserController updateUserUINotification method");
		try {
			List<UserUINotification> userUINotifications = businessDelegate
					.getAllUserUINotification(userId);
			if (userUINotifications != null) {
				return new ResponseDto<UserUINotification>(true,
						userUINotifications);
			} else {
				return new ResponseDto<UserUINotification>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all user UI notification user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserUINotification method");
			return new ResponseDto<UserUINotification>(false);
		}
	}

	/**
	 * Web service is to get all {@link UserEmailNotificationSettings} by userId
	 * 
	 * @param userId
	 * @return ResponseDto<UserEmailNotificationSettings>
	 */
	@RequestMapping(value = "/getAllUserEmailNotificationSettings", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserEmailNotificationSettings> getAllUserEmailNotificationSettings(
			Long userId) {
		logger.info("Inside UserController getAllUserEmailNotificationSettings method");
		try {
			List<UserEmailNotificationSettings> userEmailNotificationSettings = businessDelegate
					.getAllUserEmailNotificationSettings(userId);
			if (userEmailNotificationSettings != null) {
				logger.info("Exiting UserController getAllUserEmailNotificationSettings method");
				return new ResponseDto<UserEmailNotificationSettings>(true,
						userEmailNotificationSettings);
			} else {
				return new ResponseDto<UserEmailNotificationSettings>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all getAllUserEmailNotificationSettings user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAllUserEmailNotificationSettings method");
			return new ResponseDto<UserEmailNotificationSettings>(false);
		}
	}

	/**
	 * Method is to save {@link UserPrivacySettings} by userId
	 * 
	 * @param userPrivacySettings
	 * @param userId
	 * @return ResponseDto<UserPrivacySettings>
	 */
	@RequestMapping(value = "/saveUserPrivacySettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> saveUserPrivacySettings(
			@RequestBody UserPrivacyData userPrivacySettings,
			@RequestParam Long userId) {
		logger.info("Inside UserController saveUserPrivacySettings method");
		try {
			Long userPSId = businessDelegate.saveUserPrivacySettings(userId,
					userPrivacySettings);
			if (null != userPSId) {
				logger.info("Exiting from UserController saveUserPrivacySettings method");
				return new ResponseDto<Object>(true, userPSId);
			} else {
				logger.info("Exiting from UserController saveUserPrivacySettings method");
				return new ResponseDto<Object>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in saving UserPrivacySetting by userId: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController saveUserPrivacySettings method");
			return new ResponseDto<Object>(false);
		}
	}

	/**
	 * Method is to update {@link UserPrivacySettings}
	 * 
	 * @param userPrivacySettings
	 * @return ResponseDto<UserPrivacySettings>
	 */
	@RequestMapping(value = "/updateUserPrivacySettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserPrivacyData> updateUserPrivacySettings(
			@RequestBody UserPrivacyData userPrivacySettings) {
		logger.info("Inside UserController updateUserPrivacySettings method");
		try {
			if (businessDelegate.updateUserPrivacySetting(userPrivacySettings)) {
				logger.info("Exiting from UserController updateUserPrivacySettings method");
				return new ResponseDto<UserPrivacyData>(true);
			} else {
				logger.info("Exiting from UserController updateUserPrivacySettings method");
				return new ResponseDto<UserPrivacyData>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in updating UserPrivacySetting");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserPrivacySettings method");
			return new ResponseDto<UserPrivacyData>(false);
		}
	}

	/**
	 * Method is to get all {@link User} connection
	 * 
	 * @param connectionId
	 *            ,status
	 * @return
	 */
	@RequestMapping(value = "/getRequestConnection", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> getRequestConnection(
			Long connectionId, String status) {
		logger.info("Inside UserController getUserConnection method");
		try {
			List<UserOutputModelFull> connections = null;// businessDelegate.getRequestConnection(
			// connectionId, status);
			if (null != connections) {
				logger.info("Exiting from UserController getUserConnection method");
				return new ResponseDto<UserOutputModelFull>(true, connections);
			}
			logger.info("Exiting from UserController getUserConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		} catch (Exception exception) {
			logger.error("Exception in getting user connection with connectionId:"
					+ connectionId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getRequesteConnection method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
	}

	/**
	 * Web service is to get all User privacy data
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/getAllUserPrivacyData", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserPrivacyData> getAllUserPrivacyData(
			Long userId) {
		logger.info("Inside UserController getAllUserEmailNotificationSettings method");
		try {
			if (null != userId) {
				List<UserPrivacyData> userPrivacySettings = businessDelegate
						.getAllUserPrivacySettings(userId);
				return new ResponseDto<UserPrivacyData>(true,
						userPrivacySettings);
			} else {
				return new ResponseDto<UserPrivacyData>(true);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all getAllUserEmailNotificationSettings user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAllUserEmailNotificationSettings method");
			return new ResponseDto<UserPrivacyData>(false);
		}
	}

	@RequestMapping(value = "/getTestUser", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody User getTestUser() {
		return getDummyUser();
	}

	private User getDummyUser() {
		User user = new User();
		// user.setId((new Random()).nextLong());
		user.setUsername("username");
		user.setPassword("12345");
		// user.setUserprofile(getUserProfile());
		return user;
	}

	private UserProfile getUserProfile() {
		UserProfile userProfile = new UserProfile();
		userProfile.setCity("Indore");
		userProfile.setCountry("India");
		userProfile.setCurrentLocation("Metro");
		userProfile.setHobbies("Chess");
		return userProfile;
	}

	@RequestMapping(value = "/getLoggedInUser", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getLoggedInUser() {
		return FoliticsUtils.getUserName();
	}

	/**
	 * Web service is to subscribe particular user from redis
	 * 
	 * @param userId
	 * @return true if successfully subscribed
	 */

	@RequestMapping(value = "/subscribe", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> subscribe(Long userId)
			throws Exception {
		try {
			businessDelegate.subscribe(userId);
			return new ResponseDto<Object>(true);
		} catch (MessageException exception) {

			logger.error("CustomException while subscribing", exception);

			return new ResponseDto<>(exception.getMessage(), false);
		}

		catch (Exception exception) {

			logger.error("Exception while subscribing", exception);

			return new ResponseDto<>(exception.getMessage(), false);
		}

	}

	/**
	 * Web service is to unSubscribe particular user from redis
	 * 
	 * @param userId
	 * @return true if successfully unSubscribed
	 */

	@RequestMapping(value = "/unSubscribe", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> unSubscribe(Long userId)
			throws Exception {
		try {
			businessDelegate.unSubscribe(userId);
			return new ResponseDto<Object>(true);
		} catch (MessageException exception) {

			logger.error("CustomException while unSubscribing", exception);

			return new ResponseDto<>(exception.getMessage(), false);
		}

		catch (Exception exception) {

			logger.error("Exception while unSubscribing", exception);

			return new ResponseDto<>(exception.getMessage(), false);
		}
	}

	/**
	 * Web service to verifyIfUsernameExist
	 * 
	 * @return ResponseDto<username>(success)
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value = "/verifyIfUsernameExist", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserOutputModelFull> verifyIfUsernameExist(
			@RequestParam(value = "username", required = true) String username) {
		logger.info("Inside UserController getAllUsername method");
		boolean result;
		try {
			result = businessDelegate.verifyIfUsernameExist(username);
			logger.info("Username Match:" + result);
			if (result != false) {
				logger.info("user with this username:" + username + " found");
				logger.info("Exiting from UserController verifyIfUsernameExist method");
				return new ResponseDto<UserOutputModelFull>(true);
			}
			logger.info("No user with this username:" + username + " found");
			logger.info("Exiting from UserController verifyIfUsernameExist method");
			return new ResponseDto<UserOutputModelFull>(false);
		} catch (Exception exception) {
			logger.error("Exception in reading verifyIfUsernameExist method in user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController verifyIfUsernameExist method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

	}

	@RequestMapping(value = "/verifyemail", method = RequestMethod.GET)
	public @ResponseBody String verifyemail(String token) {
		logger.info("Inside UserController verifyemail method token: " + token);
		try {
			System.out.println("encodedURL " + token);
			String base64EncryptedToken = token;// URLDecoder.decode(token,
												// "UTF-8");
			System.out.println("base64EncryptedToken " + base64EncryptedToken);
			String[] userDetails = FoliticsUtils
					.parseBase64EncodedToken(base64EncryptedToken);
			User user = businessDelegate.findByUsername(userDetails[0]);
			logger.info("Username Match:" + user);
			if (user != null) {
				logger.info("user with this username:" + userDetails[0]
						+ " found");
				logger.info("Exiting from UserController verifyemail method");
				if (user.getEmailId().equals(userDetails[1])) {
					user.setStatus(UserStatus.EMAIL_VERIFIED.getValue());
					businessDelegate.update(user);
					return "Thank you for choosing Govrnn! Please login at <a href='https://www.govrnn.com'>www.govrnn.com</a>";
				} else
					return "Thank you for choosing Govrnn! Please login at <a href='https://www.govrnn.com'>www.govrnn.com</a>";
			}
			logger.info("No user with this username:" + userDetails[0]
					+ " found");
			logger.info("Exiting from UserController verifyemail method");
			return "Oops! Something went wrong. Please try after sometime. If problem persists then contact us at contact@govrnn.com.";
		} catch (Exception exception) {
			logger.error("Exception in reading verifyemail method in user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController verifyIfUsernameExist method");
			return "Oops! Something went wrong. Please try after sometime. If problem persists then contact us at contact@govrnn.com.";
		}

	}

	/**
	 * Web service is to add user connection
	 * 
	 * @param userId
	 * @param connectionId
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/checkconnection", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<UserConnection> connections(Long userId,
			Long connectionId) {
		logger.info("Inside UserController connections method");
		try {
			UserConnection userConnection = businessDelegate.connections(
					userId, connectionId);
			if (null != userConnection) {
				logger.debug("User connection is added");
				logger.info("Exiting from UserController connections method");
				return new ResponseDto<UserConnection>(true, userConnection);
			} else {
				logger.info("Exiting from UserController connections method");
				return new ResponseDto<UserConnection>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception while fatching user connection with userId: "
					+ userId + "and connectionId:" + connectionId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController connections method");
			return new ResponseDto<UserConnection>(false);
		}

	}

	/**
	 * Web service is to add user achievement
	 * 
	 * @param achievement
	 * @return ResponseDto<Achievement>
	 */

	@RequestMapping(value = "/addAchievement", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> addAchievement(
			@RequestBody Achievement achievement) {

		logger.info("Inside add achievement controller");
		try {
			Long id = businessDelegate.add(achievement);

		} catch (Exception exception) {
			logger.error("Exception while adding user achievement");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController add achievement method");
			return new ResponseDto<Object>(false);
		}

		logger.info("Exiting add achievement controller");

		return new ResponseDto<Object>(true, "successfully added achievement");

	}

	/**
	 * Web service is to add user achievement
	 * 
	 * @param achievement
	 * @return ResponseDto<Achievement>
	 */

	@RequestMapping(value = "/addLeader", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> addLeader(
			@RequestBody Leader leader) {

		logger.info("Inside add leader controller");
		try {
			Long id = businessDelegate.add(leader);

		} catch (Exception exception) {
			logger.error("Exception while adding user leader");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController add leader method");
			return new ResponseDto<Object>(false);
		}

		logger.info("Exiting add leader controller");

		return new ResponseDto<Object>(true, "successfully added leader");

	}

	/**
	 * Web service is to add user politicalView
	 * 
	 * @param achievement
	 * @return ResponseDto<Achievement>
	 */

	@RequestMapping(value = "/addPoliticalView", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> addPoliticalView(
			@RequestBody PoliticalView politicalView) {

		logger.info("Inside add politicalView controller");
		try {
			Long id = businessDelegate.add(politicalView);

		} catch (Exception exception) {
			logger.error("Exception while adding user politicalView");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController add politicalView method");
			return new ResponseDto<Object>(false);
		}

		logger.info("Exiting add politicalView controller");

		return new ResponseDto<Object>(true, "successfully added politicalView");

	}

	/**
	 * Web service is to get user achievement by id
	 * 
	 * @param userId
	 * @return ResponseDto<Achievement>
	 */

	@RequestMapping(value = "/getAchievement", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Achievement> getAchievement(
			@RequestParam Long userId) {

		logger.info("Inside get Achievement method of user controller");

		List<Achievement> achievement;

		try {
			if (userId == null) {
				logger.error("userId can not be null");
			}

			achievement = businessDelegate.getAchievement(userId);

			if (achievement == null) {
				logger.info("Exiting get achievement method of user controller");
				return new ResponseDto<Achievement>(
						"No data found in database for the corresponding userId",
						true);
			}

		} catch (Exception exception) {
			logger.error("Exception while fatching user achievement");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController get achievement method");
			return new ResponseDto<Achievement>(false);
		}

		logger.info("Exiting get Achievement method of user controller");
		return new ResponseDto<Achievement>(true, achievement,
				"successfully fatched user achievement");
	}

	/**
	 * Web service is to get user Leader by id
	 * 
	 * @param userId
	 * @return ResponseDto<Leader>
	 */

	@RequestMapping(value = "/getLeader", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Leader> getLeader(@RequestParam Long userId) {

		logger.info("Inside get leader method of user controller");

		List<Leader> leader;

		try {
			if (userId == null) {
				logger.error("userId can not be null");
			}

			leader = businessDelegate.getLeader(userId);

			if (leader == null) {
				logger.info("Exiting get leader method of user controller");
				return new ResponseDto<Leader>(
						"No data found in database for the corresponding userId",
						true);
			}

		} catch (Exception exception) {
			logger.error("Exception while fatching user leader");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController get leader method");
			return new ResponseDto<Leader>(false);
		}

		logger.info("Exiting get leader method of user controller");
		return new ResponseDto<Leader>(true, leader,
				"successfully fatched leader");
	}

	/**
	 * Web service is to get user PoliticalView by id
	 * 
	 * @param userId
	 * @return ResponseDto<PoliticalView>
	 */

	@RequestMapping(value = "/getPoliticalView", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<PoliticalView> getPoliticalView(
			@RequestParam Long userId) {

		logger.info("Inside get PoliticalView method of user controller");

		List<PoliticalView> politicalView;

		try {
			if (userId == null) {
				logger.error("userId can not be null");
			}

			politicalView = businessDelegate.getPoliticalView(userId);

			if (politicalView == null) {
				logger.info("Exiting get PoliticalView method of user controller");
				return new ResponseDto<PoliticalView>(
						"No data found in database for the corresponding userId",
						true);
			}

		} catch (Exception exception) {
			logger.error("Exception while fatching user PoliticalView");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController get PoliticalView method");
			return new ResponseDto<PoliticalView>(false);
		}

		logger.info("Exiting get PoliticalView method of user controller");
		return new ResponseDto<PoliticalView>(true, politicalView,
				"successfully fatched user PoliticalView");
	}

	@RequestMapping(value = "/updateUserContactDetailsSettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<ContactDetails> updateUserContactDetailsSettings(
			@RequestBody ContactDetails contactDetails,
			@RequestParam Long userId) {
		logger.info("Inside UserController updateUserContactDetailsSettings method");
		try {
			if (businessDelegate
					.updateUserContactDetailsSettings(contactDetails)) {
				logger.info("Exiting from UserController updateUserContactDetailsSettings method");
				return new ResponseDto<ContactDetails>(true);
			} else {
				logger.info("Exiting from UserController updateUserContactDetailsSettings method");
				return new ResponseDto<ContactDetails>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in reading all user UI notification user with userId:");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateUserEmailNotificationSettings method");
			return new ResponseDto<ContactDetails>(false);
		}
	}

	@RequestMapping(value = "/saveUserContactDetailsSettings", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> saveUserContactDetailsSettings(
			@RequestBody ContactDetails contactDetails,
			@RequestParam Long userId) {
		logger.info("Inside UserController saveUserContactDetailsSettings method");
		try {
			Long userCDId = businessDelegate.saveUserContactDetailsSettings(
					userId, contactDetails);
			if (null != userCDId) {
				logger.info("Exiting from UserController saveUserContactDetailsSettings method");
				return new ResponseDto<Object>(true, userCDId);
			} else {
				return new ResponseDto<Object>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception while saving saveUserContactDetailsSettings in userId: "
					+ userId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController saveUserContactDetailsSettings method");
			return new ResponseDto<Object>(false);
		}
	}

	/**
	 * Web service is to login {@link User} via username/email and password
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/authenticateUser", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> authenticateUser(
			@RequestParam(value = "username", required = true) String userName,
			@RequestParam(value = "password", required = true) String password) {

		logger.info("Inside UserController authienticateUser method");
		UserOutputModelFull user = null;
		try {
			user = UserOutputModelFull.getModel(businessDelegate
					.authenticateUser(userName, password));
		} catch (Exception exception) {
			logger.error("Exception in user login with user id: " + userName);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController authienticateUser method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		if (user != null) {
			logger.debug("User AuthienticateUser Success");
			logger.info("Exiting from UserController authienticateUser method");
			return new ResponseDto<UserOutputModelFull>(true, user);
		}
		logger.debug("User AuthienticateUser Un-Successfully");
		logger.info("Exiting from UserController login method");
		return new ResponseDto<UserOutputModelFull>(false);
	}

	/**
	 * Web service is for forgotPassword {@link User} via email
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> forgotPassword(
			@RequestParam(value = "emailId", required = true) String emailId) {

		logger.info("Inside UserController forgotPassword method");
		Boolean success = null;
		try {
			success = businessDelegate.forgotPassword(emailId);
		} catch (Exception exception) {
			logger.error("Exception in forgotPassword with email id: "
					+ emailId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController forgotPassword method");
			return new ResponseDto<Object>(false,
					"User doesn't exist with email : " + emailId);
		}
		if (success) {
			logger.debug("User forgotPassword Success");
			logger.info("Exiting from UserController forgotPassword method");
			return new ResponseDto<Object>(true,
					"Email has been sent on registered email id with details.");
		}
		logger.debug("User forgotPassword Un-Success");
		logger.info("Exiting from UserController forgotPassword method");
		return new ResponseDto<Object>(false,
				"User doesn't exist with email : " + emailId);
	}

	/**
	 * Web service is for forgotPassword {@link User} via email
	 * 
	 * @param id
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/forgotUserName", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<Object> forgotUserName(
			@RequestParam(value = "emailId", required = true) String emailId) {

		logger.info("Inside UserController forgotUserName method");
		Boolean success = null;
		try {
			success = businessDelegate.forgotUserName(emailId);
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception in forgotUserName with email id: "
					+ emailId);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController forgotUserName method");
			return new ResponseDto<Object>(false,
					"User doesn't exist with email : " + emailId);
		}
		if (success) {
			logger.debug("User forgotUserName Success");
			logger.info("Exiting from UserController forgotUserName method");
			return new ResponseDto<Object>(true,
					"Email has been sent on registered email id with details.");
		}
		logger.debug("User forgotUserName Un-Success");
		logger.info("Exiting from UserController forgotUserName method");
		return new ResponseDto<Object>(false,
				"User doesn't exist with email : " + emailId);
	}

	@RequestMapping(value = "/getLatestUsers", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<List<UserOutputModel>> getLatestUsers(
			@RequestParam int count) {
		logger.info("Inside UserController getLatestUsers method");
		List<UserOutputModel> users = null;
		try {
			users = businessDelegate.getLatestUsers(count);

		} catch (Exception exception) {
			logger.error("Exception in reading getLatestUsers user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAll method");
			return new ResponseDto<List<UserOutputModel>>(false);
		}
		if (users != null) {
			return new ResponseDto<List<UserOutputModel>>(true, users);
		}
		logger.debug("No user found");
		logger.info("Exiting from UserController add method");
		return new ResponseDto<List<UserOutputModel>>(false);
	}

	@RequestMapping(value = "/searchUser", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<SearchOutputModel> searchUser(
			String search,
			@RequestParam(value = "userId", required = false) Long userId)
			throws Exception {
		logger.info("Inside UserController getAll method");
		List<User> users = null;
		List<SearchOutputModel> searchOutputModels = new ArrayList<SearchOutputModel>();
		try {
			users = businessDelegate.findByUserNameOrName(search);
		} catch (Exception exception) {
			logger.error("Exception in reading getAll user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController getAll method");
			return new ResponseDto<SearchOutputModel>(false);
		}
		if (users != null) {
			searchOutputModels = businessDelegate.setSearchOutputModel(userId,
					users, searchOutputModels);
			return new ResponseDto<SearchOutputModel>(true, searchOutputModels);
		}
		logger.debug("No user found");
		logger.info("Exiting from UserController add method");
		return new ResponseDto<SearchOutputModel>(false);
	}

	@RequestMapping(value = "/updateAboutYou", method = RequestMethod.POST)
	public @ResponseBody ResponseDto<UserOutputModelFull> updateAboutYou(
			@RequestParam String summary, @RequestParam Long id) {
		logger.info("Inside UserController updateAboutYou method");
		User user = null;
		try {
			user = businessDelegate.findUserById(id);
			user.setSummary(summary);
			user = businessDelegate.update(user);
			if (user != null) {
				logger.info("Exiting from UserController updateAboutYou method");
				ResponseDto<UserOutputModelFull> responseDto = new ResponseDto<UserOutputModelFull>(
						true, UserOutputModelFull.getModel(user));
				return responseDto;
			} else {
				logger.info("Exiting from UserController updateAboutYou method");
				return new ResponseDto<UserOutputModelFull>(false);
			}
		} catch (Exception exception) {
			logger.error("Exception in finding user with user id: " + id);
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController updateAboutYou method");
			return new ResponseDto<UserOutputModelFull>(false);
		}

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/verifyIfUserEmailExist", method = RequestMethod.GET)
	public @ResponseBody ResponseDto<Boolean> verifyIfUserEmailExist(
			@RequestParam(value = "email", required = true) String email) {
		logger.info("Inside UserController verifyIfUserEmailExist method");
		boolean result;
		try {
			result = businessDelegate.verifyIfUserEmailExist(email);
			logger.info("Username Match:" + result);
			if (result != false) {
				logger.info("user with this user email:" + email + " found");
				logger.info("Exiting from UserController verifyIfUserEmailExist method");
				return new ResponseDto<Boolean>(true);
			}
			logger.info("No user with this user email:" + email + " found");
			logger.info("Exiting from UserController verifyIfUserEmailExist method");
			return new ResponseDto<Boolean>(false);
		} catch (Exception exception) {
			logger.error("Exception in reading verifyIfUserEmailExist method in user: ");
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController verifyIfUserEmailExist method");
			return new ResponseDto<Boolean>(false);
		}
	}
	
	/**
	 * Edit Contact Details {@link User}
	 * 
	 * @param user
	 * @return ResponseDto<UserOutputModelFull>
	 */
	@RequestMapping(value = "/editUserContact", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> updateUserContact(
			@RequestBody User userContactDetail, @RequestParam Long id) {
		logger.info("Inside UserController updateUserContact method");
		UserOutputModelFull userOutputModelFull = null;
		try {
			if(null != userContactDetail){
				User user = businessDelegate.findUserById(id);
				if(user != null){
					if(null != userContactDetail.getMobileNumber()){
						user.setMobileNumber(userContactDetail.getMobileNumber());
					}
					
					if(null != userContactDetail.getEmailId()){
						user.setEmailId(userContactDetail.getEmailId());
					}
					
					if(null != userContactDetail.getCity()){
						user.setCity(userContactDetail.getCity());
					}
					
					if(null != userContactDetail.getState()){
						user.setState(userContactDetail.getState());
					}
					
					if(null != userContactDetail.getDob()){
						user.setDob(userContactDetail.getDob());
					}
					user = businessDelegate.update(user);
					if (user != null) {
						logger.info("Exiting from UserController updateAboutYou method");
						ResponseDto<UserOutputModelFull> responseDto = new ResponseDto<UserOutputModelFull>(
								true, UserOutputModelFull.getModel(user));
						return responseDto;
					} else {
						logger.info("Exiting from UserController updateAboutYou method");
						return new ResponseDto<UserOutputModelFull>(false);
					}
				}
			}
		} catch (Exception exception) {
			logger.error("Exception in updating user Contact Details: "+ userContactDetail.getEmailId());
			exception.printStackTrace();
			logger.error("Exception: " + exception);
			logger.info("Exiting from UserController addUser method");
			return new ResponseDto<UserOutputModelFull>(false);
		}
		logger.info("Exiting from UserController addUser method");
		return new ResponseDto<UserOutputModelFull>(true, userOutputModelFull);
	}
	
	/**
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/opinionDetails", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseDto<UserOpinionDetailBean> getUserOpinions(
			@RequestParam("userId") Long userId,
			@RequestParam("month") String month,
			@RequestParam("year") String year,
			@RequestParam("opinionType") String opinionType) {

		logger.info("Inside UserController getUserOpinions method");
		List<UserOpinionDetailBean> opinionList = null;
		try {
			
			String opinionMonth = getMonthValue(month.toUpperCase());
			if(null == opinionMonth || "Invalid month".equalsIgnoreCase(opinionMonth)){
				return new ResponseDto<UserOpinionDetailBean>("Invalid Month.Please provide correct month.", true);
			}
			
			opinionList = opinionService.getOpinionsByUserId(userId, opinionMonth, year, opinionType);
			if (null == opinionList || opinionList.isEmpty()) {
				return new ResponseDto<UserOpinionDetailBean>("No record found in database for corresponding userId", true);
			}

		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error("Exception: " + exception);
			return new ResponseDto<UserOpinionDetailBean>(exception.getMessage(), false);
		}
		logger.info("Exiting get Achievement method of user controller");
		return new ResponseDto<UserOpinionDetailBean>(true, opinionList);
	}
	
	private String getMonthValue(String monthString){
		String monthValue;
        switch (monthString) {
            case "JANUARY"		:  	monthValue = "01";       break;
            case "FEBRUARY"		:  	monthValue = "02";       break;
            case "MARCH"		:  	monthValue = "03";       break;
            case "APRIL"		:  	monthValue = "04";       break;
            case "MAY"			:  	monthValue = "05";       break;
            case "JUNE"			:  	monthValue = "06";       break;
            case "JULY"			:  	monthValue = "07";       break;
            case "AUGUST"		:  	monthValue = "08";       break;
            case "SEPTEMBER"	:  	monthValue = "09";       break;
            case "OCTOBER"		:  	monthValue = "10";       break;
            case "NOVEMBER"		:  	monthValue = "11";       break;
            case "DECEMBER"		:  	monthValue = "12";       break;
            default	: monthValue = "Invalid month"; 		 break;
        }
        return monthValue;
	}
	
	
	@RequestMapping(value = "/editUserEducation", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody ResponseDto<UserOutputModelFull> updateUserEducationDetail(
			@RequestBody User userDetail) {
		logger.info("Inside UserController updateUserContact method");
		UserOutputModelFull userOutputModelFull = null;
		try {
			if(null != userDetail && null != userDetail.getId()){
				if(null != userDetail.getQualification()){
					User user = businessDelegate.findUserById(userDetail.getId());
					if(user != null){
						user.setQualification(userDetail.getQualification());
						user = businessDelegate.update(user);
						userOutputModelFull = UserOutputModelFull.getModel(user);
					} else{
						return new ResponseDto<UserOutputModelFull>("No Record found for this UserId",false);
					}
				}else{
					return new ResponseDto<UserOutputModelFull>("Education details required",false);
				}
			} else{
				return new ResponseDto<UserOutputModelFull>("User Id required",false);
			}
			
		} catch (Exception exception) {
			logger.error("Exception in updating user Education Details for UserId: "+ userDetail.getId());
			exception.printStackTrace();
			return new ResponseDto<UserOutputModelFull>(false);
		}
		logger.info("Exiting from UserController addUser method");
		return new ResponseDto<UserOutputModelFull>(true, userOutputModelFull);
	}
	
}
