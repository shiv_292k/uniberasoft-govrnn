package com.ohmuk.folitics.sentiment.input;

import java.io.InputStream;
import java.util.List;

import com.ohmuk.folitics.hibernate.entity.Sentiment;

public interface SentimentInputReader {
	SentimentInputModel loadSentiment(String filePath);

	Sentiment getSentimentFromInputModel(SentimentInputModel sentimentsData, String imagesPath);

	SentimentInputModel loadFile(InputStream inputStream);

	List<Sentiment> getSentimentFromInputModelForNews(SentimentInputModel inputModel);
}
