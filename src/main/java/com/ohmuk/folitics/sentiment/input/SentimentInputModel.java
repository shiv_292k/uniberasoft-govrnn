package com.ohmuk.folitics.sentiment.input;

import java.util.List;

import javax.persistence.Column;

public class SentimentInputModel {
	private String subject;
	private String description;
	private String imageUrl; 
	private String imageType;
	private String type;
	private String categories;
	private String relatedSentiments;
	private String relatedSentimentsId;
	private String keywords;
	private String feedSources;

	private List<PollInputModel> polls;
	private List<SentimentNewsInputModel> sentimentNews;
	private List<SentimentReferencesInputModel> references;

	
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getRelatedSentiments() {
		return relatedSentiments;
	}

	public void setRelatedSentiments(String relatedSentiments) {
		this.relatedSentiments = relatedSentiments;
	}

	public List<PollInputModel> getPolls() {
		return polls;
	}

	public void setPolls(List<PollInputModel> polls) {
		this.polls = polls;
	}

	public List<SentimentNewsInputModel> getSentimentNews() {
		return sentimentNews;
	}

	public void setSentimentNews(List<SentimentNewsInputModel> sentimentNews) {
		this.sentimentNews = sentimentNews;
	}

	public List<SentimentReferencesInputModel> getReferences() {
		return references;
	}

	public void setReferences(List<SentimentReferencesInputModel> references) {
		this.references = references;
	}

	/**
	 * @return the relatedSentimentsId
	 */
	public String getRelatedSentimentsId() {
		return relatedSentimentsId;
	}

	/**
	 * @param relatedSentimentsId the relatedSentimentsId to set
	 */
	public void setRelatedSentimentsId(String relatedSentimentsId) {
		this.relatedSentimentsId = relatedSentimentsId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

}
