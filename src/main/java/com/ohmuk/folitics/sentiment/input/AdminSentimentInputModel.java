package com.ohmuk.folitics.sentiment.input;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.ohmuk.folitics.dto.RelatedSentimentsDto;
import com.ohmuk.folitics.hibernate.entity.Category;

public class AdminSentimentInputModel {
	private Long id;
	private String subject;
	private String description;
	private String imageByte;
	private String imageType;
	private String type;
	private ArrayList<Category> categories;
	private String relatedSentiments;
	private ArrayList<RelatedSentimentsDto> relatedSentimentsId;
	private String keywords;
	private String feedSources;
	private String state;

	private List<PollInputModel> polls;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

	public String getRelatedSentiments() {
		return relatedSentiments;
	}

	public void setRelatedSentiments(String relatedSentiments) {
		this.relatedSentiments = relatedSentiments;
	}

	public List<PollInputModel> getPolls() {
		return polls;
	}

	public void setPolls(List<PollInputModel> polls) {
		this.polls = polls;
	}

	public ArrayList<RelatedSentimentsDto> getRelatedSentimentsId() {
		return relatedSentimentsId;
	}

	public void setRelatedSentimentsId(ArrayList<RelatedSentimentsDto> relatedSentimentsId) {
		this.relatedSentimentsId = relatedSentimentsId;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFeedSources() {
		return feedSources;
	}

	public void setFeedSources(String feedSources) {
		this.feedSources = feedSources;
	}

	public String getImageByte() {
		return imageByte;
	}

	public void setImageByte(String imageByte) {
		this.imageByte = imageByte;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "AdminSentimentInputModel [id=" + id + ", subject=" + subject + ", description=" + description
				+ ", imageByte=" + imageByte + ", imageType=" + imageType + ", type=" + type + ", categories="
				+ categories + ", relatedSentiments=" + relatedSentiments + ", relatedSentimentsId="
				+ relatedSentimentsId + ", keywords=" + keywords + ", feedSources=" + feedSources + ", state=" + state
				+ ", polls=" + polls + "]";
	}
}
