package com.ohmuk.folitics.sentiment.input;

import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Service;

import com.ohmuk.folitics.cvs.reader.input.CSVFileReader;

import au.com.bytecode.opencsv.CSVReader;

@Service
public class CSVFileReaderImpl implements CSVFileReader {

	@Override
	public CSVReader getReader(String filePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(filePath), '\t');
//		String[] record;
//		while ((record = reader.readNext()) != null) {
//			for (String value : record) {
//				System.out.print(value+"      |           "); // Debug only
//			}
//			System.out.println();
//			System.out.println("-----------------------------------------------"); // Debug only
//		}
		return reader;
	}
}
