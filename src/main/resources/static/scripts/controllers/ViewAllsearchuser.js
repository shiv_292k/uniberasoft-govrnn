'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description # MainCtrl Controller of the govrnnApp
 */
angular
	.module('govrnnApp')
	.controller(
		'ViewAllSearchUserCtrl',
		[
			'$http',
			'$scope',
			'$rootScope',
			'CreateOpinionService',
			'HttpHelper',
			'$location',
			'$window',
			'$cookies',
			function($http, $scope, $rootScope,
					 CreateOpinionService, HttpHelper, $location,$window,$cookies) {

				var urlParams = $location.search();
				console.log("componentType urls  -- "+ urlParams.search);
				
				$scope.inputsearchStr = urlParams.search;
				
				console.log("componentType -- "+ urlParams.componentType);
				
				$scope.userType=  urlParams.componentType;
				console.log("searchKeyword -- "+ urlParams.searchKeyword);
				console.log("user data -", JSON.parse(localStorage.getItem('AuthData')));
				$scope.resultSearch = [];
				$scope.totalPages = 0;
				$scope.currentPage = 0;
				$scope.pageSize = 10; // number Page Size 
				$scope.DisplayNodata =false;


				var gettoID = $cookies.getObject('userID');
				console.log("search userID",gettoID);
				
				$scope.SearchTypeForBox = false;		
				
				$scope.getConnectionList = function(strType) {
					
					
					
					if(strType==''||strType==undefined||strType==null){
						$scope.search_stringSend =urlParams.search;
						$scope.SearchTypeForBox = false;
					}
					else{
						$scope.search_stringSend =$scope.inputsearchStr;
						$scope.SearchTypeForBox = true;
					
					}
					if($scope.search_stringSend.length>2){	
						$http(
							{
								method : 'GET',
								url : $rootScope.liveHost
								+ "/user/searchUser",
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								params : {
									search : $scope.search_stringSend,
									userId : gettoID
								}
							})
							.then(
								function successCallback(response) {
									if (response.data.success === true) {
									/*	console
											.log(
												'Search : ',
												JSON
													.stringify(response.data.messages));*/
		
										
										
										
										if(response.data.messages.length ==0){
											$scope.DisplayNodata =true;
										}
										
										$scope.btnReverseStatus = true;
										$scope.OrderStatusbtn = 'Old To New';
										$scope.pageData = response.data.messages;
										$scope.orderReverseData = response.data.messages;
										$scope.resultSearch = response.data.messages;
										$scope.totalPages = Math
											.ceil($scope.pageData.length
												/ $scope.pageSize);
									} else {
										$scope.hasError = true;
										$scope.user = {};
										form.$setPristine();
										console.log("error Search");
									}
								},
								function errorCallback(response) {
									$scope.hasError = true;
									$scope.user = {};
									form.$setPristine();
									console.log("error login");
									console.log(response);
								});
						}
					
				}
				
				
				$scope.getConnectionList('');
//				$http(
//					{
//						method : 'GET',
//						url : $rootScope.liveHost
//						+ "/user/searchUser",
//						headers : {
//							'Content-Type' : 'application/x-www-form-urlencoded'
//						},
//						params : {
//							search : urlParams.search
//						}
//					})
//					.then(
//						function successCallback(response) {
//							if (response.data.success === true) {
//							/*	console
//									.log(
//										'Search : ',
//										JSON
//											.stringify(response.data.messages));*/
//
//								$scope.btnReverseStatus = true;
//								$scope.OrderStatusbtn = 'Old To New';
//								$scope.pageData = response.data.messages;
//								$scope.orderReverseData = response.data.messages;
//								$scope.resultSearch = response.data.messages;
//								$scope.totalPages = Math
//									.ceil($scope.pageData.length
//										/ $scope.pageSize);
//							} else {
//								$scope.hasError = true;
//								$scope.user = {};
//								form.$setPristine();
//								console.log("error Search");
//							}
//						},
//						function errorCallback(response) {
//							$scope.hasError = true;
//							$scope.user = {};
//							form.$setPristine();
//							console.log("error login");
//							console.log(response);
//						});

				// $scope.currentPage = 0;
				// $scope.pageSize = 10;
				//						    

				$scope.pageButtonDisabled = function(dir) {
					if (dir == -1) {
						return $scope.currentPage == 0;
					}
					return $scope.currentPage >= $scope.pageData.length
						/ $scope.pageSize - 1;
				}

				$scope.paginate = function(nextPrevMultiplier) {
					$scope.currentPage += (nextPrevMultiplier * 1);
					$scope.resultSearch = $scope.pageData
						.slice($scope.currentPage
							* $scope.pageSize);

				}

				/*$scope.shortOrderAccording = function(ord) {
					$scope.resultSearch = $scope.orderReverseData
						.slice().reverse();
					$scope.orderReverseData = $scope.resultSearch;
					$scope.pageData = $scope.resultSearch;
					console
						.log(" pageData :  - "
							+ $scope.pageData);
					console.log(" resultSearch :  - "
						+ $scope.resultSearch);
					$scope.currentPage = 0;
					// $scope.resultSearch = $scope.pageData
					// .slice(1 * $scope.pageSize);

//								console.log(" pageData After pagination:  - "
//										+ $scope.pageData);
//								console.log("length: --"
//										+ $scope.pageData.length);

					if ($scope.btnReverseStatus) {
						$scope.btnReverseStatus = false;
						$scope.OrderStatusbtn = 'New To Old'
					} else {
						$scope.btnReverseStatus = true;
						$scope.OrderStatusbtn = 'Old To New'
					}
				}*/
				
				
				$scope.shortOrderAccording = function(ord) {
					$scope.resultSearch = $scope.orderReverseData
						.slice().reverse();
					$scope.orderReverseData = $scope.resultSearch;
					$scope.pageData = $scope.resultSearch;
					console
						.log(" pageData :  - "
							+ $scope.pageData);
					console.log(" resultSearch :  - "
						+ $scope.resultSearch);
					$scope.currentPage = 0;
					// $scope.resultSearch = $scope.pageData
					// .slice(1 * $scope.pageSize);

//								console.log(" pageData After pagination:  - "
//										+ $scope.pageData);
//								console.log("length: --"
//										+ $scope.pageData.length);

					if ($scope.btnReverseStatus) {
						$scope.btnReverseStatus = false;
						/*$scope.OrderStatusbtn = 'New To Old'*/
						/*$scope.OrderStatusbtn = 'recent decending and recent ascending'*/
						$scope.OrderStatusbtn = 'recent decending'
							
					} else {
						$scope.btnReverseStatus = true;
						/*$scope.OrderStatusbtn = 'recent ascending and recent decending';*/
						$scope.OrderStatusbtn = 'recent ascending';
						/*$scope.OrderStatusbtn = 'Old To New'*/
					}
				}
				
				
				
		$scope.OnclickAddConnection = function(opinionId){
			
			if($scope.isUserLoggedIn() == true){  
				$http({
					method : 'GET',
					url : $rootScope.liveHost
					+ "/user/addConnection",
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					},
					params : {
//						componentType : urlParams.componentType,
						userId: gettoID,
						connectionId :opinionId
					}
					// data:{pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"}
				}).then(function successCallback(response) {
					if(response.data.success === true){
						/*$scope.getConnectionList();*/
						if($scope.SearchTypeForBox){								
							$scope.getConnectionList($scope.inputsearchStr);
						}
						else{
							$scope.getConnectionList('');
						}
						
					}else{
						console.log("succes : selectVote false");
						alert("Please Log in");
						if($scope.SearchTypeForBox){								
							$scope.getConnectionList($scope.inputsearchStr);
						}
						else{
							$scope.getConnectionList('');
						}
					}
				}, function errorCallback(response) {
					console.log("Error: selectVote");
				});
			}else{
				$scope.loginNow();
			}
			
			
				

				
			
				}
			} ]);
