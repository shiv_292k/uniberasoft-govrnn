'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the govrnnApp
 */
angular.module('govrnnApp')
	.controller('generalNotificationController', ['$http', '$scope', '$rootScope',	'SentimentService', 'HomeService',
		function ($http, $scope, $rootScope, SentimentService, HomeService) {

			$http({
				method: 'GET',
				url: $rootScope.liveHost+"/notification/getGeneralNotifications",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				params: {userId: 1,componentType:'General'}
			}).then(function successCallback(response) {
				if(response.data.success === true){
					console.log('getNoti : ', JSON.stringify(response.data.messages));
					$scope.TopnotificationCount = response.data.messages;
				}else{
					$scope.hasError = true;
					$scope.user = {};
					form.$setPristine();
					console.log("succes:error login");
				}
			}, function errorCallback(response) {
				$scope.hasError = true;
				$scope.user = {};
				form.$setPristine();
				console.log("error login");
				console.log(response);
			});
		}]);