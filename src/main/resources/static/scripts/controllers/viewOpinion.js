'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description # MainCtrl Controller of the govrnnApp
 */
angular
		.module('govrnnApp')
		.controller(
				'ViewOpinionCtrl',
				[
						'$http',
					    '$scope',
					    '$route',
						'$rootScope',
						'CreateOpinionService',
						'HttpHelper',
					'$location',
					'$routeParams',
					function($http, $scope, $route, $rootScope,
							CreateOpinionService, HttpHelper, $location, $routeParams) {
							$('#hola').show();
							$scope.masterReStatus = true;
							$scope.responseNull = false;
							if($location.search().id != null){
								// if($scope.isUserLoggedIn() == true){
								// }else{
								 // $scope.loginNow();
								// }
								/*:::::::::::::::::::tab section::::::::::::::::::::::::*/
								 $scope.tab=1;
									$scope.setTab = function(newTab){
										$scope.tab = newTab;};
									$scope.isSet = function(tabNum){
										return $scope.tab === tabNum;};
									
						        $scope.ClickOnSentiment = function (sentimentId) {
						       	$location.url('/?sentimentId='+sentimentId); }
						        	
						                
						        $scope.opinionId = $location.search().id;
						        if($location.search().responseId)
						        	{
						        $scope.respId=($location.search().responseId);
						        	}
						        else {
						        	$scope.respId=0;
						        }
							$http.get('/opinion/find?id=' + $scope.opinionId)
									.then(function(response) {
												if (response.data.messages != null) {
													
													$scope.opinion = response.data.messages[0];
													;
													$scope.sentimentId=$scope.opinion.sentimentOutputModel.id;
													$scope.pollId=$scope.opinion.agreeDisagreePoll.id;
													$scope.fun($scope.sentimentId);
													$scope.pollAggrigation($scope.pollId);
													$scope.pollAnsAsPerUser($scope.pollId);
													/*$scope.displayResponse();*/
													$http({
														  method: 'POST',
														  url: $rootScope.liveHost+"/common/incrementViewCount",
														  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
														  params: {
															  opinionId: $scope.opinion.id ,
														  }
														}).then(function successCallback(response) {
														}, function errorCallback(response) {
														});
												}
											});
							
							$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
									.then(
											function(response) {
												
												if (response.data.messages != null) {
													// console .log(response.data.messages);
													 $scope.opinion_responses = response.data.messages;	
												     $scope.pollId =$scope.opinion_responses.pollId
													 $scope.AgreeCount = 0;
											    	 $scope.DisagreeCount = 0;
													  for ( var res in $scope.opinion_responses) {
														if ($scope.opinion_responses[res].flag == 'Agree') {
															// console.log("agree count"+$scope.AgreeCount);
															 $scope.AgreeCount++;
														}
														if($scope.opinion_responses[res].flag == 'Disagree'){
															$scope.DisagreeCount++;
														}
													}
													for ( var res in $scope.opinion_responses) {
													//($scope.opinion_responses[res].pollId);
                                                $scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
                                              
                                                //alert($scope.opinion_responses[res].id);
                                                if($scope.opinion_responses[res].id==$location.search().responseId)
                                                	{
                                                	//alert("id matched");
                                                	//$('#supp_'+$location.search().responseId).css()
                                                	$("#supp_"+$location.search().responseId).addClass("highlighted");
                                                	$("#agst_"+$location.search().responseId).addClass("Active");
                                                	
                                                	}
													}
													
												}
											});
							$scope.textExceedslength= false;
							$('#respondOpinion').keypress(function(e) {
								var le = $('#respondOpinion').text().length;
								$scope.textExceedslength= false;
								if($('#respondOpinion').text().length >= 500) {									
									$scope.textExceedslength= true;
									return false;
								}
							});
							
			/*::::::::::....................code to submit response....................*/
							$scope.response1=  {
							   state : "",
							   created_By : "",
							   componentType : "",
							   content:"",
							    flag: "",
							    user : {
							    id : ""
					            },
							   opinion : {
							    id : ""
							   },
							   parentResponse :{
							    id : ""
							   }
							    }
							$scope.getFlagSupport = function() {
							//alert("function call for flag");
							if($scope.isUserLoggedIn() == true){ }
						    else{$scope.loginNow();}
		                    $("#resno").removeClass("respond_sec no");
		                   $("#resno").addClass("respond_sec");
							$scope.response1.flag = "Agree"
							};
							$scope.getFlagAgainst = function() {
								if($scope.isUserLoggedIn()){
								    $("#resno").removeClass("respond_sec no");
					                $("#resno").addClass("respond_sec");
									$scope.response1.flag = "Disagree";
								}
							};
							$scope.openResponse = function(parentResponseId, form){
								 if($scope.isUserLoggedIn() == true){ }
								 else{$scope.loginNow();}
								 $scope.response1.state="Active";
								 $scope.response1. created_By =$scope.getLoggedInUserId();
								 $scope.response1.componentType="response";
								 $scope.response1.user.id=$scope.getLoggedInUserId();
								 $scope.response1.opinion.id=$scope.opinionId
									 
							     $scope.response1.parentResponse.id=parentResponseId;
				 
							     if($scope.response1.content)
							    	 {
										 $scope.responseNull = false;
										 $("#resno").removeClass("respond_sec");
										 $("#resno").addClass("respond_sec  no");
										 setTimeout(function(){ 
											$scope.masterReStatus = false; 
											console.log("I am in"); console.log($scope.masterReStatus)
											$scope.$apply();
											}, 4000);
										 
										$http.post('/response/add/', JSON.stringify($scope.response1)).then(
													function(response){
														if (response.data.messages != null) {
														$scope.response1.content="";
														form.$setPristine();
														$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
														.then(
																function(response) {
																	if (response.data.messages != null) {
																		// console .log(response.data.messages);
																		 $scope.opinion_responses = response.data.messages;	
																	     $scope.pollId =$scope.opinion_responses.pollId;
																		 
															 			 $scope.AgreeCount = 0;
																		 $scope.DisagreeCount = 0;
																		 for ( var res in $scope.opinion_responses) {
																			if ($scope.opinion_responses[res].flag == 'Agree') {
																				 $scope.AgreeCount++;
																			}
																			if($scope.opinion_responses[res].flag == 'Disagree'){
																				$scope.DisagreeCount++;
																			}
																		}
																		for ( var res in $scope.opinion_responses) {
																			$scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
																		}
																	}
																});
														}});
							    	 }
							     	 else{
										 //alert("please enter your Response..!!!");
										 $scope.responseNull = true;
										 }
							};
		/*:::::::::::::::::::::::	to submit child response::::::::::::::::::::::::::::::::::::::*/
							$scope.response2=  {
							           state : "",
									   created_By : "",
									   componentType : "",
									   flag: "",
									   user : {
									    id : ""
							           },
									   opinion : {
									    id : ""
									   },
									   
									   parentResponse :{
									    id : ""
									   }
								}
								$scope.save = function(parentResponseId,ParentResponseFlag,CommentpollId){
							//	var content= $('#hello'+parentResponseId).val();
								var content= $('#hello'+parentResponseId).prop('innerHTML');
								 if($scope.isUserLoggedIn() == true){ }
								 else{$scope.loginNow();}
							     $scope.response2.state="Active";
								 $scope.response2. created_By =$scope.getLoggedInUserId();
								 $scope.response2.componentType="response";
								$scope.response2.user.id=$scope.getLoggedInUserId();
								$scope.response2.opinion.id=$scope.opinionId;
								$scope.response2.content=content;
								$scope.response2.id = CommentpollId;
								//alert("opinionid"+$scope.response1.opinion.id);
							   $scope.response2.parentResponse.id=parentResponseId;
							//alert("parentresponse id"+$scope.response2.parentResponse.id);
									     
					             //console.log($scope.response2);
						             if(ParentResponseFlag=="Agree")
									   {
						    	   $scope.response2.flag="Disagree";
						    	   
								//CreateOpinionService.data1($scope.response2);
									    }
						       else
						    	   {
						    	   $scope.response2.flag="Agree";
						    	   }
						             if($scope.response2.content)
						            	 {
						     /*  CreateOpinionService.data1($scope.response2);confirm it*/
						            	 $http.post('/response/add/', JSON.stringify($scope.response2)).then(
													function(response){
														if (response.data.messages != null) {
														$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
														.then(
																function(response) {
																	if (response.data.messages != null) {
																		// console .log(response.data.messages);
																		 $scope.opinion_responses = response.data.messages;	
																	     $scope.pollId =$scope.opinion_responses.pollId
																		 $scope.AgreeCount = 0;
																		 $scope.DisagreeCount = 0;
																		for ( var res in $scope.opinion_responses) {
																			if ($scope.opinion_responses[res].flag == 'Agree') {
																				
																				// console.log("agree count"+$scope.AgreeCount);
																				 $scope.AgreeCount++;
																			}
																			if($scope.opinion_responses[res].flag == 'Disagree'){
																				$scope.DisagreeCount++;
																			}
																		}
																		for ( var res in $scope.opinion_responses) {
																		//($scope.opinion_responses[res].pollId);
					                                                $scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
																		}
																	}
																});
														}});
						            	 }
						             	else
						            	 {
						            	 alert("Please enter your Response..!!!")
						            	 }
									    /*  if(ParentResponseFlag=="DisAgree"){
									    	 alert("parent response disagree" )
									    	 $scope.response2.flag="Agree";
									    	   
												CreateOpinionService.data1($scope.response2);
									     };*/
									     }
				/*:::::::::::::::::::::::;reply on child response:::::::::::::::::::::::::::*/
							$scope.response3=  {
									   state : "",
									   created_By : "",
									   componentType : "",
									   
									    flag: "",
									    user : {
									    id : ""
							            },
									   opinion : {
									    id : ""
									   },
									   parentResponse :{
									    id : ""
									   }
									    }
							    $scope.save1 = function(parentResponseId,ParentResponseFlag){
								 if($scope.isUserLoggedIn() == true){ }
								 else{$scope.loginNow();}
								 var content= $('#hello1'+parentResponseId).prop('innerHTML');	
							     $scope.response3.state="Active";
								 $scope.response3. created_By =$scope.getLoggedInUserId();
								 $scope.response3.componentType="response";
								$scope.response3.user.id=$scope.getLoggedInUserId();
								$scope.response3.opinion.id=$scope.opinionId;
								$scope.response3.content=content;
								
								//alert("opinionid"+$scope.response1.opinion.id);
							   $scope.response3.parentResponse.id=parentResponseId;
							   //alert("parentresponse id"+$scope.response2.parentResponse.id);
					             //console.log($scope.response2);
						             if(ParentResponseFlag=="Agree")
									   {
						    	   $scope.response3.flag="Disagree";
									    }
						       else
						    	   {
						    	   $scope.response3.flag="Agree";
						    	   }
						             if($scope.response3.content)
						            	 {
						     /*  CreateOpinionService.data1($scope.response2);confirm it*/
						            	 $http.post('/response/add/', JSON.stringify($scope.response3)).then(
													function(response){
														if (response.data.messages != null) {
														//alert("json inside service"+JSON.stringify(responseObject));
														//alert("successfully submit response");
														
														$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
														.then(
																function(response) {
																	if (response.data.messages != null) {
																		// console .log(response.data.messages);
																		 $scope.opinion_responses = response.data.messages;	
																	     $scope.pollId =$scope.opinion_responses.pollId
																		for ( var res in $scope.opinion_responses) {
																		//($scope.opinion_responses[res].pollId);
					                                                $scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
																		}
																	}
																});
														}});
						            	 }
						             else
						            	 {
						            	 alert("Please enter your Response..!!!")
						            	 }
									     }
	     /*:::::::::::::::::::::::::::: to get default poll view::::::::::::::::::::::::*/
							$scope.pollAnsAsPerUser= function(id){
								//console.log("pollid"+id);
								$http.get('/poll/getPollOption?pollId=' + id +'&userId='+$scope.getLoggedInUserId()).then(
								function(response){
									$scope.pollResp = response.data.messages[0];
								    $('#'+$scope.pollResp).attr('checked','true');
								});
						};
		/*::::::::::::::::::::::::::::::code for poll vote::::::::::::::::::::::::::::::::::::::::::::;;*/
							$scope.vote= function(pollId,pollOptnId,compType){	
								 if($scope.isUserLoggedIn() == true){ 
									$http.get('/poll/getPollOption?pollId='+pollId+'&userId='+$scope.getLoggedInUserId()).then(
										function(response){
											//alert("inside getpolloption polloptionid"+ pollOptnId + "pollId"+pollId);
											$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));	
											//alert($scope.voteRes);
											 if($scope.voteRes==pollOptnId){								
												$scope.unSelectVote(pollOptnId, pollId);
												}else if($scope.voteRes==""){									
													$scope.selectVote(pollOptnId,compType, pollId );
													}	
												else{
													$scope.selectVote(pollOptnId,compType, pollId );
												}
										});
								 }else{
									 $('#doyouagree_'+pollOptnId).attr('checked',false);
									 $scope.loginNow();
								 }
							};
							$scope.selectVote= function(pollOptnId,compType, pollId ){
								$http.post('/poll/selectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.getLoggedInUserId() +'&componentType='+compType +'&componentId='+pollId+'&parentOwnerId=').then(
										function(response){	
										//alert("inside selectvote polloptionid"+ pollOptnId + "userid="+$scope.getLoggedInUserId());
										//	$('#doyouagree_' +pollOptnId).attr('checked','true');
											//alert("inside select");							
											$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
											$scope.pollAggrigation(pollId);
									});
							};
							
							$scope.unSelectVote= function(pollOptnId, pollId){
								$http.post('/poll/unSelectVote?pollOptionId='+ pollOptnId +'&userId='+$scope.getLoggedInUserId()).then(
									function(response){		
										//alert("inside unselect vote wid polloptionid"+ pollOptnId + "userid" +$scope.getLoggedInUserId());
										//$('#doyouagree_'+pollOptnId).attr('checked','false');
										$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
										$scope.pollAggrigation(pollId);
								});
								};
								$scope.pollAggrigation=function(pollId){
								$http.get('/poll/getPollAggregationBean?pollId='+ pollId)
								.then(function(response) {
									$('#hola').hide();
											if (response.data.messages != null) {
												$scope.poll = response.data.messages[0];
											}
										});
								}
									/*:::::::::::::::::::::::api for response upvote nd downvote:::::::::::::::::::::::::::::::::::::*/
								$scope.voteResponse= function(pollId, optnId,compType,ListID) {
									console.log("ListID -- ", ListID);
									 if($scope.isUserLoggedIn() == true){ 
										$http.get('/poll/getPollOption?pollId='+ pollId + '&userId='+$scope.getLoggedInUserId())
										.then(	function(response) {
													$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));					
														if ($scope.voteRes == optnId) {
															//alert("options matched");
															$scope.unSelectVoteforResponse(pollId,optnId );
														} 
														else if($scope.voteRes=="")
														{  
															//alert("options not matched");
															$scope.selectVoteForResponse(pollId,compType, optnId,ListID);
														}else {
														   // alert("options not matched");
															$scope.selectVoteForResponse(pollId,compType, optnId,ListID);
														}
													}
										);
									 }else{
										 $scope.loginNow();
									}
									
								};
								$scope.selectVoteForResponse= function(pollId,compType, optnId,ListID ){
									//alert("inside select vote");
									$http.post('/poll/selectVote?pollOptionId='+ optnId +'&userId='+$scope.getLoggedInUserId() +'&componentType='+ compType +'&componentId='+pollId+'&parentOwnerId='+ListID).then(
											function(response){	
												$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
												/*$scope.pollAggrigationForResponse(pollId);*/
												$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
												.then(
														function(response) {
															if (response.data.messages != null) {
																// console .log(response.data.messages);
																 $scope.opinion_responses = response.data.messages;	
															     $scope.pollId =$scope.opinion_responses.pollId
																 $scope.AgreeCount = 0;
																 $scope.DisagreeCount = 0;
																for ( var res in $scope.opinion_responses) {
																	if ($scope.opinion_responses[res].flag == 'Agree') {
																		 $scope.AgreeCount++;
																		 
																	}
																	if($scope.opinion_responses[res].flag == 'Disagree'){
																		$scope.DisagreeCount++;
																	}
																}
																for ( var res in $scope.opinion_responses) {
			                                                $scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
																}
															}
														});
										});
								};
								
								$scope.unSelectVoteforResponse= function(pollId, optnId){
									$http.post('/poll/unSelectVote?pollOptionId='+ optnId +'&userId='+$scope.getLoggedInUserId()).then(
										function(response){		
											$scope.voteRes = JSOG.parse(JSOG.stringify(response.data.messages[0]));
											$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
											.then(
													function(response) {
														if (response.data.messages != null) {
															// console .log(response.data.messages);
															 $scope.opinion_responses = response.data.messages;	
														     $scope.pollId =$scope.opinion_responses.pollId
															 $scope.AgreeCount = 0;
															 $scope.DisagreeCount = 0;
															 for ( var res in $scope.opinion_responses) {
																if ($scope.opinion_responses[res].flag == 'Agree') {
																	// console.log("agree count"+$scope.AgreeCount);
																	 $scope.AgreeCount++;
																}
																if($scope.opinion_responses[res].flag == 'Disagree'){
																	$scope.DisagreeCount++;
																}

															}
															for ( var res in $scope.opinion_responses) {
															//($scope.opinion_responses[res].pollId);
		                                                     $scope.pollAggrigationForResponse($scope.opinion_responses[res].pollId);
		                                              
															}
														
														}
													});
									});
									};
									$scope.pollAggrigationForResponse=function(pollId){
										$http.get('/poll/getPollAggregationBean?pollId='+ pollId)
										.then(function(response) {
													if (response.data.messages != null) {
														$scope.responsePoll = response.data.messages[0];
													}
												});
										}
								/******* Edit and Delete Funntionality ************/
									$scope.subjectEditable =  false;
									$scope.showEditDelete = "";
									$scope.textEditable = false;
									$scope.responseContentEditable = "";
								
									if($scope.isUserLoggedIn() == true){
									   $scope.loggedInUser = $scope.getLoggedInUserId();
									   $scope.showEditDelete = $scope.loggedInUser; 
									}											
								
									function setshowEditDelete(){									
										if($scope.isUserLoggedIn() == true){
											   $scope.loggedInUser = $scope.getLoggedInUserId();
											   $scope.showEditDelete = $scope.loggedInUser; 
											}								
									}
									
									$scope.enableEditSubject = function(id,ownerId) {
										 if($scope.isUserLoggedIn() == true){ 
											 $scope.loggedInUser = $scope.getLoggedInUserId();												
											 if(ownerId == $scope.loggedInUser ){
												 $scope.subjectEditable = true;
												 $scope.showEditDelete = "";
													
											 }
										 }else{
											 $scope.loginNow();
										 }
						    		  };
						    		$scope.disableEditSubject = function(id) {
							    		    $scope.subjectEditable = false;
							    		    setshowEditDelete();
							    		    
							    	  };
							    	$scope.enableEditText = function(id,ownerId) {
							    		 if($scope.isUserLoggedIn() == true){ 
							    			 $scope.loggedInUser = $scope.getLoggedInUserId();
							    			 if(ownerId == $scope.loggedInUser ){
							    				 $scope.textEditable = true;
							    				 $scope.showEditDelete = "";
											 }						    		 
							    		 }else{$scope.loginNow();}
							    	  };
							    	 
							    	 $scope.disableEditText = function(id) {
							    		  $scope.textEditable = false;
							    		  setshowEditDelete();
							    	  };
						    		
							    	  $scope.saveOpinionSubject = function(id) {
						    				 var opinionSubject = $("#subject_"+id).val();
						    				 $http.post('/opinion/updateOpinionSubject?opinionId='+id+
						    						 '&subject='+opinionSubject).then(
														function(response){	
														if(response.data.messages[0] != null);{
															$scope.opinion.subject = response.data.messages[0].title;
														}
													});
						    				 $scope.subjectEditable = false;
						    				 setshowEditDelete();
						    			 
						    		  };
						    		  
						    		  $scope.saveOpinionText = function(id,event) {

						    				 var opiniontext = $("#text_"+id).val();
						    				 $http.post('/opinion/updateOpinionText?opinionId='+id+
						    						 '&opinionText='+opiniontext).then(
														function(response){	
														if(response.data.messages[0] != null);{
															$scope.opinion.text = response.data.messages[0].text;
														}
													});
						    				 $scope.textEditable = false;
						    				 setshowEditDelete();
						    			 
							    		  };
							    	
							    		  $scope.enableResponseEditContent = function(id,ownerId) {
							    			  if($scope.isUserLoggedIn() == true){ 							    				  
							    				  if(ownerId == $scope.loggedInUser ){
							    					  $scope.responseContentEditable = "response_content_"+id;
							    					  $scope.showEditDelete = "";									    
							    				  }
							    			  }	 else{
							    				  $scope.loginNow();
							    				  }
								    	 };
								    	 $scope.disableResponseEditContent = function(id) {
								    		    $scope.responseContentEditable = "";
								    			 setshowEditDelete();
								    			 
								    	 };
								    
								    	 
								    	  $rootScope.$on("CallParentDeleteOpinion", function(evt,id){
								    		  $scope.deleteOpinion(id);
								           });

								    	  $rootScope.$on("CallParentDeleteResponse", function(evt,id){
								    		  $scope.deleteResponse(id);
								           });

								    	 $scope.deleteOpinion = function(id) {								    		
								    		 $('#modal_delete_opinion').modal("hide");
								    		 $http.get('/opinion/deleteFromDbById?id='+id).then(
														function(response){	
														if(response.data.success){
															$location.path("/viewAllOpinion");
														}
													});
								    		  setshowEditDelete();
								    	 };								    
								    	 $scope.deleteResponse = function(id) {
								    		 $('#modal_delete_response').modal("hide");
								    		 $http.get('/response/deleteFromDBByid?id='+id).then(
														function(response){	
														if(response.data.success){
															$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
															.then(
																	function(response) {
																		if (response.data.messages != null) {
																			// console .log(response.data.messages);
																			 $scope.opinion_responses = response.data.messages;	
																		}
																	});
														}
													});
								    		  setshowEditDelete();
									    		

								    	 };
								    	 $scope.editResponse=  {
												 id:"",
								    			 content:""												   
										}
								    	 
								    	 $scope.saveResponseEditContent = function(id,event) {
								    		 if($scope.isUserLoggedIn() == true){ }
											 else{$scope.loginNow();}
						    				 var responseContent = $("#response_content_"+id).val();
						    				 $scope.editResponse.id = id;
						    				 $scope.editResponse.content = responseContent
						    				 $http.post('/response/updateResponseText' ,JSON.stringify($scope.editResponse)).then(
														function(response){	
														if(response.data.messages[0] != null);{
															$http.get('/response/getAllResponseById?opinionId='+ $scope.opinionId)
															.then(
																	function(response) {
																		if (response.data.messages != null) {
																			// console .log(response.data.messages);
																			 $scope.opinion_responses = response.data.messages;	
																		}
																	});
														}
													});
						    				 $scope.responseContentEditable = "";
						    				 setshowEditDelete();
									    		
						    			 
								    		  };
								    	
		/*:::::::::::::::::::::::::::::ApI For semtimentGraph"""""""""""""""""""""""""""""""""""*/
								$scope.fun= function(id){$http.get(
											'/opinion/getOpinionAggregation?sentimentId='+id)
									.then(
											function(response) {
												if(response.data.success){
												
												$scope.aggregate = response.data.messages[0];
												$scope.totalVotes=$scope.aggregate.totalVotes;
												$scope.supportPercentage = Math.round((($scope.aggregate.support) / ($scope.aggregate.support + $scope.aggregate.against)) * 100);
												$scope.againstPercentage = Math.round((($scope.aggregate.against) / ($scope.aggregate.support + $scope.aggregate.against)) * 100);
											
												$scope.myJson = {
													globals : {
														
														shadow : false,
														fontFamily : "Verdana",
														fontWeight : "100"
													},
													type : "ring",
													backgroundColor : "#fff",
													legend : {
														layout : "x5",
														position : "none",
														borderColor : "transparent",
														marker : {
															borderRadius : 2,
															borderColor : "‎#708090",
														}
													},
													tooltip : {
														text : "%v" + "%"
																+ "%t"
													},
													  
													plot : {
														slice:55,
														refAngle : "270",
														borderWidth : "1px",
														
														valueBox: {
															 visible:true,
															 type:"first",
											                    connected:false,
															placement: "center",
															fontweight:"normal",
															text: "",
															fontSize: "10px",
															textAlpha: 1,
														}
													},
													series :[
															{text : "against",
																values : [ $scope.againstPercentage ],
																backgroundColor : "#128807",
																
																
															},
															{ 
																text : "support",
																values : [ $scope.supportPercentage ],
																backgroundColor : "#ff7305",
																
															} ]
												};
											}
											});
								}}
						
						else{
									$location.url('/');
								}
								
								// for authentication 
								$scope.checkAuth = function(){
									if($scope.isUserLoggedIn() == true){  
									}else{
										$scope.loginNow();
									}
								}
								
								// Pinning Funntionality
								$scope.tribute.attach(document.getElementById('respondOpinion'));

								document.getElementById('respondOpinion').addEventListener('tribute-replaced', function (e) {
									if($scope.isTrend === true){
										$http({
											method: 'POST',
											url: $rootScope.liveHost+"/trend/createTrend",
											headers: {'Content-Type': 'application/json'},
											data:{"name":$scope.newTrend}
										}).then(function successCallback(response) {
											$('#hola').hide();
											if(response.data.success === true){
												$scope.trendMapingIds["#"+$scope.newTrend]= response.data.messages[0].id;
												$scope.isTrend = false;
												$("#respondOpinion").find("a.trend0").attr("href","/#/trends?id="+response.data.messages[0].id);
												$("#respondOpinion").find("a.trend0").attr("class","trend"+response.data.messages[0].id);
												console.log("success: created new trend");
											}else{
												$scope.isTrend = false;
												console.log("succes : unable to create trend");
											}
										}, function errorCallback(response) {
											$scope.isTrend = false;
											console.log("unable to create trend");
										});
									}

								});
								
							$scope.bindPinning = function(opnResId){
								debugger;
								
								$scope.tribute.attach($("#hello"+opnResId));
								$("#hello"+opnResId).on('tribute-replaced', function (e) {
									if($scope.isTrend === true){
										$http({
											method: 'POST',
											url: $rootScope.liveHost+"/trend/createTrend",
											headers: {'Content-Type': 'application/json'},
											data:{"name":$scope.newTrend}
										}).then(function successCallback(response) {
											if(response.data.success === true){
												$scope.trendMapingIds["#"+$scope.newTrend]= response.data.messages[0].id;
												$scope.isTrend = false;
												$("#hello"+opnResId).find("a.trend0").attr("href","/#/trends?id="+response.data.messages[0].id);
												$("#hello"+opnResId).find("a.trend0").attr("class","trend"+response.data.messages[0].id);
												console.log("success: created new trend");
											}else{
												$scope.isTrend = false;
												console.log("succes : unable to create trend");
											}
										}, function errorCallback(response) {
											$scope.isTrend = false;
											console.log("unable to create trend");
										});
									}

								});
							};
							
							///Added by Manoj Gupta for Upvote
							$scope.upvaote=function()
							{
								$scope.opinion_responses=[];
							}
							
						} ]);
