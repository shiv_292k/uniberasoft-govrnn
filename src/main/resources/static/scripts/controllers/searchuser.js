'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description # MainCtrl Controller of the govrnnApp
 */
angular
	.module('govrnnApp')
	.controller(
		'SearchUserCtrl',
		[
			'$http',
			'$scope',
			'$rootScope',
			'CreateOpinionService',
			'HttpHelper',
			'$location',
			'$window',
			'$cookies','$sce',
			function($http, $scope, $rootScope,
					 CreateOpinionService, HttpHelper, $location,$window,$cookies,$sce) {

				var urlParams = $location.search();
				console.log("componentType -- "+ urlParams.componentType);
				$scope.searchType =urlParams.componentType;
				$scope.userType=  urlParams.componentType;
				$scope.SearchKeyWords =urlParams.searchKeyword;
				console.log("searchKeyword -- "+ urlParams.searchKeyword);
				console.log("user data -", JSON.parse(localStorage.getItem('AuthData')));
				$scope.resultSearch = [];
				$scope.totalPages = 0;
				$scope.currentPage = 0;
				$scope.pageSize = 10;
				$scope.DisplayNodata =false;

				var gettoID = $cookies.getObject('userID');
				console.log("search userID",gettoID);
				
				
				$scope.getConnectionList = function() {
					$http(
							{
								method : 'GET',
								url : $rootScope.liveHost
								+ "/search/searchresult",
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								params : {
									componentType : urlParams.componentType,
									searchKeyword : urlParams.searchKeyword,
									userId :gettoID

								}
							})
							.then(
								function successCallback(response) {
									if (response.data.success === true) {
										
//									/*	if(){}*/
										
										if(response.data.messages.length==0){
											$scope.DisplayNodata =true;
										}
//										
									console.log("location usrl ::", urlParams.searchKeyword);
										$scope.btnReverseStatus = true;
										/*$scope.OrderStatusbtn = 'Old To New';*/
										$scope.OrderStatusbtn = 'recent ascending';
										$scope.pageData = response.data.messages;
										$scope.orderReverseData = response.data.messages;
										$scope.resultSearch = response.data.messages;
										$scope.totalPages = Math									.ceil($scope.pageData.length
												/ $scope.pageSize);
										
										$scope.UserListCount=0;
										$scope.OpinionListCount =0;
										
										angular.forEach($scope.resultSearch,function(opinion, index){
											if($scope.resultSearch[index].componentType=='Opinion'){
												$scope.OpinionListCount++;
												$scope.resultSearch[index].opinionText = $sce.trustAsHtml(opinion.text);
											if(opinion.list != undefined){
												angular.forEach(opinion.list, function(opt){
													if(opt.pollOption == "Yes"){
														$scope.resultSearch[index].agreePollId = opt.id;
													}else{
														$scope.resultSearch[index].disAgreePollId = opt.id;
													}
												});
											}
											}else if($scope.resultSearch[index].componentType=='User'){
												$scope.UserListCount++;
											}
											
											//console.log('opinion - ',opinion.description.split('&lt;').join('<').split('&gt;').join('>'));
											
											$scope.resultSearch[index].description = $sce.trustAsHtml(opinion.description.split('&lt;').join('<').split('&gt;').join('>'), '<a href="$1">$1</a>');
										});
										console.log("$scope.UserListCount", $scope.UserListCount);
										$scope.rowNum= $scope.UserListCount+5;
										console.log("$scope.OpinionListCount", $scope.rowNum);
									} else {
										$scope.hasError = true;
										$scope.user = {};
										form.$setPristine();
										console.log("error Search");
									}
								},
								function errorCallback(response) {
									$scope.hasError = true;
									$scope.user = {};
									form.$setPristine();
									console.log("error login");
									console.log(response);
								});
				}
				
				
				$scope.getConnectionList();


				// $scope.currentPage = 0;
				// $scope.pageSize = 10;
				//						    

				$scope.pageButtonDisabled = function(dir) {
					if (dir == -1) {
						return $scope.currentPage == 0;
					}
					return $scope.currentPage >= $scope.pageData.length
						/ $scope.pageSize - 1;
				}

				$scope.paginate = function(nextPrevMultiplier) {
					$scope.currentPage += (nextPrevMultiplier * 1);
					$scope.resultSearch = $scope.pageData
						.slice($scope.currentPage
							* $scope.pageSize);

				}

				$scope.shortOrderAccording = function(ord) {
					$scope.resultSearch = $scope.orderReverseData
						.slice().reverse();
					$scope.orderReverseData = $scope.resultSearch;
					$scope.pageData = $scope.resultSearch;
					console
						.log(" pageData :  - "
							+ $scope.pageData);
					console.log(" resultSearch :  - "
						+ $scope.resultSearch);
					$scope.currentPage = 0;
					// $scope.resultSearch = $scope.pageData
					// .slice(1 * $scope.pageSize);

//								console.log(" pageData After pagination:  - "
//										+ $scope.pageData);
//								console.log("length: --"
//										+ $scope.pageData.length);

					if ($scope.btnReverseStatus) {
						$scope.btnReverseStatus = false;
						/*$scope.OrderStatusbtn = 'New To Old'*/
						/*$scope.OrderStatusbtn = 'recent decending and recent ascending'*/
						$scope.OrderStatusbtn = 'recent decending'
							
					} else {
						$scope.btnReverseStatus = true;
						/*$scope.OrderStatusbtn = 'recent ascending and recent decending';*/
						$scope.OrderStatusbtn = 'recent ascending';
						/*$scope.OrderStatusbtn = 'Old To New'*/
					}
				}
				
				
				
				$scope.viewOpinion = function(opinionId){
					$location.url('/viewOpinion?id='+opinionId);
				};
				
				$scope.checkAuth = function(){
					if($scope.isUserLoggedIn() == true){  
//						$().css('','block');
					}else{
						$scope.loginNow();
					}
				}
				
				
				
				// $scope.addResponse = function(respIndex, opinionId){
					// var description = $("#rebut_resp_"+respIndex).html();
					// var pollOptionid = $("#rebut_vote_"+respIndex).val();
					// if(description != "" && pollOptionid != ""){
						// // calling setVote api
						// $http({
							// method: 'POST',
							// url: $rootScope.liveHost+"/poll/selectVote",
							// // headers: {'Content-Type': 'application/json'},
							// headers: {'Content-Type': 'application/x-www-form-urlencoded'},
							// data:$.param({pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"})
							// // data:{pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"}
						// }).then(function successCallback(response) {
							// if(response.data.success === true){
								// console.log("success: selectVote");
							// }else{
								// console.log("succes : selectVote false");
							// }
						// }, function errorCallback(response) {
							// console.log("Error: selectVote");
						// });

						// // calling response api
						// $http({
							// method: 'POST',
							// url: $rootScope.liveHost+"/response/add",
							// headers: {'Content-Type': 'application/json'},
							// data:{
								// "flag": "Agree",
								// "content": description,
								// "created_By" : $scope.getLoggedInUserId(),
								// "user": {"id":$scope.getLoggedInUserId()},
								// "opinion": {"id":opinionId},
								// "userIds": [1,3]
							// }
						// }).then(function successCallback(response) {
							// $("#rebut_resp_"+respIndex).html("");
							// if(response.data.success === true){
								// alert('Response created successfully!');
								// console.log("success: created new response");
							// }else{
								// alert('Error: unable to create response');
								// console.log("success : unable to create response");
							// }
						// }, function errorCallback(response) {
							// $("#rebut_resp_"+respIndex).html("");
							// console.log("unable to create response");
						// });
					// }else{
						// alert('Please enter your response!');
					// }
				// };
				
				// $scope.setVote = function(opinIndex, pollOptionId){
					// $("#rebut_vote_"+opinIndex).val(pollOptionId);
					// $scope.tribute.attach($("#rebut_resp_"+opinIndex));
					// $("#rebut_resp_"+opinIndex).attr("placeholder","Enter your response here.");
				// };

				/*$scope.viewOpinion = function(opinionId, form){
					$scope.closeOpinionPopup(form);
					$location.url('/viewOpinion?id='+opinionId);
				};*/

				$scope.OpnionShowMore = function(respIndex, opinionId){
					var description = $("#rebut_resp_"+respIndex).html();
					var pollOptionid = $("#rebut_vote_"+respIndex).val();
					if(description != "" && pollOptionid != ""){
						// calling setVote api
						$http({
							method : 'GET',
							url : $rootScope.liveHost
							+ "/opinion/searchOpinionbykeyword",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							params : {
//								componentType : urlParams.componentType,
								search: urlParams.searchKeyword,
//								userId :gettoID
							}
							// data:{pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"}
						}).then(function successCallback(response) {
							if(response.data.success === true){
								console.log("success: selectVote");
							}else{
								console.log("succes : selectVote false");
							}
						}, function errorCallback(response) {
							console.log("Error: selectVote");
						});

					
				}
				};
				
				$scope.OnclickAddConnection = function(opinionId){
					
					if($scope.isUserLoggedIn() == true){  
						$http({
							method : 'GET',
							url : $rootScope.liveHost
							+ "/user/addConnection",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							params : {
//								componentType : urlParams.componentType,
								userId: gettoID,
								connectionId :opinionId
							}
							// data:{pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"}
						}).then(function successCallback(response) {
							if(response.data.success === true){
								$scope.getConnectionList();
							}else{
								console.log("succes : selectVote false");
							}
						}, function errorCallback(response) {
							console.log("Error: selectVote");
						});
					}else{
						$scope.loginNow();
					}
					
				
			
				}
				
				
				
			} ]).directive('callback', function() {
				  return function(scope, element, attrs) {        
					    setTimeout(function doWork(){
					      //jquery code and plugins
						  if(attrs.sntmId == scope.sentimentIdParam){
							$(element).addClass('active');
						  }
					    }, 0);        
					  };
					});
