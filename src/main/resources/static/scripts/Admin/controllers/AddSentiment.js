'use strict';

var app = angular.module("govrnnadminApp");
window.govrnnApp=app;
app.controller("addSentimentCntrl", function ($scope, $http) {
	$scope.categories = [
	    {id: 1, name: 'Agriculture & allied activities',checked:false},
	    {id: 2, name: 'Labour & Employment',checked:false},
	    {id: 3, name: 'Commerce & Trade',checked:false},
	    {id: 4, name: 'FDI',checked:false},
	    {id: 5, name: 'Taxation',checked:false},
	    {id: 6, name: 'Banking',checked:false},
	    {id: 7, name: 'Inflation',checked:false},
	    {id:8,  name:'Core Industries',checked:false}
	  ];
	$scope.addedSentiment=[];
	$scope.selectedCategories=[];
	$scope.convertFile=function(ele) {
		var f=document.getElementById(ele.id).files[0];
		  var reader = new FileReader();
		  reader.onloadend = function(e) {
			  var image = e.target.result;
				var arr = image.split(",");
				var arr1 = arr[0].split(";");
				var arr2 = arr1[0].split(":"); 
				 $scope.image=image;
				 $scope.imageByteArr=arr[1];
				 $scope.imageType=arr2[1];
				$scope.$apply();
		  }
		  reader.readAsDataURL(f);
		};
	
	$scope.addSentiment = function() {
		$('#hola').show();
		$scope.relatedSents=[];
		angular.forEach($scope.addedSentiment,function(a,b){
			var o={"id":a}
			$scope.relatedSents.push(o);
		});
		angular.forEach($scope.categories,function(a,b){
			if($scope.categories[b].checked==true){
				var o={"id":$scope.categories[b].id,"name":$scope.categories[b].name}
				$scope.selectedCategories.push(o);
			}
			});
			$scope.uploadFile({'imageByte':$scope.imageByteArr,'subject':$scope.sentiment.subject,"description": $scope.sentiment.description,
			"keywords": $scope.sentiment.keywords,"imageType":$scope.imageType,"type": $scope.type, "categories":$scope.selectedCategories,"relatedSentimentsId":$scope.relatedSents
			});
			//,"relatedSentiments":$scope.relatedSents,"SelectedCategory":$scope.selectedCategories send your binary data via $http or $resource or do anything else with it
		}
		
	$scope.uploadFile=function(data){
		 $http({
            url: '/admin/sentiment/add',
            method: "POST",
            data: data,
            headers: {'Content-Type': 'application/json'}
        }).then(function(res){
        	$('#hola').hide();
        	document.getElementById("pollBtn").disabled = false;
        	alert("Added Sucessfully.");
        },function(err){
        	$('#hola').hide();
        	alert("Failed.");
        })
   };
	$scope.findByType=function(){
		$scope.sentimentData=[];
			$('#hola').show();
			$http({
	            url: '/admin/sentiment/getAllSentiments?sentimentType='+$scope.type,
	            method: "GET",
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(res){
	        	$scope.sentimentData=res.data.messages;
	        	$('#hola').hide();
	        	
	        },function(err){
	        	console.log('err',err);
	        	$('#hola').hide();
	        })
	};
	$scope.SelectedSentiment=function(id){
		
		if($scope.addedSentiment.indexOf(id) == -1) {
			$scope.addedSentiment.push(id);
			$('#sentbtn_'+id).val('Remove');
			}
		else{
			$scope.addedSentiment.pop(id);
			$('#sentbtn_'+id).val('Add');
	}
		
	};
		
	$('#hola').hide();
});