'use strict';

/**
 * @ngdoc service
 * @name govrnnApp.myService
 * @description
 * # myService
 * Service in the govrnnApp.
 */
angular.module('govrnnApp')
  .service('UserSettingService',['$http','$rootScope','HttpHelper',function ($http,$rootScope,HttpHelper) {
	
	  this.editPersonalDetail = function(personalDetails) {
			//alert(JSON.stringify(personalDetails));
			$http(
					{
						method : 'POST',
						url : '/user/updatePersonalDetails',
						headers : {
							'Accept': 'application/json',						
							'Content-Type' : 'application/json'
						},
						data :  JSON.stringify(personalDetails)
						
					}).then(function successCallback(response) {
						if(response.data.success==true){
							//$('#cper_details').hide();
						//alert(response.data.success);
						//$("#cper_details").hide();
		            	alert("Successfully saved your details");
						}
		            }, function errorCallback(response) {
		            	//alert("error while editing user ");
		              return {};
		            });
		};	
			
		
		this.editPassword = function(changePassword) {
			//alert(JSON.stringify(changePassword));
			$http({
						method : 'POST',
						url : '/user/changePassword',
						headers : {
							'Accept': 'application/json',						
							'Content-Type' : 'application/json'
						},
						data :  JSON.stringify(changePassword)
						
					})
					.then(function(result) {
					    // do something
					  return true;  
					})
//					.then(function successCallback(response) {
//		            	//alert("successfully edited user's password ");
//		            }, function errorCallback(response) {
//		            	//alert("error while editing user ");
//		              return {};
//		            })
		            ;
		};	
		
		this.updateNoti = function(notification) {
			//alert(JSON.stringify(notification));
			$http(
					{
						method : 'POST',
						url : '/user/updateUserUINotificationSettings',
						headers : {
							'Accept': 'application/json',						
							'Content-Type' : 'application/json'
						},
						data :  JSON.stringify(notification)
						
					}).then(function successCallback(response) {
						alert("sucessfully saved your settings");
						 $('#notiButton').hide();
						
		            }, function errorCallback(response) {
		            	alert("error");
		              return {};
		            });
		};	
		
			
			
			
}]);