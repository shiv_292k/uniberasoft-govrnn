'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
	.directive('plainOpinionModal', function () {
		return {
			templateUrl: 'templates/modal_plain_opinion.html',
			restrict: 'E',
			link: function postLink(scope,element,attributes){
				
				$('#carousel-sugest').carousel({
					interval: 3000, pause: "hover"
				});
				$(function () {
				  $('[data-toggle="tooltip"]').tooltip();
				})
			},
			controller: ['$scope', '$http', '$rootScope', 'HttpHelper', '$localStorage', '$sce', '$location', '$timeout', '$route',
				function($scope, $http, $rootScope, HttpHelper, $localStorage, $sce, $location, $timeout, $route){

					$scope.$storage = $localStorage;
					$scope.opinion = {};					
					$scope.opinion.created_By = null;
					$scope.selectedCategory = undefined;
					$scope.prmntSentiments = [];
					$scope.opinionImages = [];
					$scope.selectedOpinionImage = "";
					$scope.permanentSentimentsRaw = {};
					$scope.currentSentiments = {};
					$scope.sentimentsForSearch = [];
					$scope.selectedSntmt = {};
					$scope.selectedSntmtId = undefined; // used to redirect to the viewAllOpinion
					$scope.selectedSntmtType = "";
					$scope.selectedComponent = {};
					$scope.opinionsForRebut = {};
					$scope.searchError = false;
					$scope.searchInProgress = false;
					$scope.searchStatus = false;
					$scope.createOpinionStatus = false;
					$scope.createOpinionHasError = false;
					$scope.opinionActionStatus = false;
					$scope.hideSearch = false;
					$scope.isTrend = false; // used for pinning
					$scope.newTrend; // used for pinning
					$scope.trendMapingIds = {};
					$scope.userMapingIds = {};
					$scope.needReset = false;
					$scope.isSuportClicked = false;
					$scope.isAgainstClicked = false;
					$scope.createOpinionType = 0;
					$scope.selectedSntmtTitle = "";
					$scope.masterResponseStatus = true;
					$scope.opnImgSrchError = false;
					$scope.opinionImagesBackup = [];
					
					$scope.openOpinion = function(type, sentiment, componentId){
						if($scope.isUserLoggedIn() == true){
							$scope.selectedSntmt = {};
							$scope.searchStatus = false;
							$scope.createOpinionType = type;
							if(type == 0){ // Plain Opinion
								$scope.hideSearch = false;
								$scope.needReset = true;
								$scope.opinion.onComponentType = "Sentiment";
							}else if(type == 1){
								$scope.opinion.onComponentType = "Sentiment";
								$scope.opinion.componentId = sentiment.id;
								$scope.findSentiment(sentiment.id, 'Sentiment');
								$scope.hideSearch = true;
								$scope.needReset = false;
							}else if(type == 2){
								$scope.opinion.onComponentType = "Reference";
								$scope.opinion.componentId = componentId;
								$scope.hideSearch = true;
								$scope.needReset = false;
								$scope.findSentiment(sentiment.id, 'Reference', componentId);
							}else{
								$scope.opinion.onComponentType = "SentimentNews";
								$scope.opinion.componentId = componentId;
								$scope.hideSearch = true;
								$scope.needReset = false;
								$scope.findSentiment(sentiment.id, 'SentimentNews', componentId);
							}
							$('#opinion_plain').modal("show");
						}else{
							$scope.loginNow();
						}
					};

					$('#searchSentFrmInput').keyup(function(e) {
						if($('#searchSentFrmInput').val().length > 0) {
							// $("#sentimentDropDwn").dropdown("toggle");
							$("#sentimentDropDwn").parent(".dropdown").addClass("open");
							$("#sentimentDropDwn").attr("aria-expanded",true);
						}else{
							$("#sentimentDropDwn").parent(".dropdown").removeClass("open");
							$("#sentimentDropDwn").attr("aria-expanded",false);
						}
						$('#searchSentFrmInput').focus();
					});

					// This function reuses the sentiment loaded in the home page
					// var loadSentiment = function(sentiment){
						// $scope.selectedSntmt.opinionAggregation = {};
						// var findResult = HttpHelper.create('GET',$rootScope.liveHost+'/opinion/getOpinionAggregation?sentimentId='+sentiment.id);
						// findResult.then(function(response){
							// if(response.data.success){
								// if(response.data.messages.length > 0){
									// var aggre = response.data.messages[0];
									// $scope.selectedSntmt.opinionAggregation.support = ((aggre.support)/(aggre.support+aggre.against))*100;
									// $scope.selectedSntmt.opinionAggregation.against = ((aggre.against)/(aggre.support+aggre.against))*100;
								// }
							// }else{
								// $scope.selectedSntmt.opinionAggregation.support = 0;
								// $scope.selectedSntmt.opinionAggregation.against = 0;
							// }

						// }, function(error){
							// console.log(error);
						// });

						// $scope.searchStatus = true;

						// // Assigning the selected sentiment to popup
						// $scope.selectedSntmt.id = sentiment.id;
						// $scope.selectedSntmt.subject = sentiment.subject;
						// $scope.selectedSntmt.image = sentiment.image;
						// $scope.selectedSntmt.categoryOutputModels = sentiment.categories;

						// // Temporary assignment
						// $scope.selectedComponent.id = sentiment.id;
						// $scope.selectedComponent.title = sentiment.subject;
						// $scope.selectedComponent.image = sentiment.image;
						// $scope.selectedComponent.description = sentiment.description;
					// };

					// This function reuses the sentiment loaded in the home page
					// var loadComponent = function(selectedSentiment,componentId, onComponentType){
						// $scope.searchStatus = true;
						// $scope.selectedSntmt.opinionAggregation = {};
						// var findResult = HttpHelper.create('GET',$rootScope.liveHost+'/opinion/getOpinionAggregation?sentimentId='+selectedSentiment.id);
						// findResult.then(function(response){
							// if(response.data.success){
								// if(response.data.messages.length > 0){
									// var aggre = response.data.messages[0];
									// $scope.selectedSntmt.opinionAggregation.support = ((aggre.support)/(aggre.support+aggre.against))*100;
									// $scope.selectedSntmt.opinionAggregation.against = ((aggre.against)/(aggre.support+aggre.against))*100;
								// }else{
									// $scope.selectedSntmt.opinionAggregation.support = 0;
									// $scope.selectedSntmt.opinionAggregation.against = 0;
								// }
							// }else{
								// $scope.selectedSntmt.opinionAggregation.support = 0;
								// $scope.selectedSntmt.opinionAggregation.against = 0;
							// }
						// }, function(error){
							// console.log(error);
						// });
						// if(onComponentType === "SentimentNews"){
							// angular.forEach(selectedSentiment.sentimentNews, function(news){
								// if(news.id === componentId){
									// $scope.selectedComponent.id = news.id;
									// $scope.selectedComponent.title = news.htmlText;
									// $scope.selectedComponent.image = news.previewImageLink;
									// $scope.selectedComponent.description = news.plainText;
								// }
							// });
							// // Assigning the selected sentiment to popup
							// $scope.selectedSntmt.id = selectedSentiment.id;
							// $scope.selectedSntmt.subject = selectedSentiment.subject;
							// $scope.selectedSntmt.image = selectedSentiment.image;
							// $scope.selectedSntmt.categoryOutputModels = selectedSentiment.categories;
						// }else{
							// angular.forEach(selectedSentiment.attachments, function(refe){
								// if(refe.id === componentId){
									// $scope.selectedComponent.id = refe.id;
									// $scope.selectedComponent.title = refe.title;
									// if(refe.attachmentFile)
										// $scope.selectedComponent.image = refe.attachmentFile.data;
									// $scope.selectedComponent.description = refe.description;
								// }
							// });
							// // Assigning the selected sentiment to popup
							// $scope.selectedSntmt.id = selectedSentiment.id;
							// $scope.selectedSntmt.subject = selectedSentiment.subject;
							// $scope.selectedSntmt.image = selectedSentiment.image;
							// $scope.selectedSntmt.categoryOutputModels = selectedSentiment.categories;
						// }
					// } ;

					// This function is used in popup, when user clicks on slide or dropdown
					$scope.findSentiment = function(sentimentId, componentType, componentId){
						$(".loader").show();
						$scope.resetOpinionPopup($scope.opinionFrm);
						$scope.selectedComponent = {};
						var sntmPromise = HttpHelper.create('GET',$rootScope.liveHost+'/sentiment/findSentiment?id='+sentimentId);
						sntmPromise.then(function(response){
							if(response.data.success){
								// Assigning the selected sentiment to popup
								$scope.selectedSntmt = response.data.messages[0];
								$scope.selectedSntmtId = $scope.selectedSntmt.id;
								$scope.selectedSntmtType = $scope.selectedSntmt.type;
								$scope.searchInput = $scope.selectedSntmt.subject;
								$scope.selectedComponent.description = $scope.selectedSntmt.description;
								
								// This will change based on the component assignment
								if(componentType != undefined){
									if(componentType == "Sentiment"){
										console.log('inside '+componentType);
										$scope.selectedComponent.title = $scope.selectedSntmt.subject;
										$scope.selectedComponent.image = "data:image/png;base64,"+$scope.selectedSntmt.image;
										$scope.selectedComponent.description = $scope.selectedSntmt.description;
									}else if(componentType == "SentimentNews"){
										console.log('inside '+componentType);
										angular.forEach($scope.selectedSntmt.sentimentNews, function(news){
											if(news.id === componentId){
												$scope.selectedComponent.id = news.id;
												$scope.selectedComponent.title = news.htmlText;
												if(news.previewImagePath)
													$scope.selectedComponent.image = "data:image/png;base64,"+news.previewImagePath;
												else
													$scope.selectedComponent.image = "/images/staticNews.png";
												$scope.selectedComponent.description = news.plainText;
											}
										});
									}else if(componentType == "Reference"){
										console.log('inside '+componentType);
										angular.forEach($scope.selectedSntmt.references, function(refe){
											if(refe.id === componentId){
												$scope.selectedComponent.id = refe.id;
												$scope.selectedComponent.title = refe.title;
												$scope.selectedComponent.image = refe.iconUrl;
												$scope.selectedComponent.description = refe.description;
											}
										});
									}									
								}
								$scope.selectedSntmt.opinionAggregation = {};
								var findResult = HttpHelper.create('GET',$rootScope.liveHost+'/opinion/getOpinionAggregation?sentimentId='+sentimentId);
								findResult.then(function(response){
									if(response.data.success){
										if(response.data.messages.length > 0){
											var aggre = response.data.messages[0];
											$scope.selectedSntmt.opinionAggregation.support = Math.round(((aggre.support)/(aggre.support+aggre.against))*100);
											$scope.selectedSntmt.opinionAggregation.against = Math.round(((aggre.against)/(aggre.support+aggre.against))*100);
										}else{
											$scope.selectedSntmt.opinionAggregation.support = 0;
											$scope.selectedSntmt.opinionAggregation.against = 0;
										}
									}
									else{
										$scope.selectedSntmt.opinionAggregation.support = 0;
										$scope.selectedSntmt.opinionAggregation.against = 0;
									}
								}, function(error){
									console.log(error);
								});
								
								// Formatting the Opinion image suggestions
								// var data = $scope.selectedSntmt.imageUrls;
								console.log($scope.selectedSntmt.imageUrls);
							/*	var data = [
								"/images/sentiments/Communalism/img1.jpg",
								"/images/sentiments/Communalism/img2.jpg",
								"/images/sentiments/Communalism/img3.jpg",
								"/images/sentiments/Communalism/img4.jpg",
								"/images/sentiments/Communalism/img5.jpg",
								"/images/sentiments/Communalism/img6.jpg",
								"/images/sentiments/Communalism/img7.jpg",
								"/images/sentiments/Communalism/img8.jpg",
								"/images/sentiments/Communalism/img9.jpg",
								'/images/sentiments/4 Price rise in india - is government doing enough  /userImages/post_8_2017-04-14 16:55:30.831.png',
								'/images/sentiments/4 Price rise in india - is government doing enough  /userImages/bug-count-dipaly_8_2017-04-14 17:04:50.396.png'								
								]; */
								var data = $scope.selectedSntmt.imageUrls;
								var imageLength = data.length;

								for (var i = 0; i < imageLength; i += 3) {
									$scope.opinionImages.push({
										'first' : data[i],
										'second' : data[i + 1],
										'third' : data[i + 2]
									});
									$scope.opinionImagesBackup.push({
										'first' : data[i],
										'second' : data[i + 1],
										'third' : data[i + 2]
									});
								}
								console.log($scope.opinionImages);
								$('#carousel-opinion-imgs').carousel({
									interval: false
								});
								if(imageLength > 0){
									$scope.setOpinionImage($scope.opinionImages[0].first,0,'_first');
								}
								
								$(".loader").hide();
							}
						
						}, function(error){
							console.log(error);
						});	
						

						$scope.opinion.onComponentType = "Sentiment";
						$scope.opinion.componentId = sentimentId;

						$scope.searchStatus = true;
						$('#permntsntm').collapse("hide");
					} ;

				/*	$('#searchSentFrmInput').keyup(function(e) {
					console.log($('#searchSentFrmInput').val().length);
					// Enter pressed?

					// if(e.which == 10 || e.which == 13) {
					if($('#searchSentFrmInput').val().length >= 3) {
					//    $scope.searchInProgress = true;
					var searchResult = HttpHelper.create('GET',$rootScope.liveHost+'/tag/sentiment?match='+$scope.searchInput);
					searchResult.then(function(response){
					$scope.sentimentSearchResult = response.data.messages;
					//  console.log($scope.sentimentSearchResult);
					var searchSuggestion = [];
					for (var i = 0; i < response.data.messages.length; i++) {
					searchSuggestion.push({
					index : i, label : response.data.messages[i].title
					});
					}

					$('.typeahead').typeahead({
					hint: true,
					highlight: true, // Enable substring highlighting
					minLength: 1, // Specify minimum characters required for showing result
					source: function (query, process) {
					// sentiments = [];
					// map = {};

					// var data = searchSuggestion;

					// $.each(data, function (i, sentiment) {
					// map[sentiment.label] = sentiment;
					// sentiments.push(sentiment.label);
					// });

					// process(sentiments);

					states = [];
					map = {};

					var data = [
					{"stateCode": "CA", "stateName": "California"},
					{"stateCode": "AZ", "stateName": "Arizona"},
					{"stateCode": "NY", "stateName": "New York"},
					{"stateCode": "NV", "stateName": "Nevada"},
					{"stateCode": "OH", "stateName": "Ohio"}
					];

					$.each(data, function (i, state) {
					map[state.stateName] = state;
					states.push(state.stateName);
					});

					process(states);
					},
					updater: function (item) {
					console.log(item);
					//	$scope.selectSentiment(map[item].index);
					return item;
					}
					});	
					 $scope.searchInProgress = false;
					}, function(error){
					console.log(error);
					$scope.searchInProgress = false;
					$scope.searchError = true;
					});

					}
					});

					$scope.selectSentiment = function(index){
					console.log('selecting sentiment of '+index);
					$scope.selectedSntmt = $scope.sentimentSearchResult[index];
					$scope.searchStatus = true;
					} */

					$scope.prev = function(slide) {
						$(slide).carousel("prev");
					};
					$scope.next = function(slide) {
						$(slide).carousel("next");
					};

					var permaResult = HttpHelper.create('GET',$rootScope.liveHost+'/sentiment/getSentimentNameList?type=Permanent');
					permaResult.then(function(response){
						if(response.data.success){
							$scope.permanentSentimentsRaw = response.data.messages;
							
							angular.forEach($scope.permanentSentimentsRaw,function(sntmnt){
								$scope.sentimentsForSearch.push({"id":sntmnt.id, "subject":sntmnt.subject+' (Permanent)'});
							});
							var data = response.data.messages;
							var sentimentLength = data.length;

							for (var i = 0; i < sentimentLength; i += 3) {
								$scope.prmntSentiments.push({
									'first' : data[i],
									'second' : data[i + 1],
									'third' : data[i + 2]
								});
							}
						}
					}, function(error){
						console.log(error);
					});

					var currResult = HttpHelper.create('GET',$rootScope.liveHost+'/sentiment/getSentimentNameList?type=Temporary');
					currResult.then(function(response){
						$scope.currentSentiments = response.data.messages;
						angular.forEach($scope.currentSentiments,function(sntmnt){
							$scope.sentimentsForSearch.push({"id":sntmnt.id, "subject":sntmnt.subject+' (Current)'});
						});
					}, function(error){
						console.log(error);
					});

					/*$scope.getAggrigation = function(sentiId){
					 var aggreResult = HttpHelper.create('GET',$rootScope.liveHost+'/opinion/getOpinionAggregation?sentimentId='+sentiId);
					 aggreResult.then(function(response){
					 // $scope.selectedSntmt
					 }, function(error){
					 console.log(error);
					 });
					 };*/

					// Function : CreateOpinion Action		
					$scope.supportOrAgainst = function(vote){
						if(vote === true){
							$scope.opinion.type = "Pro";
							$scope.isSuportClicked = true;
							$scope.isAgainstClicked = false;
						}else{
							$scope.opinion.type = "Anti";
							$scope.isSuportClicked = false;
							$scope.isAgainstClicked = true;
						}
						$scope.opinionActionStatus = true;
					};

					$scope.setCategory = function(categoryId, $event){
						if($event != undefined){
							
							//if($($event.currentTarget).attr('class')=="ng-binding active"){
								//$($event.currentTarget).removeClass("active");
								//return;
							//}
							//else
								//{
								//$($event.currentTarget).addClass("active");
								//}
							$('html').click();
							$(".opin_on ul.list_item_bx li a").removeClass("active");
							$($event.currentTarget).addClass("active");
						}
						$scope.selectedCategory = categoryId;
					};
					$scope.setOpinionImage = function(imageUrl,index,boxname){
						$scope.selectedOpinionImage = imageUrl;
						$("#carousel-opinion-imgs div.item div.selec").removeClass("active-opn-img");
						$("#opnImg_"+index+boxname).addClass("active-opn-img");
						$('#carousel-opinion-imgs').carousel(index);
					};


					/* Implementing pinning functionality */
					function searchUser(text, cb) {
						var rspon = HttpHelper.create('GET',$rootScope.liveHost+'/tag/user?match='+text);
						rspon.then(function(response){
							cb(response.data.messages[0]);
						}, function(error){
							cb([]);
						});
					}

					function searchSentiment(text, cb) {
						var rspon = HttpHelper.create('GET',$rootScope.liveHost+'/tag/sentiment?match='+text);
						rspon.then(function(response){
							cb(response.data.messages);
						}, function(error){
							cb([]);
						});
					}

					function searchTrend(text, cb) {
						var rspon = HttpHelper.create('GET',$rootScope.liveHost+'/trend/matchingTrends?name='+text);
						rspon.then(function(response){
							console.log('calling trend api success');
							if(response.data.messages[0].length > 0){
								cb(response.data.messages[0]);
							}else{
								$scope.isTrend = true;
								$scope.newTrend = text;
								cb([{"id":0,"title":text}]);
							}
						}, function(error){
							console.log('calling trend api error');
							$scope.isTrend = true;
							$scope.newTrend = text;
							cb([{"id":0,"title":text}]);
						});
					}

					$scope.tribute = new Tribute({
						collection: [{
							trigger: '#',
							// The function that gets call on select that retuns the content to insert
							selectTemplate: function (item) {
								if (this.range.isContentEditable(this.current.element)) {
									$scope.trendMapingIds["#"+item.original.title]= item.original.id;
									return '<a href="/#/trends?id='+item.original.id+'" id="trendTags" class="trend'+item.original.id+'" title="' + item.original.title + '" target="_blank">#' + item.original.title + '</a>';
								}
								return '#' + item.original.title;
							},

							// the array of objects
							values: function (text, cb) {
								searchTrend(text, function (users) {
									cb(users);
								});
							},

							lookup: 'title',

							fillAttr: 'title'
						},
							{
								// The symbol to start looking for user
								trigger: '@',

								// The function that gets call on select that retuns the content to insert
								selectTemplate: function (item) {
									console.log(item);
									if (this.range.isContentEditable(this.current.element)) {
										$scope.userMapingIds["#"+item.original.userName]= item.original.id;
										console.log($scope.userMapingIds);
										return '<a href="/#/personality?id='+item.original.id+'" id="userTags" title="#'+item.original.userName+'" target="_blank">@' + item.original.title + '</a>';
									}

									return '@' + item.title;
								},

								// function retrieving an array of objects
								values: function (text, cb) {
									searchUser(text, function (users) {
										cb(users);
										$scope.isTrend = false;
									});
								},

								lookup: 'title',

								fillAttr: 'title'
							},
							{
								// The symbol to start looking for Sentiment
								trigger: '+',

								// The function that gets call on select that retuns the content to insert
								selectTemplate: function (item) {
									if (this.range.isContentEditable(this.current.element)) {
										return '<a href="/#/sentiment?id='+item.original.id+'" target="_blank">+' + item.original.subject + '</a>';
									}

									return '+' + item.subject;
								},

								// function retrieving an array of objects
								values: function (text, cb) {
									searchSentiment(text, function (users) {
										cb(users);
										$scope.isTrend = false;
									});
								},

								lookup: 'subject',

								fillAttr: 'subject'
							}]
					});

					$scope.tribute.attach(document.getElementById('pinningOpinion'));

					document.getElementById('pinningOpinion').addEventListener('tribute-replaced', function (e) {
						if($scope.isTrend === true){
							$http({
								method: 'POST',
								url: $rootScope.liveHost+"/trend/createTrend",
								headers: {'Content-Type': 'application/json'},
								data:{"name":$scope.newTrend}
							}).then(function successCallback(response) {
								if(response.data.success === true){
									$scope.trendMapingIds["#"+$scope.newTrend]= response.data.messages[0].id;
									$scope.isTrend = false;
									$("#pinningOpinion").find("a.trend0").attr("href","/#/trends?id="+response.data.messages[0].id);
									$("#pinningOpinion").find("a.trend0").attr("class","trend"+response.data.messages[0].id);
									console.log("success: created new trend");
								}else{
									$scope.isTrend = false;
									console.log("succes : unable to create trend");
								}
							}, function errorCallback(response) {
								$scope.isTrend = false;
								console.log("unable to create trend");
							});
						}

					});

					// Function : CreateOpinion
					$scope.createOpinion = function(form) {
					$scope.createOpinionType = 1;///added by manoj
						if($scope.selectedCategory != undefined){
							$(".loader").show();
							var parsedTrendIds=[];
							$("#pinningOpinion").find("a#trendTags").each(function(index){
								parsedTrendIds.push($scope.trendMapingIds[$(this).text()]);
							});
							var parsedUserIds=[];
							$("#pinningOpinion").find("a#userTags").each(function(index){
								//alert($(this).attr("title"));
								parsedUserIds.push($scope.userMapingIds[$(this).attr("title")]);
							});
							$scope.selectedSntmtTitle = $scope.selectedSntmt.subject;
							$scope.opinion.created_By = $scope.getLoggedInUserId();
							if($scope.opinion.link === undefined){
								$scope.opinion.link = {created_By:$scope.getLoggedInUserId()};
							}else{
								$scope.opinion.link.created_By = $scope.getLoggedInUserId();
							}
							$scope.opinion.sentiment = {};
							$scope.opinion.sentiment.id = $scope.selectedSntmt.id;
							// $scope.opinion.subject = "default subject";
							$scope.opinion.category = {id:$scope.selectedCategory};
							$scope.opinion.user = {id:$scope.getLoggedInUserId()};
							$scope.opinion.userIds = parsedUserIds;
							$scope.opinion.componentType = "Opinion";
							$scope.opinion.imageUrl = $scope.selectedOpinionImage;

							var regFormData = new FormData();
							// regFormData.append('file',$scope.opinion.attachment);
							regFormData.append('opinion',new Blob([JSON.stringify($scope.opinion)], {
								type: "application/json"
							}));
							regFormData.append('userIds',new Blob([JSON.stringify([1,3])], {
								type: "application/json"
							}));

							$http({
								method: 'POST',
								url: $rootScope.liveHost+"/opinion/add",
								headers: {'Content-Type': "application/json"},
								data: $scope.opinion
							}).then(function successCallback(response) {
								if(response.data.success === true){
									$scope.createOpinionStatus = true;
									// Adding Trend Mapping after creating opinion
									$http({
										method: 'POST',
										url: $rootScope.liveHost+"/trend/createMultipleTrendMapping",
										headers: {'Content-Type': 'application/json'},
										data: {"componentId": response.data.messages[0].id,
											"componentType": "Opinion",
											"userId": response.data.messages[0].user.id,
											"trendIds" : parsedTrendIds
											// "trend" : "{id:1}"
										}
									}).then(function successCallback(response) {
										console.log("successfully added trend mapping!");
									}, function errorCallback(response) {
										console.log("unable to add trend mapping!");
									});
								}else{
									$scope.createOpinionHasError = true;
									if(response.data.responseMessage)
										$scope.createOpinionError = response.data.responseMessage[0];
								}

								$scope.opinion = {};
								$scope.searchInput = undefined;
								form.$setPristine();
								$scope.selectedSntmt = {};
								$scope.searchStatus = false;
								$('#permntsntm').collapse("hide");
								$scope.opinionImages = [];
								$scope.selectedOpinionImage = "";
								$(".loader").hide();
							}, function errorCallback(response) {
								$scope.resetOpinionPopup(form);
							});

							// Also load the opinion for the sentiment selected
							var rebutType = ($scope.opinion.type == "Anti")?"Pro":"Anti";
							var opinions = HttpHelper.create('GET',$rootScope.liveHost+'/opinion/getOpinionsByType?sentimentId='+$scope.selectedSntmt.id+'&opinionType='+rebutType+'&count=3');
							opinions.then(function(response){
								var tmpOpins = response.data.messages;
								angular.forEach(tmpOpins,function(opinion, index){
									tmpOpins[index].text = $sce.trustAsHtml(opinion.text);
									angular.forEach(opinion.agreeDisagreePoll.pollOptionOutputModels, function(opt){
										if(opt.pollOption == "Yes"){
											tmpOpins[index].agreePollId = opt.id;
										}else{
											tmpOpins[index].disAgreePollId = opt.id;
										}
									});
								});
								$scope.opinionsForRebut = tmpOpins;

							}, function(error){
								console.log(error);
							});
						}else{
							alert('Please chose "This Opinion On?" ');
						}
					};

					$scope.sanitizeHtml = function(rawHtml) {
						return $sce.trustAsHtml(rawHtml);
					};

					$scope.closeOpinionPopup = function(form){
						$scope.resetOpinionPopup(form);
						$('#opinion_plain').modal("hide");
						if($scope.createOpinionType > 0){
							$location.url('/?sentimentId='+$scope.selectedSntmtId);
						}
						
					};
					$scope.viewAllOpinions = function(form, sentimentId){
						$scope.resetOpinionPopup(form);
						$('#opinion_plain').modal("hide");
						console.log('/viewAllOpinion?id='+sentimentId+"&type="+$scope.selectedSntmtType);
						if(sentimentId){
							$location.url('/viewAllOpinion?id='+sentimentId+"&type="+$scope.selectedSntmtType);
						}
					};
					$scope.resetOpinionPopup = function(form){
						$scope.opinionImages = [];
						$scope.opinionImagesBackup = [];
						$scope.createOpinionHasError = false;
						$scope.createOpinionStatus = false;
						$scope.opinion = {};
						$scope.searchInput = undefined;
						form.$setPristine();
						$scope.selectedSntmt = {};
						$scope.searchStatus = false;
						$scope.isSuportClicked = false;
						$scope.isAgainstClicked = false;
						$scope.opinionActionStatus = false;
						$scope.selectedOpinionImage = "";
					};
					
					//upload img start
					$scope.openFile = function(){
						var fileinput = document.getElementById("uploadOpnImgBtn");
						fileinput.click();	
					};
					$scope.uploadOpnFile = function(element) { 	 
					console.log("calling upload");
					  if (typeof ($("#uploadOpnImgBtn")[0].files) != "undefined") {
						  var temp_filesize=$(element)[0].files[0].size;
							var size = parseFloat(temp_filesize / 2048).toFixed(2);
							var img_mb= size/1000;
							if(img_mb<=2){
								var regFormData = new FormData();
								regFormData.append('image',$(element)[0].files[0]);
								regFormData.append('imageType','albumimage'); 
								regFormData.append('userId',$scope.getLoggedInUserId());
								regFormData.append('sentimentId',$scope.selectedSntmt.id);
																	
								$http({
								  method: 'POST',
								  url: "/opinion/uploadOpinionImage",
								  headers: {'Content-Type': undefined},
								  data: regFormData
								  }).then(function successCallback(response) {
									console.log("response  ---",response);	
									
								  }, function errorCallback(response) {
								  alert("fail");
								  });
							}else{
								alert("please choose file upto 2kb size");
							}
							
						} else {
							alert("This browser does not support HTML5.");
						}
					  
					  };
					  
					  $scope.changesearchtext = function(e) { 	
						  if (!angular.isUndefined(e)) {
							  $scope.opnImgSrchError = false;
							  if (e.keyCode === 13) {
								  $(".loader").show();
								  $scope.SearchImageFile();
							  }
						  }
					  };
					  
					  $scope.clearSearcghtext = function() { 	
						  $scope.SearchImg = '';
						  $scope.opnImgSrchError = false;
						  $scope.opinionImages = [];
						  $scope.opinionImages = $scope.opinionImagesBackup;
						  $("#carousel-opinion-imgs").carousel("pause").removeData();
						  $('#carousel-opinion-imgs').carousel({
									interval: false
								});
						  $scope.setOpinionImage($scope.opinionImages[i].first,0,'_first');
						
					  };
					  
						$scope.SearchImageFile = function() {
							var findResult = HttpHelper.create('GET',$rootScope.liveHost+'/cache/searchImage?searchkeyword='+$scope.SearchImg+'&sentimentId='+$scope.selectedSntmt.id+'');
							findResult.then(function(response){
								if(response.data.success){
									var data  =response.data.messages[0];
									var imageLength = response.data.messages[0].length;
									var oldImgCount = $scope.opinionImages.length;
									for (var j = 0; j < oldImgCount; j++) {
										$scope.opinionImages.shift();
									}
									var tmp = $scope.opinionImages;
									for (var i = 0; i < imageLength; i += 3) {
										$scope.opinionImages.push({
											'first' : data[i],
											'second' : data[i + 1],
											'third' : data[i + 2]
										});
									}
									if(imageLength > 0){
										 $scope.setOpinionImage($scope.opinionImages[0].first,0,'_first');
									}else{
										$scope.opnImgSrchError = true;
									}
									$(".loader").hide();
								}else{									
								}
							}, function(error){
								console.log(error);
							}); 
							
							// var searchImgUrl, searchIndex, searchPlace;
							// for (var i = 0; i < $scope.opinionImages.length; i ++) {
								// var srFirst = $scope.opinionImages[i].first;
								// if(srFirst != undefined && srFirst.search($scope.SearchImg) != -1){
									// searchImgUrl = $scope.opinionImages[i].first;
									// searchIndex = i;
									// searchPlace = '_first';
								// }
								// var srSecond = $scope.opinionImages[i].second;
								// if(srSecond != undefined && srSecond.search($scope.SearchImg) != -1){
									// searchImgUrl = $scope.opinionImages[i].second;
									// searchIndex = i;
									// searchPlace = '_second';
								// }
								// var srThird = $scope.opinionImages[i].third;
								// if(srThird != undefined && srThird.search($scope.SearchImg) != -1){
									// searchImgUrl = $scope.opinionImages[i].third;
									// searchIndex = i;
									// searchPlace = '_third';
								// }
							// }
							
								  
						};
				    	  
				    	  					
					$scope.addResponse = function(respIndex, opinionId){
						var description = $("#rebut_resp_"+respIndex).html();
						var pollOptionid = $("#rebut_vote_"+respIndex).val();
						$timeout(function(){
							$scope.masterResponseStatus = false;
							$("#rebut_resp_"+respIndex).html("");
							$("#rebut_resp_"+respIndex).attr("contenteditable","false");
							$("#rebut_resp_"+respIndex).attr("placeholder","Click on agree or disagree button to write a comment");
						},4000);
						if(description != "" && pollOptionid != ""){
							// calling setVote api
							$http({
								method: 'POST',
								url: $rootScope.liveHost+"/poll/selectVote",
								headers: {'Content-Type': 'application/x-www-form-urlencoded'},
								data:$.param({pollOptionId:pollOptionid, userId:1, componentId:opinionId, componentType:"Opinion"})
							}).then(function successCallback(response) {
								if(response.data.success === true){
									console.log("success: selectVote");
								}else{
									console.log("succes : selectVote false");
								}
							}, function errorCallback(response) {
								console.log("Error: selectVote");
							});
							// calling response api
							$http({
								method: 'POST',
								url: $rootScope.liveHost+"/response/add",
								headers: {'Content-Type': 'application/json'},
								data:{
									"flag": "Agree",
									"content": description,
									"created_By" : $scope.getLoggedInUserId(),
									"user": {"id":$scope.getLoggedInUserId()},
									"opinion": {"id":opinionId},
									"userIds": [1,3]
								}
							}).then(function successCallback(response) {
								if(response.data.success === true){
									console.log("success: created new response");
								}else{
									console.log("success : unable to create response");
								}
							}, function errorCallback(response) {
								$("#rebut_resp_"+respIndex).html("");
								console.log("unable to create response");
							});
						}else{
							alert('Please enter your response!');
						}
					};
					$scope.setVote = function(opinIndex, pollOptionId){
						if($scope.isUserLoggedIn()){
							$("#rebut_vote_"+opinIndex).val(pollOptionId);
							$scope.tribute.attach($("#rebut_resp_"+opinIndex));
							$("#rebut_resp_"+opinIndex).attr("placeholder","Enter your response here.");
							$("#rebut_resp_"+opinIndex).on('tribute-replaced', function (e) {
								if($scope.isTrend === true){
									$http({
										method: 'POST',
										url: $rootScope.liveHost+"/trend/createTrend",
										headers: {'Content-Type': 'application/json'},
										data:{"name":$scope.newTrend}
									}).then(function successCallback(response) {
										if(response.data.success === true){
											$scope.trendMapingIds["#"+$scope.newTrend]= response.data.messages[0].id;
											$scope.isTrend = false;
											$("#rebut_resp_"+opinIndex).find("a.trend0").attr("href","/#/trends?id="+response.data.messages[0].id);
											$("#rebut_resp_"+opinIndex).find("a.trend0").attr("class","trend"+response.data.messages[0].id);
											console.log("success: created new trend");
										}else{
											$scope.isTrend = false;
											console.log("succes : unable to create trend");
										}
									}, function errorCallback(response) {
										$scope.isTrend = false;
										console.log("unable to create trend");
									});
								}

							});
							
							$scope.masterResponseStatus = true;
						}
					};

					$scope.viewOpinion = function(opinionId, form){
						$scope.closeOpinionPopup(form);
						$location.url('/viewOpinion?id='+opinionId);
					};

				}]
		};
	}).directive('getAttachmentFileObject',['$rootScope', function(){
	return {
		require:'ngModel',
		link:function(scope,el,attrs,ngModel){
			//change event is fired when file is selected
			el.bind('change',function(evt){
				scope.$apply(function(){
					ngModel.$setViewValue(el.val());
					ngModel.$render();
				});

				var file = evt.target.files[0];
				scope.opinion.attachment = file;
				//console.log(file);
				//scope.parseCsv(file);
				//scope.isFileInvalid = false;
			});
		}
	};
}]).directive('initCallback', function() {
	return function(scope, element, attrs) {
		setTimeout(function doWork(){
			//jquery code and plugins
			element.attr("placeholder","Click on agree or disagree button to write a comment");
		}, 0);
	};
}).directive('initCarosoul', function() {
	return function(scope, element, attrs) {
		setTimeout(function doWork(){
			//jquery code and plugins
			scope.setOpinionImage(scope.opinionImages[0].first,0,'_first');
		}, 0);
	};
}).directive('handlePasteEvnt', function() {
	return function(scope, element, attrs) {
		setTimeout(function doWork(){
			//jquery code and plugins
			$(element).on("paste", function(e) {
				console.log("Inside Paste Event");
				// cancel paste
				e.preventDefault();

				// get text representation of clipboard
				var text = window.clipboardData.getData("text/plain");

				// insert text manually
				document.execCommand("insertHTML", false, text);
			});
		}, 0);
	};
});
