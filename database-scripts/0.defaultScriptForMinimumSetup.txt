INSERT INTO `userRole` (`id`,`userRole`) VALUES (1,'ADMIN');
INSERT INTO `userRole` (`id`,`userRole`) VALUES (2,'USER');
INSERT INTO `chart` (`id`,`chartID`,`chartSecondaryID`,`description`) VALUES (1,'GPIChartService','GPI','Govt Popularity Index graph based on public opinions.');
INSERT INTO `chartMetadata` (`id`,`propertyName`,`propertyValue`,`chartid`) VALUES (5,'yaxislabel','Score',1);
INSERT INTO `chartMetadata` (`id`,`propertyName`,`propertyValue`,`chartid`) VALUES (6,'xaxislabel','DD',1);
INSERT INTO `chartMetadata` (`id`,`propertyName`,`propertyValue`,`chartid`) VALUES (7,'title','GPI',1);