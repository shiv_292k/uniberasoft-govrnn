/*
-- Query: select * from module
LIMIT 0, 1000

-- Date: 2016-07-09 16:31
*/
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (1,'GovtSchemeData','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (2,'Bumper','Contest');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (3,'Mini','Contest');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (4,'Mega','Contest');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (5,'Image','GPI');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (6,'Video','GPI');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (7,'Link','GPI');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (8,'Graph','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (9,'LocalIndicatorGraph','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (10,'GlobalIndicatorGraph','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (11,'ComparisonGraph','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (12,'SubmitFact','GA');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (13,'Opinion','GPI');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (14,'LocalVerdict','Verdict');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (15,'LocalVerdictreport','Verdict');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (16,'GlobalVerdictReport','Verdict');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (17,'Sentiment','GPI');
INSERT INTO `module` (`id`,`componentType`,`module`) VALUES (18,'Task','Task');
