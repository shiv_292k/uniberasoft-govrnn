'use strict';

/**
 * @ngdoc overview
 * @name govrnnApp
 * @description
 * # govrnnApp
 *
 * Main module of the application.
 */
angular
  .module('govrnnadminApp', [
    'ngRoute'
  ])

  .config(function ($routeProvider) {
    $routeProvider
      .when('/admin', {
        templateUrl: 'adminviews/deshboardview.html'
       
      }).when('/admin/viewsentiment', {
          templateUrl: 'adminviews/sentiment_view.html',
          controller: 'viewallCntrl'
        
        }).when('/admin/addsentiment', {
            templateUrl: 'adminviews/sentiment_Add.html'
            // controller: 'addSentimentCntrl'
        
               })
               .when('/admin/editsentiment', {
        templateUrl: 'adminviews/editSentiment.html',
        controller:   'EditCtrl'
        
      })
      .otherwise({
        redirectTo: '/admin'
      });
  })
  
  .run(function ($rootScope) {
   // $rootScope.liveHost = 'http://www.govrnn.com';
    $rootScope.liveHost = 'http://localhost:8080';
})
.config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('');
}]);
