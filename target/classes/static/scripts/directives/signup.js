'use strict';

/**
 * @ngdoc directive
 * @name govrnnApp.directive:myDirective
 * @description
 * # myDirective
 */
angular.module('govrnnApp')
  .directive('signupModal', function () {
    return {
      templateUrl: 'templates/modal_signup.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        // element.text('this is the myDirective directive');

        // $('.animation_text input').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });

        // $('.animation_text textarea').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });
		
        // $('.animation_text select').on('blur', function(){
          // $(this).parent('.animation_text').removeClass('input-desc-hover');

        // }).on('focus', function(){
          // $(this).parent('.animation_text').addClass('input-desc-hover');
        // });

        // $('.animation_text.dob input').on('click', function(){
          // $(this).parent('.animation_text').addClass('has_txt');
        // });

        // var inputs = $('.animation_text input').not(':submit');

        // inputs.on('input', function() {
          // $(inputs[inputs.index(this)]).parent().toggleClass('has_txt', this.value > '');
        // });
        // inputs[0].focus();

		// $('.animation_text select').on('change', function(){
		  // $(this).parent('.animation_text').addClass('has_txt');
		// });
        // var summary = $('.animation_text textarea');
        // summary.on('textarea', function() {
          // $(summary[summary.index(this)]).parent().toggleClass('has_txt', this.value > '');
        // });
        // if(summary !== undefined){summary[0].focus();}
		
		$('#signupDob').datepicker({
			format: "dd/mm/yyyy",
			autoclose: true,
			todayHighlight: true
		});

      },
      controller: ['$scope','$location','$http','$rootScope', '$filter', function ($scope,$location, $http, $rootScope, $filter) {
        $scope.user = {};
        $scope.userImage;
        $scope.albumImage;
        $scope.hasError = false;
		$scope.userCheckError = false;
		$scope.userEmailCheckError = false;		
		$scope.userAvailability = false;
		$scope.userEmailAvailability = false;
		$scope.usernameErrorMsg = "";
		$scope.userEmailErrorMsg = "";
        $scope.currentTab = 1;
        $scope.currentSubTab = 1;
        $scope.userAvailabilOnNext = false;
        $scope.userEmailAvailabilOnNext = false;
        
        $("#full_name").focus();
		
		$scope.signUpNow = function(){
			$('#sign_pop').modal("show");
			
			// $('.animation_text input').each(function(){
				// if($(this).val().length > 0){
					// $(this).parent('.animation_text').addClass('has_txt');
				// }
			// });
			
		}
		        
        /*========================state-city==================================================*/
		// Countries
        var state_arr = new Array("Select State","Andaman and Nicobar Island (UT)","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh(UT)","Chhattisgarh","Dadra and Nagar Haveli(UT)","Daman and Diu (UT)","Delhi (NCT)","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Lakshadweep(UT)","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Puducherry(UT)","Punjab","Rajasthan","Sikkim","Tamil Nadu","Telangana","Tripura","Uttarakhand","Uttar Pradesh","West Bengal");

        $.each(state_arr, function (i, item) {
            $('#state_signup').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
        });

        
        
        // States
        var s_a = new Array();
        s_a[0]="Select City";
        
        s_a[1]="Select City|Nicobar|North and Middle Andaman|South Andaman";
        
        s_a[2]="Select City|Anantapur|Chittoor|East Godavari|Guntur|YSR Kadapa|Krishna|Kurnool|Potti sriramulu Nellore|Prakasam|" +
        		"Srikakulam|Srikakulam"|"Visakhapatnam"|"Vizianagaram"|"West Godavari";
        
        s_a[3]="Select City|Anjaw|Central Siang|Changlang|East Siang|East Kameng|Kra Daadi|Kurung Kumey|Lohit|Lower Dibang Valley|" +
        		"Longding|Namsai|Papum Pare|Upper Dibang Valley|Upper Siang|Upper Subansiri|Tawang|Tirap|West Kameng|West Siang";
        
        s_a[4]="Select City|Baksa|Barpeta|Biswanath|Bongaigaon|Cachar|Charaideo|Chirang|Darrang|Dhemaji|Dhubri|Dibrugarh|Goalpara|Golaghat|Hailakandi|Hojai|" +
				"Jorhat|Kamrup Metropolitan|Kamrup|Karbi Anglong|Karimganj|Kokrajhar|Lakhimpur|Majuli|Morigaon|Nagaon|Nalbari|Dima Hasao|Sivasagar|Sonitpur|" +
				"South Salmara-Mankachar|Tinsukia|Udalguri|West Karbi Anglong";
        
        s_a[5]="Select City|Araria|Arwal|Aurangabad|Banka|Begusarai|Bhagalpur|Bhojpur|Buxar|Darbhanga|East Champaran|Gaya|Gopalganj|Jamui|Jehanabad|Khagaria|" +
        		"Kishanganj|Kaimur|Katihar|Lakhisarai|Madhubani|Munger|Madhepura|Muzaffarpur|Nalanda|Nawada|Patna|Purnia|Rohtas|Saharsa|" +
				"Samastipur|Sheohar|Sheikhpura|Saran|Sitamarhi|Supaul|Siwan|Vaishali|West Champaran	";
        
        s_a[6]="Select City|Chandigarh";
        
        s_a[7]="Select City|Balod|Baloda Bazar|Balrampur|Bemetara|Bijapur|Bilaspur|Dantewada|Dhamtari|Durg|Gariaband|Jagdalpur|Janjgir-Champa|Jashpur|Kabirdham|Khagaria|" +
				"Kondagaon|Korba|Koriya|Mahasamund|Mungeli|Narayanpur|Raigarh|Raipur|Rajnandgaon|Sukma|Surajpur|Surguja";
        
        s_a[8]="Select City|Dadra & Nagar Haveli";
        
        s_a[9]="Select City|Daman|Diu";
        
        s_a[10]="Select City|Central Delhi|East Delhi|New Delhi|North Delhi|North East Delhi|Shahdara|South Delhi|South East Delhi|South West Delhi|West Delhi";
        
        s_a[11]="Select City|North Goa|South Goa";
        
        s_a[12]="Select City|Anand|Aravalli|Banaskantha (Palanpur)|Bharuch|Bhavnagar|Botad|Chhota Udepur|Dahod|Dangs (Ahwa)|Devbhoomi Dwarka|Gandhinagar|Gir Somnath|Jamnagar|Junagadh|" +
        		"Kachchh|Kheda (Nadiad)|Mahisagar|Mehsana|Morbi|Narmada (Rajpipla)|Navsari|Panchmahal (Godhra)|Patan|Porbandar|Rajkot|Sabarkantha (Himmatnagar)|Surat|Surendranagar|Tapi (Vyara)|Vadodara|Valsad";
   
        s_a[13]="Select City|Ambala|Bhiwani|Charkhi Dadri|Faridabad|Fatehabad|Gurgaon|Hisar|Jhajjar|Jind|Kaithal|Karnal|Kurukshetra|Mahendragarh|Nuh|Palwal|Panchkula|Panipat|Rewari|Rohtak|Sirsa|Sonipat|Yamunanagar";
        
        s_a[14]="Select City|Bilaspur|Chamba|Hamirpur|Kangra|Kinnaur|Kullu|Lahaul & Spiti|Mandi|Shimla|Sirmaur (Sirmour)|Solan|Una";
        
        s_a[15]="Select City|Anantnag|Bandipore|Baramulla|Budgam|Doda|Ganderbal|Jammu|Kargil|Kathua|Kishtwar|Kulgam|Leh|Pulwama|Rajouri|Ramban|Reasi|Samba|Shopian|Srinagar|Udhampur";
        
        s_a[16]="Select City|Bokaro|Chatra|Deoghar|Dhanbad|Dumka|East Singhbhum|Garhwa|Giridih|Godda|Gumla|Jamtara|Khunti|Koderma|Latehar|Lohardaga|Pakur|Palamu|Ramgarh|Ranchi|Sahibganj|Seraikela-Kharsawan|Simdega|West Singhbhum";
        
        s_a[17]="Select City|Bagalkot|Ballari (Bellary)|Belagavi (Belgaum)|Bengaluru (Bangalore) Rural|Bengaluru (Bangalore) Urban|Bidar|Chamarajanagar|Chikballapur|Chikkamagaluru(Chikmagalur)|Chitradurga|Dakshina Kannada|Davangere|Dharwad|Gadag|Hassan|Haveri|Kalaburagi(Gulbarga)|Kolar|Koppal|Mandya|Raichur|Ramanagara|Shivamogga (Shimoga)|Tumakuru (Tumkur)|Udupi|Uttara Kannada (Karwar)|Vijayapura (Bijapur)|Yadgir";
        
        s_a[18]="Select City|Alappuzha|Ernakulam|Idukki|Kannur|Kasaragod|Kollam|Kottayam|Kozhikode|Malappuram|Palakkad|Pathanamthitta|Thrissur|Wayanad";
        
        s_a[19]="Select City|Lakshadweep";
        
        s_a[20]="Select City|AgarMalwa|Alirajpur|Anuppur|Balaghat|Betul|Bhind|Bhopal|Burhanpur|Chhatarpur|Chhindwara|Datia|Dewas|Dhar|Guna|Gwalior|Harda|Indore|Jabalpur|Jhabua|Katni|Khandwa|Khargone|Mandla|Mandsaur|Morena|Narsinghpur|Panna|Raisen|Rajgarh|Ratlam|Rewa|Sagar|Satna|Sehore|Seoni|Shajapur|Sheopur|Shivpuri|Sidhi|Singrauli|Tikamgarh|Ujjain|Umaria|Vidisha";
        
        s_a[21]="Select City|Ahmednagar|Akola|Amravati|Beed|Bhandara|Buldhana|Chandrapur|Gadchiroli|Gondia|Hingoli|Jalgaon|Jalna|Kolhapur|Latur|Mumbai City|Mumbai Suburban|Nagpur|Nanded|Nandurbar|Nashik|Osmanabad|Palghar|Parbhani|Pune|Raigad|Ratnagiri|Satara|Sindhudurg|Solapur|Thane|Wardha|Washim|Yavatmal";
        
        s_a[22]="Select City|Bishnupur|Chandel|Churachandpur|Imphal East|Imphal West|Jiribam|Kamjong|Kangpokpi|Pherzawl|Senapati|Tengnoupal|Thoubal|Ukhrul";
       
        s_a[23]="Select City|East Garo Hills|East Jaintia Hills|East Khasi Hills|North Garo Hills|Ri Bhoi|South Garo Hills|South West Garo Hills|West Garo Hills|West Jaintia Hills|West Khasi Hills";
        
        s_a[24]="Select City|Aizawl|Champhai|Kolasib|Lawngtlai|Lunglei|Mamit|Saiha|Serchhip";
       
        s_a[25]="Select City|Dimapur|Kiphire|Kohima|Longleng|Mon|Peren|Phek|Tuensang|Wokha|Zunheboto";
        
        s_a[26]="Select City|Angul|Balangir|Balasore|Bargarh|Bhadrak|Boudh|Deogarh|Dhenkanal|Gajapati|Ganjam|Jajpur|Jharsuguda|Kalahandi|Kandhamal|Kendrapara|Kendujhar (Keonjhar)|Koraput|Malkangiri|Mayurbhanj|Nabarangpur|Nayagarh|Nuapada|Puri|Sambalpur|Sonepur|Sundargarh";
        
        s_a[27]="Select City|Karaikal| Mahe|Pondicherry|Yanam";
        
        s_a[28]="Select City|Amritsar|Barnala|Bathinda|Faridkot|Fatehgarh Sahib|Fazilka|Gurdaspur|Hoshiarpur|Jalandhar|Kapurthala|Ludhiana|Mansa|Moga|Muktsar|Nawanshahr (Shahid Bhagat Singh Nagar)|Pathankot|Patiala|Rupnagar|Sahibzada Ajit Singh Nagar (Mohali)|Sangrur|Tarn Taran";
        
        s_a[29]="Select City|Ajmer|Alwar|Banswara|Baran|Barmer|Bharatpur|Bhilwara|Bikaner|Bundi|Chittorgarh|Churu|Dausa|Dungarpur|Hanumangarh|Jaipur|Jaisalmer|Jalore|Jhalawar|Jhunjhunu|Jodhpur|Karauli|Kota|Nagaur|Pali|Rajsamand|Sawai Madhopur|Sikar|Sirohi|Sri Ganganagar|Tonk|Udaipur";
       
        s_a[30]="Select City|East Sikkim|North Sikkim|South Sikkim|West Sikkim";
        
        s_a[31]="Select City|Ariyalur|Chennai|Coimbatore|Cuddalore|Dharmapuri|Erode|Kanchipuram|Kanyakumari|Karur|Krishnagiri|Madurai|Nagapattinam|Namakkal|Nilgiris|Perambalur|Pudukkottai|Ramanathapuram|Salem|Sivaganga|Thanjavur|Theni|Thoothukudi (Tuticorin)|Tiruchirappalli|Tirunelveli|Tiruppur|Tiruvannamalai|Tiruvarur|Vellore|Viluppuram|Virudhunagar";
        
        s_a[32]="Select City|Adilabad|Bhadradri Kothagudem|Hyderabad|Jagtial|Jayashankar Bhoopalpally|Jogulamba Gadwal|Kamareddy|Karimnagar|Khammam|Komaram Bheem Asifabad|Mahabubabad|Mahabubnagar|Mancherial|Medak|Medchal|Nagarkurnool|Nalgonda|Nirmal|Nizamabad|Peddapalli|Rajanna Sircilla|Rangareddy|Sangareddy|Suryapet|Vikarabad|Wanaparthy|Warangal (Rural)|Warangal (Urban)|Yadadri Bhuvanagiri";
       
        s_a[33]="Select City|Dhalai|Gomati|Khowai|North Tripura|Sepahijala|South Tripura|Unakoti|West Tripura";
       
        s_a[34]="Select City|Almora|Bageshwar|Champawat|Dehradun|Nainital|Pauri Garhwal|Pithoragarh|Rudraprayag|Udham Singh Nagar|Uttarkashi"
       
        s_a[35]="Select City|Agra|Aligarh|Ambedkar Nagar|Amethi (Chatrapati Sahuji Mahraj Nagar)|Amroha (J.P. Nagar)|Auraiya|Azamgarh|Bahraich|Ballia|Balrampur|Banda|Barabanki|Bareilly|Basti|Bijnor|Budaun|Bulandshahr|Chandauli|Chitrakoot|Deoria|Etah|Etawah|Faizabad|Farrukhabad|Fatehpur|Firozabad|Gautam Buddha Nagar|Ghaziabad|Ghazipur|Gonda|Hamirpur|Hapur (Panchsheel Nagar)|Hardoi|Hathras|Jalaun|Jaunpur|Jhansi|Kannauj|Kanpur Dehat|Kanpur Nagar|Kanshiram Nagar (Kasganj)|Kaushambi|Kushinagar (Padrauna)|Lakhimpur - Kheri|Lalitpur|Maharajganj|Mainpuri|Mathura|Mau|Meerut|Mirzapur|Moradabad|Muzaffarnagar|Pilibhit|Pratapgarh|RaeBareli|Rampur|Saharanpur|Sambhal (Bhim Nagar)|Sant Kabir Nagar|Sant Ravidas Nagar|Shahjahanpur|Shamali (Prabuddh Nagar)|Shravasti|Siddharth Nagar|Sitapur|Sonbhadra|Sultanpur|Unnao|Varanasi"
        
        s_a[36]="Select City|Alipurduar|Bankura|Birbhum|Burdwan (Bardhaman)|Dakshin Dinajpur (South Dinajpur)|Darjeeling|Hooghly|Howrah|Jalpaiguri|Kalimpong|Kolkata|Malda|Murshidabad|Nadia|North 24 Parganas|Paschim Medinipur (West Medinipur)|Purba Medinipur (East Medinipur)|Purulia|Uttar Dinajpur (North Dinajpur)"	
        
        	$('#state_signup').change(function(){
        		if($('#city_signup').val()){$('#city_signup').clear();}
        		
        	 $scope.user.state=state_arr[$(this).val()];
        	 
    		 //alert($scope.user.state);
            var s = $(this).val();
           // if(s!=user.state){$scope.ifAnyChange=false;}
            if(s=='Select State'){
                $('#city_signup').empty();
                $('#city_signup').append($('<option>', {
                    value: '0',
                    text: 'Select City',
                }, '</option>'));
            }
           // if($('#city_signup').val()!=null){$('#city_signup').val('select city');}
            var city_arr = s_a[s].split("|");
            $('#city_signup').empty();
            $.each(city_arr, function (j, item_city) {
                $('#city_signup').append($('<option>', {
                    value: item_city,
                    text: item_city,
                }, '</option>'));
            });

        });
        $('#city_snp').change(function(){
        	$scope.user.city = $('#city_snp').val();
        	//alert($scope.user.city);
        	console.log($scope.user.city);
        });

 /*================================RELIGION-CASTE===================================================*/
       
        var religion_arr = new Array("Hindu","Islam","Christianity","Sikhism","Buddhism","Jainism","Parsi","other");
        $.each(religion_arr, function (i, item) {
            $('#religion_signup').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
        });
        
        $('#religion_signup').change(function(){
        	 $scope.user.religion=religion_arr[$(this).val()];
    		
        });
        
       /* $('#caste_signup').change(function(){
        	$scope.user.caste=$('#caste_signup').val();
        	//alert($scope.user.city);
        });*/
        
 /*================================QUALIFICATION-COURSE==============================================*/       

        var quali_arr = new Array("Select Qualification","Post Graduate or Above","Undergraduate or Diploma","Intermediate (12th)","Higher Secondary (10th) or Below");
        
        $.each(quali_arr, function (i, item) {
            $('#qualification_signup').append($('<option>', {
                value: i,
                text : item,
            }, '</option>' ));
        });
        
        var cou_a = new Array();
        cou_a[0]="Select Course";
        cou_a[1]="Select Course|MS|MA|M.Sc|M.Com|ME/Mtech|LLM|MCA|MBA|other";
        cou_a[2]="Select Course|BA|B.Sc|BBA|B.Com|BSC|BE|other";
        cou_a[3]="Select Course|HSS|SSS|8th|5th|other";
        cou_a[4]="Select Course|other";
       
       
        $('#qualification_signup').change(function(){
        	 $scope.user.qualification=quali_arr[$(this).val()];
    		// alert($scope.user.qualification);
            var s = $(this).val();
            if(s=='Select qualification'){
                $('#course').empty();
                $('#course').append($('<option>', {
                    value: '0',
                    text: 'Select Course',
                }, '</option>'));
            }
            var course_arr = cou_a[s].split("|");
            $('#course').empty();

            $.each(course_arr, function (j, item_course) {
                $('#course').append($('<option>', {
                    value: item_course,
                    text: item_course,
                }, '</option>'));
            });


        });
        $scope.passFormat= /^[a-zA-Z0-9?=.*!@#$%^&*_]{6,16}$/;
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
         $scope.usrNameFormat =/^[a-zA-Z0-9]{8,16}$/;
        
        
        $scope.setTab = function (tab) {
        	 if(tab == 2){
        		 $scope.userAvailabilOnNext = true;
        		 $scope.userEmailAvailabilOnNext = true;
           	  $scope.checkUserAvailability();
           	  $scope.checkEmailAvailability();
           	  /*if($scope.usernameErrorMsg == 'Username is available!'){
           		 $scope.currentTab = tab;
           	  }*/
             }else{
         		$scope.usernameErrorMsg = "";
         		$scope.userEmailErrorMsg = "";
                 $scope.currentTab = tab;
             }         
        };
        $scope.isActiveTab = function (tab) {
          return $scope.currentTab <= tab;
        };
        $scope.isCurrentTab = function (tab) {
          return $scope.currentTab === tab;
        };
		
		$scope.setSubTab = function (tab) {
          $scope.currentSubTab = tab;
        };
        $scope.isCurrentSubTab = function (tab) {
          return $scope.currentSubTab === tab;
        };
        

        $scope.goToPreviousTab = function (showTab) {
          if( $scope.currentTab >= showTab){
        	  $scope.currentTab  =   showTab;
          };
        };

        /* reset logged in user password*/
        $scope.register = function(form) {
        	debugger;
		  $(".loader").show();
          $scope.newUserName = $scope.user.name;
		  var regFormData = new FormData();
		  regFormData.append('userImage',$scope.userImage);
		  regFormData.append('albumImage',$scope.albumImage);
		  // regFormData.append('user',angular.toJson($scope.user));
		  // $scope.user.dob = moment(new Date($scope.user.dob)).format('YYYY-mm-dd');
		  $scope.user.dob =  $scope.user.dob.split("/").reverse().join("-");
		  var dob = $scope.user.dob.split("-");
		  var date =  new Date (dob[0], (dob[1]-1), dob[2]); 
		 // $scope.user.dob = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
		  $scope.user.dob = date;
		  console.log($scope.user.dob);
		  regFormData.append('user',new Blob([JSON.stringify($scope.user)], {
                type: "application/json"
            }));
		  
          $http({
            method: 'POST',
            url: $rootScope.liveHost+"/user/addUserMultipart",
            // headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            headers: {'Content-Type': undefined},
            // withCredentials: true,
            data: regFormData
          }).then(function successCallback(response) {
			$(".loader").hide();  
            $scope.currentTab = 4;
			$scope.currentSubTab = 1;
			$scope.user = {};
			form.$setPristine();
			//$('#master_form').reset();
          }, function errorCallback(response) {
            $scope.hasError = true;
            $scope.user = {};
            $('#sign_pop').modal("hide");
            console.log("error signup");
          });
        };

		$scope.checkUserAvailability = function(){
			$http({
				method: "GET",
				url: $rootScope.liveHost+"/user/verifyIfUsernameExist?username="+$scope.user.username,
				headers: {'Content-Type':'application/json'}
				// data: $.param({username:$scope.user.username})
			}).then(function(response){
				console.log(response.data.success);
				if(response.data.success === false){
					$scope.userAvailability = true;
					$scope.usernameErrorMsg = 'Username is available!';
					if($scope.userAvailabilOnNext == true){
						$scope.currentTab = 2;
						$scope.usernameErrorMsg = "";
						
					}
					$scope.userAvailabilOnNext = false;
			/*		if(signupFrm1.$valid){
						$scope.currentTab = 2;
						 $scope.usernameErrorMsg = "";
					}
					 console.log("signupFrm1.$valid",signupFrm1.$valid);*/
					//$scope.userAvalibalityflag = false;
				}else{
					console.log('coming in');
					$scope.usernameErrorMsg = 'Username "'+$scope.user.username+'" already taken. try something else!';
					$scope.user.username = "";
					$scope.userCheckError = true;
					$scope.currentTab = 1;
					$scope.userAvailabilOnNext = false;
				}
			}, function(error){
				$scope.user.username = "";
				$scope.userCheckError = true;
				 $scope.currentTab = 1;
				 $scope.userAvailabilOnNext = false;
			});
		};

		$scope.checkEmailAvailability = function(){
			$http({
				method: "GET",
				url: $rootScope.liveHost+"/user/verifyIfUserEmailExist?email="+$scope.user.emailId,
				headers: {'Content-Type':'application/json'}
				// data: $.param({username:$scope.user.username})
			}).then(function(response){
				console.log(response.data.success);
				if(response.data.success === false){
					$scope.userEmailAvailability = true;
					$scope.userEmailErrorMsg = 'User Email is available!';
					if($scope.userEmailAvailabilOnNext == true){
						$scope.currentTab = 2;
						$scope.userEmailErrorMsg = "";
						
					}
					$scope.userEmailAvailabilOnNext = false;
				}else{
					console.log('coming in');
					$scope.userEmailErrorMsg = 'User Email "'+$scope.user.emailId+'" already taken. try something else!';
					$scope.user.emailId = "";
					$scope.userEmailCheckError = true;
					$scope.currentTab = 1;
					$scope.userEmailAvailabilOnNext = false;
				}
			}, function(error){
				$scope.user.emailId = "";
				$scope.userEmailCheckError = true;
				 $scope.currentTab = 1;
				 $scope.userEmailAvailabilOnNext = false;
			});
		};

        $scope.showCookiePolicyPopup= function (){
        	$('#sign_pop').modal("hide");
			 $location.url('/cookiePolicy'); 
        	/*$scope.currentTab = 1;
			$scope.currentSubTab = 1;
			$scope.user = {};
			$scope.newUserName = "";
		   //  form.$setPristine();
	          $('#sign_pop').modal("hide");
	          $('#cookiepolicy_pop').modal("show");*/
	        };
		
		$scope.showPrivacyPolicyPopup = function (form) {
			 $('#sign_pop').modal("hide");
			 $location.url('/privacyPolicy'); 
			 
			/*$scope.currentTab = 1;
			$scope.currentSubTab = 1;
			$scope.user = {};
			$scope.newUserName = "";
		   //  form.$setPristine();
	          $('#sign_pop').modal("hide");
	          $('#privacypolicy_pop').modal("show");
*/	        };
	
		var d = new Date();
		var year = d.getFullYear()-25;
		var month = d.getMonth();
		var day = d.getDay();
	
	$('.datepicker').datepicker('setDate', new Date(year, month, day));
	$('.datepicker').datepicker('update');  //update the bootstrap datepicker

		$scope.closeSignup = function(form){
			$scope.currentTab = 1;
			$scope.currentSubTab = 1;
			$scope.user = {};
			$scope.newUserName = "";
			form.$setPristine();
			$('#sign_pop').modal("hide");
		};
		
      }]
    };
  }).directive('getUserImageFileObject',['$rootScope', function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(evt){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        });

        var file = evt.target.files[0];
        scope.userImage = file;
		console.log(file);
        //scope.parseCsv(file);
        //scope.isFileInvalid = false;
      });
    }
  };
}]).directive('getAlbumImageFileObject',['$rootScope', function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(evt){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        });

        var file = evt.target.files[0];
        scope.albumImage = file;
		console.log(file);
        //scope.parseCsv(file);
        //scope.isFileInvalid = false;
      });
    }
  };
}]);
