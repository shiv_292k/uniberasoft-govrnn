'use strict';

/**
 * @name govrnnApp.directive:generalDirectives
 * @description : this file contains all the common directives
 */
angular.module('govrnnApp')
.directive('contenteditable', function() {
    return {
      restrict: 'A', // only activate on element attribute
      require: '?ngModel', // get a hold of NgModelController
      link: function(scope, element, attrs, ngModel) {
        if(!ngModel) return; // do nothing if no ng-model

        // Specify how UI should be updated
        ngModel.$render = function() {
          element.html(ngModel.$viewValue || '');
        };

        // Listen for change events to enable binding
        element.on('blur keyup change', function() {
          scope.$apply(read);
        });
        read(); // initialize

        // Write data to the model
        function read() {
          var html = element.html();
          // When we clear the content editable the browser leaves a <br> behind
          // If strip-br attribute is provided then we strip this out
          if( attrs.stripBr && html == '<br>' ) {
            html = '';
          }
          ngModel.$setViewValue(html);
        }
      }
    };
  }).filter( 'filterDomain', function () {
  return function ( inputUrl ) {
	
	var hostname;
    //find & remove protocol (http, ftp, etc.) and get the hostname
    if (inputUrl.indexOf("://") > -1) {
        hostname = inputUrl.split('/')[2];
    } else {
        hostname = inputUrl.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
	
	var domain = hostname,
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    if (arrLen > 2) {
        // domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        domain = splitArr[arrLen - 2];
    }
    return domain;
	
  };
});