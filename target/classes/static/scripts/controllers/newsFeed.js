'use strict';

var app = angular.module("govrnnApp", ['ngCookies', 'ngStorage']);
/*allowCrossDomain = function(req, res, next) {
	  res.header('Access-Control-Allow-Origin', '*');
	  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	  if ('OPTIONS' === req.method) {
	    res.send(200);
	  } else {
	    next();
	  }
	};
	app.use(allowCrossDomain);*/
app.controller("addNewsCtrl", function ($scope, $http, $rootScope, $window, $location, $cookies, $localStorage) {
$scope.NewsLanguageArray = new Array();
$scope.NewsChannelArray = new Array();
$scope.NewscatagoryLinkArray = new Array();
$scope.NewSChannelsDetais = new Object();
$scope.ChannelsNewsArray = new Array();
$scope.SelectedSentiment = "";

	$scope.loadData = function (form) {
		var newsData;
		// check for language, channnel & section selected from drop down
		if($scope.selectedLanguage != "" && $scope.selectedChannel != "" && $scope.SelectedCategory != "" &&
				$scope.selectedLanguage != "-1" && $scope.selectedChannel != "-1" && $scope.SelectedCategory != "-1")
			{
		
		if (true) {
			newsData = $.param({
				rssLink: $scope.SelectCategoryLink//$scope.sourceId
			});
			$http({
				method: 'POST',
				url: "/admin/news/rss",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
				data: newsData
			}).then(function successCallback(response) {
				if (response.data.success === true) {
					//alert(response.data);
					angular.forEach(response.data.messages, function (value, key) {

						$scope.items = JSON.parse(JSON.stringify(value.channel.item))
						$scope.selectedLanguage ="";
				 		$scope.selectedChannel ="";
				 		$scope.SelectedCategory ="";
					});
					console.log("$scope.items:"+JSON.stringify($scope.items));
				} else {
					$scope.hasError = true;
					form.$setPristine();
					console.log("succes:error loadData");
				}
			});
		}
		
	}
		else
		{
		alert("Please select values from drop-downs");
		}
	}
	
	/*************************************End of load news **********************************************/
	
	

	$scope.loadRSSData = function () {
			//console.log($scope.language);
			var newsData;
			if($scope.language != ""){
				
				newsData = $.param({
					language: $scope.language
				});
				$http({
					method: 'POST',
					url: "/admin/news/preLoadNewsData",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
					data : newsData
				}).then(function successCallback(response) {
				if (response.data.success === true) {

					angular.forEach(response.data.messages, function (value, key) {
						$scope.sources = JSON.parse(JSON.stringify(value));
					});
				}
				});
			}
			else{
				$scope.sources = [];
				$scope.items = [];
			}
	}
	$scope.addNewsData = function (form) {
		console.log("You clicked submit!");
		console.log("$rootScope.liveHost::", $rootScope.liveHost);
		
		//dummy data
		var lst; 
			/*lst=[
		           {
                 Channel name ->   "feedSource": "Hindustan Times",
                    "category": "Child",
                 Title ->   "title": "More single women coming forward to adopt children in India, shows data",
                   Link -> "link": "http://www.hindustantimes.com/india-news/more-single-women-coming-forward-to-adopt-children-in-india-shows-data/story-dgcvnQJoPFvkl6kqgC2bTK.html",
                    "previewImageLink": "hr",
                Image ->    "previewImagePath": "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACWAMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD78tYeKvRqOKZCuFFP+7XrSZCLkbhU5NVp7oZ4qKSUqvWqqqZGNcs5G0YlyNt7e1WGX5c1FbIFA3VHqF+kMfWskaxi5OyHKMmr9vEu2uY/tpI+rDNXrTXFZlXd1pI1dCdr2OojQAU2bgUyzuBNGDUsy8VRyNWdjOl+8ajUZqaReaWOPmkUJHHmptuKljj4p8i/LTJIM0tNqKS4WMHmgY9lDUzyqpSasit94Cnw6tFJwWFA+Vks6/LiqEi/NWk0izLkGq0kfPSkxGdcJ8prKZfmNbkyfKayriPrUssptjdg1Q1i1E0B47VeYfNzTJ13xkUJuxXU8g13SmW8O0cGiu11LS/NkLbc0V5U6LlJs9aniFGKR6nH90Uu2nRr8op4FfQs8JEf2fcRmleEQrk1ZT5VzWXql5jgGsJNJXZvTg6krIhutREWea5TWdc6/NTtY1AorYOK4XVNUXc2TXkVsVbRH0+GwSVmy/da8dxO/AFS6D4oa4vCCflBxXn2o6tukI3YWptD1Rbf5g3JNckMV7y1Pb+rR5GrH0t4d1IXEa810BbeK8w8IatmFOeor0O1ufMjWvejJSSaPhMVS9nUaJWTJpyrinj5qr3F9BayRpJKiPIdqKzAFjjOB61Wi3ORRctEi0p4pruApJqi2oKrHmq93qQCnBpXGqbb0QX2oCHPOK5rUtbK5+bFM1PUCWODzXK6rJJJnJ4rgrYlR0ie9hcDzayH6l4nEeSCTisNviNHayYaTb9TWZq0nlxtXkvjDWvs+9c15rxdSLvc9h4Ck1Y+jdF+JdvNgecp/Gu20/xJb3iD51J+tfnJqPxB1PQ7rzLWdtoP3SeK9A+Hv7TAM0dvfuYJM4+Y8V6OHx0Kvuy0Z4WKy+VLWOqPutisq5U5FUZoRzXn3gv4nW2sQoVnVgw9a9Dgu4r6MMhzmvRa7HjO60MqaIhuKikXjNac8FVJI+DxUgY8kYZzmin3A2MaKQ7s7TeFwBUgbmqaklhzVlW2jNdMmJRGXd15ceK52+uCck1fv5tzYBrC1CYKjfSvMxFToe9g6NldnL+I77Yjc15drerHc/Ndh4rv/lcZrzO8hudWvI7S0iee4mYJHGgyWY9AK+XryblZH1tCKirsxb7XD5hy1XdF1YzMoB710uq/AHWtM0s3t3c2on6m1RizD23Y25/HHvWp4L+B+uX7rJLbixgzzJccH8F61nGjWjPla1OqWIpRhzX0O28DakTHGCc169YTPDaiSQbExnL8VyWj+H9G8Bwfv5vtV2ig73GAD7CuW8XfEouz7JcKMgc17UcQ8PD3nqeBHLpY+rzJe6d14m8eJpcD7Jctj+HgV4lqfjC48TeOvD+19txDexmPBxxuGR9CK5TxN46+0OVebdwe/em/DXQ9Zu/EVn4su7KaPw1Yl5muduWlZQcCNfvP82OQMcHvXkVMRUxVVLpdH19LL6GAw8mkuZp/PTY+pJLs7j3qldX+FPNQ+IdWh0O2V5AA7AFlb+HPbjvXk/i/4nXj6/oqaY8MVkk6rdQhMtMrEA8knoCTgYr1MRivZX5mfN4TLfbWcY6d+h6pb2b30M8xVsKPlYdM1zmozDaec44rprXVxa2scakAEEkfjXOeILd7tjJbrkkcqKylHmimnqdFKi7vTQ4jXLkGNxmvEfHSs8zHqK9R1u6kimeORWRuhDV554oiEyM2M1wOV9CnGzPJNWsUkUllrir+xRZDjj0rtvE2pLZq4PFeX6l4nUXBye9Z+hlO2zPQ/A/xV1LwPdRrJI0tqDzk8rX2V8IfjLaeJreJo7hW4GRmvzlm1+K4jPPPpWl8PfiheeAfEUFxFK32RnAkjzwPevYweNcGoVNUeBjcFGac6e5+vtvcJe24kUjpUE8fFeafBf4jW/i7RLaaOUOsiA9a9TmUMK+lsmro+XacXZnPXkfWirl3DkGis7Abyp36U6dtsZp7LtAqtdtha1kb01dozLj7pPeuZ1qQrG1dHcSfLiud1ZfMUivArS3PrcPCyPKPFTyMzYrT+COjXF14sk1A2xe3tYmBmbojMMDHvjNXtS0N76dYo13ySMFVfUnoK9S0WxtvB9ra6Hbhd3lNJOwHLyYBJ/z6V51Gn+9U5bI9GrVtD2cVq/yIPEUP22zuIcdjiuK03x7daHYM0rM8MbmGTP8AC3Y/liu5vGEiyMzYXGd2eK8w1ayhNvqtux+W4+dB3OBjP8q7sVFqfNF20OmlycnLNXOY+InxAa73Sxy7gP4VPavHNR+IgffufaM5yxrA8T+JJNFa9tbmTEkLtGMnrzwfyryXwzpfib40fESz8HeFl3XFzIVe6kOIbdOrOx9FA/p1NfM+/VlZbs+ppzp4an72yPrz4F/D3T/GmhXnjHxKGm0vzWg0+zDlFnK5DyMQclQ2VAHUq2a7fWPiZY2Wjx6NaRrawafEII41b5VRR8vXnpR4q8JD4MfCPRPDtjqjancaLZhJW2CMT4yXYDJwSSW6nrXxxLr2rfFC21mXTTcia5Z4kjjcLGFHyjLdfy9a6683QfsYaaa+YsNGnio/W6rvd+6uyXl+p9CX3xsXxB4ajuJpuVgz/vYHWtz4C2um+J11DX9VjF0ttII7SJj8u8clyO+OAPxr5+8TaTc+Gvh3emWxnE0Nm4Uo25QwU4PqBnvXZ/s8+Lm03wXaQSykM6eYSDwSeT/OsqMv3qlU1N60Y1acqdJcvp6nvnir4g2+mXyFVOzds2pzkk8YH1/nXURPqUNjHPc2stosgBxJww+o7fjXA/BS9s/EnxC1G4uCsi6ZArorAEea5O0/gEP5iur+JHi54N48zA6ls9K9WD5aPtpPfY8mpSccQqFOOiSb+ZneMtHXU7QSwr++HI9a8o1yxkNq+UIZeGU9jXrHwfvh8SdFudVnn8mwhfYhU/NI2MnHoMY/Os7xZ4cS+kuZrFlbaxUj6djXNON/3nRnn1oqM5R7bnx98QrUrv8AlrwrXiY5mwDnNfUvxM0c+RMTHtdc5GK+ZPFGyOWTPWuW5w1Y2OW+3SRtjNWluDMvNY01yDJx61Pb3WyqORWPrj9jP4pSabqUmiXM3CkNGGPav0T0nUBf2McgOcivxd+H3iiXwv4v0+/jfaokCv8AQmv1j+D/AInTXPD9q4fdujB619VgKrnT5Xuj5XH0uSpzLqehXC5Bop033aK9ex5RtzHArLvX/dk5q9cSblxVG4hMkbVlPsddHRpmHPccms26bdVq6UrIeKqS/MK+drJxk0z7Kg1KCaLng7TRda4krDKwjf8Aj2/r+VGqTi18dWMsj/I7shH1U4/lXQeC7XybV5iMF8nPt0/xrgviXqCafcQ3hOPJuI5M+wcZ/TNYS/d04y8x0H7TEyh5WN7WVe4mkt7KIpF2bOT9B6CuT1jwu9jG0spy4HOa7+O/jt7fcBlmOAF6k1xPju9kaym+zJJNNjGU+6M+9TWd1zyd2b0JS5uVHwv+1No1zb+IrKWzPl2l9mOaReoden5g/pXUfsZa3pngvxprTBVWePSmAkx83MiE/wAqd+05ayab8OJtQuOJLeVJvpltv8mNeB/su+Kn1b4nX0Mk/lRTWTRk4yTl1PH5V5+GjL2vMj3OaMkoSd7n158QPi3ZeINYktbqUvDKCjQqTvZOjdOR35puk+G/DOg2CNolhHZR7chYxj8awZvCGlaLeyXMcsl1cSfemnILY7D2FMTUPsbBQf3B4X/Z9vpUPmTana9z6KShNR9nokjbvrpNcVbSWNXSddhVhkHjnIrhvEXhFfAliyWE5tLVeYZCN0af7Deg9D+HpXReH7qOTV7ht+4RnCLnpkAk1B4+1KS50/7FCvnXF0Vgii/vs5wB+ZpSWl+pEV71kaP7Oqa5o3hXUfEs2y6GozM7JCxLIqEqDjuDjPHTNV/HXj6a/sbndMFkmVkRjnAJB5r3vwR4B0fRvCdjY/Y0hWCFYx5WUJwAO2K898Zfs+6V4x8RQfZ7y60+2QtLOI5MhxgjHzAnvXoVsLU5IQj9xxUsXFOcp79zO+Bs1w0mj+D9JuGjs5o/OluT24Jkb+WB7ivT9cWw8F6tFBaXEstrcMVkWdtx3Y+8D+FcYvg+8+C+taffaNHLf6Zse3mhYgvDlflYZ6jIAx15rrdI8N2Wu+Ef+El1wySz3YZ7WAkp5KgkBiP73HftUckkmrWau/Rdjnq8tSKk5XjZJJbt92cF8XPB6ahpMl9aLltpJUfxCvz88fRyQ6xeQ4K7WPyntX6S2WtQ63pb2hKs6ZVgPbivKNY+GnhPxlqtxoviDTUkuNjNbX0P7uZcZJQsOo7jOelct0pLzPArRlZxfQ/PTy28wmpDIUIr0343/Cr/AIVZ4iWCCdrvTLgFraeQANx1VsdxkfXNeXO25qtPmOC3LoaNrMdyt3BzX6N/sf8AjQ6locELSbiiqvX2r83bV8MBX2L+xXqkkd1LFu+TcK9nAy5ZnjY6PNC5+iEk4aFT7UVkR3Be3j+lFe/7Q+d5TqmyzYq0kI8sg88VAfvZq3EeKu2ponY5nWLPy3LY4rCYFm2jqTgV3uoWH2mPpXOw6Uf7SjVhgKd35V5uLo8y5ke/gsSorlZ0FvjT9KAyF2pjJ4rxT4u3EV3pN4HnUfIcHNe0a7cJb6ZtKGQMOgFeI+NNU0jyZobqwiIIOd65rxMY9FC57eWQu3Va6nVeB9Yj17wnpN9HHJPNd20c3yjjkDjPpnNamraU/wBiZnAyRxGOgrlfgPrljqHhFLPT1Edvp8z2oiBzsAO4D6YYV6Tq1uZ7fBOeOMdKUbSp3Cs3Trcux8Q/tbWZvPhj4ngK5C2bycDuvzD+VfH/AOyNqjaT4g1x02C7khjSJnOO7EjP5V+g37QXhM6n4W1m025Fxayx/mpFfmj8E9SbRvFMmflO/Y2fbiuSnJwjNHrUkp1qUvU+vo9a1Ce6ZbmE8n728Efzq3fR+Xb/AGh5GfZ8xAbAA79Paqn2m1uLGOZkbaRnepIxUtreQcLnevQFjmuY+pUm0alnfQWziUoFRlAV9uFHXvUXg24bxD8YNLj2efY6erXMpXkIcbUyPqf0z2rB1TVBo9nK0R3LGPmj7MPUV0/7LLQ30eueIY0Ae9uvLQgY+SPjp2+YvW1Fc1RJnPUqezg+7PqO4vI4bUMZF2AcY6VQ8IST694oOHVbK3iMknckk/KP0J/CuW8QXEzpttJRE7dYn+6x/pXUfDWz+waK85fy7i9fe3f5Rwv9T+Ne3BupW8keXKypNp6vQ6jWvD9p4i1K0srqVkt2ctKqnG5VBJX8cYrz74zeI49N0/7JpMSwWliuPJX+4Ow/nXReKo73SQut21w8rWoZ2tyB+9XaQQPQ+n0rwv4jeJzFZX11efukaBpfmI+7tyD+VcGNlKzVrXZ04OmudVOa9lovPr+BX+EetLfXWpzlt2+VlTnpg81P42J0zVLbVEJHkyB3x3XPP6Zrzz9m+5kk0GAsS7P8xYnn8a+mvDPwtHjyQyXwMGkxczP0L/7I/wAa8uEZ1lGMVqcOL5ITc5PRnyZ+1F4UvvFHgo3un2NxfPa3CTD7PGzkIcqeg6fMD+FfGpZo3IcFGU4IIwRX71eEdL0fTNHeeKxih0q1/cWsIUfvWAwSfUCvgT9ub4A6V4mmvfGvhKyjtdWtwZNRtbdQEuUHWQAfxD9RXp/V/YwUm9z56alWnLlXwnwzbyDNfYv7F9qVlaUjIZq+MbeQAj8q+4v2QNsNpEcYrro+60eLiHzRPufTxut0HtRVPTr9fJU57UV9HBRauzwWnc75Vq7BHlcmooY/lq7CvFbWIuWI4xtwaxbspHqewdVHNbfmbEFcbc33+lSyHksxP+FcGKqcsEu56GCpupJvsaWveU9uJJs+Wo6CvK/EdxoE8jx3GlyTRHgv5bH+legzXxnTDHKKMlfU1xviPUNTw/kLwOijvXg4ifVfkfW4GPL7j/Oxj/CO10Kx13WIdFRoY5PLlliLHCtyMgHpwP0r1mVPMjGK84+GcGoTajqNzf2X2Y4REkIG5+pPSvSpnWOHBPNTS1jqRi1+9sn+p5v4+8PpfWFwCu5mFfmv+0V4L8PfD34uaJpvhy0Wyil0aK5uvmJL3TTz+YxP0C8egFfqX4gCyWzknivg39sX4erfXNl4ktIN81iSk+0cmMnr+BrkrJRv5noYFtyV3sc/4L1ORNPSKbaUYYBVuDUusWn9nsbu0MhhzueNDkqfUCsXwIYdS0tEJAOBlgfm/Kty4a6sdyAtKg6Pj+dcHQ+u5jjPHHjKH+xJWSf94qHocGvob9nbSH03wDpVoiLFJ5Qkd2H8bfMx9+Sa+SPHlqvirxBpukwLumurhYzsODtzlv0Br7W+H1rcaTpEH2hcIEAXZ0HHf0ruwyu7nDWlzXR2kmh3txdLseCVicbsFdo9e9dpZ6fBotjDH5zSSRgBW6D3GPeuQ0jUnuLjZ8wQHO5RyfpW9dJctHllymO5G78q92mlGDcUcLbulJlLxl4qWHT3UPkMMYHpXxH+014+TRtBXTYJD5l6wtowWyQp6j6Y4/EV9JePrgxSSMGKqgJZTx+NfAPxAttY+N/x60jwnoSGe5mvEtLUclQ7EbnP+yoGT7A147viK1nsdeIqrC4Tnju9F8/+AfZn7FPgG/8AHWnxMitDpVuF8+6I4zj7q+rfyr7O8V30WkadZ6JpSCHzXW3Xb2JOCfw5NO+H3w/034M/DfSvDGkxqkFlAFeYjDTSY+eRvcnJryj4sfEBtHaCaEhpLaTzB9a6fcwkLPrufORqyxlb2iWi2/zO68feMI9L0+HTLM7Y4IhEi56Dufqa+afjP44Xwh8Pdf1SSQeets8cKtzukcbVH4EitK6+K2neIYXvXuRFKp5hY/Nu9AO/tXyP+1n8SJb+6svDwk2lcXU8SnhAf9Wh98EsfqKmpU9rK6+RvO2HotLT/M+fbVDNexxr1Jr7i/ZpjbTtNhyMEgV8cfD/AEk6nrEZIz8wr7v+FOlrp+nQ7VxwKic7NJHy0vePoTTdYPkqM9qK57T5ioXmiumOKlbc4/Zo+nI/uircJ+Ws6Biyj0rQh/pX1TPHHycrivL579lvJ4nOGjkZT+BxXqEmdteYeMrf7D4kZ14S4Ak/Hof5frXk46N4KXY9vK5pTlB9TU03/ScMx6npVnULOOONnAAxzuIqposiqo55q1rlxvsZBGMsRgV4ztyHuq/PZEXh1vMs2mzne559ccVNf3ROBnGKg0sG10q3ib5WVOfrWbfSkvjPFQpWirmvLeVyPWZt9qwzXgvxO0+PUrG9gmXdG6MpHsRXs99IXjYdq8w8ZWputyDgd65K/vI66L5JHygujzeC/ED6d5yvDtWSGR+G2nsa2dYvtti7FwhC8FRmum+Knhs3SrqMUKySWqhWyucr/wDWrx3xJqTw2LB5ShPCQxnJY9s56fSuS2h9RSqc0Uy58GvD8niD4hXWtXkStBafuYmC5BY8sfyx+Zr7F0OW2jjQKy/KMc4ryX4IeEX0Xw7bJ8olYb5G6kseTn8a9z060LRrvRZD/tAcfnXuYSk1FNHLWkloy1a6pbdAvnMP4UXOKzfEXimO1t2VNyN6EEGte41CPT7dnZ0iAHRRmvG/iT4+SGCZhMNig/jW2KrckOS+oUKPPK6Wh5f+0H8Ul8P+Hbkib/SJVKrzzzW7/wAEx/gq/iTxBf8Axg1iMpBZq+naUrD/AFkpGJpfwB2A+pb0r5q/4RTxH+1F8ZbDwf4f3SedJm4uCMx2sII3yv7Adu5wO9frz4V8H6P8GvhvpHhTRIxb6dpdqsEQ/icjlnb1Zjkk+pNc2FpqEfayPHzfF+0n9WplP4geMlgSWFW528V8r+P9U/taWb5ssOq5r0H4l+KNyzxs2cHKkHn8K8BvdakuLhwzfvCchs8MK8fF1HVlYvDU1Rh7pnaZYw6dqEl2/wDqoxux6nsK+X/2jY2k+LV7Nji6ghm/8cC/+y19OwznVpGEa/6PE35v3/KvKfjF4FOu+KdKvI0zutPJY+6Ox/kwp4eooOxhiacq1kcN8I7ZF1KFiO4r7X8C3KG3iVcdBXzJ4W8Bz6SQ6ocj2r3X4fTSwyxIxPHBrfm5nc5auFUYHttu/wAooqK2+aJT7UVR4T3PqqGMbBgVchG2qcDjOKuY+UEV97I+cJmG5a4H4j22+K3uAP8AVvsP0P8A+r9a9BjX93XIeKYxeWN1Cf4hx9RyP1rhxKvTaPRwF/a37HJaPebV69K0WvhIwyRjrXI21y0b46dq0I5ixBHNfMSunY+sjZ6m/PdB4fSsyVjIw5yKYZ9y7abG3zewqbmyRBeqQMVxfiKz3K+OprurptymuV1hdwfNRLVAnaR5XqNmJVnikXcpBBr5W8QaCI/iQml4JWGbzmY/3c5Ufnj8q+u9XxFIw7nrXgnxI02K08YQakg2tPGEZh3Knj+Zrmsj2sJJuVme0eDbiKw02FWxjaMNmurbW7fYSJWT3Vq8X0HxIVtUVTu4qxqXiNY4TuvCMdVHb8a9B4i0bI7/AGV3qdX4s8QQpC/+nybcd2FfP2vaX4i+MXiIeF/BFnNq2oykCa4BIgtlP8cj9FH6nsDXoHgvwfe/GXxI+nWcvk6VbkG9v9mdmeiJ2Ln9Op7A/afw38A6D8LdBjsdFsY7OL77sOXlbHLO3Vj9amlQdZ+0npE4sbmEcNB0qesmc3+y3+zHoP7Mvg2WKOVdS8S6hiTU9WZcNK3ZE/uouTgfia2/iP4qVo2jV8Bc5q/4p8ZeTA+1sV4R4u8WmZpCWzyavFYiKXJHY+YwtGTm6tTVs5LxtILktmQqc5VlPQ15Pq1zLbyi2jVJLq4bajL29Wx7VseNvGSaXay3Esm4dEjzyzHoBWb4D0i4uhJqepYN3N8yr2ReyivAb5nc9xaI2bHSxpOniEDkDk+vvVJdPS+ukWVNwVvlY/rW/qMgVT24xWfapskHsNxP0ojfm0Jvrc6vSfBUdxb7gnUccVctPDP9l3O4Liu68HWQm02JiOoqzremBYyQOa96NCyOGdbmvEzNLm3RhetFZ9nceVc7M0Vztanh1YOMj6+tucVqx8oKKK+6lsfLseGwrD2riPEUzeTKQeeaKK4MT8J6uX/xGea+YWZn6Ekg/XNaVnIdo9aKK+cqbn06LatnNG7avvRRWRuhJX+T8K5TWpj81FFJ7B9o4HxCAqsTyTXinxYPlaZFOPvRyjH48UUVyM9TDfxInJafr8gt/wCIf7pxVK81S51rUrDS7YrBPfTLAksgyE3MBuPr1ooojq9T2a7apto+5fhH4PsPA/hy306xT93CMs5+9I3d2Pcmup1zXpUjULwOlFFe9iPdp2R8NT9+bctWeV+NvEcvMQz83U15N4h1ZjasVz3zmiivl68nc9mmlynjOjQHx34tuprliLbTpvJihJ/iHVjXsVvCtnbgKM4FFFY9BmVfXW+TBFR2jGZio4yyoPYH/wCsDRRXXh0nJGNTZn0N4NUR6VF/u1Nr77Ym+lFFfRy2PKj8R5n9qK6sR2zRRRXlvcmv8Z//2Q==",
                    "htmlText": "test",
                 Descriptiom ->   "plainText": "With growing awareness and changing social mores, the trend of single women coming forward to adopt children is growing in India, government figures indicate.Since August 2015, when new adoption guidelines were introduced that made online registration mandatory for parents wanting to adopt, 412 single women have registered with the Central Adoption Resource Authority (CARA), an autonomous body under the women and child development (WCD) ministry that monitors and regulates adoption of Indian children.",
                 Link->   "feedURL": "http://www.hindustantimes.com/india-news/more-single-women-coming-forward-to-adopt-children-in-india-shows-data/story-dgcvnQJoPFvkl6kqgC2bTK.html",
                  Channel name ->     "feedName": "Hindustan Times",
                  Blank  "writer": "Deepak",
                 Sentiment Id->   "sentiment": {
                        "id": "1"
                    },
                    "createTime": 1493508338000,
                    "ranking": 0,
                    "formattedAge": "30 Apr 2017"
                }
		           ]*/
		
	/*	ye upar dummy data hai dekh lo..yehi hai laast time tumen 
*/
		var formData;
		
		if (true) {
			formData = {
					sentimentNews:lst
				/*"language": $scope.language,
				"source": $scope.sourceId,
				"section": $scope.sectionId,
				"newsHeading":$scope.items*/
			}
			/*var aContact = {
			+					"link": "hindi",
			+					"category": "my cat",
			+					"title": "helloooo",
			+					"previewImageLink":"hhhhhhhhhhhhhhh"
			+				  };
			+				  var sentimentNews1 = [];
			+				  $scope.addContact = function(firstName, email, phone) {
			+					  sentimentNews1.push(aContact);
			+				    //$scope.aContact = {}; //clear contact after added to contact list
			Add a comment to this linexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx              
			+				  };*/
			//alert(JSON.stringify(formData));
			/*$http({
				method: 'POST',
				//url: "/admin/news/addNewsFeed",
				url: "/admin/news/addSentimentNews",
				headers: { 'Content-Type': 'application/json' },
				data: JSON.stringify(formData)
			}).then(function successCallback(response) {
				if (response.data.success === true) {
					angular.forEach(response.data.messages, function (value, key) {
						$scope.responseStatus = JSON.parse(JSON.stringify(value));
						$scope.items = [];
					});

				} else {
					$scope.hasError = true;
					form.$setPristine();
					console.log("succes:error in addNewsData");
				}
			});*/
		}

	}
	
	
	/************************************************************** Get News Language *********************************************/
	
	$scope.GetNewsLanguage = function(){
		$scope.GetAllSentimentsForNews('Temporary');
		 		$scope.selectedLanguage ="";
		 		$scope.selectedChannel ="";
		 		$scope.SelectedCategory ="";
		 		
		var Channels = new Object();
		  var LanguageArray =  new Array();
		  var LanguagechannelArray =  new Array();
		  var channelCategoryArray =  new Array();
		  var tempLanguageArray = new Array();
		  var promise = $http.get('/admin/news/preLoadNewsData').then(function(response) {
			  if (response.data != null) {
				  // For each loop on response data
				  jQuery.each(response.data.messages, function(Itemkey, ItemValue) {
				  // For each loop
					  jQuery.each(ItemValue, function(key, value) {
					// check for duplicate entry  
						  if(jQuery.inArray(value.language, tempLanguageArray) == -1 )
							  {
							 tempLanguageArray.push(value.language);
							  }
				
					    // Create Channel Object
						 var objChannel = new Object();
						 objChannel.Language=value.language;
						 objChannel.Name=value.channel;
						 //Add channel object into channel array
						 LanguagechannelArray.push(objChannel);
				         // for each loop on channels category links	
						  jQuery.each(value.catagoryLink, function(catkey, catValue) {
							  var objChannelCategory = new Object();
								 objChannelCategory.CategoryName=catkey;
								 objChannelCategory.CategoryValue=catValue;
								 objChannelCategory.ChannelName = value.channel;
								 objChannelCategory.ChannelLanguage = value.language;
							 channelCategoryArray.push(objChannelCategory);
						  });
				  });
		});
	}
		
			  // This code only for internal use
			 // Create a language object from language array
			  jQuery.each(tempLanguageArray, function(key, value) {
				  var Language = new Object();
				  Language.Name = value;
				  LanguageArray.push(Language);
			  });
			  Channels.Languages = LanguageArray;
			  Channels.ChannelNames = LanguagechannelArray;
			  Channels.ChannelCategory = channelCategoryArray;
			  /// Assign all arrays to $scope arrays
			  $scope.NewsLanguageArray = LanguageArray;
			  $scope.NewsChannelArray = LanguagechannelArray;
			  $scope.NewscatagoryLinkArray = channelCategoryArray;
			  // final object may be we can use it in future
			  $scope.NewSChannelsDetais = Channels;
			 //console.log("Category:"+JSON.stringify(channelCategoryArray));
				 return Channels;
		  });
	}
		
	
	/// **********************************************Get Channels based on selected language from drop-down list***************************************************///
	$scope.channelsArray = new Array();
	//Selected language from drop-down list
	  $scope.selectedLanguage = "";
	  
	$scope.LoadChannels = function(){
		
		$scope.selectedChannel ="";
 		$scope.SelectedCategory ="";
		jQuery.each($scope.NewsChannelArray, function(key, value) {
			// Get selected language channels
			if(value.Language == $scope.selectedLanguage.Name){
			$scope.channelsArray.push(value);
			}
		});
	}
	
	/// **********************************************Get categories based on selected channel from drop-down list***************************************************///
	//Selected channel from drop-down list
	$scope.selectedChanne = "";
	//This channel array will be filled based on selected language
	$scope.CategoryArray = new Array();
	
	$scope.LoadChannelsCategory = function(){
		$scope.SelectedCategory ="";
		jQuery.each($scope.NewscatagoryLinkArray, function(key, value) {
			// Get all channels category link of selected language
			if(value.ChannelName == $scope.selectedChannel.Name){
			$scope.CategoryArray.push(value);
			}
		});
	}
	
	
	/// **********************************************Get selected category link*************************************************************///
	
	$scope.SelectedCategory = "";
	$scope.SelectCategoryLink ="";
	var tempChannelsArray = new Array();
	$scope.LoadChannelsCategoryLink = function(){
		 $scope.SelectCategoryLink = $scope.SelectedCategory.CategoryValue;
	}
	// End of controller
	
	$scope.sentimentData=[];
	$scope.NewsList = [];
	// Get Sentiments
	$scope.GetAllSentimentsForNews = function(sentimentType){
		$http({
            url: '/admin/sentiment/getAllSentiments?sentimentType='+sentimentType,
            method: "GET",
            headers: {'Content-Type': 'application/json'}
        }).then(function(res){
        	$scope.ddlSentimentItem ="";
        	$scope.sentimentData=res.data.messages;
        	$('#hola').hide();
        },function(err){
        	console.log('err',err);
        })
	}
	// End of Getting Sentiments by sentiment type API
	
	// **********************************************Start function AddNewsForSentiment ***********************************************************
	 $scope.AddNewsForSentiment = function(){
	 // check for sentiment selected or not
		 if($scope.ddlSentimentItem != null && $scope.ddlSentimentItem != "")
		 {
		 var NewsItemList = new Array();
		 var formData = {
					sentimentNews:NewsItemList
			};
		 var count = 0;
		 /// loop apll rows in table
		 $('#tab_logic > tbody  > tr').each(function() {
			
			 var row = $(this);
			 // check is checkbox selected or not, if selected then add this row in array
			 $(this).find('input:checkbox:checked').each(function() {
				 var chkId = "cb_Sentiment_"+count;
				 if($(this).attr("id") == chkId){
					var channelRowData = row.find('input[type="text"]:first').attr("data-item");
					var currentNewsitem = JSON.parse(channelRowData);
					console.log("currentNewsitem:"+currentNewsitem);
					console.log(JSON.stringify(currentNewsitem));
				    var NewsItem = new Object();
				    //currentNewsitem
				   NewsItem.feedSource = $scope.selectedChannel.Name;
					 NewsItem.category = $scope.ddlSentimentItem.subject;
					 NewsItem.title = currentNewsitem.title;
					 NewsItem.link =  currentNewsitem.link;//row.find('input[type="text"]:first').attr("data-item");
					 NewsItem.PlanText = currentNewsitem.description;
					 NewsItem.feedURL = currentNewsitem.link;
					 NewsItem.feedName = $scope.selectedChannel.Name;
					 NewsItem.writer = "";
					 NewsItem.sentiment = {
							 "id":  $scope.ddlSentimentItem.id
			          };
					 NewsItem.createTime = $.now();
					 NewsItem.active = "Y";
					
					 NewsItem.ranking = 0;
					 NewsItem.formattedAge = "";
					 NewsItem.guid = currentNewsitem.guid;
					 NewsItemList.push(NewsItem);
				  }
			    });
			
		      count++;
		 });
		
		 //***************************** Api call start for adding sentiment news ******************** ***************************
		 $http({
				method: 'POST',
				url: "/admin/news/addSentimentNews",
				headers: { 'Content-Type': 'application/json' },
				data: JSON.stringify(formData)
			}).then(function successCallback(response) {
				if (response.data.success === true) {
					console.log("Success news add");
					alert("News add Success");
					angular.forEach(response.data.messages, function (value, key) {
						$scope.responseStatus = JSON.parse(JSON.stringify(value));
						console.log("News Add Result:"+$scope.responseStatus);
						$scope.items = [];
					});

				} else {
					alert("Error news add");
					$scope.hasError = true;
					form.$setPristine();
					console.log("succes:error in addNewsData");
				}
			});
		 //  ***************************** End of API Call for adding sentiment news ******************** ***************************
	 }
	 else{alert("Please select sentiment from dropdown");}
	 }
	
	 
	 
	 //  ***************************** Get sentiments news on sentiments drop-down selected value change ***************************
	 $scope.BackUpSentimentNewsData = new Array();
	 $scope.ddlSentimentId = "";
	 $scope.ddlSentimentItem;
	 $('#ddlSentimentType').on('change', function() {
		 $scope.SentimentNewsBySentimentId(this.value);
		  $scope.ddlSentimentItem = JSON.parse(this.value);
		 // alert("subject:"+JSON.stringify($scope.ddlSentimentItem));
		});
	 // ************************* End of select change event ******************************************************************
	 // Get Sentiment news based on selected drop-down list selected value
	 $scope.SentimentNewsForUpdate = new Array();
	 
	// *****************************start function SentimentNewsBySentimentId - get sentiments based on selected sentiment from dro-down******************
	 $scope.SentimentNewsBySentimentId = function(selectedSentiment){
		
		 var sentimentItem =JSON.parse(selectedSentiment);
		 $scope.ddlSentimentId = sentimentItem.id;
		 $http({
				method: 'GET',
				url: "/sentiment/findSentiment?id="+sentimentItem.id,
				headers: { 'Content-Type': 'application/json' },
			}).then(function successCallback(response) {
				if (response.data.success === true) {	
					var data = response.data.messages;
					$scope.BackUpSentimentNewsData = data;
					$scope.SentimentNewsForUpdate = data;
					$scope.AddPropertiesIntoSentimentNewsArray(data);
				} else {
					alert("Error news add");
					$scope.hasError = true;
					form.$setPristine();
					console.log("succes:error in addNewsData");
				}
			});
	 }
	// ***************************************** End function function SentimentNewsBySentimentId  **********************************************
	// ***************************************** start function AddPropertiesIntoSentimentNewsArray  **********************************************
	 //  Add some new required properties into json array
	 $scope.AddPropertiesIntoSentimentNewsArray = function(sentimentNewsArray)
	 {
		 var tempArray = new Array();
		 
		 var count = 0;
		 angular.forEach(sentimentNewsArray, function (Item, key) {
			 // Get sentiments news from response data
			var tempsentimentNews = Item.sentimentNews;
			// For each all sentiments news array 
			 angular.forEach(tempsentimentNews, function (value, key) {
				 // each sentiment news
			 var arrayItem = value;
			 // check active property exist or not
			 if(arrayItem.active)
				 {
				 // If active key exist in news array object, then check value 'Y" or not if not then set active value to 'Y'
				 if(arrayItem.active != null && arrayItem.active != "Y")
					 arrayItem.active = true;
				 }
			 else{
				 //if active key not exist in sentiment news array add property it self
				 arrayItem.active = true;
			 }
			// Add sentiment Id
			 arrayItem.sentiment = {"id":$scope.ddlSentimentId};
			 // add updated object to array
			 tempArray.push(arrayItem);
			 count++;
			 });
			
		 });
		 console.log("tempArray:	"+tempArray.length);
		 $scope.SentimentNewsForUpdate = tempArray;
		 console.log("SentimentNewsForUpdate:"+ $scope.SentimentNewsForUpdate.length);
		 return tempArray;
	 }
	 //**************************************End of function AddPropertiesIntoSentimentNewsArray ********************************************************
	 // End of AddPropertiesIntoSentimentNewsArray function
	 
	 // Update sentiments News
	 
	// *****************************************  function UpdateSentimentsNews call on button click on update news **********************************************
	 $scope.UpdateSentimentsNews = function(){
		 
	     // Check for sentiment selected or not from dropdown
		 if($scope.ddlSentimentItem != null && $scope.ddlSentimentItem != "")
		 {
		 GetUncheckedNews();
		 $scope.BackUpSentimentNewsData.sentimentNews = $scope.SentimentNewsForUpdate;
		 console.log( JSON.stringify($scope.SentimentNewsForUpdate));
		 var sentimentNewsFeedData;
		 if (true) {
			 sentimentNewsFeedData = {
						sentimentNews:$scope.SentimentNewsForUpdate
				}
		 }
		
		 
		// ***************************************** Start api call fro updating sentiment news ******************************************************************
		 $http({
				method: 'PUT',
				url: "/admin/news/updatedSentimentNews",
				headers: { 'Content-Type': 'application/json' },
				data:JSON.stringify(sentimentNewsFeedData)
			}).then(function successCallback(response) {
				if (response.data.success === true) {	
					alert("Update Success");
				} else {
					alert("Error news add");
					$scope.hasError = true;
					//form.$setPristine();
					console.log("succes:error in addNewsData");
				}
			});
		// ***************************************** end of api call for updating sentimenmt news ******************************************************************
	 }
	 else{
		 alert("Please select sentiment from dropdown");
	 }
	 }
	 ///***************************************** end of function UpdateSentimentsNews *****************************************************************************
	 
//	 ********************************************** GetUncheckedNews Function Get unchecked news that will be delete from sentiment news ***************************
	 function GetUncheckedNews(){
		 angular.forEach($scope.SentimentNewsForUpdate, function (Item, key) {
			if(Item.active == false)
				{
				Item.active = "N";
				}
			else{
				Item.active = "Y"
					
			}
			delete Item["@id"];
			delete Item["$$hashKey"];
		 });
		
	 }
});

// ***************************************** end of GetUncheckedNews function *******************************************************************************

// End of Controller


