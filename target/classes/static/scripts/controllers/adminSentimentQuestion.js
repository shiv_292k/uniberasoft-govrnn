'use strict';

var app = angular.module("govrnnadminApp");
app.controller("adminAddPollQuestionCntrl", function ($scope, $http, $rootScope, $window, $location,) {

	
	$scope.questionList=[];
	$scope.linkList=[];
	var linkBase={titleText:"",linkText:""};
	
	var questionBase={questionText:"",options:[{optionText:""},{optionText:""},{optionText:""}],imgFile:""};
	
	init();
	$scope.addQuestion=function(){
		$scope.questionList.push(angular.copy(questionBase));
	}
	$scope.addLinks=function(){
		$scope.linkList.push(angular.copy(linkBase));
	}
	$scope.removeLinks=function(){
		//todo;
		if($scope.linkList.length>1)
		$scope.linkList.length=$scope.linkList.length-1;
	}
	$scope.removeQuestion=function(){
		//todo;
		if($scope.questionList.length>1){
		$scope.questionList.length=$scope.questionList.length-1;
		$scope.image.length=$scope.image.length-1;
		}
	}
	$scope.submitPoll=function(){
		angular.forEach($scope.image,function(a,b){
			$scope.questionList[b].imgFile=a.split(',')[1];
		})
		var quesLinkObj={
				"polls":$scope.questionList,
				"links":$scope.linkList
				
		};
		addPollLink(quesLinkObj);
	}
	function init(){
		$scope.questionList.push(angular.copy(questionBase));
		$scope.linkList.push(angular.copy(linkBase));
	}
	
	function addPollLink(questionLink){
		
		console.log(questionLink);
		 $http({
	            url: '/admin/sentiment/addPoll',
	            method: "POST",
	            data: angular.toJson(questionLink),
	            headers: {'Content-Type': 'application/json'}
       }).then(function(res){
       	console.log(res)
       },function(err){
       	console.log('err',err);
       })
  }
	$scope.image=[];
	$scope.convertFile=function(ele) {
		var f=document.getElementById(ele.id).files[0];
		  var reader = new FileReader();
		  reader.onloadend = function(e) {
			  var image = e.target.result;
				var arr = image.split(",");
				var arr1 = arr[0].split(";");
				var arr2 = arr1[0].split(":"); 
				 $scope.image.push(image);
				$scope.$apply();
		  }
		  reader.readAsDataURL(f);
		};
	/*function addPoll(questionList){
		debugger;
		var data = angular.toJson(questionList);
		questionList=JSON.parse(data);
		 $http({
	            url: '/admin/sentiment/add',
	            method: "POST",
	            data: questionList,
	            headers: {'Content-Type': 'application/json'}
       }).then(function(res){
       	console.log(res)
       },function(err){
       	console.log('err',err);
       })
  }
	function addLink(linkList){
		
		var data = angular.toJson(linkList);
		linkList=JSON.parse(data);
		 $http({
	            url: '/admin/sentiment/addLink',
	            method: "POST",
	            data: questionList,
	            headers: {'Content-Type': 'application/json'}
       }).then(function(res){
       	console.log(res)
       },function(err){
       	console.log('err',err);
       })
  }*/
});
