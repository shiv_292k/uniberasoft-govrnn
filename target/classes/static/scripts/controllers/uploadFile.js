'use strict';

/**
 * @ngdoc function
 * @name govrnnApp.controller:MainCtrl
 * @description # MainCtrl Controller of the govrnnApp
 */
angular.module('govrnnApp')
	   .controller(
		'UploadFileCtrl',
		[
				'$http',
				'$scope',
				'$rootScope',
				'CreateUploadFileService',
				'HttpHelper',
				function($http, $scope, $rootScope,
						CreateUploadFileService, HttpHelper) {
					
					$scope.showUploadFile = function() {
						$('#uploadFile').appendTo("body").modal('show');
					};

					$scope.uploadFile = function(element) { 
				    	  
				    	  if (typeof ($("#uploadBtn")[0].files) != "undefined") {
			                	$('#fileUp').css("background", "green");
			                	$('#uploadFile').modal('hide');			                	
			                	var regFormData = new FormData();
			                	regFormData.append('file',$(element)[0].files[0]);			                	
			                	$http({
							      method: 'POST',
							      url: "/sentiment/loadSentimentNews",
							      headers: {'Content-Type': undefined},
							      data: regFormData
							      }).then(function successCallback(response) {
							    	 alert("uploaded sucessfully");

							      }, function errorCallback(response) {
							      alert("fail");
							      });
			                
				                
				            } else {
				                alert("This browser does not support HTML5.");
				            }
				    	  
				    	  
				    	  };

					

				} ]);
